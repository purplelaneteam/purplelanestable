<?php 
  require 'includes/initialize.php';
  $error = $_SESSION['error'];
$success = $_SESSION['success'];

if ($success) {
    unset($_SESSION['success']);
}
if ($error) {
    unset($_SESSION['error']);
}
?>
<!DOCTYPE html>
<html lang="en">


<head>
    <title>Guidelines For Mentees | PurpleLane</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <?php include 'head.php' ?>
	
	<style>
    nav.navbar.navbar-expand-lg.navbar-togglable.navbar-dark.navbarall{
      background: inherit !important;
      background-color: #2a0653 !important;
    }

.accordion a {
  position: relative;
  display: -webkit-box;
  display: -webkit-flex;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-orient: vertical;
  -webkit-box-direction: normal;
  -webkit-flex-direction: column;
  -ms-flex-direction: column;
  flex-direction: column;
  width: 100%;
  padding: 1rem 3rem 1rem 1rem;
  color: #7288a2;
  font-size: 1.15rem;
  font-weight: 400;
  border-bottom: 1px solid #e5e5e5;
}

.accordion a:hover,
.accordion a:hover::after {
  cursor: pointer;
  color: #2c0754;
}

.accordion a:hover::after {
  border: 1px solid #2c0754;
}

.accordion a.active {
  color: #2c0754;
  border-bottom: 1px solid #2c0754;
}

.accordion a::after {
  font-family: 'FontAwesome';
  content: '\f067';
  position: absolute;
  float: right;
  right: 1rem;
  font-size: 1rem;
  color: #7288a2;
  padding: 4px;
  width: 30px;
  height: 30px;
  -webkit-border-radius: 50%;
  -moz-border-radius: 50%;
  border-radius: 50%;
  border: 1px solid #7288a2;
  text-align: center;
}

.accordion a.active::after {
  font-family: 'FontAwesome';
  content: '\f068';
  color: #2c0754;
  border: 1px solid #2c0754;
}

.accordion .content {
  opacity: 0;
  padding: 0 1rem;
  max-height: 0;
  border-bottom: 1px solid #e5e5e5;
  overflow: hidden;
  clear: both;
  -webkit-transition: all 0.2s ease 0.15s;
  -o-transition: all 0.2s ease 0.15s;
  transition: all 0.2s ease 0.15s;
}

.accordion .content p {
  font-size: 1rem;
  font-weight: 300;
}

.accordion .content.active {
  opacity: 1;
  padding: 1rem;
  max-height: 100%;
  -webkit-transition: all 0.35s ease 0.15s;
  -o-transition: all 0.35s ease 0.15s;
  transition: all 0.35s ease 0.15s;
}

.alt-color {
    color: #491066;
}
	</style>
	

	
</head>
<body data-spy="scroll" data-target=".navbar" data-offset="82">
    <div class="site-loader">
        <!---======Preloader===========-->
        <div class="loader-dots">
            <div class="circle circle-1"></div>
            <div class="circle circle-2"></div>
        </div>
    </div>
    <div class="site__layer"></div>
    <?php $link = '/'; ?>
    <?php include 'header.php' ?>
	
	
   
	<section class="burger2 app" id="app">
        <div class="container">
            <!-- container-start -->
			<div class="row">
									<div class="col-lg-12 col-sm-12">
										
										<h5 class="section-heading">Guidelines For Mentees</h5>
									
                                        <OL>
	<LI><P ALIGN=JUSTIFY STYLE="margin-top: 0.17in; margin-bottom: 0in; line-height: 150%">
	<FONT COLOR="#000000"><FONT FACE="Arial, serif"><B>Know what you
	want</B></FONT></FONT></P>
	<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><BR>
</P>
    <UL>
		<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><FONT COLOR="#000000"><FONT FACE="Arial, serif"><FONT SIZE=2>As
		a mentee, it is always important to step back, set aside some time
		and be honest with yourself. Ask yourself – where is it I want to
		go and why do I want to go there.</FONT></FONT></FONT></P>
	</UL>
</OL>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><BR>
</P>
<OL>
	<UL>
		<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><FONT COLOR="#000000"><FONT FACE="Arial, serif"><FONT SIZE=2>Once
		you know the answer to these questions – you can then find a
		mentor who can help you get there!</FONT></FONT></FONT></P>
	</UL>
</OL>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><BR>
</P>
<OL>
	<UL>
		<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><FONT COLOR="#000000"><FONT FACE="Arial, serif"><FONT SIZE=2>Prepare
		list of questions you want to clarify with mentors before the
		meeting. </FONT></FONT></FONT>
		</P>
	</UL>
</OL>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><BR>
</P>
<OL START=2>
	<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><FONT COLOR="#000000"><FONT FACE="Arial, serif"><B>Pick
	the right mentor for the right stage of your career or life</B></FONT></FONT></P>
</OL>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><BR>
</P>
<OL>
	<UL>
		<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><FONT COLOR="#000000"><FONT FACE="Arial, serif"><FONT SIZE=2>Ask
		yourself - what are you looking for help with respect to specific
		aspects of your career or life, whether is is a current challenge
		or a future goal?</FONT></FONT></FONT></P>
	</UL>
</OL>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><BR>
</P>
<OL>
	<UL>
		<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><FONT COLOR="#000000"><FONT FACE="Arial, serif"><FONT SIZE=2>You
		can have more than one mentor – one to help you with the current
		challenges and one to help guide you for the long term direction of
		your career – but make sure you understand how each mentor is
		able to help you.</FONT></FONT></FONT></P>
	</UL>
</OL>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><BR>
</P>
<OL>
	<UL>
		<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><FONT COLOR="#000000"><FONT FACE="Arial, serif"><FONT SIZE=2>Keep
		in mind these questions when picking your mentor</FONT></FONT></FONT></P>
		<UL>
			<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%">
			<FONT COLOR="#000000"><FONT FACE="Arial, serif"><FONT SIZE=2>Do
			you need support for specific challenges you are facing now,
			personally or in your role or career?</FONT></FONT></FONT></P>
			<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%">
			<FONT COLOR="#000000"><FONT FACE="Arial, serif"><FONT SIZE=2>Are
			you looking to develop your own strengths, skills and experience
			for the next five years?</FONT></FONT></FONT></P>
			<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%">
			<FONT COLOR="#000000"><FONT FACE="Arial, serif"><FONT SIZE=2>Are
			you trying to take your career or leadership to the next level?</FONT></FONT></FONT></P>
		</UL>
	</UL>
</OL>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><BR>
</P>
<OL>
	<UL>
		<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><FONT COLOR="#000000"><FONT FACE="Arial, serif"><FONT SIZE=2>Once
		you have an idea of what sort of guidance and advice you need –
		whether it is for a current challenge, or with a future goal in
		mind, it is important to start asking the right questions about
		your mentor. Start with:</FONT></FONT></FONT></P>
	</UL>
</OL>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><BR>
</P>
<OL>
	<UL>
		<UL>
			<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%">
			<FONT COLOR="#000000"><FONT FACE="Arial, serif"><FONT SIZE=2>What
			is your mentor’s career experience?</FONT></FONT></FONT></P>
			<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%">
			<FONT COLOR="#000000"><FONT FACE="Arial, serif"><FONT SIZE=2>Have
			they done what you are trying to do?</FONT></FONT></FONT></P>
			<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%">
			<FONT COLOR="#000000"><FONT FACE="Arial, serif"><FONT SIZE=2>Does
			your mentor have the experience and skillset to provide guidance
			on how to tackle your existing problem or achieve your goal?</FONT></FONT></FONT></P>
			<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%">
			<FONT COLOR="#000000"><FONT FACE="Arial, serif"><FONT SIZE=2>Can
			your mentor help position you for the next career step? Do they
			have the experience and network to open doors for you?</FONT></FONT></FONT></P>
		</UL>
	</UL>
</OL>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><BR>
</P>
<OL>
	<UL>
		<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><FONT COLOR="#000000"><FONT FACE="Arial, serif"><FONT SIZE=2>And
		don’t forget – you can learn a lot from a mentor – not just
		off their successes, but also from where they had difficulties –
		as the mentor who faced the hurdles and experienced difficulties
		was able to share with the mentee how to avoid common pitfalls and
		what to focus on.</FONT></FONT></FONT></P>
	</UL>
</OL>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><BR>
</P>
<OL START=3>
	<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><A NAME="_GoBack"></A>
	<FONT COLOR="#000000"><FONT FACE="Arial, serif"><B>Set Goals</B></FONT></FONT></P>
</OL>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><BR>
</P>
<OL>
	<UL>
		<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><FONT COLOR="#000000"><FONT FACE="Arial, serif"><FONT SIZE=2>A
		successful mentee sets goals and expectations at the beginning to
		base the mentorship on a solid foundation and avoid these awkward
		mishaps.</FONT></FONT></FONT></P>
	</UL>
</OL>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><BR>
</P>
<OL>
	<UL>
		<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><FONT COLOR="#000000"><FONT FACE="Arial, serif"><FONT SIZE=2>Work
		with your mentor to set realistic expectations and define timelines
		to achieve them</FONT></FONT></FONT><FONT COLOR="#000000"><FONT FACE="Arial, serif">.</FONT></FONT></P>
	</UL>
</OL>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><BR>
</P>
<OL START=4>
	<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><FONT COLOR="#000000"><FONT FACE="Arial, serif"><B>Be
	prepared with agenda</B></FONT></FONT></P>
</OL>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><BR>
</P>
<OL>
	<UL>
		<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><FONT COLOR="#000000"><FONT FACE="Arial, serif"><FONT SIZE=2>Your
		mentor is a busy person. The more productive the meeting, the more
		they will feel like it's time well spent. So be fully prepared and
		don't be afraid to have a full agenda</FONT></FONT></FONT></P>
	</UL>
</OL>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><BR>
</P>
<OL START=5>
	<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><FONT COLOR="#000000"><FONT FACE="Arial, serif"><B>Be
	engaged and energizing</B></FONT></FONT></P>
</OL>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><BR>
</P>
<OL>
	<UL>
		<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><FONT COLOR="#000000"><FONT FACE="Arial, serif"><FONT SIZE=2>The
		best mentees are fun to work with. They come to work with
		enthusiasm, excitement, and eagerness to move discussions ahead.</FONT></FONT></FONT></P>
	</UL>
</OL>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><BR>
</P>
<OL>
	<UL>
		<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><FONT COLOR="#000000"><FONT FACE="Arial, serif"><FONT SIZE=2>Be
		prepared to ask for specific advice on your skill sets, ideas,
		plans, and goals. The more specific you are the better.</FONT></FONT></FONT></P>
	</UL>
</OL>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><BR>
</P>
<OL START=6>
	<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><FONT COLOR="#000000"><FONT FACE="Arial, serif"><B>Establish
	Mutual Respect</B></FONT></FONT></P>
</OL>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><BR>
</P>
<OL>
	<UL>
		<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><FONT COLOR="#000000"><FONT FACE="Arial, serif"><FONT SIZE=2>The
		mentoring relationship should be based on mutual respect, trust and
		support.</FONT></FONT></FONT></P>
	</UL>
</OL>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><BR>
</P>
<OL>
	<UL>
		<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><FONT COLOR="#000000"><FONT FACE="Arial, serif"><FONT SIZE=2>Always
		maintain confidentiality between one another.&nbsp;</FONT></FONT></FONT></P>
	</UL>
</OL>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><BR>
</P>
<OL>
	<UL>
		<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><FONT COLOR="#000000"><FONT FACE="Arial, serif"><FONT SIZE=2>Keep
		any commitments that are made.&nbsp;</FONT></FONT></FONT></P>
	</UL>
</OL>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><BR>
</P>
<OL>
	<UL>
		<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><FONT COLOR="#000000"><FONT FACE="Arial, serif"><FONT SIZE=2>Evaluate
		the relationship at various points within an agreed-upon time
		frame.</FONT></FONT></FONT></P>
	</UL>
</OL>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><BR>
</P>
<OL>
	<UL>
		<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><FONT COLOR="#000000"><FONT FACE="Arial, serif"><FONT SIZE=2>Build
		trust with your mentor by being open and honest.</FONT></FONT></FONT></P>
	</UL>
</OL>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><BR>
</P>
<OL START=7>
	<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><FONT COLOR="#000000"><FONT FACE="Arial, serif"><B>Mind
	your mentor’s time</B></FONT></FONT></P>
</OL>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><BR>
</P>
<OL>
	<UL>
		<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><FONT COLOR="#000000"><FONT FACE="Arial, serif"><FONT SIZE=2>Good
		mentors are successful for a reason: They manage their time wisely,
		often doing multiple things at any given time in order to ensure
		success. As a mentee, you must learn to respect your mentor’s
		time.</FONT></FONT></FONT></P>
	</UL>
</OL>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><BR>
</P>
<OL START=8>
	<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><FONT COLOR="#000000"><FONT FACE="Arial, serif"><B>Be
	open to feedback</B></FONT></FONT></P>
    <P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><BR>
</P>
	<UL>
		<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0.17in; line-height: 100%">
		<FONT COLOR="#000000"><FONT FACE="Arial, serif"><FONT SIZE=2>As a
		mentee, it is crucial that you listen to the feedback, do not take
		it personally, and actually consider it. Remember, this feedback is
		coming from the right place.</FONT></FONT></FONT></P>
	</UL>
</OL>

  
</div>
				
				</div>
            </div>
           
          
			
			
        </div>
        <!-- container end -->
    </section>

<!-- Forgot Password End-->
<!-- foooter started -->
<?php
    include("footer.php");
    include("footer_tags.php");
?>
    
</body>

</html>