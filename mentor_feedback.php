<?php 
  require 'includes/initialize.php';
?>
<!DOCTYPE html>
<html lang="en">


<head>
    <title><?php echo SITE_NAME; ?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <?php include 'head.php' ?>
	
	<style>
    nav.navbar.navbar-expand-lg.navbar-togglable.navbar-dark.navbarall{
      background: inherit !important;
      background-color: #2a0653 !important;
    }

.h5, h5 {
    font-size: 1rem;
    text-align: left;
    padding-left: 0%;
    padding-bottom: 1%;
}

.form-inline label {
    margin-bottom: 10px;
}

.btn-group {
    padding-left: 30px;
}
.btn-secondary {
    border-color: #f8f9fa;
    padding-right: 30px;
}
input, optgroup, select, textarea {
    margin-left: 30px;
    padding-top: 1%;
}

.alt-color {
    color: #491066;
}
	</style>
 
</head>
<body data-spy="scroll" data-target=".navbar" data-offset="82">
    <div class="site-loader">
        <!---======Preloader===========-->
        <div class="loader-dots">
            <div class="circle circle-1"></div>
            <div class="circle circle-2"></div>
        </div>
    </div>
    <div class="site__layer"></div>
    <?php $link = '/'; ?>
    <nav class="navbar navbar-expand-lg navbar-togglable navbar-dark navbarall">
        <div class="container">
            <?php
                $link = '/';
                if($_SESSION['type'] == 'mentee' ) { 
                    $link = "/mentee/my_appointments.php";
                }
                elseif($_SESSION['type'] == 'mentor' ) {
                    $link = "/mentor/my_appointment.php";
                }
            ?>
            <!-- Brand/ logo -->
            <a class="navbar-brand" style="max-width:40%" href="<?php echo $link ?>"><img src="images/logo.png" alt="logo image" class="img-fluid-logo"></a>
            <!-- Toggler -->
            <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarCollapse"
                aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <span></span><span></span><span></span>
            </button>
            <!-- Collapse -->
            
            <!-- / .navbar-collapse -->
        </div>
        <!-- / .container -->
    </nav>

   <?php 

// Check if any action session 
// Check if the given URL is valid or not
// Check if the feedback already submited by the participant 

// if none of them pass then start session and set below varibles 

// Check if the given URL is valid or not
// Check if the feedback already submited by the participant 
// http://localhost/mentee_feedback.php?feedbackid=50009&feedbacktoken=4f1ff84bf3e50408e69e7000293aa4a5

if(isset($_GET['feedbackid']) && isset($_GET['feedbacktoken']) && !empty($_GET['feedbackid']) && !empty($_GET['feedbacktoken']) )
    {
     $feedback_id=mysqli_real_escape_string($con, $_REQUEST['feedbackid']);
     $feedback_token=mysqli_real_escape_string($con, $_REQUEST['feedbacktoken']);

    $sql = "SELECT feedback_id,participant_id,participant_type,meetings_id 
            FROM feedback_participants WHERE feedback_id = '".$feedback_id."' AND feedback_token =  '".$feedback_token."' AND status = 'inprog' AND participant_type='mentor'";
    $result = mysqli_query($con, $sql);
    if(!($result && $pcpn_row = mysqli_fetch_array($result)))
    {
        ?>
        
  <!-- Question container end-->
  <section class="burger2 app" id="app"> <div class="container"> <div class="row"><div class="col-lg-12 col-sm-12"> <h5 class="section-heading">Invalid Request or Feedback already completed</h5> </div> </div> </div> </section>
  <?php
    include("footer.php");
    include("footer_tags.php");
?>
    <script src="js/jquery3.2.1.min.js"></script>
    <!-- JQUERY LIBRARY -->
    <script src="js/particles.min.js"></script>
    <!-- Particles JS -->
    <script src="js/bootstrap.min.js"></script>
    <!-- Bootstrap JS -->
    <script src="js/main.js"></script>
    <!-- Main JS --> <?php
           exit();
    }

    }
    else {
         ?>
  <section class="burger2 app" id="app"> <div class="container"> <div class="row"><div class="col-lg-12 col-sm-12"> <h5 class="section-heading">Invalid Request</h5> </div> </div> </div> </section>
  <?php
    include("footer.php");
    include("footer_tags.php");
?>
    <script src="js/jquery3.2.1.min.js"></script>
    <!-- JQUERY LIBRARY -->
    <script src="js/particles.min.js"></script>
    <!-- Particles JS -->
    <script src="js/bootstrap.min.js"></script>
    <!-- Bootstrap JS -->
    <script src="js/main.js"></script>
    <!-- Main JS --> <?php
           exit();
       }   

?>

<?php

session_start();
 
// Storing session data
$_SESSION["feedback_id"] = $feedback_id;
$_SESSION["participant_id"] = $pcpn_row["participant_id"];
$meetings_id = $pcpn_row["meetings_id"];

// get mentor/mentee and meeting details to render 
$mentor_row = getMentorBasicInfo($con, $pcpn_row["participant_id"]);
$sql = "SELECT mentor_id,mentee_id,topic_id,title,comments FROM meetings WHERE id = ".$meetings_id;
$result = mysqli_query($con, $sql);
$meetings_row = mysqli_fetch_array($result);
$mentee_row = getMenteeBasicInfo($con, $meetings_row["mentee_id"]);
$topic_name = getTopics($con,$meetings_row["topic_id"]);

?>

	<section class="burger2 app" id="app">
        <div class="container">
            <!-- container-start -->
			<div class="row">
				<div class="col-lg-12 col-sm-12">							
						<h5 class="section-heading">Share Your Feedback</h5>
							<!--<p class="spcev">-->
							<!--Please privide your feedback to  <a href="mailto:support@purplelane.com" class="alt-color">support@purplelane.com</a>-->
							<!--</p>-->
            
                    <div class="col-lg-12 col-sm-12">							
                    <p class="spcev" style="font-size:15px"> Your Email :  <?php echo $mentor_row["email"]; ?> </p>
                    </div>

                    <div class="col-lg-12 col-sm-12">							
                    <p class="spcev" style="font-size:15px"> Mentee Name : <?php echo $mentee_row["fname"]." ".$mentee_row["lname"]; ?> </p>
                    </div>

                    <div class="col-lg-12 col-sm-12">							
                    <p class="spcev" style="font-size:15px"> Menting Details  : <?php echo $topic_name." :- ".$meetings_row["title"]; ?> </p>
                    </div>
                
                </div>
                
                </div> 
            
        </div>
        <!-- container end -->
        <!-- Question container start -->
            
          
    <div class="container">
    <div id="surveyContainer"></div>
    </div>
    </section>


<!-- Forgot Password End-->
<!-- foooter started -->
<?php
    include("footer.php");
    include("footer_tags.php");
?>

<script src="https://surveyjs.azureedge.net/1.1.1/survey.jquery.min.js"></script> 
   
<script>
Survey.Survey.cssType = "bootstrap";

var surveyJSON = {pages:[{name:"Mentor FeedBack",elements:[{type:"radiogroup",name:"Did the mentee able to communicate and discuss the problem properly?",title:"Did the mentee able to communicate and discuss the problem properly?",isRequired:true,choices:["Yes","No",{value:"Maybe",text:"Somewhat"}]},{type:"radiogroup",name:"Did mentor provide all the required information about the topic ?",title:"Did the mentee preprared prior the meeting ?",isRequired:true,choices:[{value:"item1",text:"Yes"},{value:"item2",text:"No"},{value:"item3",text:"Somewhat"}]},{type:"radiogroup",name:"Overall, how satisfied were you with the mentor?",title:"What is the mentee understanding level in the topic ?",isRequired:true,choices:[{value:"item1",text:"Very Good"},{value:"item2",text:"Good"},{value:"item3",text:"Neutral"},{value:"item4",text:"Beginer "}]},{type:"comment",name:"Any final comments about the mentor ?",title:"Please provide feedback you want to share with Mentee ?",isRequired:true},{type:"rating",name:"How do you rate the ease of scheduling a meeting on PurpleLane ?",title:"How do you rate the ease of scheduling a meeting on PurpleLane ?",isRequired:true,rateMax:10,minRateDescription:"Very Difficult",maxRateDescription:"Very Easy"},{type:"rating",name:"How satisfied are you with the PurpleLane ?",title:"How satisfied are you with the PurpleLane ?",isRequired:true,rateMax:10,minRateDescription:"Not Satisfied",maxRateDescription:"Completely Satisfied"},{type:"rating",name:"How likely are you to recommend the PurpleLane to a friend or co-worker?",visibleIf:"{How satisfied are you with the PurpleLane ?} >= 5",title:"How likely are you to refer a Mentor to PurpleLane ?",isRequired:true,rateMax:10,minRateDescription:"Will not recommend",maxRateDescription:"I Will recommend"},{type:"comment",name:"Any final comments about PurpleLane ?",title:"Any final comments about PurpleLane ?"}]}]}

function sendDataToServer(survey) {
    //send Ajax request to your web server.

    var url = "proc_mentor_feedback.php";
    // var formData = {
    //     feedback_data : JSON.stringify(survey.data);
    //     // feedbackid : 
    // };
    var formData1 = {
        data : survey.data,
        feedback_id : <?php echo $feedback_id; ?>,
        participant_id : <?php echo $pcpn_row["participant_id"]; ?>,
        meetings_id : <?php echo $meetings_id; ?>
    };
    $.ajax({
              type : 'POST',
              url : url,
              data : JSON.stringify(formData1),
              dataType : 'JSON',
              // encode : true,
              success: function (response, status, xhr) {
                console.log( "succes" + response.msg);
                // alert("The response is:" + response.msg); 
              },
              error: function (xhr, status, error) {
                console.log( "failed" ,status,error);
              }
          });

    // alert("The results are:" + JSON.stringify(survey.data));
    //alert("The results are:" + JSON.stringify(formData1));

}

var survey = new Survey.Model(surveyJSON);
// survey.completedHtml = "<h4>Thank you for proving feedback!</h4>";
survey.completedHtml = '<div class="alert alert-success"> Thank you for proving feedback! </div>';

$("#surveyContainer").Survey({
    model: survey,
    onComplete: sendDataToServer
    
});
</script>
  <!-- Question container end-->

    <script src="js/jquery3.2.1.min.js"></script>
    <!-- JQUERY LIBRARY -->
    <script src="js/particles.min.js"></script>
    <!-- Particles JS -->
    <script src="js/bootstrap.min.js"></script>
    <!-- Bootstrap JS -->
    <script src="js/main.js"></script>
    <!-- Main JS -->


    
</body>

</html>