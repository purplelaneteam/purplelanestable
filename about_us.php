<?php
require 'includes/initialize.php';


$error = $_SESSION['error'];
$success = $_SESSION['success'];

if ($success) {
    unset($_SESSION['success']);
}
if ($error) {
    unset($_SESSION['error']);
}
// dd([$error || $success, $error, $success]);
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <title>About Us | PurpleLane</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <?php include 'head.php' ?>
</head>
<style>
    nav.navbar.navbar-expand-lg.navbar-togglable.navbar-dark.navbarall {
        background: inherit !important;
        background-color: #2a0653 !important;
    }
</style>


<body>
    <?php
    include 'header.php';
    ?>
     <div class="container">
        <div class="row">
            <div class="new_contact_us about">
                <h4 class="text-center">About PurpleLane</h4>
                <div class="row one">
                    <div class="col-md-6 left">
                        <img src="images/road.jpg" alt="images">
                    </div>
                    <div class="col-md-6 right">
                        <div class="content">
                            <h4>What is PurpleLane?</h4>
                            <p>This is true in most of our lives. We stand at the fork many times, and wonder which road should I take?      </p>
                            <p> We believe that, through learning from the experience of those who walked the roads at the fork, you can pick your PurpleLane </p>
                            <p> We may face the fork during our Graduation, Post Graduation, Employment, or at any phase of life. We all need someone, who has travelled the lane, to help us understand how the lane is. </p>
                            <p> PurpleLane enables you to choose a mentor, from a large pool of mentors from a variety of backgrounds. We handpick the mentors, and ensure that they would add value to our mentees in a way that enables the mentee to choose the correct lane, which would lead them towards their ambition.</p>
                            <h4>Why do we need a mentor? </h4>
                            <p>Getting the guidance,encouragement and support from an experienced mentor provides you with a broad range of personal and professional benefits,
                             which ultimately lead to improved performance in the career.
                            <a href="benefits_of_mentor.php" class="more">More</a></p>
                            <p>Read about <a href="guidelines_for_mentees.php" class="more">Guidelines For Mentees,</a> 
                            <a href="guidelines_for_mentors.php" class="more">Guidelines For Mentors</a></P>
                            <!-- <h4>Our mission</h4>
                            <p>To build India’s largest mentoring platform.</p> -->
                        </div>
                    </div>
                </div>  <!--row one end -->
                <div class="row two">
                <div class="col-md-6 left">
                        <img src="images/about_us.png" alt="images">
                    </div>
                    <div class="col-md-6 right">
                        <div class="content">
                        <!-- <h4>Who are we</h4>
                            <p>We are a group of young and ambitious people, set out to enrich students’
                                lives through mentorship. PurpleLane is an online platform designed to give
                                students and working professionals the opportunity to connect with their
                                mentors over a period of time to establish goals and develop skills, knowledge</p> -->
                            <h4>Our vision</h4>
                            <p>Help every individual to choice the right path and succeed in life.</p>
                            <h4>Our mission</h4>
                            <p>To build India’s largest mentoring platform.</p>
                        </div>
                    </div>
                    
                </div><!--row two end -->
            </div>
        </div>
    </div>
</body>


<!-- Forgot Password End-->
<!-- foooter started -->
<?php
include("footer.php");
include("footer_tags.php");
?>