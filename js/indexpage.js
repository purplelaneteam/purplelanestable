$(document).ready(function(e){
// Change Password

    $('#change_password_button').on('click', function(e){
        var email = $("#email").val();
        var type = $("#type").val();
        var current_password = $("#current_password").val();
        var new_password = $("#new_password").val();
        var confirm_password = $("#confirm_password").val();
        if(current_password !="" && new_password != ""  && confirm_password !=""){
            if(new_password==confirm_password){
                if(new_password.length >= 6){
                    $.ajax({
                        url: "processreq/proc_check_current_pass.php",
                        type: "POST",
                        data: {current_password: current_password, email: email, type:  type},
                        dataType: "json",
                        success: function(data){
                            if(data["code"]=="1"){
                                $.ajax({
                                    url: "processreq/proc_change_password.php",
                                    type: "POST",
                                    data: {new_password: new_password, email: email,type: type},
                                    dataType: "json",
                                    success: function(inner_data){
                                        $("#output").html(inner_data["msg"]);
                                    }
                                });
                            } else {
                                $("#output").html(data["msg"]);
                            }
                        }
                    });
                } else {
                    $("#output").html("Minimum password length should be 6 characters");
                }
            } else {
                $("#output").html("New Passwords Do not match"+new_password+confirm_password);
            }
        } else {
            $("#output").html("Fields cannot be  Empty");
        }
    });

    // Mentor/Mentee Registration
       
    $('#registartion_form').submit(function (e) {
        e.preventDefault();
            var formData = new FormData(this);
            $.ajax({
                type: 'POST',
                url:'processreq/proc_registration.php',
                data: formData,
                dataType: "json",
                cache: false,
                contentType: false,
                processData: false,
                success: function (response) {
                    if(response.code==1)
                    {
                        $('#register_message').removeClass("alert-danger");
                        $('#register_message').addClass("alert-success");
                        $('#register_message').html(response.msg);
                    }
                    else
                    {
                        $('#register_message').removeClass("alert-success");
                        $('#register_message').addClass("alert-danger");
                        $('#register_message').html(response.msg);
                    }
                    setTimeout(function(){ 
                    $('#register_message').html("");
                    $('#register_message').removeClass("alert-success");
                    $('#register_message').removeClass("alert-danger");
                 }, 3000);   
                }
            });
       
    })
// new pass
$("#new_pass_button").on('click', function(e){
    var pass = $('#pass').val();
    var cpass = $('#cpass').val();
    var email = $('#email').val();
    if(email != "" && pass != "" && cpass !=""){
        if(pass==cpass){
            if(pass.length>=6){
                $.ajax({
                    url: "processreq/proc_forgot_change_pass.php",
                    method: "POST",
                    data: {email: email, pass: pass,cpass:cpass},
                    dataType: "json",
                    success: function(data){
                        $('#output').html(data["msg"]);
                    }
                });
            } else {
                $("#output").html("Minimum length of password should be 6 characters.");
            }
        } else {
            $("#output").html("Confirmed password not correct");
        }
    } else {
        $("#output").html("Enter Valid Details...");
    }
});
// mobile password check
$("#name_mobile_check_button").on('click', function(e){
    var email = $('#email').val();
    var name = $('#name').val();
    var mobile = $('#mobile').val();
    if(email != "" && name != "" && mobile !=""){
        $.ajax({
            url: "processreq/proc_name_mobile_check.php",
            method: "POST",
            data: {email: email, name: name, mobile: mobile},
            dataType: "json",
            success: function(data){
                if(data["code"] === "1"){
                    $('#output').html(data["msg"]);
                } else {
                    $('#output').html(data["msg"]);
                }
            }
        });
    } else {
        $("#output").html("Enter Valid Details...");
    }
});

// FORGOT PASSWORD
    $('#forgot-pass-button').on('click',function(e){
        var email = $('#forgot_email').val();
        if(email!=""){
            $.ajax({
                url: "processreq/proc_forgot_pass.php",
                method: "POST",
                data:{email:email},
                dataType: "json",
                success: function(data){
                    if(data["code"] ==="1"){
                        window.location.href = "forgot_pass.php?email="+email;
                    }  else {
                        $('#forgot-output').html(data["msg"]);
                    }
                }
            });
        } else {
            $('#forgot-output').html("Enter Valid Email");
        }
    });
    // EDIT PROFILE
    $('#edit_button').on('click',function(e){
        var user_email = $('#user_email').val();
        var user_name = $('#user_name').val();
        var user_mobile = $('#user_mobile').val();
        var original_email = $('#original_email').val();
        var type = $('#type').val();
        if(user_email != "" && user_mobile != "" && user_name != ""){
            if(user_mobile.length==10){
                $.ajax({
                    url: "processreq/proc_update_profile.php",
                    data: {type:type, user_email: user_email, user_name: user_name, user_mobile: user_mobile, original_email: original_email},
                    dataType: "json",
                    method: "POST",
                    success: function(data){
                        $('#output').html(data["msg"]);
                    }
                });
            } else {
                $('#output').html("Enter Valid Mobile Number");
            }
        } else {
            $("#output").html("Enter Valid Data");
        }
    });

// LOGIN
    $('#login_button').on('click', function(e){
        var user_email = $('#user_email').val();
        var user_password = $('#user_password').val();
        if(user_email != "" && user_password != ""){
            $.ajax({
                url: "processreq/proc_login.php",
                type: "POST",
                data: {user_email: user_email, user_password: user_password},
                dataType: "json",
                success:function(data){
                    if(data["msg"]=="mentee_login"){
                        window.location.href = 'choose-mentor.php';
                    }else if(data["msg"] == "mentor_login") {
                        window.location.href = 'mentor-schedules.php';
                    } else {
                        $("#loginoutput").html(data["msg"]);
                    }
                }
            });
        } else {
            $("#loginoutput").html("Enter Valid Input");
        }
    });

// REGISTRATION
    $('#registration_button').on('click',function(e){
        var first_name = $('#first_name').val();
        var last_name = $('#last_name').val();
        var email = $('#email').val();
        var password = $('#password').val();
        var mobile = $('#mobile').val();
        var user_type = $("input[name='user_type']:checked").val();
        var goal_id = $('#goal_id').val();
        var question_id = $("#question_id").val();
        if(first_name != "" && last_name != "" && email !="" && mobile !="" && password!="" && password.length >=6 && goal_id !="-1" && question_id!="-1"){
            if(mobile.length == 10){
                $.ajax({
                    url: "processreq/proc_registration.php",
                    type: "POST",
                    data: {first_name: first_name,
                            last_name:last_name,
                            email: email,
                            password:password,
                            mobile: mobile,
                            user_type: user_type,
                            goal_id: goal_id,
                            question_id: question_id},
                    dataType: "json",
                    success: function(data){
                        $('#output').html(data["msg"]);
                    }
                });
            } else {
                $('#output').html('Enter Valid Mobile Number');
            }
        } else{
            $('#output').html("Fields cannot be empty.");
        }
    });
});   