<?php
require_once 'dbx.php';

    function doesHaveMeetingRights($id) {
        global $con;
        $user_id = $_SESSION['id'];
        $type = $_SESSION['type'];

        $sql = "SELECT id FROM meetings WHERE {$type}_id = $user_id AND id = $id";
        $result = mysqli_query($con, $sql);
        if($result = mysqli_fetch_assoc($result))
        {
            return true;
        }
        return false;
    }

    function checkMeeting($id)
    {
        if (!doesHaveMeetingRights($id))
        {
            header('Location: ../index.php');
        }
    }

    function get_requested_meetings($requested){
        global $con;
        $requested_meeting_list = '';
        foreach($requested as $req)
        {
            $get_rating_query = 'SELECT AVG(ratings) as rating FROM mentor_ratings WHERE meetings_id IN (SELECT id FROM meetings WHERE mentor_id = '.$req['mentor_id'].')';
            $get_rating_result = mysqli_query($con, $get_rating_query);
            $get_rating_row = mysqli_fetch_array($get_rating_result);
            $rating_val = $get_rating_row['rating'];
            $rating_val = $rating_val * 20;
            $ratings = '<div class="star-ratings-sprite"><span style="width:'.$rating_val.'%" class="star-ratings-sprite-rating"></span></div>';
            $requested_meeting_list .= '
            <div class="row review-box">
                <div class="col-md-3 col-sm-12 order-first">
                        <div class="app-image">
                            <img src="../images/mentor/'.$req['profile_pic'].'"  class="img-fluid rev-mentor-pic">
                        </div>
                        <h4><center>'.$req['mentor'].'</center></h4>
                </div>
                <!---/col-->
                <div class="col-md-7 col-sm-12 mb-5 mb-btm mb-md-0 order-md-0">
                    <div class="app-nfo app-info mb-4">
                        
                        <h5 class="" style="font-weight:normal;">Topic: <b>'.$req['topic'].'</b></h5>
                        '.$ratings.'
                        <h5 style="font-weight:normal;">Meeting ID: <b>'.$req['id'].'</b></h5>
                        
                    </div>
                </div>
            </div><br>
            ';
        }
        return $requested_meeting_list;
    }

    function get_rescheduled_meetings($data){
        global $con;
        $requested_rescheduled_list = '';
        foreach($data as $req)
        { 
            $get_rating_query = 'SELECT AVG(ratings) as rating FROM mentor_ratings WHERE meetings_id IN (SELECT id FROM meetings WHERE mentor_id = '.$req['mentor_id'].')';
            $get_rating_result = mysqli_query($con, $get_rating_query);
            $get_rating_row = mysqli_fetch_array($get_rating_result);
            $rating_val = $get_rating_row['rating'];
            $rating_val = $rating_val * 20;
            $ratings = '<div class="star-ratings-sprite"><span style="width:'.$rating_val.'%" class="star-ratings-sprite-rating"></span></div>';
            $temp = '<form class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="Cancel Meeting" id="mentee-confirmed" ><input type="hidden" name="meeting_id" id="result" value="'.$req['id'].'" /><i class="fa fa-calendar-check"></i></form>';
            $requested_rescheduled_list .= '
            <div class="row review-box">
                <div class="col-md-3 col-sm-12 order-first">
                        <div class="app-image">
                            <img src="../images/mentor/'.$req['profile_pic'].'"  class="img-fluid rev-mentor-pic">
                        </div>
                        <h4><center>'.$req['mentor'].'</center></h4>
                </div>
                <!---/col-->
                <div class="col-md-7 col-sm-12 mb-5 mb-btm mb-md-0 order-md-0">
                    <div class="app-nfo app-info mb-4">
                        
                        <h5 class="" style="font-weight:normal;">Topic: <b>'.$req['topic'].'</b></h5>
                        '.$ratings.'
                        <h5 style="font-weight:normal;">Cancel Meeting: <b>'.$temp.' </b></h5>
                        <h5 style="font-weight:normal;">Meeting ID: <b>'.$req['id'].'</b></h5>
                        
                    </div>
                </div>
            </div><br>
            ';
        }
        return $requested_rescheduled_list;
    }

    function get_scheduled_meetings($data){
        global $con;
        $requested_scheduled_list = '';
        foreach($data as $req)
        {
            $get_rating_query = 'SELECT AVG(ratings) as rating FROM mentor_ratings WHERE meetings_id IN (SELECT id FROM meetings WHERE mentor_id = '.$req['mentor_id'].')';
            $get_rating_result = mysqli_query($con, $get_rating_query);
            $get_rating_row = mysqli_fetch_array($get_rating_result);
            $rating_val = $get_rating_row['rating'];
            $rating_val = $rating_val * 20;
            $ratings = '<div class="star-ratings-sprite"><span style="width:'.$rating_val.'%" class="star-ratings-sprite-rating"></span></div>';
            $date = date('Y-m-d');
            $db_date = $req['meeting_date'];
            $time = date('H:i:s');
            $db_start_time = $req['start_time'];
            $temp_end_time = strtotime($db_start_time) + 60*30;
            $cal_end_time = date('H:i:s', $temp_end_time);
                $email = $req['email'];
                $invites = "[{ id : '".$email."', invite_type : 'EMAIL' }]";
                $hangout_button = '<button type="button" class="btn btn-info btn-sm connect_with_hangout" data-start_time="'.$db_start_time.'" data-end_time="'.$cal_end_time.'" data-email="'.$email.'">Connect</button>';                                   
            $requested_scheduled_list .= '
            <div class="row review-box">
                <div class="col-md-3 col-sm-12 order-first">
                        <div class="app-image">
                            <img src="../images/mentor/'.$req['profile_pic'].'"  class="img-fluid rev-mentor-pic">
                        </div>
                        <h4><center>'.$req['mentor'].'</center></h4>
                </div>
                <!---/col-->
                <div class="col-md-7 col-sm-12 mb-5 mb-btm mb-md-0 order-md-0">
                    <div class="app-nfo app-info mb-4">
                        
                        <h5 style="font-weight:normal;" class="">Topic: <b>'.$req['topic'].'</b></h5>
                        '.$ratings.'
                        <h5 style="font-weight:normal;">Meeting ID: <b>'.$req['id'].'</b></h5>
                        <h5 style="font-weight:normal;">Meeting Date: <b>'.$req['meeting_date'].'</b></h5>
                        <h5 style="font-weight:normal;">Selected Time: <b>'.$req['start_time'].'</b></h5>
                        <h5 style="font-weight:normal;">Hangout: '.$hangout_button.' </h5>

                    </div>
                </div>
            </div><br>
            ';
        }
        return $requested_scheduled_list;
    }

    function get_completed_meetings($data){
        global $con;
        $requested_completed_list = '';
        foreach($data as $req)
        {                                
            $get_rating_query = 'SELECT AVG(ratings) as rating FROM mentor_ratings WHERE meetings_id IN (SELECT id FROM meetings WHERE mentor_id = '.$req['mentor_id'].')';
            $get_rating_result = mysqli_query($con, $get_rating_query);
            $get_rating_row = mysqli_fetch_array($get_rating_result);
            $rating_val = $get_rating_row['rating'];
            $rating_val = $rating_val * 20;
            $ratings = '<div class="star-ratings-sprite"><span style="width:'.$rating_val.'%" class="star-ratings-sprite-rating"></span></div>';                    
            $requested_completed_list .= '
            <div class="row review-box">
                <div class="col-md-3 col-sm-12 order-first">
                        <div class="app-image">
                            <img src="../images/mentor/'.$req['profile_pic'].'"  class="img-fluid rev-mentor-pic">
                        </div>
                        <h4><center>'.$req['mentor'].'</center></h4>
                </div>
                <!---/col-->
                <div class="col-md-7 col-sm-12 mb-5 mb-btm mb-md-0 order-md-0">
                    <div class="app-nfo app-info mb-4">
                        
                        <h5 class="" style="font-weight:normal;">Topic: <b>'.$req['topic'].'</b></h5>
                        '.$ratings.'
                        <h5 style="font-weight:normal;">Meeting ID: <b>'.$req['id'].'</b></h5>
                        <h5 style="font-weight:normal;">Meeting Date: <b>'.$req['meeting_date'].'</b></h5>
                        <h5 style="font-weight:normal;">Selected Time: <b>'.$req['start_time'].'</b></h5>
                        
                    </div>
                </div>
            </div><br>
            ';
        }
        return $requested_completed_list;
    }

    function check_date_time($start_date_time,$end_date_time){
        $start_date_time = new DateTime($start_data_time);
        $end_date_time = new DateTime($end_date_time);
        $start_date = $start_date_time->format('Y-m-d');
        $end_date = $end_date_time->format('Y-m-d');
        $start_time = $start_date_time->format('H:i:s');
        $start_time_val = strtotime($start_time)+10800; //add 3hours to start time for checking
        $end_time = $end_date_time->format('H:i:s');
        $end_time_val = strtotime($end_time);
        $daysLeft = abs(strtotime($start_date) - strtotime($end_date));
        $days = $daysLeft/(60 * 60 * 24);
        if($start_date==$end_date)
        {
            if($end_time_val >= $start_time_val)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        elseif($start_date > $end_date)
        {
            return false;
        }
        else
        {
            if($days > 7)
            {
                return false;
            }
            else
            {
                return true;
            }
        }  
    }

    function send_mail($email_subject, $mailbody, $email)
    {
        
        require_once "class.phpmailer.php";
        
        $reply_address = CONTACTUS_REPLY_ADD;
        $reply_person_name = CONTACTUS_REPLY_NAME;
        $from_address = CONTACTUS_FROM_ADD;
        $from_name = CONTACTUS_FROM_NAME;
        $cc_email=CC_EMAIL_ADDRESS;
        $cc_email1=CC_EMAIL_ADDRESS1;
        $alt_body = "To view the message, please use an HTML compatible email viewer!";

        $mail = new PHPMailer();

        if(USE_SMTP_SERVER==1)
        {
            // print_r($mail_file);
            $mail->IsSMTP();
            $mail->SMTPDebug  = SMTP_DEBUGGING;
            $mail->SMTPAuth   = true;
            $mail->Host       = SMTP_HOST;
            $mail->Port       = SMTP_HOST_PORT;
            $mail->Username   = SMTP_HOST_USERNAME;
            $mail->Password   = SMTP_HOST_PASSWORD;
            $mail->SMTPSecure = SMTP_SECURE;
        }

        $mail->From=$from_address;
        $mail->FromName =  $from_name;
        $mail->AddReplyTo($reply_address, $reply_person_name);
        $mail->AddCC($cc_email);
        $mail->AddCC($cc_email1);
        $mail->AddAddress($email);

        $mail->Subject = $email_subject;
        $mail->AltBody = $alt_body; // optional, comment out AND test
        $mail->MsgHTML($mailbody);
        try {
            $mail->Send();
        } catch (\Throwable $th) {
            //throw $th;
        } 

        
        return true;
        
    }
    
    function send_mail_manoj($email_subject, $mailbody, $email)
    {
        
        require_once "class.phpmailer.php";
        
        $reply_address = "admin@purplelane.in";
        $reply_person_name = CONTACTUS_REPLY_NAME;
        $from_address = "admin@purplelane.in";
        $from_name = CONTACTUS_FROM_NAME;
        $cc_email=CC_EMAIL_ADDRESS;
        $cc_email1=CC_EMAIL_ADDRESS1;
        $alt_body = "To view the message, please use an HTML compatible email viewer!";

        $mail = new PHPMailer();

        if(USE_SMTP_SERVER==1)
        {
            // print_r($mail_file);
            $mail->IsSMTP();
            $mail->SMTPDebug  = SMTP_DEBUGGING;
            $mail->SMTPAuth   = true;
            $mail->Host       = SMTP_HOST;
            $mail->Port       = SMTP_HOST_PORT;
            $mail->Username   = SMTP_HOST_USERNAME;
            $mail->Password   = SMTP_HOST_PASSWORD;
            $mail->SMTPSecure = SMTP_SECURE;
        }

        $mail->From=$from_address;
        $mail->FromName =  $from_name;
        $mail->AddReplyTo($reply_address, $reply_person_name);
        $mail->AddCC($cc_email);
        $mail->AddCC($cc_email1);
        $mail->AddAddress($email);

        $mail->Subject = $email_subject;
        $mail->AltBody = $alt_body; // optional, comment out AND test
        $mail->MsgHTML($mailbody);
        try {
            $mail->Send();
        } catch (\Throwable $th) {
            //throw $th;
        } 

        
        return true;
        
    }

    function logincheck($role)
    {
        global $con;
        $return = 0;
        if(isset($_COOKIE["USER_NAME"]) && isset($_COOKIE["USER_ID"]) && isset($_COOKIE["USER_TOKEN"]))
        {
            //sql query
            $sql = 'SELECT name FROM user WHERE id="'.$_COOKIE["USER_ID"].'" AND token ="'.$_COOKIE["USER_TOKEN"].'" AND role="'.$role.'"';
           // echo $sql;exit;
            $result = mysqli_query($con, $sql);
            if($myrow = mysqli_fetch_array($result))
            {
                $return = 1;
            }
            else
            {
                $return = 0;
            }
        }
        return $return;
        
    }

    function randomPassword() 
    {
        $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }

    function sanitize_input($data)
    {
        global $con;
        $data = trim($data);  
        $data = mysqli_real_escape_string($con, $data);
        return $data;
    }
    
    function sanitaizeString($data)
    {
        $temp_string = str_replace(' ', '',trim(strtolower($data)));
        return $temp_string;
    }

    function getCodename($data)
    {
        $temp_string = str_replace(' ', '_',trim(strtolower($data)));
        return $temp_string;
    }

    function ajax_error($error)
    {
        $jsonarray["code"] = 1;
        $jsonarray["msg"] = $error;
        echo json_encode($jsonarray);
        exit;
    }

    function ajax_send($status,$message){
        $json_myarray["code"] = $status;
        $json_myarray["msg"] = $message;
        echo json_encode($json_myarray);
        // exit;
    }

    function logthis($message, $logfile, $is_logging_on, $extranewlines=0)
    {
	    if($is_logging_on==1)
	    {
            if($extranewlines=="after")
            {
                $message = date("m/d/Y h:i:s a", time())." ".$message."\r\n\r\n";
            }
            if($extranewlines=="before")
            {
                $message = "\r\n\r\n".date("m/d/Y h:i:s a", time())." ".$message;
            }
            
            error_log($message, 3, $logfile);        
	    }
    }


    //send sms
    function send_sms($mobile_no, $message)
    {
        global $sms_gateway_url;
        
        $send_sms_url = $sms_gateway_url;
        
        $send_sms_url = str_replace("{mobile_no}", $mobile_no, $send_sms_url);
        $send_sms_url = str_replace("{msg}", urlencode($message), $send_sms_url);
        
        $sms_msg_id = file_get_contents($send_sms_url);
        return $sms_msg_id;
    }

    // check NULL
    function checkNull($data)
    {
        if($data == NULL){
            return "";
        }else{
            return $data;
        }
    }

    // Generate Random string
    function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    function displayError($error_code = "1305", $error = "Some error occurred try again some time.")
    {
        $response["response"] = "error";
        $response["response_data"]["error_code"] = $error_code;
        $response["response_data"]["error_description"] = $error;
        
        echo json_encode($response);exit;
    }
    


    function send_multiple_mail($email_subject, $mailbody, $toemails, $ccemails, $otherparams, &$failuremessage,$mail_file)
    {
        require_once($mail_file);
        $reply_address = $otherparams["reply_address"];
        $reply_person_name = $otherparams["reply_person_name"];
        $from_address = $otherparams["from_address"];
        $from_name = $otherparams["from_name"];
        $alt_body = "To view the message, please use an HTML compatible email viewer!";

        $mail = new PHPMailer(); // defaults to using php "mail()"

        if(USE_SMTP_SERVER==1)
        {
            $mail->IsSMTP(); // telling the class to use SMTP
            // 1 = errors AND messages
            // 2 = messages only
            $mail->SMTPDebug  = SMTP_DEBUGGING;                     // enables SMTP debug information (for testing)
            $mail->SMTPAuth   = true;                  // enable SMTP authentication
            $mail->Host       = SMTP_HOST; // sets the SMTP server
            $mail->Port       = SMTP_HOST_PORT;                    // set the SMTP port for the GMAIL server
            $mail->Username   = SMTP_HOST_USERNAME; // SMTP account username
            $mail->Password   = SMTP_HOST_PASSWORD;  // SMTP account password 
            $mail->SMTPSecure = 'tls';                     
        }                

        $body = $mailbody;
        $mail->SetFrom($from_address, $from_name);
        $mail->AddReplyTo($reply_address,$reply_person_name);
        if(!empty($toemails))
        {
            foreach($toemails as $key=>$emailarray)
            {
                $mail->AddAddress($emailarray["email"], $emailarray["name"]);
            }
        }
        if(!empty($ccemails))
        {
            foreach($ccemails as $key=>$emailarray)
            {
                $mail->AddCC($emailarray["email"], $emailarray["name"]);
            }
        }

        $mail->Subject = $email_subject;
        $mail->AltBody = $alt_body; // optional, comment out AND test
        $mail->MsgHTML($body);
        if(!$mail->Send())
        {
            $failuremessage = "Something went wrong while sending mail.";
            return false;
        }
        else
        {
            return true;
        }
    }
    function get_category_list($cat_id, $level_string, &$string)
    {
        global $con;
        if(!$level_string)
        {
            $level_string='';
        }
       
        $sql_category = "SELECT id, name FROM category WHERE id = (SELECT parent_id FROM category WHERE id = '$cat_id')";
        if($result_category = mysqli_query($con, $sql_category))
        {
            if($myrow_category = mysqli_fetch_array($result_category))
            {
                if($myrow_category['id']!=$cat_id)
                {
                $string = $myrow_category['name']." -> ".$level_string;
                get_category_list($myrow_category['id'], $myrow_category['name']." -> ".$level_string, $string);
                }
            }
            else 
            {
                $string = $level_string;
            }
        }
    }

    function getMentorBasicInfo($con, $id){
        $query = "SELECT concat(fname,' ',lname) as name, id, fname, lname, email, description, mobile, paytm_mobile, profile_pic, total_experience,cost_card_type_id FROM mentor WHERE id = $id";
        $result = mysqli_query($con, $query);
        $row = mysqli_fetch_assoc($result);
        return $row;
    }
    function getMenteeBasicInfo($con, $id){
        $query = "SELECT id, fname, lname, email, mobile FROM mentee WHERE id = $id";
        $result = mysqli_query($con, $query);
        $row = mysqli_fetch_assoc($result);
        return $row;
    }
    function getMentorCategory($con){
        $query = "SELECT name,id FROM category WHERE parent_id IS NULL AND active='1'";
        $result = mysqli_query($con, $query);
        return $result;
    }
    function getTopicsBySubCategory($con,$cat,$subcat){
        $query = "SELECT name,id FROM topic WHERE category_id=".$subcat." AND active='1'";
        $result = mysqli_query($con, $query);
        return $result;
    }
    function getLevel($con){
        $query = "SELECT id,name,price FROM cost_card_type";
        $result = mysqli_query($con, $query);
        return $result;
    }
    function getAllCompany($con,$mentors){
        $mentors_id="'".implode("','", $mentors)."'";
        
        $company_id="SELECT company_id from mentor_company where mentor_id IN($mentors_id)";
        $company_result=mysqli_query($con,$company_id);
        while($fetch_company_id=mysqli_fetch_array($company_result)){
        $fetch_company_ids[]=$fetch_company_id['company_id'];
        }

        $fetch_company="'".implode("','", $fetch_company_ids)."'";
        $query = "SELECT id,name FROM company where id IN($fetch_company)";
        $result = mysqli_query($con, $query);
        return $result;
    }
    function getAllSkill($con,$mentors){
        $mentors_id="'".implode("','", $mentors)."'";
        $skill_id="SELECT skill_id from mentor_skill where mentor_id IN($mentors_id)";
        $skill_result=mysqli_query($con,$skill_id);
        while($fetch_skill_id=mysqli_fetch_array($skill_result)){
        $fetch_skill_ids[]=$fetch_skill_id['skill_id'];
        }
        $fetch_skill="'".implode("','", $fetch_skill_ids)."'";
        $query = "SELECT id,name FROM skill where id IN($fetch_skill)";
        $result = mysqli_query($con, $query);
        return $result;
    }
    function getRatings($con){
        $query = "SELECT id,name FROM cost_card_type";
        $result = mysqli_query($con, $query);
        return $result;
    }
    function getTopics($con, $topic_id){
        $query = "SELECT id , name FROM topic WHERE id = '$topic_id'";
        $result = mysqli_query($con, $query);
        if($myrow_topic=mysqli_fetch_assoc($result))
        {
            $topic = $myrow_topic['name'];
        }
        return $topic;
    }
    function getMentorByCategory($con,$cat,$subcat){
        $topics=getTopicsBySubCategory($con,$cat,$subcat);
        $mentor=array();
        while($topic=mysqli_fetch_assoc($topics))
        {
            $query_get_mentor = "SELECT mentor_id FROM mentor_categories WHERE topic_id=".$topic['id'];
            $result_get_mentor = mysqli_query($con, $query_get_mentor);
            while($row=mysqli_fetch_assoc($result_get_mentor))
            {
                $mentor[]=$row['mentor_id'];
            }
        }
        return $mentor;
    }
    function getMentorByTopics($con,$topics){
        
        $query_get_mentor = "SELECT mentor_id FROM mentor_categories c,mentor m  WHERE c.mentor_id = m.id and m.is_active = 1 and topic_id=".$topics ;
        $result_get_mentor = mysqli_query($con, $query_get_mentor);
        while($row=mysqli_fetch_assoc($result_get_mentor))
        {
            $mentor[]=$row['mentor_id'];
        }
        return $mentor;
    }
    function getMentorByCompany($con,$company){
        
        $query_get_mentor = "SELECT mentor_id FROM mentor_company WHERE company_id=".$company;
        $result_get_mentor = mysqli_query($con, $query_get_mentor);
        while($row=mysqli_fetch_assoc($result_get_mentor))
        {
            $mentor[]=$row['mentor_id'];
        }
        return $mentor;
    }
    function getMentorBySkill($con,$skill){
        
        $query_get_mentor = "SELECT mentor_id FROM mentor_skill WHERE skill_id=".$skill;
        $result_get_mentor = mysqli_query($con, $query_get_mentor);
        while($row=mysqli_fetch_assoc($result_get_mentor))
        {
            $mentor[]=$row['mentor_id'];
        }
        return $mentor;
    }
    function compareMentorWithLevel($con,$mentor_id,$level_id)
    {
        $query = "SELECT id FROM mentor WHERE id = $mentor_id and cost_card_type_id = $level_id";
        $result = mysqli_query($con, $query);
        $row = mysqli_num_rows($result);
        return $row;
    }
    function getMentorByCategoryAndFilter($con,$topics,$companies,$skills)
    {
        $mentor=[];
        $query_get_mentor = "SELECT m.id FROM mentor m  INNER JOIN mentor_skill s ON s.mentor_id = m.id INNER JOIN mentor_company c ON c.mentor_id = m.id INNER JOIN mentor_categories ct ON ct.mentor_id = m.id ";

        if (isset($skills) || isset($companies) || isset($topics))
        {
            $where = "";
            if ($companies) $company = "'".implode("','", $companies)."'";
            if ($skills) $skill = "'".implode("','", $skills)."'";

            foreach([0, 1, 2] as $value)
            {
                if ($where)
                {
                    if ($value === 0 && $skills) $where .= " AND s.skill_id IN  ($skill) ";
                    if ($value === 1 && $companies) $where .= " AND c.company_id IN ($company) ";
                    if ($value === 2 && $topics) $where .= " AND ct.topic_id = $topics ";
                }
                else
                {
                    if ($value === 0 && $skills) $where .= " WHERE  s.skill_id IN ($skill) ";
                    if ($value === 1 && $companies) $where .= " WHERE  c.company_id IN ($company) ";
                    if ($value === 2 && $topics) $where .= " WHERE  ct.topic_id = $topics ";
                }
            }
            $query_get_mentor .= $where;
        }
       // echo $query_get_mentor;
        $result_get_mentor = mysqli_query($con, $query_get_mentor);
        while($row=mysqli_fetch_assoc($result_get_mentor))
        {
            $mentor[]=$row['id'];
        }
        return $mentor;

    }
    function getCategoryAndSubCategory($con, $category_id = false)
    {
        $sql = "SELECT id, name, codename FROM category WHERE parent_id IS NULL";
        $sql .= ($category_id) ? " AND id = $category_id" : '';
        $result = mysqli_query($con, $sql);
        $categories = [];
        foreach ($result as $key => $value) {
            $sql_sub = "SELECT id, name, codename FROM category WHERE parent_id = ". $value['id'] ." AND id IN (SELECT c.id FROM category c INNER JOIN topic t ON t.category_id = c.id WHERE parent_id IS NOT NULL )";
            // dd($sql_sub);
            $result_sub = mysqli_query($con, $sql_sub);
            $categories[] = [
                'id' => $value['id'],
                'name' => $value['name'],
                'codename' => $value['codename'],
                'sub_categories' => ($result_sub)? $result_sub : []
            ];
        }
        // var_dump($categories);exit;
        return $categories;
    }
    
    function getCategoryDetailById($con,$cat_id)
    {
        $query = "SELECT id, name,codename,parent_id FROM category WHERE id =".$cat_id;
        $result = mysqli_query($con, $query);
        $row = mysqli_fetch_assoc($result);
        return $row;
    }
    function getMentorRate($con, $mentor_id)
    {
        $sql = "SELECT m.overided_price, cct.price FROM mentor m, cost_card_type cct WHERE cct.id = m.cost_card_type_id  AND m.id = $mentor_id";
        $result = mysqli_query($con, $sql);
        if(!$result) {
            return 0;
        }
        if(mysqli_num_rows($result) > 0){
            $row = mysqli_fetch_array($result);
            if($row['overided_price'] > 0){
                $rate = $row['overided_price'];
            } else {
                $rate = $row['price'];
            }
        } else {
            $rate = 0;
        }
        return $rate;
    }

    function dd($data)
    {
        echo '<pre>';print_r($data);exit;
    }
    
    function selectedMentorInfo($con,$mentor,$topic_id = NULL)
    {
        $mentor_info = getMentorBasicInfo($con,$mentor);
        $profile_pic_path="/images/mentee.jpg";
        if($mentor_info['profile_pic']!='')
        {
            $profile_pic_path="/images/mentor/".$mentor_info['profile_pic'];
        }
        $rate = getMentorRate($con,$mentor_info['id']);
        $ratings = getMentorRating($con, $mentor_info['id']);
        if(mysqli_num_rows($ratings)>0)
        {
            $row = mysqli_fetch_assoc($ratings);
            if ($row['count'])
            {
                $average = $row['sum']/$row['count'] * 20;
            }
            else
            {
                $average = 0;
            }
        }
        $settings_price = getTotalSettingsPrice($con, $rate);

        $total_price = $settings_price + $rate;

        $mentor_information='<div class="row review-box">
                        <div class="col-md-3 col-sm-12">
                            <div class="app-image">
                                <img src="'.$profile_pic_path.'"  class="img-fluid rev-mentor-pic">
                            </div>
                            
                            <div style="text-align:center" id="book_mentor">
                                <a href="schedule_appointment.php?mentor_id='.$mentor_info['id'].'&topic_id='.$topic_id.'" class="btn btn-alpha mr-lg-3 mr-2 cnect"> Schedule Meeting <i class="fa fa-angle-right ml-3"></i></a>
                            </div>
                        </div>
                        <div class="col-md-9 col-sm-12 mb-5 mb-md-0 order-first order-md-0 reviews-view">
                            <div class="app-nfo app-info mb-4">
                            <a  href="about_mentor.php?id='.$mentor_info['id'].'"><h5 class="" style="padding-left:0px; color:black;">'.$mentor_info['name'].'</h5></a><h5>
                            </h5>
                                <p class="experience">Total experience '.$mentor_info['total_experience'].'yrs.</p>
                               
                                
                                <div class="row">
                                    <div class="star-ratings-sprite"><span style="width:'.$average.'%" class="star-ratings-sprite-rating"></span></div>
                                </div>
                                    <p class="spcev">'.$mentor_info['description'].'</p>
                                
                            </div>
                        </div>
                    </div>';
        return $mentor_information;
    }

    function getMeetingByOrder($con, $id)
    {
        $query = "SELECT data FROM orders WHERE order_id = '$id'";
        $result = mysqli_query($con, $query);
        if(mysqli_num_rows($result)>0)
        {
            $row = mysqli_fetch_assoc($result);
            
            return $row['data'];
        }
        return null;
       
    }

    function selectedMentorInfoArray($con,$mentor,$topic_id = NULL)
    {
        $mentor_info = getMentorBasicInfo($con,$mentor);
        
        $profile_pic_path="/images/mentee.jpg";
        if($mentor_info['profile_pic']!='')
        {
            $profile_pic_path="/images/mentor/".$mentor_info['profile_pic'];
        }
        $rate = getMentorRate($con,$mentor_info['id']);
        $ratings = getMentorRating($con, $mentor_info['id']);
        if(mysqli_num_rows($ratings)>0)
        {
            $row = mysqli_fetch_assoc($ratings);
            if ($row['count'])
            {
                $average = $row['sum']/$row['count'] * 20;
            }
            else
            {
                $average = 0;
            }
        }

        return array(
            'profile' => $profile_pic_path,
            'id' => $mentor_info['id'],
            'topic_id' => $topic_id,
            'name' => $mentor_info['name'],
            'rate' => $rate,
            'email' => $mentor_info['email'],
            'mobile' => $mentor_info['mobile'],
            'description' => $mentor_info['description'],
            'experience' => $mentor_info['total_experience'],
            'rating' => $average,
        );
    }

    function checkSelected($current_id, $db_id)
    {
        if (is_array($db_id)) {
            $selected_array = $db_id;
        }else {
            $selected_array[] = $db_id;
        }

        if (in_array($current_id, $selected_array)) {
            return 'selected';
        }else{
            return '';
        }
    }
   
    function selecMentorInfoWithMeetingDate($con,$mentor)
    {
        
        $mentor_info = getMentorBasicInfo($con,$mentor);

        $profile_pic_path="/images/mentee.jpg";
        if($mentor_info['profile_pic']!='')
        {
            $profile_pic_path="/images/mentor/".$mentor_info['profile_pic'];
        }
        $rate = getMentorRate($con,$mentor_info['id']);
        $mentor_information='<div class="row review-box">
                        <div class="col-md-3 col-sm-12">
                            <div class="app-image">
                                <img src="'.$profile_pic_path.'"  class="img-fluid rev-mentor-pic">
                            </div>
                        </div>
                        <div class="col-md-7 col-sm-12 mb-5 mb-md-0 order-first order-md-0 reviews-view">
                            <div class="app-nfo app-info mb-4">
                                <h5 class="" style="padding-left:0px">'.$mentor_info['name'].'
                                </h5>
                                <div class="row">
                                    <fieldset class="rating">
                                        <input type="radio" id="star5" name="rating" value="5" />
                                        <label class = "full" for="star5"></label>
                                    </fieldset>
                                </div>
                                    <p class="spcev">Mentorship is a relationship in which a more experienced or more knowledgeable person helps to guide a less experienced or less knowledgeable person.</p>
                                    <p class="spcev">Mentorship is a relationship in which a more experienced or more knowledgeable person helps to guide a less experienced or less knowledgeable person.</p>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-12 ">
                            <div class="chiller_cb2">
                            <p class="pull-right hor-rate small-caption">
                                <label for="myCheckbox"><i class="fa fa-calendar" aria-hidden="true"></i></label>
                                
                                </p>
                            </div>
                                
                        </div>
                    </div>';
        return $mentor_information;
    }
   function getMeeting($con,$status)
   {
        $query = "SELECT mentor_id, topic_id, meeting_date, meeting_selected_slots_id, selected_time, cost_card_price, service_tax_price, platform_charges FROM meetings WHERE meeting_status_id=".$status." and mentee_id=".$_SESSION['id'];
        $result = mysqli_query($con, $query);
        return $result;
   }
   function getMeetingSlots($con,$id)
   {
        $query = "SELECT id, meeting_date, start_time, end_time FROM meeting_selected_slots WHERE meetings_id=$id ORDER BY id";
        $result = mysqli_query($con, $query);
        return $result;
   }
   function getMeetingOfMentor($con,$status)
   {
       $query="SELECT m.title, m.comments, ms.codename, m.id, m.hangout_link,CONCAT(mt.fname, ' ', mt.lname) as mentee, mt.profile_pic,msl.meeting_date,msl.start_time, t.name as topic, ms.name as status, m.addedon, m.mentee_id, m.cost_card_price FROM meetings m LEFT JOIN meeting_selected_slots msl ON msl.id = m.meeting_selected_slots_id LEFT JOIN mentee mt ON mt.id = m.mentee_id LEFT JOIN meeting_status ms ON ms.id = m.meeting_status_id LEFT JOIN topic t ON t.id = m.topic_id WHERE m.mentee_id = mt.id AND m.topic_id = t.id AND m.meeting_status_id = ms.id AND ms.codename='".$status."' and m.mentor_id=".$_SESSION['id'];
       $result = mysqli_query($con, $query);
       return $result;
   }
   function getMeetingOfMentee($con,$status)
   {
       $query="SELECT m.title, m.comments, m.id, m.hangout_link, mn.email ,mn.id as mentor_id,CONCAT(mn.fname, ' ', mn.lname) AS mentor,mn.profile_pic,msl.meeting_date,msl.start_time, t.name as topic, ms.name as status, m.addedon, m.mentee_id, mn.description AS mentor_description, ms.codename, m.comments FROM meetings m LEFT JOIN meeting_selected_slots msl ON msl.id = m.meeting_selected_slots_id LEFT JOIN mentor mn ON mn.id = m.mentor_id LEFT JOIN meeting_status ms ON ms.id = m.meeting_status_id LEFT JOIN topic t ON t.id = m.topic_id WHERE m.mentor_id = mn.id AND m.topic_id = t.id AND m.meeting_status_id = ms.id AND ms.codename='".$status."' and m.mentee_id=".$_SESSION['id'];
       $result = mysqli_query($con, $query);
        return $result;
   }
   function getMenteeTransaction($con,$type)
   {
        $query = "SELECT mt.meetings_id,mt.amount,mt.addedon FROM mentee_transactions mt JOIN meetings m ON m.id=mt.meetings_id WHERE type='".$type."' and mentee_id=".$_SESSION['id'];
        $result = mysqli_query($con, $query);
        return $result;
   }
   function getMentorTransaction($con,$type)
   {
        $query = "SELECT mt.meetings_id,mt.amount,mt.addedon FROM mentor_transactions mt JOIN meetings m ON m.id=mt.meetings_id WHERE type='".$type."' and m.mentor_id=".$_SESSION['id'];
        $result = mysqli_query($con, $query);
        return $result;
   }
   function getMentorRating($con, $id)
   {
       $sql = "SELECT SUM(mr.ratings) as sum, COUNT(mr.ratings) as count FROM mentor_ratings mr LEFT JOIN meetings mt ON mt.id = mr.meetings_id LEFT JOIN mentor m ON m.id = mt.mentor_id WHERE m.id = $id";
       $result = mysqli_query($con, $sql);
       return $result;
   }
   
   function getMentorBankDeatils($con, $id)
   {
       $sql = "SELECT bank_name, ifsc, account_number FROM mentor_bank_account WHERE mentor_id = $id";
       
       $result = mysqli_query($con, $sql);
       $row = mysqli_fetch_assoc($result);
       if ($row)
       {
           return $row;
       }
       return [];
   }
   function getMentorEducation($con, $id)
   {
       $sql = "SELECT id, name, description FROM mentor_education WHERE mentor_id = $id";
       $result = mysqli_query($con, $sql);
       return $result;
   }

   function getMentorExperience($con, $id)
   {
       $sql = "SELECT id, name, description FROM mentor_experience WHERE mentor_id = $id";
       $result = mysqli_query($con, $sql);
       return $result;
   }

   function getSkillsOfMentor($con, $id)
   {
       $sql = "SELECT s.name, s.codename FROM skill s INNER JOIN `mentor_skill` ms ON ms.skill_id = s.id INNER JOIN mentor m ON m.id = ms.mentor_id WHERE m.id = $id";
       $result = mysqli_query($con, $sql);
       
       return $result;
   }
   
   function getCompanyOfMentor($con, $id)
   {
       $sql = "SELECT c.name, c.codename FROM company c INNER JOIN mentor_company mc ON mc.company_id = c.id INNER JOIN mentor m ON m.id = mc.mentor_id WHERE m.id = $id ORDER BY mc.addedon DESC LIMIT 1";
       $result = mysqli_query($con, $sql);
       return $result;
   }

   function getCostCardTypeOfMentor($con, $id)
   {
       $sql = "SELECT c.name, c.codename, c.price FROM cost_card_type c INNER JOIN mentor m ON m.cost_card_type_id = c.id WHERE m.id = $id LIMIT 1";
       $result = mysqli_query($con, $sql);
       return $result;
   }

   function insertCouponState($coupon, $mentee_id, $coupon_id)
   {
	   global $con;
	   $order_id = $_SESSION['order_id'];
	   $sql = "UPDATE orders SET coupon = '$coupon' WHERE order_id = '$order_id'";
	   mysqli_query($con, $sql);

	   $sql = "INSERT INTO coupon_used_by_mentee SET coupon_id = $coupon_id, mentee_id = $mentee_id";
	   mysqli_query($con, $sql);

   }

   function getDiscount($coupon = NULL, $total = 0)
   {
	   global $con;
	   $mentee_id = $_SESSION['id'];
	   if($coupon)
	   {
		   $sql = "SELECT id, type, value, title, use_limit from coupons WHERE title = '$coupon' AND active = 1";
		   $coupon_valid = mysqli_query($con, $sql);
		   if (mysqli_num_rows($coupon_valid))
		   {
				$coupon_valid = mysqli_fetch_assoc($coupon_valid);
				$id = $coupon_valid['id'];
				$sql = "SELECT id from coupon_used_by_mentee WHERE coupon_id = $id AND mentee_id = $mentee_id AND used = 1";
				$result = mysqli_query($con, $sql);
				if (mysqli_num_rows($result)) {
					$_SESSION['coupon'] = 'You have already applied this coupon';
					return 0;
				}
				$sql = "SELECT COUNT(id) as used_number from coupon_used_by_mentee WHERE coupon_id = $id AND used = 1";
				$result = mysqli_query($con, $sql);
				if ($coupon_used = mysqli_fetch_assoc($result)) {
					if ($coupon_used['used_number'] >= $coupon_valid['use_limit']) {
						$_SESSION['coupon'] = 'This coupon has expired';
						return 0;
					}
				}
				insertCouponState($coupon, $mentee_id, $id);
				return discountAmount($coupon_valid, $total);
			}
			$_SESSION['coupon'] = 'Invalid Coupon';
		}
		
		return 0;
    }
    
    function getDiscountOnly($coupon = NULL, $amountAfterDiscount = 0)
   {
       global $con;
       $amountAfterDiscount = $amountAfterDiscount - getTotalSettingsPrice($con, $amountAfterDiscount);
	   $mentee_id = $_SESSION['id'];
	   if($coupon)
	   {
		   $sql = "SELECT id, type, value, title, use_limit from coupons WHERE title = '$coupon' AND active = 1";
		   $coupon_valid = mysqli_query($con, $sql);
		   if (mysqli_num_rows($coupon_valid))
		   {
				$coupon_valid = mysqli_fetch_assoc($coupon_valid);
				$id = $coupon_valid['id'];
				return discountAmountFromDiscount($coupon_valid, $amountAfterDiscount);
			}
			
		}
		
		return 0;
	}
	
	
	function discountAmountFromDiscount($discount, $total = 0)
	{
		$_SESSION['coupon_message_type'] = 'alert-success';
		$type = $discount['type'];
		if ($type === 'flat') {
            return $discount['value'];
		}
		if($type === 'percent') {
            if ($discount['value'] < 100)
            return ($total * 100 / $discount['value']) - $total;
			return $total;
		}

		return 0;
   }
   function discountAmount($discount, $total = 0)
   {
       $_SESSION['coupon'] = 'Coupon applied successfully';
       $_SESSION['coupon_message_type'] = 'alert-success';
       $type = $discount['type'];
       if ($type === 'flat') {
           if ($total < $discount['value']) return $total;
           return $discount['value'];
       }
       if($type === 'percent') {
           if ($discount['value'] < 100)
           return ($total * $discount['value'] / 100);
           return $total;
       }

       return 0;
  }

   function getCouponInfo($coupon)
   {
       global $con;
       $sql = "SELECT title, `value`, `type` FROM coupons WHERE title = '$coupon'";
       $coupon_data = mysqli_query($con, $sql);
       if(mysqli_num_rows($coupon_data)) {
           $coupon_data = mysqli_fetch_assoc($coupon_data);
            if ($coupon_data['type'] === 'percent') {
                return $coupon_data['title']." - Discount coupon of ".$coupon_data['value']. "%";
            }
            if ($coupon_data['type'] === 'flat') {
                return $coupon_data['title']." - Discount coupon of ". CURRENCY_SYMBOL." ".$coupon_data['value'];
            }
       }
       return false;
   }

   function getCoupon($meeting_id)
   {
       global $con;
       $sql = "SELECT m.id, o.order_id, o.coupon FROM meetings m LEFT JOIN orders o ON o.order_id = m.order_id WHERE m.id = $meeting_id";
       $data = mysqli_query($con, $sql);
       if(mysqli_num_rows($data)) {
            return mysqli_fetch_assoc($data);
       }
       return [];
   }

   function calculateSettingsPrice($result, $amount, $discount = 0)
   {
       $total = 0;
       foreach ($result as $rate)
       {
            if($rate['codename'] == 'platform_charges') {
                $total += (float)($amount + $discount)* $rate['price'] / 100;
            }
            else {
                $total += (float)$amount * $rate['price'] / 100;
            }
       }
       return $total;
   }

   function getTotalSettingsPrice($con, $amount)
   {
       $sql = "SELECT price, name, codename FROM settings";
       $result = mysqli_query($con, $sql);
       return calculateSettingsPrice($result, $amount);
   }
   
   function getSettingsPrice($con, $codename = false)
   {
       $sql = "SELECT price, name, codename FROM settings ";
       $sql .= ($codename) ? " WHERE codename = '$codename'" : '';
       
       $result = mysqli_query($con, $sql);
       return $result;
   }

   function calculateRate($rate, $total = 0, $discount = 0)
   {
       return (float)($total + $discount) * $rate / 100;
   }
   function getSlots($start, $end = false)
   {
    //    dd($start);
        if(!$end) {
            $end = date('l, j F Y H:i', strtotime($start . " +3 hour"));
            // dd($end);
        }
        $slots = [];
        while(strtotime($end) > strtotime($start)) {
            $slotEnd = strtotime($start." +30 minutes");
            $slots[] = ['start' => strtotime($start), 'end' => $slotEnd];
            
            $start = date('l, j F Y H:i', $slotEnd);
            // dd($start);
        }
        // dd($slots);
        return $slots;

   }
?>