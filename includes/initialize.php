<?php
require __DIR__.'/../vendor/autoload.php';

require_once 'config.php';
require_once 'dbx.php';
require_once 'functions.php';
require_once 'session.php';
require_once 'email_templates.php';
?>
