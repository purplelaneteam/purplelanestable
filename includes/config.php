<?php
    session_start();
    // General Config Details : Starts Here
    error_reporting(E_ALL & ~E_NOTICE);
    defined("HOST") ? null : define("HOST", $_SERVER['HTTP_HOST']);
    defined("SITE_URL") ? null : define("SITE_URL", "https://".HOST);
    defined("MEMBERS_PAGE_URL") ? null : define("MEMBERS_PAGE_URL", SITE_URL."/members/");
    defined("SITE_NAME") ? null : define("SITE_NAME", "PurpleLane - Online Mentoring Platform");
    $scriptname = $_SERVER['SCRIPT_FILENAME'];
    date_default_timezone_set("Asia/Kolkata");
    define("USER_ID",$_SESSION['USER']['USER_ID']);
    // Storage Folder Path
    define("STORAGE_MAIN_FOLDER", "images");
    defined("EMAIL_VERIFICATION_LINK") ? null : define("EMAIL_VERIFICATION_LINK",SITE_URL."/verify_email.php" );
    // Mail details 
    //defined("ADMIN_EMAIL_ADDRESS") ? null : define("ADMIN_EMAIL_ADDRESS", "team@purplelane.in");
    defined("ADMIN_EMAIL_ADDRESS") ? null : define("ADMIN_EMAIL_ADDRESS", "manoj.mareedu@gmail.com");
    defined('CC_EMAIL_ADDRESS') ? null : define("CC_EMAIL_ADDRESS", "");
    defined('CC_EMAIL_ADDRESS1') ? null : define("CC_EMAIL_ADDRESS1", "");
    defined('CONTACTUS_REPLY_ADD') ? null : define("CONTACTUS_REPLY_ADD", "team@purplelane.in");
    defined('CONTACTUS_REPLY_NAME') ? null : define("CONTACTUS_REPLY_NAME", "PurpleLane");
    defined('CONTACTUS_FROM_ADD') ? null : define("CONTACTUS_FROM_ADD", "team@purplelane.in");
    defined('CONTACTUS_FROM_NAME') ? null : define("CONTACTUS_FROM_NAME", "PurpleLane");

    // 1 : Use; 0: Use default php mail function settings
    defined('USE_SMTP_SERVER') ? null : define("USE_SMTP_SERVER", "0");
    defined('SMTP_HOST') ? null : define("SMTP_HOST", "purplelane.in");
    defined('SMTP_HOST_PORT') ? null : define("SMTP_HOST_PORT", "465");
    defined('SMTP_SECURE') ? null : define("SMTP_SECURE", "tls");
    defined('SMTP_HOST_USERNAME') ? null : define("SMTP_HOST_USERNAME", "no-reply@purplelane.in");
    defined('SMTP_HOST_PASSWORD') ? null : define("SMTP_HOST_PASSWORD", "x5GIV=AmOlf*");
    // 1 = errors and messages; 2 = messages only
    defined('SMTP_DEBUGGING') ? null : define("SMTP_DEBUGGING", "0");
    // define('CLIENT_ID', '251732379647-vi46c1nb708usuti0ohiqavg965ou98m.apps.googleusercontent.com');
    // define('CLIENT_ID', '696944857968-1ujh1fldr7g16kfqfrdtpgccbtvibvla.apps.googleusercontent.com');
    define('CLIENT_ID', '653119097852-r1pn1elrh5gs3vevrfu4rc0la5ijpmup.apps.googleusercontent.com');

    /* Google App Client Secret */
    // define('CLIENT_SECRET', 'G9O458-sMG1fH3LZ902k3-ZX');
    // define('CLIENT_SECRET', 'soqSfJdt_3ARjReSY36FxgkV');
    define('CLIENT_SECRET', 'XYM9RQReoKT_pM-xBB8KpdcZ');

    /* Google App Redirect Url */
    define('CLIENT_REDIRECT_URL', SITE_URL.'/processreq/proc_registration.php');

    $login_url = 'https://accounts.google.com/o/oauth2/v2/auth?scope=' . urlencode('https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/userinfo.email') . '&redirect_uri=' . urlencode(CLIENT_REDIRECT_URL) . '&response_type=code&client_id=' . CLIENT_ID . '&access_type=online';

    define('GOOGLE_LOGIN_URL',  $login_url);

    define('SEND_EMAIL_RESCHEDULING_ADMIN', 1);
   

    //-------------------- Mentor config starts here-------------------------------------//
    defined('PASSWORD_LENGTH_MENTOR') ? null : define("PASSWORD_LENGTH_MENTOR", "6");

    define("MENTOR_IMAGE_UPLOAD_FOLDER", STORAGE_MAIN_FOLDER."/"."mentor");

    defined('MENTOR_IMAGE_UPLOAD_FOLDER_LINK') ? null : define("MENTOR_IMAGE_UPLOAD_FOLDER_LINK", SITE_URL."/".STORAGE_MAIN_FOLDER."/mentor/");

    defined('MENTOR_PROFILE_IMAGE_SIZE') ? null : define("MENTOR_PROFILE_IMAGE_SIZE", "3000");

    defined('SEND_MAIL_ON_MENTOR_REGISTRATION') ? null : define("SEND_MAIL_ON_MENTOR_REGISTRATION", "1");

    define("MENTEE_IMAGE_UPLOAD_FOLDER", STORAGE_MAIN_FOLDER."/"."mentee");

    defined('CURRENCY_SYMBOL') ? null : define("CURRENCY_SYMBOL", "₹");

    defined('MAX_SLOT_FOR_MENTEE') ? null : define("MAX_SLOT_FOR_MENTEE", "3");

    $mentor_profile_pic_allowed_ext = array(
        "png","PNG","webp","WEBP","jpg","JPG","jpeg","JPEG"
    );
    $mentor_profile_pic_allowed_type = array(
        "image/jpeg",
        "image/jpg",
        "image/png",
        "image/webpimage/webp",
        "image/webp",
    );

    $mentor_dir_path = __DIR__."/../".MENTOR_IMAGE_UPLOAD_FOLDER;
    if (!file_exists($mentor_dir_path)) {
        mkdir($mentor_dir_path, 0777, true);
    }

    define('SEND_EMAIL_RESCHEDULING_MENTOR', 1);
    define('SEND_EMAIL_CANCEL_MENTOR', 1);
    define('SEND_EMAIL_HANGOUT_LINK_MENTOR', 1);
    define('SEND_EMAIL_SCHEDULE_MENTOR', 1);

    
    //-------------------- Mentor config ends here-------------------------------------//
    //-------------------- Mentee config starts here-----------------------------------//
    
    define("RATING_COUNT", "5");
    define('SEND_EMAIL_RESCHEDULING_MENTEE', 1);
    define('SEND_EMAIL_CANCEL_MENTEE', 1);
    define('SEND_EMAIL_HANGOUT_LINK_MENTEE', 1);
    define('SEND_EMAIL_SCHEDULE_MENTEE', 1);
    define('SEND_EMAIL_APPOINTMENT_MENTEE', 1);

    //-------------------- Mentee config ends here-------------------------------------//

    define("CONFIRM_MEETING_MAIL_MENTEE", 1);
    define("RESCHEDULE_MEETING_MAIL_MENTEE", 1);
    define("SCHEDULE_MEETING_MAIL_MENTEE", 1);
 
    define("CANCEL_MEETING_MAIL_ADMIN",1);
    define("CANCEL_MEETING_MAIL_MENTEE", 1);


    ?>
