<?php 
  require 'includes/initialize.php';
?>
<!DOCTYPE html>
<html lang="en">


<head>
    <title>Terms and Conditions | PurpleLane</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <?php include 'head.php' ?>
	
	<style>
    nav.navbar.navbar-expand-lg.navbar-togglable.navbar-dark.navbarall{
      background: inherit !important;
      background-color: #2a0653 !important;
    }

.accordion a {
  position: relative;
  display: -webkit-box;
  display: -webkit-flex;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-orient: vertical;
  -webkit-box-direction: normal;
  -webkit-flex-direction: column;
  -ms-flex-direction: column;
  flex-direction: column;
  width: 100%;
  padding: 1rem 3rem 1rem 1rem;
  color: #7288a2;
  font-size: 1.15rem;
  font-weight: 400;
  border-bottom: 1px solid #e5e5e5;
}

.accordion a:hover,
.accordion a:hover::after {
  cursor: pointer;
  color: #2c0754;
}

.accordion a:hover::after {
  border: 1px solid #2c0754;
}

.accordion a.active {
  color: #2c0754;
  border-bottom: 1px solid #2c0754;
}

.accordion a::after {
  font-family: 'FontAwesome';
  content: '\f067';
  position: absolute;
  float: right;
  right: 1rem;
  font-size: 1rem;
  color: #7288a2;
  padding: 4px;
  width: 30px;
  height: 30px;
  -webkit-border-radius: 50%;
  -moz-border-radius: 50%;
  border-radius: 50%;
  border: 1px solid #7288a2;
  text-align: center;
}

.accordion a.active::after {
  font-family: 'FontAwesome';
  content: '\f068';
  color: #2c0754;
  border: 1px solid #2c0754;
}

.accordion .content {
  opacity: 0;
  padding: 0 1rem;
  max-height: 0;
  border-bottom: 1px solid #e5e5e5;
  overflow: hidden;
  clear: both;
  -webkit-transition: all 0.2s ease 0.15s;
  -o-transition: all 0.2s ease 0.15s;
  transition: all 0.2s ease 0.15s;
}

.accordion .content p {
  font-size: 1rem;
  font-weight: 300;
}

.accordion .content.active {
  opacity: 1;
  padding: 1rem;
  max-height: 100%;
  -webkit-transition: all 0.35s ease 0.15s;
  -o-transition: all 0.35s ease 0.15s;
  transition: all 0.35s ease 0.15s;
}

.alt-color {
    color: #491066;
}
	</style>
	

	
</head>
<body data-spy="scroll" data-target=".navbar" data-offset="82">
    <div class="site-loader">
        <!---======Preloader===========-->
        <div class="loader-dots">
            <div class="circle circle-1"></div>
            <div class="circle circle-2"></div>
        </div>
    </div>
    <div class="site__layer"></div>
    <?php $link = '/'; ?>
    <?php include 'header.php' ?>
	
	
   
	<section class="burger2 app" id="app">
        <div class="container">
            <!-- container-start -->
			<div class="row">
									<div class="col-lg-12 col-sm-12">
										
										<h5 class="section-heading">Terms &amp; Conditions</h5>
									
									<p class="spcev">
									<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in">The<A HREF="http://purplelane.in">
</A><A HREF="http://purplelane.in"><U>PurpleLane.in</U></A> website
(&quot;Website&quot;) is an internet based mentoring platform incorporated under the laws of India,
with its registered office at Hyderabad.</P>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in">These terms and
conditions (“<B>Terms” </B>or <B>“Terms &amp; Conditions”</B>)
apply to the PurpleLane (<B>Website</B>) and the use of the site and
its services and Online Platform. You acknowledge and agree that, by
accessing or using the site and/or services, by registering as a
Mentee and/or Mentor, searching for a Mentor via the site or
services, or by posting any content on the site or through the
services, you are indicating that you have read and that you
understand and agree to be bound by these Terms &amp; Conditions,
whether or not you have registered via the site. If you do not agree
to these Terms &amp; Conditions, then you have no right to access or
use the site, services or content. If you accept or agree to these
Terms &amp; Conditions on behalf of a company or other legal entity,
you represent and warrant that you have the authority to bind that
company or other legal entity to these Terms &amp; Conditions and, in
such event, “you” and “your” will refer and apply to that
company or other legal entity.</P>
									</p></div>
						<!-- <div class="col-lg-3 col-sm-12 hidden-xs hidden-sm"> -->
							<!-- <label class="pull-right"> -->
							    <!-- <div class="d-flex justify-content-center h-100"> -->
									<!-- <div class="searchbar"> -->
									    <!-- <input class="search_input" type="text" name="" placeholder="Search..."> -->
									    <!-- <a href="#" class="search_icon"><i class="fa fa-search"></i></a> -->
									<!-- </div> -->
								<!-- </div> -->
							<!-- </label> -->
						<!-- </div> -->
									
								</div>
			<div class="container">
				<div class="row">
				
				    <div class="container">


                    <P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><BR>
</P>
<OL>
	<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><B>OUR SERVICE</B></P>
	<OL>
		<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>The<A HREF="http://purplelane.in">
		</A></FONT><A HREF="http://purplelane.in"><FONT SIZE=2><U>PurpleLane.in</U></FONT></A><FONT SIZE=2>
		website (“</FONT><FONT SIZE=2><B>PurpleLane</B></FONT><FONT SIZE=2>”
		or “</FONT><FONT SIZE=2><B>us” </B></FONT><FONT SIZE=2>or “</FONT><FONT SIZE=2><B>we</B></FONT><FONT SIZE=2>”
		or </FONT><FONT SIZE=2><B>“our</B></FONT><FONT SIZE=2>”)
		provides an online platform that connects Mentors (“</FONT><FONT SIZE=2><B>Mentor</B></FONT><FONT SIZE=2>”
		or </FONT><FONT SIZE=2><B>“you”</B></FONT><FONT SIZE=2>) with
		individuals seeking Mentoring services (“</FONT><FONT SIZE=2><B>Mentee”
		</B></FONT><FONT SIZE=2>or </FONT><FONT SIZE=2><B>“you”</B></FONT><FONT SIZE=2>)
		via videoconferencing, chat room, messaging, email and document
		collaboration (</FONT><FONT SIZE=2><B>Online Platform</B></FONT><FONT SIZE=2>).</FONT></P>
		<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>You
		acknowledge that through this Website and Online Platform, we
		promote Mentors and their provision of Mentor Sessions. In
		performing this function, you acknowledge and agree that:</FONT></P>
		<OL>
			<!--<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>We do-->
			<!--not guarantee the accuracy or validity of Mentor profiles;</FONT></P>-->
			<!--<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>Mentor-->
			<!--profiles are created by each individual Mentor;</FONT></P>-->
			<!--<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>Mentees-->
			<!--are responsible for confirming the identity of their Mentor;</FONT></P>-->
			<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>We
			can only monitor the Mentor Sessions conducted via the Online
			Platform. Any Mentor Sessions undertaken outside of this will not
			be monitored and we will not be of any assistance in case of a
			dispute as to the completion or non-completion of a Mentor
			Session. If a Mentee agrees with a Mentor to conduct a Mentor
			Session outside of our messaging capability, you agree that we
			cannot supervise these Mentor Sessions and any disputes that arise
			will be dealt with in accordance with clause 11; and</FONT></P>
			<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>there
			is no relationship of trust between us and Mentors and nothing in
			these terms constitutes or deems any Mentors as our employees or
			agents.</FONT></P>
			<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>The
			</FONT><FONT SIZE=2><B>Company</B></FONT><FONT SIZE=2> or the
			</FONT><FONT SIZE=2><B>Website</B></FONT><FONT SIZE=2> has no
			control over the Mentoring services or conduct of Mentees, Mentors
			and other users of the site and services, and disclaims all
			liability in this regard. PurpleLane does not verify the identity
			of Mentees nor Mentors. Further, PurpleLane does not verify the
			accuracy or correctness of a Mentor’s profile listed on the
			website.</FONT></P>
			<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>If a
			Mentoring Session is unable to commence, or is interrupted due to
			technical issues of the site, PurpleLane does not take
			responsibility for the Mentoring Session – and the Mentor and
			Mentee are expected to reschedule their Mentoring session to
			another time.</FONT></P>
		</OL>
	</OL>
	<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><B>ACCOUNT
	REGISTRATION</B></P>
	<OL>
		<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>General</FONT></P>
		<OL>
			<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>In
			order to access the Online Platform as either a Mentor or Mentee,
			you must register to create an account.</FONT></P>
			<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>To
			register an account you must:</FONT></P>
			<UL>
				<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>have
				legal capacity to understand and accept these Terms &amp;
				Conditions;</FONT></P>
				<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>provide
				certain personal information, including your full name, address,
				a valid email address;</FONT></P>
				<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>an
				easy to read summary of your qualifications and certifications</FONT><FONT SIZE=2><B>;
				</B></FONT><FONT SIZE=2>and</FONT></P>
				<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>Mentors,
				where they are required under Indian law to have a Permanent
				Account Number (</FONT><FONT SIZE=2><B>PAN</B></FONT><FONT SIZE=2>),
				must also provide your Permanent Account Number. If a Mentor does
				not provide a valid PAN and they are required to have a PAN, we
				may withhold 49% (or any other amount as required by Indian law)
				of any payments due to the Mentor and pass that amount on to the
				Income Tax Office or as required by Indian law.</FONT></P>
				<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>Mentors
				must have a PayTM account and provide us with those details.</FONT></P>
			</UL>
			<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>You
			can register to join the Online Platform via our Website or by
			logging into certain social networking sites that connect you to
			our Website. When you register using social networking sites we
			may obtain personal information from your social networking site
			account.</FONT></P>
			<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>You
			must not provide third parties with access to the Online Portal
			via your registered account. You are responsible for safeguarding
			and maintaining the confidentiality of your login information and
			corresponding account information. You agree not to disclose your
			password to any third party and that you are entirely and solely
			responsible for any and all activities or actions that occur under
			your account, whether or not you have authorized such activities
			or actions. You will immediately notify PurpleLane of any
			unauthorized use of your password or account.</FONT></P>
			<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>We
			maintain the right to reject or ban the registration of accounts
			at our discretion.</FONT></P>
			<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>You
			agree that you will not permit, enable, introduce or facilitate
			(i) persons who do not have an account to have access to or use
			the features of the website and services only made available to
			Mentees and Mentors; and (ii) other users to use a specific
			service through your account, including at the same time that you
			are doing so.</FONT></P>
		</OL>
		<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><B>For a Mentor:</B></P>
		<OL>
			<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>By
			registering an account in accordance with clause 2.1, you consent
			to our disclosure of your personal information to potential
			Mentees and to our marketing of your profile.</FONT></P>
			<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>You
			are responsible for keeping secure and confidential your account
			login details.</FONT></P>
			<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>When
			you register with PurpleLane and set up your account, you: (i)
			agree to provide PurpleLane with accurate and complete
			information; (ii) agree to promptly update your account
			information with any new information that may affect the operation
			of your account; and (iii) authorize PurpleLane, directly or
			through third parties, to make any inquiries we consider necessary
			or appropriate to verify your account information or the
			information you provide to us related to any transactions you
			initiate via the website and services.</FONT></P>
			<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>You
			will not use false identities or impersonate any other person or
			use a password that you are not authorized to use.</FONT></P>
			<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>If
			you register via the website to serve as a Mentor, you represent
			and warrant to PurpleLane that you (i) have requisite knowledge in
			the categories and industries you select to provide Mentoring
			services in such subjects to Mentees; (ii) will utilize
			Mentee-paid Mentoring sessions solely to provide Mentoring
			services only; (iii) your profile you listed at the time of
			registration to use the website is correct and true. You
			understand and agree that Mentees will contact you via the website
			and request that you provide Mentoring services. You have no
			obligation to provide any Mentoring services, and any Mentoring
			services you may choose to provide are in your sole discretion. If
			you choose not to provide Mentoring services, please contact us at
			</FONT><A HREF="mailto:team@purplelane.in"><FONT SIZE=2><U>team@purplelane.in</U></FONT></A></P>
			<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>You
			are solely responsible for all equipment necessary to access and
			use the website and services and to provide Mentoring services.</FONT></P>
			<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>You
			will not record or otherwise store any Mentoring sessions that you
			provide.</FONT></P>
		</OL>
		<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><B>For a Mentee:</B></P>
		<OL>
			<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>By
			registering an account in accordance with clause 2.1 you consent
			to our disclosure of your personal information to your selected
			Mentor.</FONT></P>
			<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>You
			are responsible for confirming the identity and qualifications of
			the Mentor you select. PurpleLane makes no guarantees and takes no
			responsibility for the accuracy or validity of the Mentor’s
			profile registered on the website nor the Mentor’s own
			assertions. You are expected to undertake your own checks and
			investigations and engage Mentors at your own risk.</FONT></P>
			<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>When
			you select a Mentor, you may request areas that you wish a Mentor
			to focus on – this is a guide only. The Mentor makes no
			guarantee that they can or will focus on these areas.</FONT></P>
			<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>You
			are solely responsible for all equipment necessary to access and
			use the website and services and to receive Mentoring services.</FONT></P>
			<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>You
			will not record or otherwise store any Mentoring sessions that you
			receive.</FONT></P>
		</OL>
	</OL>
	<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><B>FEES, PAYMENT AND
	INVOICING</B></P>
	<OL>
		<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><B>Fees</B></P>
		<OL>
			<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>You
			as a Mentor must offer one (1) Mentor Session which spans over
			half an hour for the Mentee to choose a three hour slot.</FONT></P>
			<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>The
			price specified by the Mentor will be exclusive of Goods and
			Services Tax (</FONT><FONT SIZE=2><B>GST</B></FONT><FONT SIZE=2>)
			(if applicable) and our 20% fee (</FONT><FONT SIZE=2><B>Service
			Fee</B></FONT><FONT SIZE=2>) for use of the Website and Online
			Platform.</FONT></P>
			<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>Our
			Service Fee will include any GST (if applicable).</FONT></P>
			<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>You
			acknowledge and agree that PurpleLane reserves the right, in its
			sole discretion, to prospectively modify its commission on
			payments to Mentors at any time. If we modify the commission for
			Mentor payments, we will post the modified commission on the
			website with thirty (30) days advance notice of the modification
			effective date or otherwise provide you with notice of the
			modification. By continuing to provide Mentoring Services after we
			have posted a modification on the website or have provided you
			with notice of a modification, you are indicating that you agree
			to be bound by the modified PurpleLane commission amount. If
			the modified Mentor commission rates are not acceptable to you,
			your only recourse is to cease providing Mentoring services via
			the website.</FONT></P>
			<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>When
			a Mentee purchases Mentor Sessions, the Mentee enters into a
			direct contract with the relevant Mentor for the provision of
			those Mentor Sessions at the price specified by the website for
			the Mentor. Only the Mentee is contracted to attend Mentor
			Sessions with the Mentor, no third parties are to attend the
			Mentor Sessions unless agreed to by the Mentor.</FONT></P>
			<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>The
			prices for Mentor Sessions or other fees and charges, displayed on
			the Website are in currency of Indian rupees, and are current at
			the time of display, but may be subject to change from time to
			time in accordance with clause 12(b).</FONT></P>
			<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>We
			will issue a tax invoice to the Mentee on behalf of the Mentor for
			the services provided by the Mentor which will also include our
			Service Fee. We will send to the Mentor a recipient created tax
			invoice for the fee payable to the Mentor and for this purpose if
			Mentor is providing services in India then the Mentor is required
			to provide to us a valid Indian Business Number.</FONT></P>
		</OL>
		<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><B>Payment</B></P>
		<OL>
			<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>We
			only accept payments from Mentees via Paytm.</FONT></P>
			<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>It is
			preferable that payments to Mentors be made via Paytm however, in
			the event a Mentor does not have a Paytm account, payment will be
			made via bank transfer. If a Mentor does not reside in India and
			requests a bank transfer, any cost for foriegn transfer should be
			borne by the Mentor.</FONT></P>
			<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>Mentor
			Sessions are only purchased once full payment has been received
			from the Mentee by us.</FONT></P>
			<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>We
			may reject a Mentee’s order at our absolute discretion if we
			believe, acting reasonably, a Mentor is unable to provide the
			Mentee with the Mentor Sessions for any reason whatsoever or if
			there has been an error in the price charged by the Mentor. In the
			event we reject a Mentee’s order, we will provide the Mentee
			with a full refund of any payment received.</FONT></P>
			<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>After
			each Mentor Session the Mentee and Mentor will be invited to
			complete a feedback form. The feedback form provides you with an
			opportunity to review your experience with the Mentor Session(s).
			Once submitted, you will not be able to edit your comments and
			such comments will be made available to your Mentee/Mentor and may
			be made public via the website so please exercise care when
			completing this form. The completion of this form will inform us
			that a Mentor Session has been conducted and we will then issue
			the Mentee with an invoice breaking down the Mentor’s fee, our
			fee and any applicable taxes for the Mentor session. We will then
			process payment to the Mentor for that Mentor Session within 48
			hours of completion of the feedback form.</FONT></P>
		</OL>
		<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><B>Invoices</B></P>
		<OL>
			<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>An
			invoice will be emailed to the Mentee within 10 days of receipt of
			the feedback in accordance with clauses 3.2.5 and 3.2.6.</FONT></P>
			<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>The
			invoice will outline the Mentor’s fee, our Service Fee and any
			applicable tax or additional fees charged.</FONT></P>
		</OL>
	</OL>
	<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2><B>WARRANTIES
	AND OUR LIABILITY</B></FONT></P>
	<OL>
		<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>We </FONT><FONT SIZE=2><U>do
		not</U></FONT><FONT SIZE=2> make any warranties or guarantees as to
		the Mentors who are registered on our Online Platform including
		without limitation, any guarantee the Mentor Sessions will be of a
		particular quality or standard. You may be entitled to certain
		guarantees under the Australian Consumer Law, which may be enforced
		against Mentors.</FONT></P>
		<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>We </FONT><FONT SIZE=2><U>do
		not </U></FONT><FONT SIZE=2>make any warranties or guarantees
		regarding the qualifications or certifications of Mentors as
		displayed on their profile.</FONT></P>
		<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>We </FONT><FONT SIZE=2><U>do
		not</U></FONT><FONT SIZE=2> make any warranties or guarantees
		regarding the quality of advice or views of Mentors given during
		Mentor Sessions.</FONT></P>
		<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>We </FONT><FONT SIZE=2><U>do
		not </U></FONT><FONT SIZE=2>warrant guarantee or make any
		representation that the Website, Online Platform or the server that
		makes the site available on the World Wide Web are free of software
		viruses, free from errors, or that they will operate uninterrupted.</FONT></P>
		<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>We </FONT><FONT SIZE=2><U>do
		not</U></FONT><FONT SIZE=2> provide professional advice and we give
		no warranty, guarantee or representation about the accuracy,
		reliability or timeliness or otherwise, of the information
		contained on the Website or Online Platform.</FONT></P>
		<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>We </FONT><FONT SIZE=2><U>do
		not</U></FONT><FONT SIZE=2> warrant or make any representation as
		to the accuracy and are not liable for the content of, any links to
		third-party websites or resources. Links are provided for your
		convenience and interest only.</FONT></P>
		<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>If you
		ever believe that another Mentee or Mentor has violated law or is
		defrauding, threatening or endangering anyone, PurpleLane urges
		you to immediately contact the police directly for help</FONT></P>
		<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>To the
		extent permitted by law, you release us from and we exclude all
		liability to you or any other person for, any loss (including
		indirect, special or consequential loss), damages, costs (including
		legal costs) or expenses of any kind, arising from or in connection
		with:</FONT></P>
		<OL>
			<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>any
			act or omission of a Mentor or Mentee;</FONT></P>
			<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>your
			use and access of the Website or Online Platform;</FONT></P>
			<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>the
			conduct or provision of Mentor Sessions;</FONT></P>
			<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>any
			errors or inaccuracies on Mentor profiles, in respect of
			information provided to us by the Mentors;</FONT></P>
			<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>delays
			to, interruptions of or cessation of transmission to or from the
			Website or Online Platform.</FONT></P>
		</OL>
		<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>You
		understand and agree that provision of Mentoring Services may
		require use of a third party website or service (each a “Third
		Party Service”). Your use of any Third Party Service is
		voluntary. You shall be solely responsible for acquiring and
		maintaining all telecommunications and internet services and other
		hardware and software you may decide to use to provide the
		Mentoring Services, including, without limitation, any and all
		costs, fees, expenses, and taxes of any kind related to use of any
		Third Party Service. PurpleLane shall not be responsible for
		any loss or corruption of data, lost communications, or any other
		loss or damage of any kind arising from any such Third Party
		Service.</FONT></P>
		<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><A NAME="_GoBack"></A>
		<FONT SIZE=2>Nothing in these Terms is intended to exclude,
		restrict or modify any guarantees under the Australian Consumer Law
		which apply and cannot be excluded, restricted or modified.
		Otherwise, all terms, conditions, warranties and representations,
		express or implied by statute or otherwise are excluded.</FONT></P>
	</OL>
	<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0.03in"><FONT SIZE=2><B>UNACCEPTABLE
	CONDUCT</B></FONT></P>
</OL>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0.03in"><FONT SIZE=2><B>You
must not engage in the following unacceptable conduct when providing
or attending Mentor Sessions:</B></FONT></P>
<OL>
	<OL>
		<LI><P ALIGN=JUSTIFY STYLE="margin-top: 0.17in; margin-bottom: 0in">
		<FONT SIZE=2>attending a Mentor Session more than 15 minutes late
		or failing to attend at all without reasonable prior notice;</FONT></P>
		<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>engage
		in conduct that risks the health and safety of the Mentor/Mentee;</FONT></P>
		<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>assault,
		harass, bully or threaten the Mentor/ Mentee;</FONT></P>
		<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>use
		abusive or offensive language; and/or</FONT></P>
		<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>attend
		a Mentor Session when intoxicated or impacted by drugs.</FONT></P>
	</OL>
	<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2><B>Termination
	and Refunds</B></FONT></P>
	<OL>
		<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>A
		Mentor may without cause, cancel any Mentor Sessions purchased and
		not yet undertaken. In this circumstance, the amount will be
		refunded to the Mentee.</FONT></P>
		<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>A
		Mentee may without cause, request to cancel any Mentor Sessions
		purchased by contacting the administrative support. In this
		circumstance, the amount will be refunded to the Mentee..</FONT></P>
		<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>If a
		Mentor engages in any of the conduct defined in clause 9 during a
		Mentor Session, the Mentee may request a refund of that Mentor
		Session. </FONT><FONT COLOR="#ff0000"><FONT SIZE=2>Such request
		will be made in accordance with clause 7.2.</FONT></FONT></P>
		<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>If a
		Mentee engages in any of the conduct defined in clause 5 during a
		Mentor Session, the Mentor may terminate the engagement with the
		Mentee.</FONT></P>
		<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>We can
		terminate these Terms at any time in respect of you, or disable
		(temporarily or permanently) your use of the Website and Online
		Portal at any time, with immediate effect and without notice to you
		at our sole discretion</FONT></P>
		<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>PurpleLane
		will be entitled to its fee regardless of any assertions, claims,
		guarantees (eg. Satisfaction guaranteed, money back guarantee) or
		similar made by Mentors. Any such claim, guarantee or assertion
		made by Mentors does not extend to PurpleLane’s fee. Nor will
		PurpleLane be liable for any Mentoring fees paid to Mentors who do
		not live up to or meet the expectations of the Mentee or the
		guarantee, assertion or claim stipulated by the Mentor.</FONT></P>
		<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>We
		will use reasonable efforts to process any refund fifteen (15) days
		after any accepted request.</FONT></P>
	</OL>
	<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2><B>COMPLAINTS
	AND DISPUTES</B></FONT></P>
	<OL>
		<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>If you
		have a complaint or dispute regarding the provision, non-provision
		or conduct of a Mentor or Mentee during a Mentor Session, you
		should immediately contact the relevant Mentor or Mentee and try to
		reach an agreement going forward.</FONT></P>
		<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>If you
		are a Mentee and have a dispute with your Mentor, you should also
		contact us within 48 hours of the alleged conduct or Mentor
		Session, providing as much detail as possible and we will hold off
		payment for that Mentor Session to the Mentor until the dispute is
		resolved or thirty (30) days after the disputed Mentor Session or
		alleged conduct, whichever is earlier.</FONT></P>
		<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>We
		will use reasonable efforts to assist both parties to resolve the
		complaint.</FONT></P>
		<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>If the
		complaint or dispute is not resolved within thirty (30) days from
		the provision of the Mentor Session or the alleged conduct, we will
		at our discretion, determine how the fees paid by the Mentee should
		be dealt with.</FONT></P>
	</OL>
	<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2><B>Website
	AND ONLINE PORTAL</B></FONT></P>
	<OL>
		<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>Ownership</FONT></P>
		<OL>
			<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>The
			material on the Website and Online Portal is our copyright and is
			owned by us. We reserve all Intellectual Property Rights in the
			Website, Online Portal and material contained in it.</FONT></P>
			<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>You
			may view, download, and print pages from the Website or Online
			Portal for your own personal and non-commercial use, provided you
			do not remove any copyright and trademark notices contained on the
			material.</FONT></P>
			<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>You
			must not:</FONT></P>
			<UL>
				<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>upload,
				repost or republish material from the Website or Online Portal
				(including to any other site on the World Wide Web);</FONT></P>
				<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in">“<FONT SIZE=2>frame”
				the material on the Website or Online Portal with other material
				on any other World Wide Web site.</FONT></P>
				<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>sell,
				rent or sub-license material from the Website or Online Portal;</FONT></P>
				<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>show
				any material from the Website or Online Portal in public;</FONT></P>
				<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>reproduce,
				duplicate, copy or otherwise exploit material on the Website or
				Online Portal for a commercial purpose;</FONT></P>
				<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>edit
				or otherwise modify any material on the Website or Online Portal;
				or</FONT></P>
				<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>redistribute
				material from the Website or Online Portal.</FONT></P>
			</UL>
			<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>We
			may, in our sole discretion, permit Mentees/Mentors to post,
			upload, publish, submit or transmit content. By making available
			any content on or through the website, you hereby grant the
			following license to PurpleLane, which PurpleLane may exercise
			solely in connection with PurpleLane’s provision of it’s
			services: a worldwide, irrevocable, perpetual, non-exclusive,
			transferable, royalty-free license, with the right to sublicense
			to other users of the services (including Mentors, as applicable),
			to use, view, copy, adapt, modify, distribute, license, sell,
			transfer, publicly display, publicly perform, transmit, stream,
			broadcast and otherwise exploit such content.</FONT></P>
			<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>You
			acknowledge and agree that you are solely responsible for all
			content that you make available through the website or services
			provided. Accordingly, you represent and warrant that: (i) you
			either are the sole and exclusive owner of all content that you
			make available through the website or services or you have all
			rights, licenses, consents and releases that are necessary to
			grant to PurpleLane and to the rights in such content, as
			contemplated under these Terms &amp; Conditions; and (ii) neither
			the content nor your posting, uploading, publication, submission
			or transmittal of the content or PurpleLane’s use of the content
			(or any portion thereof) on, through or by means of the website or
			services will infringe, misappropriate or violate a third party’s
			patent, copyright, trademark, trade secret, moral rights or other
			intellectual property rights, or rights of publicity or privacy,
			or result in the violation of any applicable law or regulation.</FONT></P>
		</OL>
		<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>Acceptable
		use</FONT></P>
		<OL>
			<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>Whether
			you are a Mentor, Mentee or visitor to our Website, you must not:</FONT></P>
			<UL>
				<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>use
				the Website or Online Portal in any way that causes, or may
				cause, damage to the Website/Online Portal or impairment of the
				availability or accessibility of the Website/ Online Portal; or
				in any way which is unlawful, illegal, fraudulent or harmful, or
				in connection with any unlawful, illegal, fraudulent or harmful
				purpose or activity;</FONT></P>
				<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>use
				the Website/Online Portal to copy, store, host, transmit, send,
				use, publish or distribute any material which consists of (or is
				linked to) any spyware, computer virus, Trojan horse, worm,
				keystroke logger, rootkit or other malicious computer software;
				conduct any systematic or automated data collection activities
				(including without limitation scraping, data mining, data
				extraction and data harvesting) on or in relation to the
				Website/Online Portal without Our express written consent;</FONT></P>
				<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>use
				the Website/Online Portal to transmit or send unsolicited
				commercial communications;</FONT></P>
				<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>use
				the Website/Online Portal for any purposes related to marketing
				without Our express written consent.</FONT></P>
				<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>Use,
				display, mirror or frame the website, or any individual element
				within the website, PurpleLane’s name, any PurpleLane
				trademark, logo or other proprietary information, or the layout
				and design of any page or form contained on a page, without
				PurpleLane’s express written consent;</FONT></P>
				<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>Access,
				tamper with, or use non-public areas of the website, PurpleLane’s
				computer systems, or the technical delivery systems of
				PurpleLane’s providers;</FONT></P>
				<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>Attempt
				to probe, scan, or test the vulnerability of any PurpleLane
				system or network or breach any security or authentication
				measures; Avoid, bypass, remove, deactivate, impair, descramble
				or otherwise circumvent any technological measure implemented by
				PurpleLane or any of PurpleLane’s providers or any other third
				party (including another user) to protect the website, services
				or content;</FONT></P>
				<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>Attempt
				to access or search the website, services or content or download
				content from the website or services through the use of any
				engine, software, tool, agent, device or mechanism (including
				spiders, robots, crawlers, data mining tools or the like) other
				than the software and/or search agents provided by PurpleLane or
				other generally available third party web browsers;</FONT></P>
				<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>Use
				any meta tags or other hidden text or metadata utilizing a
				PurpleLane trademark, logo, URL or product name without
				PurpleLane’s express written consent;</FONT></P>
				<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>Use
				the website, services or content for any commercial purpose or
				the benefit of any third party or in any manner not permitted by
				these Terms &amp; Conditions;</FONT></P>
				<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>Forge
				any TCP/IP packet header or any part of the header information in
				any email or newsgroup posting, or in any way use the website,
				services or content to send altered, deceptive or false
				source-identifying information;</FONT></P>
				<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>Attempt
				to decipher, decompile, disassemble or reverse engineer any of
				the software used to provide the website, services or content;</FONT></P>
				<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>Interfere
				with, or attempt to interfere with, the access of any user, host
				or network, including, without limitation, sending a virus,
				overloading, flooding, spamming, or mail-bombing the website or
				services;</FONT></P>
				<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>Collect
				or store any personally identifiable information from the website
				or services from other users of the website or services without
				their express permission;</FONT></P>
				<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>Impersonate
				or misrepresent your affiliation with any person or entity;</FONT></P>
				<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>Violate
				any applicable law or regulation; or</FONT></P>
				<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=2>Encourage
				or enable any other individual to do any of the foregoing.</FONT></P>
			</UL>
			<LI><P STYLE="margin-bottom: 0in"><FONT SIZE=2>PurpleLane will
			have the right to investigate and prosecute violations of any of
			the above to the fullest extent of the law regardless of
			jurisdiction. PurpleLane may involve and cooperate with law
			enforcement authorities in prosecuting users who violate these
			Terms &amp; Conditions. You acknowledge that PurpleLane has no
			obligation to monitor your access to or use of the website, the
			services or content or to review or edit any content, but has the
			right to do so for the purpose of operating the website and
			services, to ensure your compliance with these Terms &amp;
			Conditions, or to comply with applicable law or the order or
			requirement of a court, administrative agency or other
			governmental body. PurpleLane reserves the right, at any time and
			without prior notice, to remove or disable access to any content
			that PurpleLane, at its sole discretion, considers to be in
			violation of these Terms &amp; Conditions or otherwise harmful to
			the website or services provided.</FONT></P>
		</OL>
		<LI><P STYLE="margin-bottom: 0in"><FONT SIZE=2>DEFINITIONS</FONT></P>
		<OL>
			<LI><P STYLE="margin-bottom: 0in"><FONT SIZE=2><B>Intellectual
			Property</B></FONT><FONT SIZE=2> means all copyright, trade marks,
			designs, confidential information, patents, inventions and
			discoveries; and</FONT></P>
			<LI><P STYLE="margin-bottom: 0in"><FONT SIZE=2><B>Intellectual
			Property Rights</B></FONT><FONT SIZE=2> means all rights in the
			Intellectual Property, including current and future, registered
			and unregistered rights conferred by statute, common law or
			equity.</FONT></P>
			<LI><P STYLE="margin-bottom: 0.17in"><FONT SIZE=2><B>Mentor
			Session</B></FONT><FONT SIZE=2> means a 30 mins to one  hour
			meeting between a Mentor and Mentee where the Mentor provides
			Mentoring. Such meeting may be undertaken across various mediums
			of communication whether face to face, via telephone or online via
			our website. Should the Mentor or Mentee be unable to participate
			in a Mentoring Session for a full hour, the Mentor and Mentee may
			negotiate to make up the remaining time in another session.</FONT></P>
		</OL>
	</OL>
</OL>
 




  
</div>
				
				</div>
            </div>
           
          
			
			
        </div>
        <!-- container end -->
    </section>

<!-- Forgot Password End-->
<!-- foooter started -->
<?php
    include("footer.php");
    include("footer_tags.php");
?>

  

</body>

</html>