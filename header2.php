    <nav class="navbar navbar-expand-lg navbar-togglable navbar-dark navbarall">
        <div class="container">
            <!-- Brand/ logo -->
            <a class="navbar-brand" href="/index.php"><img src="images/logo.png" alt="logo image" class="img-fluid-logo"></a>
            <!-- Toggler -->
            <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarCollapse"
                aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <span></span><span></span><span></span>
            </button>
            <!-- Collapse -->
            <div class="collapse navbar-collapse" id="navbarCollapse">
                <!-- Links -->
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link active" href="#banner">Home
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="">Services</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="">About Us
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#" target="_blank">Faq
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="">Contact
                        </a>
                    </li>
                    <?php
                    if(!isset($_SESSION['id']))
                    {?>
                    <li class="nav-item">
                        <a class="btn btn-alpha ml-lg-3 mt-3 mt-lg-0" data-toggle="modal" data-target="#login">Login</a>
                    </li>
                    <li class="nav-item">
                        <a class="btn btn-alpha ml-lg-3 mt-3 mt-lg-0" data-toggle="modal" data-target="#signup">Signup</a>
                    </li>
                    <?php 
                    }
                    else 
                    {?>
                        <?php if($_SESSION['type'] == 'mentee' ) { ?>
                            <li class="nav-item">
                                <a class="nav-link" href="mentee/browse_mentor.php">Browser Mentor
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="mentee/my_appointments.php">My Appoinments
                                </a>
                            </li>
                        <?php } elseif($_SESSION['type'] == 'mentor' ) {?>
                            <li class="nav-item">
                                <a class="nav-link" href="/mentor/my_appointment.php">My Appoinments
                                </a>
                            </li>
                        <?php } ?>
                       <li class="nav-item">
                        <a href="logout.php" class="btn btn-alpha ml-lg-3 mt-3 mt-lg-0" >Logout</a>
                    </li>
                    <?php
                    }
                    ?>
                    


                </ul>
            </div>
            <!-- / .navbar-collapse -->
        </div>
        <!-- / .container -->
    </nav>