

    <!-- Login start-->
    <div id="login" class="modal fade  sign_in_up" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Sign In</h4>
                    <p class="reg-btn">New to Purple Lane ? <a href="#" data-toggle="modal" data-target="#signup"
                            id='signup_modal'>Signup</a> </p>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div id="login-overlay" class="modal-dialog2">
                        <div class="modal-content">
                            <div class="modal-body">
                                <div class="sign_up_form">
                                    <form id="login_form" action="#" method="POST">
                                        <div id="error_div">

                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="field">
                                                    <input type="email" name="email" id="email_id" placeholder=""
                                                        required>
                                                    <label for="email_id">Email</label>
                                                </div>
                                                <div class="field">
                                                    <input type="password" name="password" id="password" placeholder=""
                                                        required>
                                                    <label for="password">Password</label>
                                                </div>
                                                <div class="register">
                                                    <label class="ct_box">Remember Me
                                                        <input type="radio" name="radio">
                                                        <span class="checkmark"></span>
                                                    </label>
                                                    <a data-dismiss="modal" data-toggle="modal"
                                                        data-target="#forgot-password" class="forgot-password">Forgot
                                                        Password ?</a>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="reltive_sec">
                                                    <ul id="google_sign_in_element">
                                                        <li>
                                                            <a href="<?php echo  $login_url ?>"><img
                                                                    src="images/google.png" alt="images">
                                                                <span>Sign in with Google</span>
                                                            </a>
                                                        </li>

                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="col-md-12 sign_button text-center">
                                                <button type="button" id="login_form_submit"> Sign In</button>
                                            </div>
                                            <div class="col-md-12 sign_content">
                                                <div class="text-center">
                                                    <p>* By signing up, you agree to our <a href="#">Terms of Use</a>
                                                        and to receive
                                                        Purple Lane emails
                                                        &
                                                        updates and acknowledge that you read our<a href="#"> Privacy
                                                            Policy.</a></p>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>

    <!-- Login End-->


    <!-- Forgot Password start-->
    <div id="forgot-password" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Forgot Passowrd</h4>
                    <p class="reg-btn">*Reset New Passowrd <a href="" data-dismiss="modal" data-toggle="modal"
                            data-target="#signup">Sign Up</a>

                    </p>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>

                </div>
                <div class="modal-body">

                    <div id="login-overlay" class="modal-dialog2">
                        <div class="modal-content">

                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-sm-12 form-rt">
                                        <div class="well">
                                            <form id="forgot_pass_form_add" method="POST" enctype=multipart/form-data>
                                                <div class="form-group">
                                                    <div style="display:none;" id="alert_div_forgot"
                                                        class="alert alert-danger alert-dismissible fade show" role="alert">
                                                        <p>Danger</p>
                                                        <button style="margin-top: -30px;" type="button" class="close"
                                                            data-dismiss="alert" aria-label="Close">
                                                            <span aria-hidden="true"
                                                                style="font-size: 31px;line-height: 0;">&times;</span>
                                                        </button>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <input type="text" class="form-control" id="forgot_email"
                                                        name="forgot_email" placeholder="Email">
                                                    <span class="help-block"></span>
                                                </div>

                                                <button type="submit" class="btn btn-success btn-block">Submit</button>
                                            </form>
                                            <div id="forgot-output"></div>

                                        </div>
                                    </div>

                                </div>

                                <p class=" reg-btn">* By signing up, you agree to our <a href="#">Terms of Use</a> and to
                                    receive Purple Lane emails & updates and acknowledge that
                                    you read our <a href="#">Privacy Policy.</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>


    <!-- signup start-->
    <div id="signup" class="modal fade sign_in_up" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Sign Up </h4>
                    <p class="reg-btn"> Already Have a Purple Lane account ? <a href="#" data-dismiss="modal"
                            data-toggle="modal" data-target="#login" id='sign_in_modal'>Sign In</a>
                    </p>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>

                </div>
                <div class="modal-body">
                    <div id="login-overlay" class="modal-dialog2">
                        <div class="modal-content">
                            <div class="modal-body">
                                <div class="sign_up_form">
                                    <form id="registartion_form" name="registartion_form" method="post">
                                        <div id="register_message">

                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="equal_width clearfix">
                                                    <div class="field left">
                                                        <input type="text" name="firstname" id="firstname"
                                                            placeholder="">
                                                        <label for="firstname">Name</label>
                                                    </div>

                                                    <div class="field right">
                                                        <input type="lastname" name="lastname" id="lastname"
                                                            placeholder="">
                                                        <label for="lastname">Last Name</label>
                                                    </div>
                                                </div>
                                                <div class="field">
                                                    <input type="email" name="email" id="email" placeholder="">
                                                    <label for="email">Email</label>
                                                </div>
                                                <div class="field">
                                                    <input type="password" name="password" id="password1"
                                                        placeholder="">
                                                    <label for="password1">Password</label>
                                                </div>
                                                <div class="field">
                                                    <input type="text" name="mobile" id="mobile" maxlength="13"
                                                        placeholder="">
                                                    <label for="mobile">Mobile Number</label>
                                                </div>
                                            </div>


                                            <div class="col-md-6">
                                                <div class="reltive_sec">
                                                    <ul>
                                                        <li>
                                                            <a href="<?php echo  $login_url ?>"><img
                                                                    src="images/google.png" alt="images">
                                                                <span>Sign in with Google</span>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="col-md-12 sign_button text-center">
                                                <button type="submit"> Sign Up</button>
                                                <!-- <div id="mymessage"></div> -->
                                            </div>

                                            <div class="col-md-12 sign_content">
                                                <div class="text-center">
                                                    <p>* By signing up, you agree to our <a href="#">Terms of Use</a>
                                                        and to receive
                                                        Purple Lane emails
                                                        &
                                                        updates and acknowledge that you read our<a href="#"> Privacy
                                                            Policy.</a></p>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- signup End-->

    <!-- Mentor Signup -->
    <div id="mentor_signup" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Sign Up</h4>
                    <p class="reg-btn">*Already Have a Purple Lane account ?
                        <a href="#" data-dismiss="modal" data-toggle="modal" data-target="#login"
                            id='sign_in_modal1'>Sign In</a>
                    </p>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>

                </div>
                <div class="modal-body">

                    <div id="login-overlay" class="modal-dialog2">
                        <div class="modal-content">
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-sm-12 form-rt">
                                        <div class="well">
                                            <form method="post" id="mentor_registration_form">
                                                <div class="row">
                                                    <div class="form-group col-md-6">
                                                        <input type="text" class="form-control" id="mentor_first_name"
                                                            name="mentor_first_name" placeholder="First Name">
                                                        <span class="help-block"></span>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <input type="text" class="form-control" id="mentor_last_name"
                                                            name="mentor_last_name" placeholder="Last Name">
                                                        <span class="help-block"></span>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <input type="text" class="form-control" id="mentor_email"
                                                        name="mentor_email" placeholder="Email">
                                                    <span class="help-block"></span>
                                                </div>
                                                <div class="form-group">
                                                    <input type="password" class="form-control" id="mentor_password"
                                                        name="mentor_password" placeholder="Password">
                                                    <span class="help-block"></span>
                                                </div>
                                                <div class="form-group">
                                                    <input type="text" class="form-control" id="mentor_mobile"
                                                        name="mentor_mobile" placeholder="Mobile">
                                                    <span class="help-block"></span>
                                                </div>
                                                <div class="form-group">
                                                    <input type="text" class="form-control" id="mentor_cost_card"
                                                        name="mentor_cost_card" placeholder="Cost card price">
                                                    <span class="help-block"></span>
                                                </div>
                                                <div class="form-group">
                                                    <input type="text" class="form-control" id="mentor_overided_price"
                                                        name="mentor_overided_price" placeholder="overided price">
                                                    <span class="help-block"></span>
                                                </div>
                                                <div class="form-group">
                                                    <input type="text" class="form-control" id="mentor_experience"
                                                        name="mentor_experience" placeholder="Total Experience">
                                                    <span class="help-block"></span>
                                                </div>
                                                <!-- <div class="form-group">
									    <input type="file" class="form-control" id="photo" name="photo">
									    <span class="help-block"></span>
								</div> -->

                                                <button type="button" class="btn btn-success btn-block"
                                                    id="mentor_registration_button">Sign Up</button>
                                                <div id="mentor_output"></div>
                                            </form>
                                        </div>
                                    </div>

                                </div>

                                <p class=" reg-btn">* By signing up, you agree to our <a href="#">Terms of Use</a> and
                                    to
                                    receive Purple Lane emails & updates and acknowledge that
                                    you read our <a href="#">Privacy Policy.</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>
    <!-- Mentor Signup End -->