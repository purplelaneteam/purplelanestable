<?php
require_once "includes/initialize.php";
if(isset($_COOKIE["USER_ID"]) && isset($_COOKIE["USER_TOKEN"]))
    {
        header("Location: /index.php");
    }

    if(isset($_GET['reset_code']) && $_GET['reset_code']!="" && isset($_GET['type']) && $_GET['type']!="")
    {
        $reset_code = sanitize_input($_GET['reset_code']);
        $type = sanitize_input($_GET['type']);
        $sql = "SELECT id FROM $type WHERE reset_code = '".$reset_code."'";
        $result = mysqli_query($con, $sql);
        // dd($sql);
        if(!($result && $myrow = mysqli_fetch_array($result)))
        {
            $error = "Reset password link expired.";
        }
    }
    else 
    {
        header("Refresh:3; url=/index.php");
        exit;
    }	
    
    if(!isset($error))
    {
        if(count($_POST)>0)
        {
            if($_POST['newpassword'] == $_POST['confirmnewpassword'])
            {
                $newpassword = md5(sanitize_input($_POST['newpassword']));
                $newtoken = md5(uniqid());
                $sql_updatepassword = "UPDATE $type SET token = '$newtoken', password = '$newpassword', reset_code = '' WHERE reset_code = '$reset_code'";
                // dd($sql_updatepassword);
                if(mysqli_query($con, $sql_updatepassword))
                {
                    header("Refresh:3; url=index.php");
                    $success = "Password changed successfully.";
                }
                else
                {
                    $error = "Something went wrong while updating password.";
                }
            }
            else
            {
                $error = "Password and confirm password mismatched.";
            }
        }
    }

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?php echo SITE_NAME;?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="/css/main.css">
    <link rel="stylesheet" href="/css/design.css" id="dynamic-style">
    <link rel="stylesheet" href="/css/responsive.css"></head>
    <style>
    .nav{
    
    display: block;
        flex-wrap: wrap;
        padding-left: 0;
        margin-bottom: 0;
        list-style: none;
    }
    </style>

  <link rel="stylesheet" type="text/css" href="/css/jquery.datetimepicker.css"/>
   
</head>
<body data-spy="scroll" data-target=".navbar" data-offset="82">
    <div class="site-loader">
        <!---======Preloader===========-->
        <div class="loader-dots">
            <div class="circle circle-1"></div>
            <div class="circle circle-2"></div>
        </div>
    </div>
    <div class="site__layer"></div>

	<section class="burger app" id="app">
	<div class="container">
		<!-- container-start -->
		<div class="container">
			<div class="row">
				<div class="col-md-12 text-center" style="margin-top:40px">
					<div class="tab-content" id="nav-tabContent">
						<div class="tab-pane fade show active" id="pop2" role="tabpanel" aria-labelledby="pop2-tab">
							<!-- Choose a Mentor start -->
							<div class="row">
								<div class="col-md-12 col-sm-12  ">
									<div class="store-btns">
										<div class=" review-box">
											<div class="tabs">
												<div class="container">
													<div class="row">
														<div class="col-md-12">
															<div class="tab-content">
																<div class="tab-pane fade active show" id="pop4" role="tabpanel" aria-labelledby="pop4-tab">
																	<p>
																		<div class="card border-primary rounded-0">
																			<div class="card-header p-0">
																				<div class="bg-info text-white text-center py-2">
																					<h6> Reset Password
                                                               <span class="edt"><i class="fa fa-pencil-square"></i></span>
                                                            </h6>
																				</div>
																			</div>
																			<form action="" method="POST" enctype="multipart/form-data">
																			<div class="card-body p-3">
                                                                            <?php if(isset($error)): ?>
                                                                                <div class="alert alert-danger alert-dismissible" role="alert">			
                                                                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
                                                                                    <span><?php echo  $error; ?></span>
                                                                                </div>
                                                                                <?php endif ?>
                                                                            <?php if(isset($success)): ?>
                                                                                <div class="alert alert-success alert-dismissible" role="alert">			
                                                                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
                                                                                    <span><?php echo  $success; ?></span>
                                                                                </div>
                                                                            <?php endif ?>
																				<!--Body-->
																				<div class="form-group">
																					<div style="display:none;" id="alert_div_password" class="alert alert-danger alert-dismissible fade show" role="alert">
																						<p>Danger</p>
																						<button style="margin-top: -30px;" type="button" class="close" data-dismiss="alert" aria-label="Close">
																							<span aria-hidden="true"style="font-size: 31px;line-height: 0;">&times;</span>
																						</button>
																					</div>
																				</div>
																				
																				<div class="form-group">
																					<div class="input-group mb-2">
																						<div class="input-group-prepend">
																							<div class="input-group-text"><i class="fa fa-key text-info"></i>
																							</div>
																						</div>
																						<input type="password" class="form-control" id="newpassword" name="newpassword" placeholder="New Password" required>
																						<div class="input-group-addon">
																						</div>
																					</div>
																				</div>
																				<div class="form-group">
																					<div class="input-group mb-2">
																						<div class="input-group-prepend">
																							<div class="input-group-text"><i class="fa fa-key text-info"></i>
																							</div>
																						</div>
																						<input type="password" class="form-control" id="confirmnewpassword" name="confirmnewpassword" placeholder="Retype New Password" required>
																						<div class="input-group-addon">
																						</div>
																					</div>
																				</div> <button type="submit" class="btn btn-alpha mr-lg-3 mr-2 cnect"> Update Password <i class="fa fa-angle-right ml-3"></i></button>
																				</form>
																			</div>
																		</div>
																	</p>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<!---/col-->
								</div>
								<!-- /row  -->
							</div>
							<div class="tab-pane fade" id="pop3" role="tabpanel" aria-labelledby="pop3-tab">
								<div class="pt-3"></div>
								<p>3. There's another way to do this for layouts that doesn't have to put the navbar inside the container, and which doesn't require any CSS or Bootstrap overrides. Simply place a div with the Bootstrap container class around the navbar. This will center the links inside the navbar:</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- container end -->
</section>
<?php 
require_once('footer.php');
require_once('footer_tags.php');
?>
	