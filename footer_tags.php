
    <script src="<?php echo SITE_URL;?>/js/jquery3.2.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.1/dist/jquery.validate.js"></script>
    <!-- JQUERY LIBRARY -->
    <script src="<?php echo SITE_URL;?>/js/particles.min.js"></script>
    <!-- Particles JS -->
    <script src="<?php echo SITE_URL;?>/js/bootstrap.min.js"></script>
    <!-- Bootstrap JS -->
    <script src="<?php echo SITE_URL;?>/js/main.js"></script>
    <script src="<?php echo SITE_URL;?>/js/custom.js"></script>
    <script src="<?php echo SITE_URL;?>/js/select2.js"></script>
 
    <!-- Main JS -->
    <script type="text/javascript" src="<?php echo SITE_URL;?>/js/indexpage.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js" type="text/javascript"></script>
     <script src="<?php echo SITE_URL;?>/assets/js/common.js"></script>
    <script type="text/javascript" src="<?php echo SITE_URL;?>/assets/js/frontend.js?<?php echo time() ?>"></script>
	
    <script type="text/javascript" src="<?php echo SITE_URL;?>/js/jquery.datetimepicker.js"></script>

    <script>
        $(document).ready(function() {
            $("#show_hide_password a").on('click', function(event) {
                event.preventDefault();
                if($('#show_hide_password input').attr("type") == "text"){
                    $('#show_hide_password input').attr('type', 'password');
                    $('#show_hide_password i').addClass( "fa-eye-slash" );
                    $('#show_hide_password i').removeClass( "fa-eye" );
                }else if($('#show_hide_password input').attr("type") == "password"){
                    $('#show_hide_password input').attr('type', 'text');
                    $('#show_hide_password i').removeClass( "fa-eye-slash" );
                    $('#show_hide_password i').addClass( "fa-eye" );
                }
            });
            function logEvent(type, date) {
                $("<div class='log__entry'/>").hide().html("<strong>"+type + "</strong>: "+date).prependTo($('#eventlog')).show(200);
            }
            $('#clearlog').click(function() {
                $('#eventlog').html('');
            });

            $('#demo2').dateTimePicker({
                date: new Date(),
                viewMode: 'YMD',
                onDateChange: function(){
                    $('#date-text2').text(this.getText());
                    $('#date-text-ymd2').text(this.getText('YYYY-MM-DD'));
                    $('#date-value2').text(this.getValue());
                }
            });
        });
    </script>


  
<script src="<?php echo SITE_URL;?>/js/datatable/jquery.dataTables.min.js"></script>
<script src="<?php echo SITE_URL;?>/js/datatable/dataTables.buttons.min.js"></script>
<script src="<?php echo SITE_URL;?>/js/datatable/jszip.min.js"></script>
<script src="<?php echo SITE_URL;?>/js/datatable/pdfmake.min.js"></script>
<script src="<?php echo SITE_URL;?>/js/datatable/vfs_fonts.js"></script>
<script src="<?php echo SITE_URL;?>/js/datatable/buttons.html5.min.js"></script>
<script src="<?php echo SITE_URL;?>/js/sweetalert.js"></script>
<script src="<?php echo SITE_URL;?>/js/bs-stepper.js"></script>
<script src="<?php echo SITE_URL;?>/js/slick.js"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.0/moment-with-locales.min.js"></script>
<script src="<?php echo SITE_URL;?>/js/datetimepicker.js"></script>
<script src="<?php echo SITE_URL;?>/js/jQuery-datetime-plugin/jquery.datetimepicker.full.min.js"></script>

<script>
    $('.testimonials .slider').slick({
        dots: true,
        infinite: false,
        arrows: false,
        speed: 300,
        slidesToShow: 2,
        slidesToScroll: 1,
        responsive: [
            {
            breakpoint: 881,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
            },
            {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
            }
        ]
    });


    $('.want_to_get_homeSlider .aaalll').slick({
        dots: false,
        infinite: true,
        speed: 300,
        arrows: true,
        slidesToShow: 1,
        slidesToScroll: 1,
  });
	
</script>

<script type="text/javascript">
	
    $(document).ready( function () {
		// moment().format("YYYY-MM-DD HH:mm")
		minute = moment().minute()
		if (minute >= 30) minute = 30
		else minute = 0;
		const tomorrow = moment().add(1, 'days').minute(minute)
		// $('#slot1').val(moment(tomorrow).format("YYYY-MM-DD HH:mm"))
		// $('#slot2').val(moment(tomorrow).format("YYYY-MM-DD HH:mm"))

        // $('#picker').dateTimePicker({selectDate: tomorrow, startDate: tomorrow, minuteIncrement: 30});

		// $('#picker2').dateTimePicker({selectDate: tomorrow, startDate: tomorrow, minuteIncrement: 30});

    });
    </script>
</body>

</html>