ALTER TABLE `orders` ADD `temp_key` VARCHAR(199) NULL AFTER `customer_id`, ADD `addedon` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `temp_key`;

ALTER TABLE `meetings` ADD `order_id` VARCHAR(199) NULL DEFAULT NULL AFTER `hangout_link`;
