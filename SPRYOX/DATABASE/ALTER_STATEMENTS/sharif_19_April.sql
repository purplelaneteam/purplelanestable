ALTER TABLE `mentor_ratings` CHANGE `ratings` `ratings` FLOAT(10) UNSIGNED NOT NULL;
ALTER TABLE `mentor` ADD `description` TEXT NOT NULL DEFAULT '' AFTER `total_experience`;