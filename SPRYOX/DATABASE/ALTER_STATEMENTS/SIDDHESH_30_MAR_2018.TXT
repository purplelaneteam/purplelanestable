DROP TABLE cost_card_type_prices;

ALTER TABLE `cost_card_type` ADD `price` DECIMAL(10,2) NOT NULL AFTER `codename`;

ALTER TABLE `category` CHANGE `modifiedon` `modifiedon` DATETIME NULL DEFAULT NULL;

ALTER TABLE `topic` CHANGE `modifiedon` `modifiedon` DATETIME NULL DEFAULT NULL;

ALTER TABLE `mentor` CHANGE `active` `is_active` TINYINT(1) NULL DEFAULT '0' COMMENT '1 for active and 0 for inactive';

ALTER TABLE `mentor_categories` ADD UNIQUE( `mentor_id`, `topic_id`);

ALTER TABLE `meetings` DROP `selected_time`;

ALTER TABLE `meetings` ADD `slot_1` DATETIME NULL DEFAULT NULL AFTER `meeting_status_id`, ADD `slot_2` DATETIME NULL DEFAULT NULL AFTER `slot_1`;

ALTER TABLE `meetings` DROP `meeting_date`;

ALTER TABLE `meeting_selected_slots` ADD `meeting_date` DATE NULL DEFAULT NULL AFTER `meetings_id`;

ALTER TABLE `meetings` CHANGE `meeting_selected_slots_id` `meeting_selected_slots_id` INT(10) UNSIGNED NULL DEFAULT NULL;


CREATE TABLE mentee_transactions (
    id INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    meetings_id INT(11) UNSIGNED NOT NULL,
    amount double(10,2) NOT NULL,
    type ENUM('debit', 'credit') DEFAULT NULL,
    addedon DATETIME NOT NULL,
    modifiedon DATETIME NULL,
    CONSTRAINT mente_meetings_id
        FOREIGN KEY (meetings_id)
        REFERENCES meetings (id)
        ON DELETE CASCADE
        ON UPDATE CASCADE
);

CREATE TABLE mentor_transactions (
    id INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    meetings_id INT(11) UNSIGNED NOT NULL,
    amount double(10,2) NOT NULL,
    type ENUM('debit', 'credit') DEFAULT NULL,
    transaction_type ENUM('open', 'close') DEFAULT NULL,
    addedon DATETIME NOT NULL,
    modifiedon DATETIME NULL,
    CONSTRAINT mento_meetings_id
        FOREIGN KEY (meetings_id)
        REFERENCES meetings (id)
        ON DELETE CASCADE
        ON UPDATE CASCADE
);

CREATE TABLE admin_transactions (
    id INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    meetings_id INT(11) UNSIGNED NOT NULL,
    amount double(10,2) NOT NULL,
    type ENUM('debit', 'credit') DEFAULT NULL,
    addedon DATETIME NOT NULL,
    modifiedon DATETIME NULL,
    CONSTRAINT admin_meetings_id
        FOREIGN KEY (meetings_id)
        REFERENCES meetings (id)
        ON DELETE CASCADE
        ON UPDATE CASCADE
);

CREATE TABLE taxes_transactions (
    id INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    meetings_id INT(11) UNSIGNED NOT NULL,
    amount double(10,2) NOT NULL,
    type ENUM('debit', 'credit') DEFAULT NULL,
    addedon DATETIME NOT NULL,
    modifiedon DATETIME NULL,
    CONSTRAINT taxes_meetings_id
        FOREIGN KEY (meetings_id)
        REFERENCES meetings (id)
        ON DELETE CASCADE
        ON UPDATE CASCADE
);