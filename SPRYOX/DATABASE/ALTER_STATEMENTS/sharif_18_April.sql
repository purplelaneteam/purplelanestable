ALTER TABLE `meetings` ADD `title` VARCHAR(199) NOT NULL DEFAULT '' AFTER `meeting_status_id`;

ALTER TABLE `meetings` ADD `distribution_status` BOOLEAN NOT NULL DEFAULT FALSE AFTER `comments`;