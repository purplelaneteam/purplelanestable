ALTER TABLE `meetings` DROP `slot_1`, DROP `slot_2`;
ALTER TABLE `meetings` ADD `meeting_date` DATE NOT NULL AFTER `meeting_selected_slots_id`, ADD `selected_time` TIME NULL DEFAULT NULL AFTER `meeting_date`;
ALTER TABLE `meeting_selected_slots` DROP `meeting_date`;

ALTER TABLE `mentee` CHANGE `password` `password` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;

CREATE TABLE meetings_status_history(
    id INT(10) UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
    meetings_id INT(10) UNSIGNED NOT NULL,
    meetings_status_id INT(10) UNSIGNED NOT NULL,
    meeting_selected_slots_id INT(10) UNSIGNED NOT NULL,
    meeting_date DATE NOT NULL,
    cost_card_price double(10,2) NOT NULL,
    service_tax_price double(10,2) NOT NULL,
    platform_charges double(10,2) NOT NULL,
    selected_time time NOT NULL,
    addedon datetime NOT NULL
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;;
ALTER TABLE meetings_status_history
ADD CONSTRAINT meet_stat_meet_idfk FOREIGN KEY (meetings_id) REFERENCES meetings(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `meetings_status_history` ADD CONSTRAINT `meet_stat_meet_stathistidfk` FOREIGN KEY (`meetings_status_id`) REFERENCES `meeting_status`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `meetings_status_history` ADD CONSTRAINT `meet_stat_sel_slotidfk` FOREIGN KEY (`meeting_selected_slots_id`) REFERENCES `meeting_selected_slots`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;
    
