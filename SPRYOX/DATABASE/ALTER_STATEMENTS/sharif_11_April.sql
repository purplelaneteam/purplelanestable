ALTER TABLE `orders` ADD `customer_id` VARCHAR(199) NOT NULL AFTER `order_id`;

ALTER TABLE `orders` ADD `mid` VARCHAR(199) NOT NULL AFTER `transaction_date`, ADD `response_code` VARCHAR(199) NOT NULL AFTER `mid`, ADD `response_msg` VARCHAR(199) NOT NULL AFTER `response_code`, ADD `check_sum_hash` VARCHAR(199) NOT NULL AFTER `response_msg`;