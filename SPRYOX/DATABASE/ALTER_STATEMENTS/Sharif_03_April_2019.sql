ALTER TABLE `meetings` CHANGE `addedon` `addedon` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `meetings` ADD `comments` TEXT NOT NULL AFTER `meeting_status_id`;

ALTER TABLE `meetings_status_history` CHANGE `addedon` `addedon` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP;

ALTER TABLE `meetings_status_history` ADD `comments` TEXT NOT NULL AFTER `selected_time`;

ALTER TABLE `meeting_selected_slots` CHANGE `addedon` `addedon` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP;

ALTER TABLE `meetings_status_history` CHANGE `meeting_selected_slots_id` `meeting_selected_slots_id` INT(10) UNSIGNED NULL;