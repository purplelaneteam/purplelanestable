CREATE TABLE `mentor_mentee`.`coupons` ( `id` INT UNSIGNED NOT NULL AUTO_INCREMENT , `title` VARCHAR(199) NULL , `description` TEXT NULL , `type` ENUM('percent','flat') NOT NULL DEFAULT 'flat' , `value` INT(10) NOT NULL DEFAULT '0' , `use_limit` INT(10) NOT NULL DEFAULT '0' , `active` BOOLEAN NOT NULL DEFAULT TRUE , `addedon` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP , `modefiedon` TIMESTAMP NULL DEFAULT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;

CREATE TABLE `mentor_mentee`.`coupon_used_by_mentee` ( `id` INT UNSIGNED NOT NULL AUTO_INCREMENT , `mentee_id` INT(10) UNSIGNED NOT NULL , `coupon_id` INT(10) UNSIGNED NOT NULL , `addedon` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP , `modefiedon` TIMESTAMP NULL DEFAULT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;

ALTER TABLE `coupon_used_by_mentee` ADD CONSTRAINT `mentee_coupon_used_fk` FOREIGN KEY (`mentee_id`) REFERENCES `mentee`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE `coupon_used_by_mentee` ADD CONSTRAINT `coupon_used_fk` FOREIGN KEY (`coupon_id`) REFERENCES `coupons`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE `coupon_used_by_mentee` ADD `used` BOOLEAN NOT NULL DEFAULT FALSE AFTER `coupon_id`;

INSERT INTO `coupons` (`id`, `title`, `description`, `type`, `value`, `use_limit`, `active`, `addedon`, `modefiedon`) VALUES (NULL, 'MENT50', 'Desscription', 'percent', '50', '15', '1', CURRENT_TIMESTAMP, NULL), (NULL, 'MENTOFF', 'Description', 'flat', '10', '15', '1', CURRENT_TIMESTAMP, NULL);

ALTER TABLE `orders` ADD `coupon` VARCHAR(199) NULL DEFAULT NULL AFTER `order_id`;