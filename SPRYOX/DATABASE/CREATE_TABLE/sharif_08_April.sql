CREATE TABLE `mentor_bank_account` ( `id` INT NOT NULL , `bank_name` VARCHAR(199) NOT NULL , `account_number` VARCHAR(199) NOT NULL , `ifsc` VARCHAR(199) NOT NULL , `mentor_id` INT UNSIGNED NOT NULL , `addedon` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP , `modifiedon` DATETIME NULL DEFAULT NULL , PRIMARY KEY (`id`), INDEX `mentor_bank_index` (`mentor_id`)) ENGINE = InnoDB;

ALTER TABLE `mentor_bank_account` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `mentor_bank_account` ADD CONSTRAINT `mentor_bank_fk` FOREIGN KEY (`mentor_id`) REFERENCES `mentor`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;


/* Education */
CREATE TABLE `mentor_education` ( `id` INT NOT NULL AUTO_INCREMENT , `mentor_id` INT(10) NOT NULL , `name` VARCHAR(199) NOT NULL , `description` TEXT NOT NULL , `addedon` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP , `modefiedon` DATETIME NULL DEFAULT NULL , PRIMARY KEY (`id`), INDEX `mentor_education_index` (`mentor_id`)) ENGINE = InnoDB;

ALTER TABLE `mentor_education` CHANGE `mentor_id` `mentor_id` INT(10) UNSIGNED NOT NULL;


ALTER TABLE `mentor_education` ADD CONSTRAINT `mentor_education_fk` FOREIGN KEY (`mentor_id`) REFERENCES `mentor`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;


/* Experience */
CREATE TABLE `mentor_experience` ( `id` INT(10) NOT NULL AUTO_INCREMENT , `mentor_id` INT(10) UNSIGNED NOT NULL , `name` VARCHAR(199) NOT NULL , `description` TEXT NOT NULL , `addedon` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP , `modefiedon` DATETIME NULL DEFAULT NULL , PRIMARY KEY (`id`), INDEX `mentor_education_index` (`mentor_id`)) ENGINE = InnoDB;

ALTER TABLE `mentor_experience` ADD CONSTRAINT `mentor_experience_fk` FOREIGN KEY (`mentor_id`) REFERENCES `mentor`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
