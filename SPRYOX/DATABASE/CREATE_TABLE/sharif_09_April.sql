CREATE TABLE `orders` ( `id` INT NOT NULL AUTO_INCREMENT , `order_id` VARCHAR(199) NOT NULL , `transaction_id` VARCHAR(199) NOT NULL , `amount` VARCHAR(10) NOT NULL , `payment_mode` VARCHAR(199) NOT NULL , `currency` VARCHAR(3) NOT NULL , `transaction_date` DATETIME NOT NULL , `status` VARCHAR(199) NOT NULL , `gate_way_name` VARCHAR(199) NOT NULL , `bank_txn_id` VARCHAR(199) NOT NULL , `bank_name` VARCHAR(199) NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;

ALTER TABLE `orders` ADD `data` TEXT NOT NULL AFTER `order_id`;

ALTER TABLE `orders` ADD UNIQUE(`order_id`);

/* Null values */
ALTER TABLE `orders` CHANGE `transaction_id` `transaction_id` VARCHAR(199) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL;

ALTER TABLE `orders` CHANGE `amount` `amount` VARCHAR(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL;

ALTER TABLE `orders` CHANGE `payment_mode` `payment_mode` VARCHAR(199) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL;

ALTER TABLE `orders` CHANGE `currency` `currency` VARCHAR(3) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL;

ALTER TABLE `orders` CHANGE `transaction_date` `transaction_date` DATETIME NULL;

ALTER TABLE `orders` CHANGE `status` `status` VARCHAR(199) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL;

ALTER TABLE `orders` CHANGE `gate_way_name` `gate_way_name` VARCHAR(199) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL;

ALTER TABLE `orders` CHANGE `bank_txn_id` `bank_txn_id` VARCHAR(199) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL;

ALTER TABLE `orders` CHANGE `bank_name` `bank_name` VARCHAR(199) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL;

