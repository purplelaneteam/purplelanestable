CREATE TABLE `settings` (
 `id` int(11) NOT NULL,
 `name` varchar(199) NOT NULL,
 `codename` varchar(199) NOT NULL,
 `price` int(11) NOT NULL,
 `addedon` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
 `modefiedon` datetime DEFAULT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `settings` (`id`, `name`, `codename`, `price`, `addedon`, `modefiedon`) VALUES (1, 'Service Tax', 'service_tax_price', '10', CURRENT_TIMESTAMP, NULL), (2, 'Platform Charges', 'platform_charges', '15', CURRENT_TIMESTAMP, NULL);