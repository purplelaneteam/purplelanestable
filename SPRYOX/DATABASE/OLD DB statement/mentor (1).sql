
CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `name` varchar(191) NOT NULL,
  `codename` varchar(191) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `active` tinyint(1) NOT NULL,
  `addedon` datetime NOT NULL,
  `modifiedon` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `cost_card_type` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `codename` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `Addedon` datetime NOT NULL,
  `Modifiedon` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



CREATE TABLE `cost_card_type_prices` (
  `id` int(10) UNSIGNED NOT NULL,
  `cost_cardtype_id` int(10) UNSIGNED NOT NULL,
  `cost_cardtype_price` int(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


CREATE TABLE `employee` (
  `id` int(10) UNSIGNED NOT NULL,
  `fname` varchar(250) NOT NULL,
  `lname` varchar(250) NOT NULL,
  `name` varchar(250) NOT NULL COMMENT 'fullname column',
  `phone` varchar(150) NOT NULL,
  `email` varchar(250) NOT NULL,
  `password` varchar(250) NOT NULL,
  `reset_code` varchar(250) DEFAULT NULL,
  `token` varchar(250) NOT NULL,
  `active` tinyint(1) DEFAULT '0' COMMENT '1 for actve and 0 for inactive',
  `is_deleted` tinyint(1) DEFAULT '0' COMMENT '1 for deleted_account and 0 for active_account',
  `role` enum('employee','admin') NOT NULL DEFAULT 'employee',
  `addedon` datetime NOT NULL,
  `modifiedon` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `goal` (
  `id` int(11) NOT NULL,
  `name` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `mentee` (
  `id` int(11) NOT NULL,
  `name` varchar(100) CHARACTER SET utf8 NOT NULL,
  `email` varchar(100) CHARACTER SET utf8 NOT NULL,
  `mobile_no` varchar(13) CHARACTER SET utf8 NOT NULL,
  `profile_pic` varchar(255) CHARACTER SET utf8 NOT NULL,
  `password` varchar(20) CHARACTER SET utf8 NOT NULL,
  `google_id` varchar(255) CHARACTER SET utf8 NOT NULL,
  `register_type` enum('manual','google') CHARACTER SET utf8 NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `addedon` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modifiedon` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `mentor` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'fullname column',
  `email` varchar(100) CHARACTER SET utf8 NOT NULL,
  `mobile` varchar(13) COLLATE utf8_unicode_ci NOT NULL,
  `paytm_mobile` varchar(13) CHARACTER SET utf8 NOT NULL,
  `profile_pic` text COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `cost_card_price` double(10,2) NOT NULL,
  `overided_price` double(10,2) NOT NULL,
  `total_experience` int(11) NOT NULL,
  `active` tinyint(1) DEFAULT '0' COMMENT '1 for active and 0 for inactive',
  `addedon` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modifiedon` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



CREATE TABLE `question` (
  `id` int(11) NOT NULL,
  `name` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `topic` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `addedon` datetime NOT NULL,
  `modifiedon` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


ALTER TABLE `category`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `codename` (`codename`),
  ADD KEY `category_fk` (`parent_id`);

ALTER TABLE `cost_card_type`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `codename` (`codename`);

ALTER TABLE `cost_card_type_prices`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `employee`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `goal`
  ADD PRIMARY KEY (`id`);


ALTER TABLE `mentee`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`,`mobile_no`);

ALTER TABLE `mentor`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `phone` (`mobile`),
  ADD UNIQUE KEY `paytm_phone` (`paytm_mobile`);

ALTER TABLE `question`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `topic`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_id_fk` (`category_id`);

ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;


ALTER TABLE `cost_card_type`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `cost_card_type_prices`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `employee`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `goal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `mentee`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `mentor`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `question`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `topic`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `category`
  ADD CONSTRAINT `category_fk` FOREIGN KEY (`parent_id`) REFERENCES `category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `topic`
  ADD CONSTRAINT `category_id_fk` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON UPDATE CASCADE;
