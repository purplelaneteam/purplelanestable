<?php
	include('includes/config.php');
	class GoogleLoginApi
	{
		public function GetAccessToken($client_id, $redirect_uri, $client_secret, $code) {	
			$url = 'https://www.googleapis.com/oauth2/v4/token';			
			
			$curlPost = 'client_id=' . $client_id . '&redirect_uri=' . $redirect_uri . '&client_secret=' . $client_secret . '&code='. $code . '&grant_type=authorization_code';
			$ch = curl_init();		
			curl_setopt($ch, CURLOPT_URL, $url);		
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);		
			curl_setopt($ch, CURLOPT_POST, 1);		
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $curlPost);	
			$data = json_decode(curl_exec($ch), true);
			$http_code = curl_getinfo($ch,CURLINFO_HTTP_CODE);		
			if($http_code != 200) 
				throw new Exception('Error : Failed to receieve access token');
				
			return $data;
		}

		public function GetUserProfileInfo($access_token) {	
			$url = 'https://www.googleapis.com/oauth2/v2/userinfo?fields=name,email,gender,id,picture,verified_email';			
			
			$ch = curl_init();		
			curl_setopt($ch, CURLOPT_URL, $url);		
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: Bearer '. $access_token));
			$data = json_decode(curl_exec($ch), true);
			$http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);		
			if($http_code != 200) 
				throw new Exception('Error : Failed to get user information');
				
			return $data;
		}
	}
	if(isset($_GET['code'])) 
	{
		try {
			$gapi = new GoogleLoginApi();
			
			// Get the access token 
			$data = $gapi->GetAccessToken(CLIENT_ID, CLIENT_REDIRECT_URL, CLIENT_SECRET, $_GET['code']);
			
			// Get user information
			$user_info = $gapi->GetUserProfileInfo($data['access_token']);
		}
		catch(Exception $e) {
			echo $e->getMessage();
			exit();
		}
	}
?>
<head>
<style type="text/css">

#information-container {
	width: 400px;
	margin: 50px auto;
	padding: 20px;
	border: 1px solid #cccccc;
}

.information {
	margin: 0 0 30px 0;
}

.information label {
	display: inline-block;
	vertical-align: middle;
	width: 150px;
	font-weight: 700;
}

.information span {
	display: inline-block;
	vertical-align: middle;
}

.information img {
	display: inline-block;
	vertical-align: middle;
	width: 100px;
}

</style>
</head>

<body>

<div id="information-container">
	<div class="information">
		<label>Name</label><span><?php echo  $user_info['name']; ?></span>
	</div>
	<div class="information">
		<label>ID</label><span><?php echo  $user_info['id']; ?></span>
	</div>
	<div class="information">
		<label>Email</label><span><?php echo  $user_info['email']; ?></span>
	</div>
	<div class="information">
		<label>Email Verified</label><span><?php echo  $user_info['verified_email'] == true ? 'Yes' : 'No' ?></span>
	</div>
	<div class="information">
		<label>Picture</label><img src="<?php echo  $user_info['picture']; ?>" />
	</div>
</div>

</body>
</html>

?>