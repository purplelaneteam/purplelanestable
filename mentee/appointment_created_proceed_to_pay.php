<?php
    require_once "../includes/initialize.php";

    $temp_key = $_GET['order'];

    $sql = "SELECT id, data, order_id, addedon FROM orders WHERE temp_key = '$temp_key'";
    $result = mysqli_query($con, $sql);
    if ($result)
    {
        $order = mysqli_fetch_assoc($result);
        if (date_create() < date_create($order['addedon'] .' +10min'))
        {
            $data = unserialize($order['data']);
    
            $mentor_id = $data['mentor_id'];
            $topic_id = $data['topic_id'];
    
            $_SESSION['order_id'] = $order['order_id'];
    
            $mentee = getMenteeBasicInfo($con, $data['mentee_id']);
            
            $_SESSION['id'] = $mentee['id'];
            $_SESSION['type'] = 'mentee';
            $_SESSION['fname'] = $mentee['fname'];
            $_SESSION['lname'] = $mentee['lname'];
            $_SESSION['email'] = $mentee['email'];
            
            header("Location: /mentee/proceed_to_payment.php?mentor_id=$mentor_id&topic_id=$topic_id");
        }
        echo 'Link Expired Try again';
    }