<?php
require_once "../includes/initialize.php";
require_once "logincheck.php";

$id = $_SESSION['id'];
$user = getMentorBasicInfo($con, $id);
require_once "header.php";
?>


<?php require_once '../header.php' ?>
<?php require_once 'navbar.php' ?>

	<section class="burger2 app" id="app">
        <div class="container">
            <!-- container-start -->
			
			<div class="container">
				<div class="row">
					<div class="col-md-12 text-center" style="margin-top:40px">
						<nav class="nav-justified ">
						  <div class="nav nav-tabs " id="nav-tab" role="tablist">
							<a class="nav-item nav-link " id="pop1-tab" data-toggle="tab" href="#pop1" role="tab" aria-controls="pop1" aria-selected="true"> <b>Your Requirments</b></a>
							<a class="nav-item nav-link active" id="pop2-tab" data-toggle="tab" href="#pop2" role="tab" aria-controls="pop2" aria-selected="false"> <b>View Mentors</b></a>
							<a class="nav-item nav-link" id="pop3-tab" data-toggle="tab" href="#pop3" role="tab" aria-controls="pop3" aria-selected="false">  <b>Confirm Your Schedule</b></a>
							
						  </div>
						</nav>
						<div class="tab-content" id="nav-tabContent">
						    <div class="tab-pane fade" id="pop1" role="tabpanel" aria-labelledby="pop1-tab">
								<div class="pt-3"></div>
								<p>1. There's another way to do this for layouts that doesn't have to put the navbar inside the container, and which doesn't require any CSS or Bootstrap overrides.

								Simply place a div with the Bootstrap container class around the navbar. This will center the links inside the navbar:</p>
							</div>
							  
						    <div class="tab-pane fade show active" id="pop2" role="tabpanel" aria-labelledby="pop2-tab">
							   <div class="pt-3"></div>
							   
							    <div class="row">
									<div class="col-lg-12 col-sm-12 text-center">
										<h3 class="mb-0 mb-lg-3 lh-1">Choose a Mentor</h3>
									</div>
								</div>
								
								<!-- Choose a Mentor start -->
								
													
								<div class="row">
									<div class="col-md-3 col-sm-12 order-first mb-0 mb-md-0 order-md-0">
									  
										<div class="side-box">
											 <h6 class="side-heading">Courses</h6>
											<div class="chiller_cb">
												<input id="myCheckbox" type="checkbox" checked>
												<label for="myCheckbox">Frontend Development</label>
												<span></span>
											</div>
											<div class="chiller_cb">
												<input id="myCheckbox2" type="checkbox">
												<label for="myCheckbox2">Backend Development</label>
												<span></span>
											</div>
											<div class="chiller_cb">
												<input id="myCheckbox3" type="checkbox" checked>
												<label for="myCheckbox3">Testing Tools</label>
												<span></span>
											</div>
											<div class="chiller_cb">
												<input id="myCheckbox4" type="checkbox">
												<label for="myCheckbox4">Oracle DBA</label>
												<span></span>
											</div>
											
										</div>
										
										<div class="side-box">
											 <h6 class="side-heading">Company</h6>
											<div class="chiller_cb">
												<input id="myCheckbox5" type="checkbox" checked>
												<label for="myCheckbox5">Activision Blizzard</label>
												<span></span>
											</div>
											<div class="chiller_cb">
												<input id="myCheckbox6" type="checkbox">
												<label for="myCheckbox6">Panasonic Corporation</label>
												<span></span>
											</div>
											<div class="chiller_cb">
												<input id="myCheckbox7" type="checkbox" checked>
												<label for="myCheckbox7">Prudential Financial</label>
												<span></span>
											</div>
											<div class="chiller_cb">
												<input id="myCheckbox8" type="checkbox">
												<label for="myCheckbox8">HCL Technologies</label>
												<span></span>
											</div>
											
										</div>
										
										<div class="side-box">
											 <h6 class="side-heading">Skills</h6>
											<div class="chiller_cb">
												<input id="myCheckbox9" type="checkbox" checked>
												<label for="myCheckbox9">Facility Management </label>
												<span></span>
											</div>
											<div class="chiller_cb">
												<input id="myCheckbox10" type="checkbox">
												<label for="myCheckbox10">Training Methods</label>
												<span></span>
											</div>
											<div class="chiller_cb">
												<input id="myCheckbox11" type="checkbox" checked>
												<label for="myCheckbox11">Training Consultant</label>
												<span></span>
											</div>
											<div class="chiller_cb">
												<input id="myCheckbox12" type="checkbox">
												<label for="myCheckbox12">Database Management</label>
												<span></span>
											</div>
											
										</div>
										
									</div>
									<!---/col-->
									<div class="col-md-9 col-sm-12  ">
									   
										<div class="store-btns">
											 <h2 class="section-heading review-head"></h2>
										
												<div class="row review-box">
													<div class="col-md-3 col-sm-12">
															<div class="app-image">
																<img src="images/mentee.jpg"  class="img-fluid rev-mentor-pic">
															</div>
															<div style="text-align:center">
										<a href="mentor-schedules.php" class="btn btn-alpha mr-lg-3 mr-2 cnect"> See Availability <i class="fa fa-angle-right ml-3"></i></a>
										</div>
													</div>
													<!---/col-->
													<div class="col-md-9 col-sm-12 mb-5 mb-md-0 order-first order-md-0 reviews-view">
														<div class="app-nfo app-info mb-4">
															
															<h5 class="">keny Mentee
															   <label class="pull-right hor-rate small-caption">
																   $12 / hr
																</label>
															</h5>
															<div class="row">
																<fieldset class="rating">
																	<input type="radio" id="star5" name="rating" value="5" />
																	<label class = "full" for="star5"></label>
																	<input type="radio" id="star4half" name="rating" value="4 and a half" />
																	<label class="half" for="star4half" ></label>
																	<input type="radio" id="star4" name="rating" value="4" />
																	<label class = "full" for="star4" ></label>
																	<input type="radio" id="star3half" name="rating" value="3 and a half" />
																	<label class="half" for="star3half" ></label>
																	<input type="radio" id="star3" name="rating" value="3" />
																	<label class = "full" for="star3" ></label>
																	<input type="radio" id="star2half" name="rating" value="2 and a half" />
																	<label class="half" for="star2half" ></label>
																	<input type="radio" id="star2" name="rating" value="2" />
																	<label class = "full" for="star2" ></label>
																	<input type="radio" id="star1half" name="rating" value="1 and a half" />
																	<label class="half" for="star1half" ></label>
																	<input type="radio" id="star1" name="rating" value="1" />
																	<label class = "full" for="star1" ></label>
																	<input type="radio" id="starhalf" name="rating" value="half" />
																	<label class="half" for="starhalf" ></label>
																</fieldset>
															</div>
															
															
																<p class="spcev">Mentorship is a relationship in which a more experienced or more knowledgeable person helps to guide a less experienced or less knowledgeable person.</p>
																<p class="spcev">Mentorship is a relationship in which a more experienced or more knowledgeable person helps to guide a less experienced or less knowledgeable person.</p>
															
														</div>
													   
													</div>
												</div>
												
												
												<div class="row review-box">
														<div class="col-md-3 col-sm-12">
															<div class="app-image">
																<img src="images/mentee2.jpg"  class="img-fluid rev-mentor-pic">
															</div>
															<div style="text-align:center">
																 <a href="mentor-schedules.php" class="btn btn-alpha mr-lg-3 mr-2 cnect"> See Availability <i class="fa fa-angle-right ml-3"></i></a>
															 </div>
														</div>
													<!---/col-->
													<div class="col-md-9 col-sm-12 mb-5 mb-md-0 order-first order-md-0 reviews-view">
														<div class="app-nfo app-info mb-4">
															
															<h5 class="">keny Mentee
															   <label class="pull-right hor-rate small-caption">
																   $12 / hr
																</label>
															</h5>
															<div class="row">
																<fieldset class="rating">
																	<input type="radio" id="star5" name="rating" value="5" />
																	<label class = "full" for="star5"></label>
																	<input type="radio" id="star4half" name="rating" value="4 and a half" />
																	<label class="half" for="star4half" ></label>
																	<input type="radio" id="star4" name="rating" value="4" />
																	<label class = "full" for="star4" ></label>
																	<input type="radio" id="star3half" name="rating" value="3 and a half" />
																	<label class="half" for="star3half" ></label>
																	<input type="radio" id="star3" name="rating" value="3" />
																	<label class = "full" for="star3" ></label>
																	<input type="radio" id="star2half" name="rating" value="2 and a half" />
																	<label class="half" for="star2half" ></label>
																	<input type="radio" id="star2" name="rating" value="2" />
																	<label class = "full" for="star2" ></label>
																	<input type="radio" id="star1half" name="rating" value="1 and a half" />
																	<label class="half" for="star1half" ></label>
																	<input type="radio" id="star1" name="rating" value="1" />
																	<label class = "full" for="star1" ></label>
																	<input type="radio" id="starhalf" name="rating" value="half" />
																	<label class="half" for="starhalf" ></label>
																</fieldset>
															</div>
															
															
																<p class="spcev">Mentorship is a relationship in which a more experienced or more knowledgeable person helps to guide a less experienced or less knowledgeable person.</p>
																<p class="spcev">Mentorship is a relationship in which a more experienced or more knowledgeable person helps to guide a less experienced or less knowledgeable person.</p>
															
														</div>
													   
													</div>
												</div>
												
											
										</div>
												
										
									</div>
									<!---/col-->
								   
								</div>
								<!-- /row  -->
								
								

								<!-- Choose a Mentor END -->
								
								
							  
						    </div>
						  
						  
						    <div class="tab-pane fade" id="pop3" role="tabpanel" aria-labelledby="pop3-tab">
							   <div class="pt-3"></div>
								<p>3. There's another way to do this for layouts that doesn't have to put the navbar inside the container, and which doesn't require any CSS or Bootstrap overrides.

								Simply place a div with the Bootstrap container class around the navbar. This will center the links inside the navbar:
							  
						    </div>
						  
						</div>
					
					</div>
				
				    </div>
				
				</div>
            </div>
           
          
			
			
        </div>
        <!-- container end -->
    </section>
	
    <footer class="pb-2 footer" id="footer">
        <div class="container">
            <!-- container-start -->
            <div class="row text-white text-center burger">
                <div class="col-lg-10 offset-lg-1">
                    <div class="newsletter mb-5">
                        <h3 class="mb-3">Don't miss out, Stay updated</h3>
                        <form class="form form-newsletter input-group w-50 mx-auto position-relative" method="post" action="">
                            <div id="mail-messages" class="notification subscribe"></div>
                            <input class="form-control mr-md-2 mr-1" placeholder="Enter@email.com" type="email" name="subscribe_email">
                            <button type="submit" class="input-group-append btn btn-alpha " name="button">Subscribe <i class="fa fa-angle-right ml-3"></i></button>
                        </form>
                    </div>
                    
                </div>
                <!---/column-->
            </div>
            <!---/row-->
			<div class="card-body footer-card-body">
                <div class="col-lg-12 col-12 col-sm-12 sm-offset-2">
						
				<div class="row">
					<div class="col-sm-6 col-lg-3 res-margin-xs">
						<h6 class="foot-head">About Purplelane</h6>
						<div class="spce"></div>
						<a href=""><p>Mentorship is a relationship in which a more experienced or <br/><br/>more knowledgeable person helps to guide a less experienced or less knowledgeable person.
							</p></a>
						<div class="spce"></div>
						
					</div>
					<div class="col-sm-6 col-lg-3 col-sm-offset-1 res-margin-xs">
						<h6 class="foot-head">Our Services</h6>
						<div class="spce"></div>
						<ul>
							<li class="ser-list"><a href="#">Mobile App Development</a></li>
							<li class="ser-list"><a href="#">Web App Development</a></li>
							<li class="ser-list"><a href="#">UI/UX Design</a></li>
							<li class="ser-list"><a href="#">Offshore Development</a></li>
							<li class="ser-list"><a href="#">Software Development</a></li>
						</ul>
					</div>
					<div class="col-sm-6 col-lg-3 col-sm-offset-1">
						<h6 class="foot-head">Main Menu</h6>
						<div class="spce"></div>
						<ul>
							<li class="ser-list"><a href="#">General Help</a></li>
							<li class="ser-list"><a href="#">Contact</a></li>
							<li class="ser-list"><a href="#">Portfolio</a></li>
							<li class="ser-list"><a href="#">Terms &amp; Conditions</a></li>
							<li class="ser-list"><a href="#">Privacy &amp; Policy</a></li>
						</ul>
					</div>
					<div class="col-sm-6 col-lg-3 res-margin-xs">
						<h6 class="foot-head">Contact Us</h6>
						<div class="spce"></div>
						<a href=""><p>102, Alekya bliss, Camelot layout,
							Botanical gardens, Kondapur,
							Hyderabad Telangana, India-500084.
							<br/><br/>
							<i class="fa fa-phone-square" aria-hidden="true"></i> +91 99999 99999.<br/>
							<i class="fa fa-envelope-o" aria-hidden="true"></i> Info@Purplelane.com
							</p></a>
						<div class="spce"></div>
						<div class="social-holder">
							<a href="#"><i class="fa fa-facebook social-icon"></i></a>
							<a href="#"><i class="fa fa-twitter social-icon"></i></a>
							<a href="#"><i class="fa fa-linkedin social-icon"></i></a>
							<a href="#"><i class="fa fa-google social-icon"></i></a>
						</div>
					</div>
				</div>
											
                </div>
                <!---/col-->
                                          
            </div>
			<div class="container">
				
		</div>
			
			
            <div class="row text-white text-center">
                <div class="copyright col-lg-12">
				
                    <p class="">  Copyright © 2018 <a href="" target="_blank" class="alt-color">Purplelane</a>. All rights reserved</p>
                </div>
            </div>
            <!---/row-->
        </div>
        <!---/container-->
    </footer>
  
  
  
  



    <script src="../js/jquery3.2.1.min.js"></script>
    <!-- JQUERY LIBRARY -->
    <script src="../js/particles.min.js"></script>
    <!-- Particles JS -->
    <script src="../js/bootstrap.min.js"></script>
    <!-- Bootstrap JS -->
    <script src="../js/main.js"></script>
    <!-- Main JS -->

<!-- foooter started -->
<?php
    include("../footer.php");
?>
<!-- foooter end -->