<?php
require_once "../includes/initialize.php";
require_once "logincheck.php";

$id = $_SESSION['id'];
$user = getMenteeBasicInfo($con, $id);

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?php echo SITE_NAME;?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <?php require_once '../head.php' ?>
    <style>
    .nav{
    
    display: block;
        flex-wrap: wrap;
        padding-left: 0;
        margin-bottom: 0;
        list-style: none;
    }
    </style>

  <link rel="stylesheet" type="text/css" href="../css/jquery.datetimepicker.css"/>
   
</head>
<body data-spy="scroll" data-target=".navbar" data-offset="82">
    <div class="site-loader">
        <!---======Preloader===========-->
        <div class="loader-dots">
            <div class="circle circle-1"></div>
            <div class="circle circle-2"></div>
        </div>
    </div>
    <div class="site__layer"></div>
    <?php require_once '../header.php' ?>
    <?php require_once 'navbar.php' ?>

	<section class="burger app" id="app">
	<div class="container">
		<!-- container-start -->
		<div class="container">
			<div class="row">
				<div class="col-md-12 text-center" style="margin-top:40px">
					<div class="tab-content" id="nav-tabContent">
						<div class="tab-pane fade show active" id="pop2" role="tabpanel" aria-labelledby="pop2-tab">
							<!-- Choose a mentee start -->
							<div class="row">
								<div class="col-md-12 col-sm-12  ">
									<div class="store-btns">
										<div class=" review-box">
											<div class="tabs">
												<div class="container">
													<div class="row">
														<div class="col-md-3">
															<nav class="nav-justified ">
																<div class="nav nav-tabs " id="nav-tab" role="tablist">
																	<a class="nav-item nav-link active" id="pop1-tab" data-toggle="tab" href="#pop1" role="tab" aria-controls="pop1" aria-selected="true"> <b>Basic Information</b>
																	</a>
																	<!-- <a class="nav-item nav-link" id="pop2a-tab" data-toggle="tab" href="#pop2a" role="tab" aria-controls="pop2a" aria-selected="false"> <b>Engagement Preferences</b>
																	</a>
																	<a class="nav-item nav-link" id="pop3-tab" data-toggle="tab" href="#pop3" role="tab" aria-controls="pop3" aria-selected="false"> <b>Bank Account Details</b>
																	</a> -->
																	<a class="nav-item nav-link" id="pop4-tab" data-toggle="tab" href="#pop4" role="tab" aria-controls="pop4" aria-selected="false"> <b>Change password</b>
																	</a>
																</div>
															</nav>
														</div>
														<div class="col-md-9">
															<div class="tab-content">
																<div class="tab-pane fade show active" id="pop1" role="tabpanel" aria-labelledby="pop1-tab">
																	<p>
																		<div class="card border-primary rounded-0">
																			<div class="card-header p-0">
																				<div class="bg-info text-white text-center py-2">
																					<h6> Basic Information 
                                                                                        <span class="edt"><i class="fa fa-pencil-square"></i></span>
                                                                                    </h6>
																				</div>
                                                                            </div>
                                                                            <form id="mentee_update_form"  method="POST" enctype="multipart/form-data">
																			<input type="hidden" name="mentee_id" value="<?php echo $user['id']?>">
																			<div class="card-body p-3">
																				<!--Body-->
																				<div class="form-group">
																					<div style="display:none;" id="alert_div_basic" class="alert alert-danger alert-dismissible fade show" role="alert">
																						<p>Danger</p>
																						<button style="margin-top: -30px;" type="button" class="close" data-dismiss="alert" aria-label="Close">
																							<span aria-hidden="true"style="font-size: 31px;line-height: 0;">&times;</span>
																						</button>
																					</div>
																				</div>
																				<div class="form-group">
																					<div class="input-group mb-2">
																						<div class="input-group-prepend">
																							<div class="input-group-text"> <i class="fa fa-user text-info"></i>
																							</div>
																						</div>
																						<input type="text" class="form-control" id="fname" name="fname" placeholder="First Name" value="<?php echo $user['fname']?>"  required>
																					</div>
                                                                                </div>
                                                                                <div class="form-group">
																					<div class="input-group mb-2">
																						<div class="input-group-prepend">
																							<div class="input-group-text"> <i class="fa fa-user text-info"></i>
																							</div>
																						</div>
																						<input type="text" class="form-control" id="lname" name="lname" placeholder="Last Name" value="<?php echo $user['lname']?>" required>
																					</div>
																				</div>
																				<div class="form-group">
																					<div class="input-group mb-2">
																						<div class="input-group-prepend">
																							<div class="input-group-text"> <i class="fa fa-envelope text-info"></i>
																							</div>
																						</div>
																						<input type="email" class="form-control" id="email" name="email" placeholder="Email" required value="<?php echo $user['email']?>" readonly>
																					</div>
																				</div>
																				
																				<div class="form-group">
																					<div class="input-group mb-2">
																						<div class="input-group-prepend">
																							<div class="input-group-text"><i class="fa fa-phone text-info"></i>
																							</div>
																						</div>
																						<input type="text" class="form-control" id="prsn_phone" name="mobile" placeholder="Mobile"  value="<?php echo $user['mobile']?>" required>
			
																					</div>
																					<small id="nombre" class="form-text text-muted" style="text-align: start;">Please enter <span style="color:#54136d !important;"> mobile</span> number.</small>
																				</div>
																				 <button type="submit" class="btn btn-alpha mr-lg-3 mr-2 cnect"> Save Changes <i class="fa fa-angle-right ml-3"></i></button>
																				</form>
																			</div>
																		</div>
																	</p>
																</div>
																<div class="tab-pane fade" id="pop2a" role="tabpanel" aria-labelledby="pop2a-tab">
																	<div class="row">
																		<div class="col-md-5">
																			<p>
																				<div class="card border-primary rounded-0">
																					<div class="card-header p-0">
																						<div class="bg-info text-white text-center py-2">
																							<h6> Availability
                                                                                            <span class="edt"><i class="fa fa-pencil-square"></i></span>
                                                                                            </h6>
																						</div>
																					</div>
																					<div class="card-body p-3">
																						<div id="demo2">

                                                                                        </div>
																					</div>
																				</div>
																			</p>
																		</div>
																		<div class="col-md-7">
																			<p>
																				<div class="card border-primary rounded-0">
																					<div class="card-header p-0">
																						<div class="bg-info text-white text-center py-2">
																							<h6> Price Range
                                                                     <span class="edt"><i class="fa fa-pencil-square"></i></span>
                                                                  </h6>
																						</div>
																					</div>
																					<div class="card-body p-3">
																						<!--Body-->
																						<div class="form-group">
																							<lable>Set call rate( Hourly)</lable>
																							<div class="input-group mb-2">
																								<div class="input-group-prepend">
																									<div class="input-group-text"><i class="fa fa-inr text-info"></i>
																									</div>
																								</div>
																								<input type="text" class="form-control" id="nombre" name="nombre" placeholder="Hourly" required>
																							</div>
																						</div> <span>Once the password is changed, you'll be redirected to the login page.</span>
																						<a href="#" class="btn btn-alpha mr-lg-3 mr-2 cnect"> Save Changes <i class="fa fa-angle-right ml-3"></i></a>
																					</div>
																				</div>
																			</p>
																		</div>
																	</div>
																</div>
																<div class="tab-pane fade" id="pop3" role="tabpanel" aria-labelledby="pop3-tab">
																	<p>
																		<div class="card border-primary rounded-0">
																			<div class="card-header p-0">
																				<div class="bg-info text-white text-center py-2">
																					<h6> Bank Account Details 
                                                               <span class="edt"><i class="fa fa-pencil-square"></i></span>
                                                            </h6>
																				</div>
																			</div>
																			<div class="card-body p-3">
																				<!--Body-->
																				<div class="form-group">
																					<div class="input-group mb-2">
																						<div class="input-group-prepend">
																							<div class="input-group-text"><i class="fa fa-user text-info"></i>
																							</div>
																						</div>
																						<input type="text" class="form-control" id="nombre" name="nombre" placeholder="Bank Name" required>
																					</div>
																				</div>
																				<div class="form-group">
																					<div class="input-group mb-2">
																						<div class="input-group-prepend">
																							<div class="input-group-text"><i class="fa fa-check-square text-info"></i>
																							</div>
																						</div>
																						<input type="text" class="form-control" id="nombre" name="email" placeholder="Account Number" required>
																					</div>
																				</div>
																				<div class="form-group">
																					<div class="input-group mb-2">
																						<div class="input-group-prepend">
																							<div class="input-group-text"><i class="fa fa-check-square text-info"></i>
																							</div>
																						</div>
																						<input type="text" class="form-control" id="nombre" name="text" placeholder="IFS Code" required>
																					</div>
																				</div> <a href="#" class="btn btn-alpha mr-lg-3 mr-2 cnect"> Save Changes <i class="fa fa-angle-right ml-3"></i></a>
																			</div>
																		</div>
																	</p>
																</div>
																<div class="tab-pane fade" id="pop4" role="tabpanel" aria-labelledby="pop4-tab">
																	<p>
																		<div class="card border-primary rounded-0">
																			<div class="card-header p-0">
																				<div class="bg-info text-white text-center py-2">
																					<h6> Change Password
                                                               <span class="edt"><i class="fa fa-pencil-square"></i></span>
                                                            </h6>
																				</div>
																			</div>
																			<form id="mentee_password_form"  method="POST" enctype="multipart/form-data">
																				<input type="hidden" name="mentee_id" value="<?php echo $user['id']?>">
																			<div class="card-body p-3">
																				<!--Body-->
																				<div class="form-group">
																					<div style="display:none;" id="alert_div_password" class="alert alert-danger alert-dismissible fade show" role="alert">
																						<p>Danger</p>
																						<button style="margin-top: -30px;" type="button" class="close" data-dismiss="alert" aria-label="Close">
																							<span aria-hidden="true"style="font-size: 31px;line-height: 0;">&times;</span>
																						</button>
																					</div>
																				</div>
																				<div class="form-group">
																					<div class="input-group mb-2">
																						<div class="input-group-prepend">
																							<div class="input-group-text"><i class="fa fa-lock text-info"></i>
																							</div>
																						</div>
																						<input type="password" class="form-control" id="current_password" name="current_password" placeholder="Current Password" required>
																						<div class="input-group-addon">
																						</div>
																					</div>
																				</div>
																				<div class="form-group">
																					<div class="input-group mb-2">
																						<div class="input-group-prepend">
																							<div class="input-group-text"><i class="fa fa-key text-info"></i>
																							</div>
																						</div>
																						<input type="password" class="form-control" id="new_password" name="new_password" placeholder="New Password" required>
																						<div class="input-group-addon">
																						</div>
																					</div>
																				</div>
																				<div class="form-group">
																					<div class="input-group mb-2">
																						<div class="input-group-prepend">
																							<div class="input-group-text"><i class="fa fa-key text-info"></i>
																							</div>
																						</div>
																						<input type="password" class="form-control" id="confirm_password" name="confirm_password" placeholder="Retype New Password" required>
																						<div class="input-group-addon">
																						</div>
																					</div>
																				</div> <button type="submit" class="btn btn-alpha mr-lg-3 mr-2 cnect"> Save Changes <i class="fa fa-angle-right ml-3"></i></button>
																				</form>
																			</div>
																		</div>
																	</p>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<!---/col-->
								</div>
								<!-- /row  -->
							</div>
							<div class="tab-pane fade" id="pop3" role="tabpanel" aria-labelledby="pop3-tab">
								<div class="pt-3"></div>
								<p>3. There's another way to do this for layouts that doesn't have to put the navbar inside the container, and which doesn't require any CSS or Bootstrap overrides. Simply place a div with the Bootstrap container class around the navbar. This will center the links inside the navbar:</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- container end -->
</section>
<?php 
require_once('../footer.php');
require_once('../footer_tags.php');
?>
	