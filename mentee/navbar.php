<nav class="nav-fixed navbar navbar-expand-lg navbar-togglable navbar-dark" style="">
    <div class="container">
        <!-- Brand/ logo -->
        <?php if($_SESSION['type'] == 'mentee' ) { 
                $link = "/mentee/my_appointments.php";
            }
            elseif($_SESSION['type'] == 'mentor' ) {
                $link = "/mentor/my_appointment.php";
            }
        ?>
        <a class="navbar-brand" style="max-width:20%" href="<?php echo $link ?>"><img src="/images/logo.png" alt="logo image" class="img-fluid-logo"></a>
        <!-- Toggler -->
        <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span></span><span></span><span></span>
        </button>
        <!-- Collapse -->
        <div class="collapse navbar-collapse" id="navbarCollapse">
            <!-- Links -->
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="browse_mentor.php">Browse Mentor
                </a>
                </li>
               
            </ul>
                <div class="">
                    <div class="card-header msg_head">
                        <div class="d-flex bd-highlight">
                            <div class="img_cont">
                                <?php
                                $profile_pic_path="/images/mentee.jpg";
                                if($mentee_info['profile_pic']!='')
                                {
                                    $profile_pic_path="/images/".MENTEE_IMAGE_UPLOAD_FOLDER_LINK."/".$mentor_info['profile_pic'];
                                }
                                ?>
                                <img src="<?php echo $profile_pic_path;?>" class="rounded-circle user_img">
                            </div>
                            <div class="user_info">
                                <span><?php echo $user['fname']." ".$user['lname']?></span>
                                
                            </div>
                        
                        </div>
                        <div class="d-flex bd-highlight">
                            <i class="fa fa-ellipsis-v dropbtn" id="action_menu_btn" onclick="myFunction()"></i>

                            <div id="myDropdown" class="dropdown-content" style="left:25%;">
                                <a href="edit_profile.php"><i class="fa fa-user-o"></i> Edit profile</a>
                            
                                <a href="my_appointments.php"><i class="fa fa-file-text"></i> My Appointments</a>
                                <a href="my_transactions.php"><i class="fa fa-money"></i> My Transactions</a>
                                <a href="../contact_us.php"><i class="fa fa-info-circle"></i> Help Center</a>
                                <a href="../logout.php"><i class="fa fa-sign-out"></i> Logout</a>
                            </div>
                        </div>
                    </div>
                
                </div>
        </div>
        <!-- / .navbar-collapse -->
    </div>
    <!-- / .container -->
</nav>