<!DOCTYPE html>
<html lang="en">
<head>
	<?php 
$user = getMenteeBasicInfo($con, $id);
?>
    <title><?php echo SITE_NAME;?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <?php require_once '../head.php' ?>
    <style>
    .nav{
    
    display: block;
        flex-wrap: wrap;
        padding-left: 0;
        margin-bottom: 0;
        list-style: none;
    }
    </style>

  <link rel="stylesheet" type="text/css" href="/css/jquery.datetimepicker.css"/>

 
 <link href="/css/datetimepicker.css" rel="stylesheet" type="text/css"/>


 
</head>
<body data-spy="scroll" data-target=".navbar" data-offset="82">
<div class="site-loader">
	<!---======Preloader===========-->
	<div class="loader-dots">
		<div class="circle circle-1"></div>
		<div class="circle circle-2"></div>
	</div>
</div>
<div class="site__layer"></div>