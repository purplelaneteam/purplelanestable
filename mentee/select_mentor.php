<?php
require_once "../includes/initialize.php";
require_once "logincheck.php";
error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE);
$id = $_SESSION['id'];
$mentors = getMentorByTopics($con,$_GET['subject']);
// dd($mentors);exit;
$categories = getMentorCategory($con);
$subcategory = getCategoryDetailById($con,$_GET['subcat']);
$topics = getTopicsBySubCategory($con,$_GET['cat'],$_GET['subcat']);

$starting_point = $_GET['starting_point'];
$goal = $_GET['goal'];
$_SESSION['starting_point'] = mysqli_real_escape_string($con, $starting_point);
$_SESSION['goal'] = mysqli_real_escape_string($con, $goal);

$companies = getAllCompany($con,$mentors);
$skills = getAllSkill($con,$mentors);
require_once "header.php";
?>
<link rel="stylesheet" href="<?php echo SITE_URL;?>/css/mm.css" rel="stylesheet">
<style>
    .select2-container .select2-selection--single {
        box-sizing: border-box;
        cursor: pointer;
        display: block;
        height: 42px;
        user-select: none;
        -webkit-user-select: none;
    }

    .select2-container--default .select2-selection--single .select2-selection__rendered {
        color: #444;
        line-height: 41px;
    }

    .select2-container--default .select2-selection--single .select2-selection__arrow b {
        border-color: #888 transparent transparent transparent;
        border-style: solid;
        border-width: 5px 4px 0 4px;
        height: 0;
        left: 50%;
        margin-left: -4px;
        margin-top: 4px;
        position: absolute;
        top: 50%;
        width: 0;
    }
</style>
<?php require_once '../header.php' ?>
<?php require_once 'navbar.php' ?>
<section class="burger app" id="app">
    <div class="container">
        <!-- container-start -->
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="tab-content  text-center" id="nav-tabContent">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12 text-center" style="margin-top:40px">
                                    <div class="tab-content" id="nav-tabContent">
                                        <div class="tab-pane fade show active" id="pop2" role="tabpanel"
                                            aria-labelledby="pop2-tab">
                                            <div class="pt-3"></div>
                                            <div class="row">
                                                <div class="col-lg-12 col-sm-12 text-center">
                                                    <h4 class="mb-0 mb-lg-3 lh-1">Choose a Mentor</h4>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-3 col-sm-12 order-first mb-0 mb-md-0 order-md-0">
                                                    <!-- <div class="side-box">
                                                                <h6 class="side-heading">Topics</h6>
                                                                
                                                            </div> -->
                                                    <input class="topics" id="topics" type="checkbox" value="<?php echo $_GET['subject'] ?>" hidden>
                                                    <input class="category" id="category" type="checkbox" value="<?php echo $_GET['cat'] ?>" hidden>
                                                    <input class="subcategory" id="subcategory" type="checkbox" value="<?php echo $_GET['subcat'] ?>" hidden>
                                                    <div class="side-box">
                                                        <h6 class="side-heading">Company</h6>
                                                        <?php
                                                                foreach($companies as $company)
                                                                {
                                                                ?>
                                                        <div class="chiller_cb">
                                                            <input class="check"
                                                                id="companies_<?php echo $company['id'];?>"
                                                                type="checkbox">
                                                            <label
                                                                for="companies_<?php echo $company['id'];?>"><?php echo  $company['name'];?></label>
                                                            <span></span>
                                                        </div>
                                                        <?php
                                                                }
                                                                ?>

                                                    </div>
                                                    <div class="side-box">
                                                        <h6 class="side-heading">Skills</h6>

                                                        <?php
                                                                foreach($skills as $skill)
                                                                {
                                                                ?>
                                                        <div class="chiller_cb">
                                                            <input class="check" id="skills_<?php echo $skill['id'];?>"
                                                                type="checkbox">
                                                            <label
                                                                for="skills_<?php echo $skill['id'];?>"><?php echo  $skill['name'];?></label>
                                                            <span></span>
                                                        </div>
                                                        <?php
                                                                }
                                                                ?>

                                                    </div>
                                                </div>
                                                <div class="col-md-9 col-sm-12  ">
                                                    <div class="store-btns" id='mentor_info'><br><br>
                                                        <?php
                                                                if(isset($mentors))
                                                                {
                                                                    foreach($mentors as $mentor)
                                                                    {
                                                                        $mentor = selectedMentorInfoArray($con,$mentor, $_GET['subject']);
                                                                        include './include/mentor_info.php';
                                                                    }
                                                                }
                                                                else
                                                                {

                                                                }
                                                                ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    </div>
    <!-- container end -->
</section>
<?php 
require_once('../footer.php');
require_once('../footer_tags.php');
?>