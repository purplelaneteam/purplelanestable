<?php
   require_once("../../includes/initialize.php");
   require("../logincheck.php"); 
                  
   $cat_id = mysqli_real_escape_string($con, $_GET["id"]);
   $topics = getTopicsBySubCategory($con, '', $cat_id);
    $data = [];
    while ($row = mysqli_fetch_assoc($topics))
    {
        $data[] = $row;
    }
   echo json_encode([
        'topics' => $data,
        'msg' => 'Topics for sub category',
    ]);