<?php
   require_once("../../includes/initialize.php");

    $comment = sanitize_input($_POST['comment']);
    $meeting_id = sanitize_input($_POST['meeting_id']);
    $rating = isset($_POST['rating'])? sanitize_input($_POST['rating']): 0;

    
    $sql = "START TRANSACTION";
    $result = mysqli_query($con, $sql);
    if(!$result)
    {
        $error = "Something went wrong. Please try again later.";
        ajax_error($error);
    }
    

        if(!$comment || !$meeting_id || !$rating)
        {
            $jsonarray['code']=1;
            $jsonarray['msg'] = "All field is mandatory";
            echo json_encode($jsonarray);exit;
        }

        

    $sql_insert = "INSERT INTO mentor_ratings SET meetings_id = $meeting_id, ratings = $rating, comment = '$comment', addedon = NOW()";
    // echo $sql_insert;exit;
    $result_insert = mysqli_query($con, $sql_insert);
    if(!$result_insert) 
    {   
        $error = "Something went wrong. Please try again later.";
        ajax_error($error);
    }

    $sql = "COMMIT";
    $result = mysqli_query($con, $sql);
    if(!$result)
    {
        $error = "commit: Something went wrong. Please try again later.";
        ajax_error($error);
    }
    else
    {
        $jsonarray["msg"] = "Feedback sent";
        echo json_encode($jsonarray);
    }
?>