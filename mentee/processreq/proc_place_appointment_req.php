<?php
    require_once("../../includes/initialize.php");
    require("../logincheck.php");
    if (!isset($_POST['ORDERID']) || empty($_POST['ORDERID']))
    {
        $cost_card_price = getMentorRate($con, $_POST['mentor_id']);

        $service_tax_price = mysqli_fetch_assoc(getSettingsPrice($con, 'service_tax'));
        $platform_charges = mysqli_fetch_assoc(getSettingsPrice($con, 'platform_charges'));
        // dd($platform_charges['price']);

        $service_tax_price = number_format(calculateRate($service_tax_price['price'], $cost_card_price), 2, '.', '');
        $platform_charges = number_format(calculateRate($platform_charges['price'], $cost_card_price), 2, '.', '');

        $data = $_POST;
        $data['cost_card_price'] = $cost_card_price;
        
        $data['service_tax_price'] = $service_tax_price;
        $data['platform_charges'] = $platform_charges;
        
        $data = serialize($data);
        /* Check input before order */
        $slot1 = $_POST["slot1"].':00';
        $slot2 = $_POST["slot2"].':00';

        $is_date_valid = check_date_time($slot1,$slot2);

        $selected_date1 = strtotime("$slot1");
        $selected_date2 = strtotime("$slot2");
        
        $meeting_date1 = date("Y-m-d", $selected_date1);
        $start_time1 = date("H:i:s", $selected_date1);
        $end_time1 = date("H:i:s", strtotime("$slot1 + 30min"));

        $meeting_date2 = date("Y-m-d", $selected_date2);
        $start_time2 = date("H:i:s", $selected_date2);
        $end_time2 = date("H:i:s", strtotime("$slot2 + 30min"));

        $comment = sanitize_input($_POST["comment"]);
        $title = sanitize_input($_POST["title"]);
        $mentor_id = sanitize_input($_POST["mentor_id"]);
        $topic_id = sanitize_input($_POST["topic_id"]);
        
        $date = date('Y-m-d H:i:s');


        $starting_point = $_SESSION['starting_point'];
        $goal = $_SESSION['goal'];

        $mentee_id = sanitize_input($_POST["mentee_id"]);

        if(!($slot1 || $slot2 ) || !$comment || !$mentor_id || !$topic_id || !$mentee_id)
        {
            $jsonarray['code']="0";
            $jsonarray['msg'] = "All field is mandatory";
            ajax_error($error);
        }
        /* Generate order when inputs are present */
        
        do {
            $order_id = "ORDS" . rand(10000,99999999999);
            $oid="SELECT order_id From orders where order_id='$order_id' ";
            $oid_result=mysqli_query($con,$oid);
            $oid_row=mysqli_num_rows($oid_result);
        } while ($oid_row >= 1);

        $customer_id = $_SESSION['id'];
        $sql_insert = "INSERT INTO orders SET order_id = '$order_id', customer_id = '$customer_id', data = '$data', total = '$cost_card_price'";

        $result = mysqli_query($con, $sql_insert);
        if(!$result) 
        {
            $error = "Something went wrong while adding meeting order. Please try again later.";
            ajax_error($error);
        }
        else
        {
            $_SESSION['order_id'] = $order_id;
            $jsonarray["code"] = 0;
            $jsonarray["msg"] = "Procced to Payment.";
            echo json_encode($jsonarray);
        }
    }
    else
    {
        $date = date('Y-m-d H:i:s');

        $starting_point = $_SESSION['starting_point'];
        $goal = $_SESSION['goal'];
		
        $order_id = $_POST['ORDERID'];
        $sql = "SELECT data, coupon, total FROM orders WHERE order_id = '$order_id'";
        $result = mysqli_query($con, $sql);

        $discount = 0;
        
        if($result)
        {
            $row = mysqli_fetch_assoc($result);
			$schedule_form_data = unserialize($row['data']);
            $coupon = sanitize_input($row["coupon"]);
            $total = sanitize_input($row["total"]);
            $discount = getDiscount($coupon, $total);
        }
        else
        {
            $error = "Something went wrong order error. Please try again later.";
        }

        $mid = $_POST['MID'];
        $txnid = $_POST['TXNID'];
        $txnamount = $_POST['TXNAMOUNT'];
        $paymentmode = $_POST['PAYMENTMODE'];
        $currency = $_POST['CURRENCY'];
        $txndate = $_POST['TXNDATE'];
        $status = $_POST['STATUS'];
        $respcode = $_POST['RESPCODE'];
        $respmsg = $_POST['RESPMSG'];
        $gatewayname = $_POST['GATEWAYNAME'];
        $banktxnid = $_POST['BANKTXNID'];
        $bankname = $_POST['BANKNAME'];
        $checksumhash = $_POST['CHECKSUMHASH'];

        $sql = "UPDATE orders SET
            `mid` = '$mid',
            transaction_id = '$txnid',
            amount = '$txnamount',
            payment_mode = '$paymentmode',
            currency = '$currency',
            transaction_date = '$txndate',
            status = '$status',
            response_code = '$respcode',
            response_msg = '$respmsg',
            gate_way_name = '$gatewayname',
            bank_txn_id = '$banktxnid',
            bank_name = '$bankname',
            check_sum_hash = '$checksumhas',
            discount = '$discount'

            WHERE order_id =  '$order_id';
        ";
        $result = mysqli_query($con, $sql);


        if(!$result)
        {
            $error = "Something went wrong orders. Please try again later.";
        }
        if($status === 'TXN_FAILURE')
        {
            $error = "Something went wrong. Please try again later.";
        }

        $jsonarray = array();
        
        $sql = "INSERT INTO `mentee_transactions`(`meetings_id`, `amount`, `type`, `addedon`) VALUES (NULL, $txnamount,'debit','$date')";
            $result_insert = mysqli_query($con, $sql);
            $mentee_last_id=mysqli_insert_id($con);
            // dd($sql);
            if(!$result_insert) 
            {
                $error = "Something went wrong while adding Meeting Transaction. Please try again later.";
            }
            
        $sql = "START TRANSACTION";
        $result = mysqli_query($con, $sql);
        if(!$result)
        {
            $error = "Something went wrong. Please try again later.";
		}
		
		$mentee_id = sanitize_input($schedule_form_data["mentee_id"]);
		
		if($status === 'TXN_SUCCESS') {
			$sql = "SELECT id, type, value, title from coupons WHERE title = '$coupon'";
			$coupon_valid = mysqli_query($con, $sql);
			if (mysqli_num_rows($coupon_valid))
			{
				$coupon_valid = mysqli_fetch_assoc($coupon_valid);
				$id = $coupon_valid['id'];
				$sql = "SELECT id FROM coupon_used_by_mentee WHERE coupon_id = $id AND mentee_id = $mentee_id ORDER BY addedon DESC LIMIT 1";
				$id = mysqli_query($con, $sql);
				if ($id = mysqli_fetch_assoc($id)) {
					$id = $id['id'];
					$sql = "UPDATE coupon_used_by_mentee SET used = 1 WHERE id = $id";
					mysqli_query($con, $sql);
				}
			}
		}
        $slot1 = $schedule_form_data["slot1"].':00';
        $slot2 = $schedule_form_data["slot2"].':00';

        $is_date_valid = check_date_time($slot1,$slot2);

        $selected_date1 = strtotime("$slot1");
        $selected_date2 = strtotime("$slot2");
        
        $meeting_date1 = date("Y-m-d", $selected_date1);
        $start_time1 = date("H:i:s", $selected_date1);
        $end_time1 = date("H:i:s", strtotime("$slot1 + 30min"));

        $meeting_date2 = date("Y-m-d", $selected_date2);
        $start_time2 = date("H:i:s", $selected_date2);
        $end_time2 = date("H:i:s", strtotime("$slot2 + 30min"));

        $comment = sanitize_input($schedule_form_data["comment"]);
        $title = sanitize_input($schedule_form_data["title"]);
        $mentor_id = sanitize_input($schedule_form_data["mentor_id"]);
        $topic_id = sanitize_input($schedule_form_data["topic_id"]);

        $cost_card_price = sanitize_input($schedule_form_data["cost_card_price"]);
        $service_tax_price = sanitize_input($schedule_form_data["service_tax_price"]);


        $rates = getSettingsPrice($con, 'service_tax');
        if($rates) {
            $service_tax_price = calculateSettingsPrice($rates, $total - $discount);
        }
        $platform_charges = sanitize_input($schedule_form_data["platform_charges"]);
        $date = date('Y-m-d H:i:s');

        
        if(!($slot1 || $slot2 ) || !$comment || !$mentor_id || !$topic_id || !$mentee_id)
        {
            $jsonarray['code']="0";
            $jsonarray['msg'] = "All field is mandatory";
        }
        if(!$is_date_valid){
            $jsonarray['code']="0";
            $jsonarray['msg']="Please select valid time.";
        }
 
        do {
            if($status === 'TXN_SUCCESS')
            {
            
                $sql_insert_meetings = "INSERT INTO meetings(mentor_id, mentee_id, topic_id, meeting_date, cost_card_price, service_tax_price, platform_charges, meeting_status_id, order_id, title, comments, addedon, starting_point, goal) values('$mentor_id','$mentee_id','$topic_id','$meeting_date','$cost_card_price','$service_tax_price','$platform_charges',(SELECT id FROM meeting_status WHERE codename = 'pending'), '$order_id', '$title', '$comment', '$date', '$starting_point', '$goal')";
                // echo $sql_insert_meetings;exit;
                $result_insert_meetings = mysqli_query($con, $sql_insert_meetings);
                if(!$result_insert_meetings) 
                {
                    $error = "Something went wrong while adding meeting. Please try again later.";
                    break;
                }
                $meetings_id = mysqli_insert_id($con);

                

                $sql = "INSERT INTO meetings_status_history(meetings_id, meeting_date, cost_card_price, service_tax_price, platform_charges, meetings_status_id, comments, addedon) values('$meetings_id', '$meeting_date','$cost_card_price','$service_tax_price','$platform_charges',(SELECT id FROM meeting_status WHERE codename = 'pending'), '$comment','$date')";
                // echo $sql;exit;
                $result_insert = mysqli_query($con, $sql);
                if(!$result_insert) 
                {
                    $error = "Something went wrong while adding meeting status. Please try again later.";
                    break;
                }

                $sql_insert_meetings_slot = "INSERT INTO  meeting_selected_slots(meetings_id, meeting_date, start_time, end_time,  addedon) values('$meetings_id', '$meeting_date1', '$start_time1','$end_time1','$date')";
                $result_insert = mysqli_query($con, $sql_insert_meetings_slot);
                if(!$result_insert) 
                {
                    $error = "Something went wrong while adding slot1. Please try again later.";
                    break;
                }

                $sql_insert_meetings_slot = "INSERT INTO  meeting_selected_slots(meetings_id, meeting_date, start_time, end_time,  addedon) values('$meetings_id', '$meeting_date2', '$start_time2','$end_time2','$date')";
                $result_insert = mysqli_query($con, $sql_insert_meetings_slot);
                if(!$result_insert) 
                {
                    $error = "Something went wrong while adding slot2. Please try again later.";
                    break;
                }

                $sql_update = "UPDATE mentee_transactions SET meetings_id=$meetings_id, amount='$txnamount', type='debit' WHERE id=$mentee_last_id"; 
                $result_update = mysqli_query($con, $sql_update);
                //  dd($sql_update);
                if(mysqli_affected_rows($con)<=0)  
                {
                    $error = "Something went wrong while updating Meeting Transaction. Please try again later.";
                    break;
                }
                
                //send email to Mentor 
        
                $sql_get_mentor_email="SELECT email,fname, lname  FROM mentor WHERE id=$mentor_id";
                $result_get_mentor_email=mysqli_query($con,$sql_get_mentor_email);
                if($row=mysqli_fetch_assoc($result_get_mentor_email))
                {
                    $email=$row['email'];
                    $fname = $row['fname'];
                }
                $email_subject = "PurpleLane.in: Meeting Request Created - $meetings_id";
                $mailbody = "Hi $fname,<br/><br/>
                            Meeting request has been created and waiting for your confirmation. Please visit https://purplelane.in/ and confirm your available slot.<br>
                            <br/>
                            Thanks & Regards<br/>
                            PurpleLane Team
                            ";
        
                $mail_file = "../../../../includes/class.phpmailer.php";
                $send_mail = send_mail($email_subject, $mailbody, $email, $mail_file);
        
                //send email to Mentee 
        
                $sql_get_mentor_email="SELECT email,fname, lname  FROM mentee WHERE id=$mentee_id";
                $result_get_mentor_email=mysqli_query($con,$sql_get_mentor_email);
                if($row=mysqli_fetch_assoc($result_get_mentor_email))
                {
                    $email=$row['email'];
                    $fname = $row['fname'];
                }
                $email_subject = "PurpleLane.in: Your meeting has been successfully placed - $meetings_id";
                $mailbody = "Hi $fname,<br/><br/>
                            Your meeting request has been successfully placed.<br>
                            Meeting ID : ".$meetings_id."<br>
                            We have also notified the mentor and waiting for confirmation.
                            <br/>
                            Thanks & Regards<br/>
                            PurpleLane Team
                            ";
        // $mail_file = "../../../../includes/class.phpmailer.php";
                $send_mail = send_mail($email_subject, $mailbody, $email, $mail_file);
            }
        } while (false);
        
        $sql = "COMMIT";
        
        
        
        $result = mysqli_query($con, $sql);
        if(!$result)
        {
            $error = "commit: Something went wrong. Please try again later.";
        }
        if(isset($error) && !empty($error)) {
            $_SESSION['error'] = "Your Payment Could Not Be Processed. Please try again.";
            header('Location: /mentee/my_appointments.php');
            echo "<h1>There has been an error</h1>
            <h2>$error</h2>
            <a href=\"/\">Please go back to site</a>";exit();
        }

        header("Location: /mentee/my_appointments.php");
    }
    ?>
