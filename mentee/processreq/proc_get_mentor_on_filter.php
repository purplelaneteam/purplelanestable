<?php
require_once "../../includes/initialize.php";
require_once "../logincheck.php";
$jsonarray = array();
$id = $_SESSION['id'];
// dd($_POST['topics']);
$mentors = getMentorByCategoryAndFilter($con,$_POST['topics'],$_POST['companies'],$_POST['skills']);
//print_r($mentors);
$mentors = array_unique($mentors);
//print_r($mentors);

$mentor_list='';
if(isset($mentors))
{
    foreach($mentors as $mentor)
    {
        $mentor_list.=selectedMentorInfo($con,$mentor,$_POST['topics']);
    }
}

if($mentor_list=='')
{
    $mentor_list="<br><br>Mentor not found for search criteria";
}
else
{
    $mentor_list="<br><br>".$mentor_list;
}

$jsonarray["code"] = 0;
$jsonarray["mentor_list"] = $mentor_list;
echo json_encode($jsonarray);
