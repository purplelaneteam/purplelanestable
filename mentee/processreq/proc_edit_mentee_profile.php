<?php
    require_once("../../includes/initialize.php");
    require("../logincheck.php");
    $jsonarray = array();

    $sql = "START TRANSACTION";
    $result = mysqli_query($con, $sql);
    if(!$result)
    {
        $error = "Something went wrong. Please try again later.";
        ajax_error($error);
    }
    $fname = sanitize_input($_POST["fname"]);
    $lname = sanitize_input($_POST["lname"]);
    $email = sanitize_input($_POST["email"]);
    $mobile = sanitize_input($_POST["mobile"]);
  
    $date=date('Y-m-d H:i:s');

    $mentee_id = sanitize_input($_POST["mentee_id"]);
        if(($fname =="" || $lname == ""|| $email=="" || $mobile=="")
        )
        {
            $jsonarray['code']="0";
            $jsonarray['msg'] = "All field is mandatory";
            echo json_encode($jsonarray);exit;
        }
       
        $check_email = "SELECT email
                        FROM mentee
                        WHERE email='".$email."' AND id!= $mentee_id";

        $result_check = mysqli_query($con, $check_email);
        $row_cnt = mysqli_num_rows($result_check);
        if($row_cnt > 0)
        {
            $error = "Email already exists.";
            ajax_error($error);
        }

        $sql_mentor_detail = "SELECT profile_pic
                        FROM mentee
                        WHERE id = $mentee_id";

        $result_mentor_detail = mysqli_query($con, $sql_mentor_detail);
        $cnt_mentor_detail = mysqli_num_rows($result_mentor_detail);
        if($cnt_mentor_detail > 0)
        {
            if($myrow_mentor_details =mysqli_fetch_array($result_mentor_detail))
            {
                $profile_pic = $myrow_mentor_details['profile_pic'];
            }
        }

        if(count($_FILES) > 0)
        {
            if($_FILES["profile_pic"]["name"] != "")
            {
                $temp = explode(".", $_FILES["profile_pic"]["name"]);
                $type = ($_FILES["profile_pic"]["type"]);
                $extension = end($temp);
                if (in_array($type, $mentor_profile_pic_allowed_type)
                && ($_FILES["profile_pic"]["size"] < MENTOR_PROFILE_IMAGE_SIZE * 1000)
                && in_array($extension, $mentor_profile_pic_allowed_ext))
                {   
                    if ($_FILES["profile_pic"]["error"] > 0)
                    {
                        $error = "Error occured";
                        ajax_error($error);
                    }
                
                    else
                    {
                        $target_dir = MENTOR_IMAGE_UPLOAD_FOLDER;

                        $profile_pic = uniqid().".".$extension;

                        $file_path = $target_dir."/".$profile_pic;
                        $attachment_file_path = $_SERVER["DOCUMENT_ROOT"]."/".$file_path;
                        //move file to temp image folder
                        move_uploaded_file($_FILES["profile_pic"]["tmp_name"], $attachment_file_path);
                        

                    }
                }
                else
                {
                    $error = "Please check file dimension and size of icon";
                    ajax_error($error);
                }
            }
            else
            {
                $profile_pic = $profile_pic;
            }
        }
        else 
        {
            $profile_pic = $profile_pic;
        }
        $sql_update = "UPDATE mentee SET fname = '".$fname."', lname = '".$lname."', email = '".$email."' , mobile = '".$mobile."', profile_pic = '".$profile_pic."' WHERE id = '$mentee_id'";
        $result_update = mysqli_query($con, $sql_update);
        if(!$result_update) 
        {
            $error = "Something went wrong while updating mentor. Please try again later.";
            ajax_error($error);
        }

    $sql = "COMMIT";
    $result = mysqli_query($con, $sql);
    if(!$result)
    {
        $error = "commit: Something went wrong. Please try again later.";
        ajax_error($error);
    }
    else
    {
        $jsonarray["code"] = 0;
        $jsonarray["msg"] = "Mentee updated succesfully.";
        echo json_encode($jsonarray);
    }