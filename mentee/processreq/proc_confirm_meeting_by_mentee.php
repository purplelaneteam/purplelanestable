<?php
    require_once("../../includes/initialize.php");
    require("../logincheck.php");
    $jsonarray = array();

    $sql = "START TRANSACTION";
    $result = mysqli_query($con, $sql);
    if(!$result)
    {
        $error = "Something went wrong. Please try again later.";
        $_SESSION['error'] = $error;
    }
    $meeting_id = sanitize_input($_POST["meeting_id"]);

    $sql = "SELECT id FROM meeting_status WHERE codename = 'scheduled'";

    $result = mysqli_query($con, $sql);
    $status_id = mysqli_fetch_assoc($result)['id'];
    if (!$status_id)
    {
        $error = "Something went wrong while adding meeting. Please try again later.";
        $_SESSION['error'] = $error;
    }
    // $sql = "SELECT id FROM meeting_selected_slots WHERE meetings_id = '$meeting_id' ORDER BY addedon DESC LIMIT 1";
    
    $sql = "SELECT meeting_selected_slots_id FROM meetings WHERE id = '$meeting_id' ORDER BY addedon DESC LIMIT 1";
    
    
    $result = mysqli_query($con, $sql);
    $meeting_selected_slots_id = mysqli_fetch_assoc($result)['meeting_selected_slots_id'];
    if (!$meeting_selected_slots_id)
    {
        $error = "Something went wrong while adding meeting. Please try again later.";
        $_SESSION['error'] = $error;
    }

    $sql_update = "update meetings set meeting_status_id=$status_id, meeting_selected_slots_id = '$meeting_selected_slots_id' WHERE id=$meeting_id";

    $result_update = mysqli_query($con, $sql_update);
    if(!$result_update) 
    {
        $error = "Something went wrong while cancelling meeting. Please try again later.";
        $_SESSION['error'] = $error;
    }

    $sql = "SELECT * FROM  meetings WHERE id = $meeting_id";
    $result = mysqli_query($con, $sql);
    $meeting = mysqli_fetch_assoc($result);

    if (SEND_EMAIL_SCHEDULE_MENTOR)
    {
        $sql = "SELECT email, fname, lname FROM mentor WHERE id = ".$meeting['mentor_id'];
        $result = mysqli_query($con, $sql);
        $mentor = mysqli_fetch_assoc($result);
        if (!$mentor)
        {
            $error = "Unable To send email";
            $_SESSION['error'] = $error;
        }
        else
        {
            $email_subject = "PurpleLane.in: Your Meeting Scheduled - $meeting_id";
            $mailbody = "Hi ".ucwords($mentor['fname']. " " . $mentor['lname']).",<br/>
                        <p>Your meeting has been scheduled successfully.</p>
                        <a href='".SITE_URL."/mentor/my_appointments.php'>See your scheduled meeting.</a><br/><br/>
                        Thanks & 
                        Regards<br/>
                        PurpleLane Team";

            $send_mail = send_mail($email_subject, $mailbody, $mentor['email']);
        }
    }

    if (SEND_EMAIL_SCHEDULE_MENTEE)
    {
        $sql = "SELECT email, fname, lname FROM mentee WHERE id = ".$meeting['mentee_id'];
        $result = mysqli_query($con, $sql);
        $mentee = mysqli_fetch_assoc($result);
        if (!$mentee)
        {
            $error = "Unable To send email";
            $_SESSION['error'] = $error;
        }
        else
        {
            $email_subject = "PurpleLane.in: Your Meeting Scheduled - $meeting_id";
            $mailbody = "Hi ".ucwords($mentee['fname']. " " . $mentee['lname']).",<br/> 
                        <p>Your meeting has been scheduled successfully.</p>
                        <a href='".SITE_URL."/mentee/my_appointments.php'>See your Scheduled meeting.</a><br/><br/>
                        Thanks & 
                        Regards<br/>
                        PurpleLane Team";

            $send_mail = send_mail($email_subject, $mailbody, $mentee['email']);
        }
    }
    
    $sql = "COMMIT";
    $result = mysqli_query($con, $sql);
    if(!$result)
    {
        $error = "commit: Something went wrong. Please try again later.";
        $_SESSION['error'] = $error;
    }
    if(!$error) $_SESSION['success'] = "Meeting confirmed successfully";
    header("Location: {$_SERVER['HTTP_REFERER']}");