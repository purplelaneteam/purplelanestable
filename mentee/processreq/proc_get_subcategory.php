<?php
   require_once("../../includes/initialize.php");
   require("../logincheck.php"); 
                  
   $cat_id = mysqli_real_escape_string($con, $_GET["id"]);
   $all_sub_cat = "<option value=''>Select Subcategory</option>";
   $sql_sub_cat = "SELECT id, name FROM category WHERE parent_id = '".$cat_id."' ORDER BY name ASC";
   $result_sub_cat = mysqli_query($con, $sql_sub_cat);
   if($result_sub_cat && $myrow_sub_cat = mysqli_fetch_array($result_sub_cat))
   {
       do
       {
           $all_sub_cat .= '<option value="'.$myrow_sub_cat['id'].'">'.$myrow_sub_cat['name'].'</option>';
       }
       while($myrow_sub_cat = mysqli_fetch_array($result_sub_cat));
   }
   else
   {
    $all_sub_cat = '<option value="">No Subcategory Found</option>';
   }
   $all_topics='';
   $jsonarray["all_sub_cat"] = $all_sub_cat;
   $jsonarray["all_topics"] = $all_topics;
   echo json_encode($jsonarray);
?>