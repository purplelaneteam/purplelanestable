<?php
   require_once("../../includes/initialize.php");
   require("../logincheck.php"); 
                  
   $cat_id = mysqli_real_escape_string($con, $_GET["id"]);
   $all_topics = "<option value=''>Select Subject</option>";
  
   $sql_topics = "SELECT id, name FROM topic WHERE category_id = '".$cat_id."' ORDER BY name ASC";
   $result_topics = mysqli_query($con, $sql_topics);
   if($result_topics && $myrow_topics = mysqli_fetch_array($result_topics))
   {
       do
       {
           $all_topics .= '<option value="'.$myrow_topics['id'].'">'.$myrow_topics['name'].'</option>';
       }
       while($myrow_topics = mysqli_fetch_array($result_topics));
   }
   else
   {
    $all_topics = '<option value="">No Topic Found</option>';
   }

   $jsonarray["all_topics"] = $all_topics;
   echo json_encode($jsonarray);