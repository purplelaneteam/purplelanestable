<?php
require_once "../includes/initialize.php";
require_once "logincheck.php";

$id = $_SESSION['id'];
$user = getMenteeBasicInfo($con, $id);
$credits=getMenteeTransaction($con,'credit');
$debits=getMenteeTransaction($con,'debit');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?php echo SITE_NAME; ?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="" />
    <meta name="description" content="" />
	<meta name="author" content="" />
	<?php
	include("../head.php");
	?>
</head>
<body data-spy="scroll" data-target=".navbar" data-offset="82">
	

    <div class="site__layer"></div>
    <?php include('navbar.php');?>s
	<section class="burger2 app" id="app">
        <div class="container">
            <!-- container-start -->
			<div class="row">
									<div class="col-lg-9 col-sm-12">
										<h5 class="section-heading">My Transactions</h5>
									<p class="spcev">
									
									</p></div>
						<!-- <div class="col-lg-3 col-sm-12 hidden-xs hidden-sm"> -->
							<!-- <label class="pull-right"> -->
							    <!-- <div class="d-flex justify-content-center h-100"> -->
									<!-- <div class="searchbar"> -->
									    <!-- <input class="search_input" type="text" name="" placeholder="Search..."> -->
									    <!-- <a href="#" class="search_icon"><i class="fa fa-search"></i></a> -->
									<!-- </div> -->
								<!-- </div> -->
							<!-- </label> -->
						<!-- </div> -->
									
								</div>
			<div class="container">
				<div class="row">
					<div class="col-md-12 text-center" style="margin-top:40px">
						<nav class="nav-justified ">
						  <div class="nav nav-tabs " id="nav-tab" role="tablist">
							
							<a class="nav-item nav-link active" id="pop1-tab" data-toggle="tab" href="#pop1" role="tab" aria-controls="pop1" aria-selected="true"> <b><i class="fa fa-money" aria-hidden="true"></i> Debit</b></a>
							
							<a class="nav-item nav-link " id="pop2-tab" data-toggle="tab" href="#pop2" role="tab" aria-controls="pop2" aria-selected="false"> <b><i class="fa fa-money" aria-hidden="true"></i> Credit</b></a>
							
						  </div>
						</nav>
						<div class="tab-content" id="nav-tabContent">
						    
							
						    <div class="tab-pane fade show active" id="pop1" role="tabpanel" aria-labelledby="pop1-tab">
							   <div class="pt-3"></div>
							   
								<!-- Scheduled start -->
								
                                <div class="row">
								   
									<!---/col-->
									<div class="col-md-12 col-sm-12  ">
									   
										<div class="store-btns">
											 <h2 class="section-heading review-head"></h2>
											 <table class='table table-striped' id='menteeDTransaction'  width='100%'>
                                            	<thead>
													<tr>
													<td>Sr No</td>
													<td>Meeting ID</td>
													<td>Amount</td>
													<td>Debit</td>
													<td>Date</td>
													</tr>
												</thead>
												<tbody>
												<?php
													if(isset($debits))
													{
														$i=1;
														foreach($debits as $debit)
														{
															echo '<tr><td>'.$i++.'</td><td>'.$debit['meetings_id'].'</td><td>'.$debit['amount'].'</td><td>Debit</td><td>'.date_format(date_create($debit['addedon']),"Y/m/d ").'</td></tr>';
														}
													}
													else
													{

													}
												?> 
												</tbody>
                                            </table>
										</div>
									</div>
									<!---/col-->
								   
								</div>
                                 
								<!-- Scheduled END -->
							
						    </div>
						
    						<div class="tab-pane fade " id="pop2" role="tabpanel" aria-labelledby="pop2-tab">
								<div class="pt-3"></div>
								
								<!-- Requests start -->
								
                                <div class="row">
								   
									<!---/col-->
									<div class="col-md-12 col-sm-12  ">
									   
										<div class="store-btns">
											 <h2 class="section-heading review-head"></h2>
											 <table class='table table-striped' id='menteeCTransaction' width='100%'>
                                            	<thead>
													<tr>
													<td>Sr No</td>
													<td>Meeting ID</td>
													<td>Amount</td>
													<td>Credit</td>
													<td>Date</td>
													</tr>
												</thead>
												<tbody>
												<?php
													if(isset($credits))
													{
														$i=1;
														foreach($credits as $credit)
														{
															echo '<tr><td>'.$i++.'</td><td>'.$credit['meetings_id'].'</td><td>'.$credit['amount'].'</td><td>Credit</td><td>'.date_format(date_create($credit['addedon']),"Y/m/d ").'</td></tr>';
														}
													}
													else
													{

													}
												?> 
												</tbody>
                                            </table>
										</div>
									</div>
									<!---/col-->
								   
								</div>
                                 
								<!-- Requests END -->
							
							</div>
							
							
							<div class="tab-pane fade" id="pop3" role="tabpanel" aria-labelledby="pop3-tab">
							   <div class="pt-3"></div>
								
								<!-- completed start -->
								
                                <div class="row">
								   
									<!---/col-->
									<div class="col-md-12 col-sm-12  ">
									   
										<div class="store-btns">
											 <h2 class="section-heading review-head"></h2>
										
											 <?php
												if(isset($completed_mentors))
												{
													foreach($completed_mentors as $mentor)
													{
														echo selecMentorInfoWithMeetingDate($con,$mentor['mentor_id']);
													}
												}
												else
												{

												}
											?> 
										</div>
									</div>
									<!---/col-->
								   
								</div>
                                 
								<!-- completed END -->
							
							  
						    </div>
							
							<div class="tab-pane fade" id="pop4" role="tabpanel" aria-labelledby="pop4-tab">
								<div class="pt-3"></div>
								
								<!-- cancelled start -->
								
                                <div class="row">
								   
									<!---/col-->
									<div class="col-md-12 col-sm-12  ">
									   
										<div class="store-btns">
											 <h2 class="section-heading review-head"></h2>
										
											 <?php
												if(isset($cancelled_mentors))
												{
													foreach($cancelled_mentors as $mentor)
													{
														echo selecMentorInfoWithMeetingDate($con,$mentor['mentor_id']);
													}
												}
												else
												{

												}
											?> 
											
										</div>
									</div>
									<!---/col-->
								   
								</div>
                                 
								<!-- cancelled END -->
							
							</div>
						  
						</div>
					</div>
				</div>
            </div>
           
          
			
			
        </div>
        <!-- container end -->
    </section>
	<?php 
require_once('../footer.php');
require_once('../footer_tags.php');

?>