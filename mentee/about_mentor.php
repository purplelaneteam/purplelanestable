<?php
require_once "../includes/initialize.php";
require_once "logincheck.php";

$id = $_GET['id'];
$query = "SELECT concat(mn.fname, ' ', mn.lname) as name, mn.description, AVG(mr.ratings) as rating,mn.profile_pic FROM mentor mn, mentor_ratings mr WHERE mn.id = $id AND mr.meetings_id IN (SELECT id FROM meetings WHERE mentor_id = $id)";
$result = mysqli_query($con, $query);
if(mysqli_num_rows($result) > 0){
    $row = mysqli_fetch_array($result);
    $name = $row['name'];
    $description = $row['description'];
    $rating = ($row['rating'])*20;
    $profile_pic="/images/mentor/".$row['profile_pic'];
}
$get_reviews_sql = "SELECT concat(me.fname, ' ', me.lname) as name, me.profile_pic, mr.ratings, mr.comment FROM mentee me, meetings m, mentor_ratings mr WHERE me.id = m.mentee_id AND mr.meetings_id = m.id AND m.mentor_id = $id";
$get_reviews_result = mysqli_query($con, $get_reviews_sql);

$id = $_SESSION['id'];
$user = getMenteeBasicInfo($con, $id);
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<title>Purple Lane</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="keywords" content="" />
	<meta name="description" content="" />
	<meta name="author" content="" />
	<?php
    // include "header.php";
    include "../head.php";
    include 'navbar.php';
	?>
<link rel="stylesheet" href="<?php echo SITE_URL;?>/css/mm.css" rel="stylesheet">
	
</head>

<body data-spy="scroll" data-target=".navbar" data-offset="82">

<section class="burger2 app" id="app">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-12 order-first mb-5 mb-md-0 order-md-0">
                    <div class="app-image">
                        <img src="<?php echo $profile_pic ?>"  class="img-fluid mentor-pic">
                    </div>
					<div style="text-align:center">
					<!-- <a href="mentor-schedules.html" class="btn btn-alpha mr-lg-3 mr-2 cnect"> Connect <i class="fa fa-angle-right ml-3"></i></a> -->
					</div>
                </div>
                <div class="col-md-9 col-sm-12  ">
                    <div class="app-info mb-4">
                        <h5 class="section-heading"><?php echo ucwords($name); ?></h5><br>
                        <div class="row">
                            <div class="star-ratings-sprite"><span style="width:<?php echo $rating; ?>%" class="star-ratings-sprite-rating"></span></div>
                        </div><br>
                        <p align="justify"><?php echo $description; ?></p>
                    </div>
					<hr/>
                    <div class="store-btns">
                        <!-- <h5 class="section-heading review-head">Reviews</h5> -->
                            <?php
                            if(mysqli_num_rows($get_reviews_result) > 0){
                                while($get_reviews_row = mysqli_fetch_assoc($get_reviews_result))
                                {
                                    $mentee_name = $get_reviews_row['name'];
                                    $mentee_pic = "../images/mentee/".$get_reviews_row['profile_pic'];
                                    $ratings = $get_reviews_row['ratings']*20;
                                    $comment = $get_reviews_row['comment'];
                                    ?>
                                    <div class="row review-box">
                                        <div class="col-md-3 col-sm-12">
                                                <div class="app-image">
                                                    <img src="<?php echo $mentee_pic; ?>"  class="img-fluid rev-mentor-pic">
                                                </div>
                                                
                                        </div>
                                        <!---/col-->
                                        <div class="col-md-9 col-sm-12 mb-5 mb-md-0 order-first order-md-0 reviews-view">
                                            <div class="app-info mb-4">
                                                
                                                <h5 class=""><?php echo ucwords($mentee_name); ?></h5>
                                            
                                                <div class="row">
                                                    <div class="star-ratings-sprite"><span style="width:<?php echo $ratings; ?>%" class="star-ratings-sprite-rating"></span></div>
                                                </div><br>
                                                <p align="justify"><?php echo $comment; ?></p>
                                            </div>
                                        
                                        </div>
                                    </div><br>
                                    <?php
                                }
                            }
                            else {
                                echo '';
                            }
                            ?>
	                        
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php 
require_once('../footer.php');
require_once('../footer_tags.php');
?>