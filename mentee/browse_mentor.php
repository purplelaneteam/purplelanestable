<?php
require_once "../includes/initialize.php";
require_once "logincheck.php";
// $categories = getCategoryAndSubCategory($con);


$id = $_SESSION['id'];
$user = getMenteeBasicInfo($con, $id);
// $categories = getMentorCategory($con);

require_once "header.php";
?>
<style>
    .select2-container .select2-selection--single {
        box-sizing: border-box;
        cursor: pointer;
        display: block;
        height: 42px;
        user-select: none;
        -webkit-user-select: none;
    }

    .select2-container--default .select2-selection--single .select2-selection__rendered {
        color: #444;
        line-height: 41px;
    }

    .select2-container--default .select2-selection--single .select2-selection__arrow b {
        border-color: #888 transparent transparent transparent;
        border-style: solid;
        border-width: 5px 4px 0 4px;
        height: 0;
        left: 50%;
        margin-left: -4px;
        margin-top: 4px;
        position: absolute;
        top: 50%;
        width: 0;
    }
</style>

<?php   require_once '../header.php';
        require_once 'navbar.php';
        $category_id = false;
        if (isset($_GET['category']) && !empty($_GET['category']))
        {
            $category_id = $_GET['category'];
        }
        $categories = getCategoryAndSubCategory($con, $category_id);
        
        ?>

<section class="burger2 app" id="app">
    <div id="categories-select">
        <?php foreach($categories as $i => $category): ?>
            <div class="card category-item <?php echo ($category_id) ? 'single': ''; ?>" >
                <h3 class="category-title">
                    <?php echo $category['name']; ?>
                </h3>
                <div class="sub-categories">
                    <?php foreach($category['sub_categories'] as $j => $sub_category): ?>
                        <div class="item" data-cat-id="<?php echo $category['id']; ?>"  data-sub-id="<?php echo $sub_category['id']?>" data-title="<?php echo $sub_category['name'] ?>">
                            <?php echo $sub_category['name']; ?> <span class="mentors">mentors</span>
                        </div>
                        <?php if (!$category_id && $j >= 8) {
                            break;
                        }
                        ?>
                    <?php endforeach;?>
                </div>
                <?php if (!$category_id): ?>
                    <a href="/mentee/browse_mentor.php?category=<?php echo $category['id'] ?>" class="see-all" data-id="<?php echo $category['id']; ?>">
                        See All
                    </a>
                <?php endif; ?>
            </div>
        <?php endforeach; ?>
    </div>
    <div id="topics-modal">
        <div class="modal-container">
            <div class="close-topics">
                <i class="fas fa-times"></i>
            </div>
            <form id="submit_select_mentor" method="GET" action="/mentee/select_mentor.php">
                <div class="questions active" id="question1" data-current="1">
                    <div class="title">
                        <h2>Let's go more specific</h2>
                        <p class="sub-title">Which kind of Web development mentor do you need?</p>
                    </div>
                    <div class="body">
                        <div id="specific-div">

                        </div>
                    </div>
                </div>
                <div class="questions" id="question2" data-current="2">
                    <div class="title">
                        <h2>Share your starting point</h2>
                        <p class="sub-title">What's your current level of learning</p>
                    </div>
                    <div class="body">
                        <div class="other-question-div">
                            <label class="topics-select" for="starting_point_1">
                                <input type="radio" required name="starting_point" id="starting_point_1" value="I'm starting from scratch">
                                I'm starting from scratch
                            </label>
                            <label class="topics-select" for="starting_point_2">
                                <input type="radio" required name="starting_point" id="starting_point_2" value="I'm an beginner with a bit of relevant experience">
                                I'm an beginner with a bit of relevant experience
                            </label>
                            <label class="topics-select" for="starting_point_3">
                                <input type="radio" required name="starting_point" id="starting_point_3" value="I'm proficient, but still have room to grow">
                                I'm proficient, but still have room to grow
                            </label>
                            <label class="topics-select" for="starting_point_4">
                                <input type="radio" required name="starting_point" id="starting_point_4" value="I'm very experienced">
                                I'm very experienced
                            </label>
                        </div>
                    </div>
                </div>
                <div class="questions" id="question3" data-current="3">
                    <div class="title">
                        <h2>Declare your goal</h2>
                        <p class="sub-title">Which kind of are you most looking for from your mentor</p>
                    </div>
                    <div class="body">
                        <div class="other-question-div">
                            <label class="topics-select" for="goal_1">
                                <input type="radio" required name="goal" id="goal_1" value="I want help learning something new">
                                I want help learning something new
                            </label>
                            <label class="topics-select" for="goal_2">
                                <input type="radio" required name="goal" id="goal_2" value="I want help growing my career">
                                I want help growing my career
                            </label>
                            <label class="topics-select" for="goal_3">
                                <input type="radio" required name="goal" id="goal_3" value="I want help building a business">
                                I want help building a business
                            </label>
                            <label class="topics-select" for="goal_4">
                                <input type="radio" required name="goal" id="goal_4" value="Something else">
                                Something else
                            </label>
                        </div>
                    </div>
                </div>

            </form>
            <div class="topics-footer">
                <button class="btn btn-primary navigation-button" id="back">Back</button>
                <button class="btn btn-primary navigation-button" id="next">Next</button>
            </div>
        </div>
    </div>
    <!-- container end -->
</section>
<?php 
require_once('../footer.php');

require_once('../footer_tags.php');
?>

<script>
    $('#topics-modal .close-topics').click(function() {
        $('#topics-modal').hide();
    })
</script>