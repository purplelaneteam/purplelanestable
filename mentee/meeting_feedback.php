<?php
require_once "../includes/initialize.php";
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <title>Purple Lane</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <?php
	include("../head.php");
	?>
</head>

<body data-spy="scroll" data-target=".navbar" data-offset="82">


    <div class="site__layer"></div>
    <?php include('navbar.php');?>s
    <section class="burger2 app" id="app">
        <div class="container">
            <!-- container-start -->
            <div class="row">
                <div class="col-lg-9 col-sm-12">
                    <h5 class="section-heading">Please take your time to give feedback</h5>
                    <p class="spcev">
                    </p>
                </div>

            </div>
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center" style="margin-top:40px">
                        <div class="tab-content">
                            <div class="tab-content">
                                <div class="tab-pane fade show active" id="pop1" role="tabpanel"
                                    aria-labelledby="pop1-tab">
                                    <p>
                                        <div class="card border-primary rounded-0">
                                            <div class="card-header p-0">
                                                <div class="bg-info text-white text-center py-2">
                                                    <h6> Feedback
                                                        <span class="edt"><i class="fa fa-pencil-square"></i></span>
                                                    </h6>
                                                </div>
                                            </div>
                                            <div class="form-group">

                                                <div style="display:none;" id="alert_div_basic"
                                                    class="alert alert-danger alert-dismissible fade show" role="alert">
                                                    <p>Danger</p>
                                                    <button style="margin-top: -30px;" type="button" class="close"
                                                        data-dismiss="alert" aria-label="Close">
                                                        <span aria-hidden="true"
                                                            style="font-size: 31px;line-height: 0;">&times;</span>
                                                    </button>
                                                </div>
                                            </div>
                                            <form id="meeting_feedback" method="POST" enctype="multipart/form-data">
                                                <input type="hidden" name="meeting_id" id="meeting_id"
                                                    value="<?php echo $_GET['meeting_id'];?>">
                                                <div class="card-body p-3">
                                                    <div class="row">
                                                        <div class="col-md-12 col-sm-12">
                                                            <textarea class="form-control" rows="5" name="comment"
                                                                id="comment" placeholder="Enter your comment here."
                                                                style="border:1px solid #ccc"></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="rating-container">
                                                        <fieldset class="rating">
                                                            <input type="radio" id="star5" name="rating" value="5" />
                                                            <label class="full" for="star5"
                                                                title="Awesome - 5 stars"></label>

                                                            <input type="radio" id="star4half" name="rating"
                                                                value="4.5" />
                                                            <label class="half" for="star4half"
                                                                title="Pretty good - 4.5 stars"></label>

                                                            <input type="radio" id="star4" name="rating" value="4" />
                                                            <label class="full" for="star4"
                                                                title="Pretty good - 4 stars"></label>

                                                            <input type="radio" id="star3half" name="rating"
                                                                value="3.5" />
                                                            <label class="half" for="star3half"
                                                                title="Meh - 3.5 stars"></label>

                                                            <input type="radio" id="star3" name="rating" value="3" />
                                                            <label class="full" for="star3"
                                                                title="Meh - 3 stars"></label>

                                                            <input type="radio" id="star2half" name="rating"
                                                                value="2.5" />
                                                            <label class="half" for="star2half"
                                                                title="Kinda bad - 2.5 stars"></label>

                                                            <input type="radio" id="star2" name="rating" value="2" />
                                                            <label class="full" for="star2"
                                                                title="Kinda bad - 2 stars"></label>

                                                            <input type="radio" id="star1half" name="rating"
                                                                value="1.5" />
                                                            <label class="half" for="star1half"
                                                                title="Meh - 1.5 stars"></label>

                                                            <input type="radio" id="star1" name="rating" value="1" />
                                                            <label class="full" for="star1"
                                                                title="Sucks big time - 1 star"></label>

                                                            <input type="radio" id="starhalf" name="rating"
                                                                value="0.5" />
                                                            <label class="half" for="starhalf"
                                                                title="Sucks big time - 0.5 stars"></label>
                                                        </fieldset>
                                                    </div>
                                                    <button type="submit" class="btn btn-alpha mr-lg-3 mr-2 cnect"> Send
                                                        Feedback <i class="fa fa-angle-right ml-3"></i></button>
                                            </form>
                                        </div>
                                </div>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php 
require_once('../footer.php');
require_once('../footer_tags.php');
?>