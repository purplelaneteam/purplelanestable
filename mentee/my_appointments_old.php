<?php
require_once "../includes/initialize.php";
require_once "logincheck.php";

$id = $_SESSION['id'];
$user = getMenteeBasicInfo($con, $id);
$requested=getMeetingOfMentee($con,'pending');
$rescheduled=getMeetingOfMentee($con,'rescheduled');
$scheduled=getMeetingOfMentee($con,'scheduled');
$completed=getMeetingOfMentee($con,'completed');
$rejected=getMeetingOfMentee($con,'cancelled');

$mentor_cancelled=getMeetingOfMentee($con,'mentor cancelled');
$admin_cancelled=getMeetingOfMentee($con,'admin cancelled');

$date = date('Y-m-d');
$time = date('H:i:s');
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<title>Purple Lane</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="keywords" content="" />
	<meta name="description" content="" />
	<meta name="author" content="" />
	<?php
	include("../head.php");
	?>
</head>

<body data-spy="scroll" data-target=".navbar" data-offset="82">


	<div class="site__layer"></div>
	<?php include('navbar.php');?>s
	<section class="burger2 appointments app" id="app">
		<div class="container">
			<!-- container-start -->
			<div class="row">
				<div class="col-lg-9 col-sm-12">
					<h5 class="section-heading">My Appointments</h5>
					<p class="spcev">
					</p>
				</div>
				<!-- <div class="col-lg-3 col-sm-12 hidden-xs hidden-sm"> -->
				<!-- <label class="pull-right"> -->
				<!-- <div class="d-flex justify-content-center h-100"> -->
				<!-- <div class="searchbar"> -->
				<!-- <input class="search_input" type="text" name="" placeholder="Search..."> -->
				<!-- <a href="#" class="search_icon"><i class="fa fa-search"></i></a> -->
				<!-- </div> -->
				<!-- </div> -->
				<!-- </label> -->
				<!-- </div> -->

			</div>
			<div class="container">
				<div class="row">
					<div class="col-md-12 text-center" style="margin-top:40px">
						<nav class="nav-justified ">
							<div class="nav nav-tabs " id="nav-tab" role="tablist">

								<a class="nav-item nav-link active" id="pop1-tab" data-toggle="tab" href="#pop1"
									role="tab" aria-controls="pop1" aria-selected="true"> <b><i class="fa fa-handshake"
											aria-hidden="true"></i> Requested</b></a>
								<a class="nav-item nav-link " id="pop2-tab" data-toggle="tab" href="#pop2" role="tab"
									aria-controls="pop2" aria-selected="false"> <b><i class="fa fa-handshake"
											aria-hidden="true"></i> Re-scheduled</b></a>
								<a class="nav-item nav-link " id="pop2-tab" data-toggle="tab" href="#pop3" role="tab"
									aria-controls="pop3" aria-selected="false"> <b><i class="fa fa-handshake"
											aria-hidden="true"></i> Scheduled</b></a>
								<a class="nav-item nav-link " id="pop3-tab" data-toggle="tab" href="#pop4" role="tab"
									aria-controls="pop4" aria-selected="false"> <b><i class="fa fa-handshake"
											aria-hidden="true"></i> Completed</b></a>
								<a class="nav-item nav-link " id="pop4-tab" data-toggle="tab" href="#pop5" role="tab"
									aria-controls="pop5" aria-selected="false"> <b><i class="fa fa-handshake"
											aria-hidden="true"></i> Cancelled</b></a>

							</div>
						</nav>
						<div class="tab-content" id="nav-tabContent">


							<div class="tab-pane fade show active" id="pop1" role="tabpanel" aria-labelledby="pop1-tab">

								<!-- Scheduled start -->

								<div class="row">

									<!---/col-->
									<div class="col-md-12 col-sm-12  ">
										<div class="store-btns">
											<h2 class="section-heading review-head"></h2>
												<?php
													
													if(mysqli_num_rows($requested) > 0)
													{
														foreach ($requested as $key => $meeting)
														{
															include './include/meeting_info_old.php';
														}
													}
													else
													{
														echo "No meetings Found";
													}
												?>
										</div>
									</div>
									<!---/col-->

								</div>

								<!-- Scheduled END -->

							</div>

							<div class="tab-pane fade " id="pop2" role="tabpanel" aria-labelledby="pop2-tab">
								<div class="pt-3"></div>

								<!-- Requests start -->

								<div class="row">

									<!---/col-->
									<div class="col-md-12 col-sm-12  ">

										<div class="store-btns">
											<h2 class="section-heading review-head"></h2>
												<?php
													if(mysqli_num_rows($rescheduled) > 0)
													{
														foreach ($rescheduled as $key => $meeting)
														{
															include './include/meeting_info_old.php';
														}
													}
													else
													{
														echo "No meetings Found";
													}
												?>
										</div>
									</div>
									<!---/col-->
								</div>

								<!-- Requests END -->

							</div>


							<div class="tab-pane fade" id="pop3" role="tabpanel" aria-labelledby="pop3-tab">
								<div class="pt-3"></div>

								<!-- completed start -->

								<div class="row">

									<!---/col-->
									<div class="col-md-12 col-sm-12  ">

										<div class="store-btns">
											<h2 class="section-heading review-head"></h2>
													<?php
													if(mysqli_num_rows($scheduled) > 0)
													{
														foreach ($scheduled as $key => $meeting)
														{
															include './include/meeting_info_old.php';
														}
													}
													else
													{
														echo "No meetings Found";
													}
												?>
												</tbody>
											</table>
										</div>
									</div>
									<!---/col-->

								</div>

								<!-- completed END -->


							</div>

							<div class="tab-pane fade" id="pop4" role="tabpanel" aria-labelledby="pop4-tab">
								<div class="pt-4"></div>

								<!-- cancelled start -->

								<div class="row">

									<!---/col-->
									<div class="col-md-12 col-sm-12  ">

										<div class="store-btns">
											<h2 class="section-heading review-head"></h2>
											<?php
											if(mysqli_num_rows($completed) > 0)
											{
												foreach ($completed as $key => $meeting)
												{
													include './include/meeting_info_old.php';
												}
											}
											else
											{
												echo "No Meetings Found.";
											}
											?>
										</div>
									</div>
									<!---/col-->

								</div>

								<!-- cancelled END -->

							</div>

							<div class="tab-pane fade" id="pop5" role="tabpanel" aria-labelledby="pop5-tab">
								<div class="pt-5"></div>

								<!-- cancelled start -->

								<div class="row">

									<!---/col-->
									<div class="col-md-12 col-sm-12  ">

										<div class="store-btns">
											<h2 class="section-heading review-head"></h2>
													<?php
													if(mysqli_num_rows($mentor_cancelled) > 0)
													{
														foreach ($mentor_cancelled as $key => $meeting)
														{
															include './include/meeting_info_old.php';
														}
													}
													if(isset($admin_cancelled))
													{
														foreach ($admin_cancelled as $key => $meeting)
														{
															include './include/meeting_info_old.php';
														}
													}
												?>
												</tbody>
											</table>

										</div>
									</div>
									<!---/col-->

								</div>

								<!-- cancelled END -->

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- container end -->
		<!-- Modal -->
		<div class="modal fade" id="connect_client_modal" role="dialog">
			<div class="modal-dialog">

				<!-- Modal content-->
				<div class="modal-content" style="text-align:center;">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>
					<div class="modal-body">
						<p id="hang_hint" style="margin-bottom: 10px;"><b>Are you sure, you want to connect?</b></p>
						<div id="placeholder-div"></div>
					</div>
				</div>

			</div>
		</div>
	</section>
	<?php 
require_once('../footer.php');
require_once('../footer_tags.php');

?>
	<script src="https://apis.google.com/js/platform.js" async defer></script>
	<script src="https://apis.google.com/js/api.js" async defer></script>
	<script>
		// var today = new Date();
		// var dd = String(today.getDate()).padStart(2, '0');
		// var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
		// var yyyy = today.getFullYear();
		// today = yyyy + '/' + mm + '/' + dd;
		// console.log(today);
		// var time = <?php echo $db_start_time; ?>
		// console.log(time);

		$(document).on('click', '.connect_with_hangout', function (e) {
			e.preventDefault()
			var email = $(this).attr('data-email');
			var start_time = $(this).attr('data-start_time');
			var end_time = $(this).attr('data-end_time');
			var href = $(this).attr('href');

			var stt = getCurrentTime(start_time);

			var ett = getCurrentTime(end_time);

			var dt = new Date();
			var curr_time = dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds();

			var ctt = getCurrentTime(curr_time);

			if (ctt >= stt && ctt <= ett) {

				$('#placeholder-div').html(`<a href="${href}" class="btn btn-info btn-sm">Connect</a>`)
				
				// var invites = "[{ id : '" + email + "', invite_type : 'EMAIL' }]";

				// gapi.hangout.render('placeholder-div', {
				// 	'render': 'createhangout',
				// 	'invites': invites
				// });
				// var hang_hint = '<b>Are you sure, you want to connect?</b>';
				// $('#hang_hint').html(hang_hint);

			} else {
				// var hang_hint = '<b>Sorry!</b><br>You can connect with Mentor only between <b>' + start_time + '</b> to <b>' + end_time +
				// 	'</b>. Thanks.';
				// $('#hang_hint').html(hang_hint);

				var msg = 'You can connect with Mentor only between <b>' + start_time + '</b> to <b>' + end_time +
					'</b>. Thanks.';
				$('#placeholder-div').html(msg);
			}

			$('#connect_client_modal').modal('toggle');
		})
	</script>