<?php
require_once "../includes/initialize.php";
require_once "logincheck.php";

$id = $_SESSION['id'];
$categories = getMentorCategory($con);

require_once "header.php";
?>
<style>
.select2-container .select2-selection--single {
    box-sizing: border-box;
    cursor: pointer;
    display: block;
    height: 42px;
    user-select: none;
    -webkit-user-select: none;
}
.select2-container--default .select2-selection--single .select2-selection__rendered {
    color: #444;
    line-height: 41px;
}
.select2-container--default .select2-selection--single .select2-selection__arrow b {
    border-color: #888 transparent transparent transparent;
    border-style: solid;
    border-width: 5px 4px 0 4px;
    height: 0;
    left: 50%;
    margin-left: -4px;
    margin-top: 4px;
    position: absolute;
    top: 50%;
    width: 0;
}
</style>

<?php require_once '../header.php' ?>
<?php require_once 'navbar.php' ?>

	<section class="burger2 app" id="app">
        <div class="container">
            <!-- container-start -->
			<div class="container">
				<div class="row">
					<div class="col-md-12 text-center" style="margin-top:40px">
						
						<div class="tab-content  text-center" id="nav-tabContent">
							
						    <div class="card border-primary rounded-0 ">
                                <form id='search_mentor'>
                                    <div class="card-header p-0">
                                        <div class="bg-info text-white text-center py-2">
                                            <h6> Search Mentor
                                                <span class="edt"><i class="fa fa-search"></i></span>
                                            </h6>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="card-body">
                                        <select name='category' class="form-control category col-sm-3">
                                            <option value=''>Select Category</option>
                                            <?php
                                            while($category = mysqli_fetch_assoc($categories))
                                            {
                                                echo "<option value='".$category['id']."'>".$category['name']."</option>";
                                            }
                                            ?>
                                        </select>
                                        <select name='subcategory' class="form-control subcategory col-sm-3">
                                            <option value=''>Select Subcategory</option>
                                            <?php
                                            while($category = mysqli_fetch_assoc($categories))
                                            {
                                                echo "<option value='".$category['id']."'>".$category['name']."</option>";
                                            }
                                            ?>
                                        </select>
                                        <select class="form-control topics col-sm-3" name='topic'>
                                            <option value=''>Select Topics</option>
                                            <?php
                                            while($category = mysqli_fetch_assoc($categories))
                                            {
                                                echo "<option value='".$category['id']."'>".$category['name']."</option>";
                                            }
                                            ?>
                                        </select>
                                    <button type="submit" class="cnect" style="padding: 9px;padding-left: 20px;padding-right: 20px;border-radius: 25px;color: white;">Search</button>
                                </form>
                                </div>
                                
                                <!-- Search a Mentor End -->
								<!-- row  -->
						    </div>
						    <div class="tab-pane fade" id="pop3" role="tabpanel" aria-labelledby="pop3-tab">
							   <div class="pt-3"></div>
								<p>3. There's another way to do this for layouts that doesn't have to put the navbar inside the container, and which doesn't require any CSS or Bootstrap overrides.

								Simply place a div with the Bootstrap container class around the navbar. This will center the links inside the navbar:
							  						    </div>
						</div>
					
					</div>
				
				    </div>
				
				</div>
            </div>
        </div>
        <!-- container end -->
    </section>
	<?php 
require_once('../footer.php');
require_once('../footer_tags.php');
?>