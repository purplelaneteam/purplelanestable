<?php
    $meeting_id = $meeting['id'];
    $get_rating_query = 'SELECT AVG(ratings) as rating FROM mentor_ratings WHERE meetings_id IN (SELECT id FROM meetings WHERE mentor_id = '.$meeting['mentor_id'].')';
    $get_rating_result = mysqli_query($con, $get_rating_query);
    $get_rating_row = mysqli_fetch_array($get_rating_result);
    $rating_val = $get_rating_row['rating'];
    $rating_val = $rating_val * 20;

    if( in_array($meeting['codename'], ['rescheduled', 'mentor cancelled', 'admin cancelled', 'mentee cancelled', 'completed'])) {
        $get_rtime_sql = "SELECT id,`meeting_date`, `start_time` FROM `meeting_selected_slots` WHERE meetings_id='".$meeting_id."' ORDER BY addedon DESC LIMIT 1";
        $get_rtime_result = mysqli_query($con, $get_rtime_sql);
        $get_rtime_row = mysqli_fetch_array($get_rtime_result);
        $db_date = $get_rtime_row['meeting_date'];
        $db_start_time = $get_rtime_row['start_time'];
        $temp_end_time = strtotime("$db_start_time + 30min" );
        $cal_end_time = date('H:i:s', $temp_end_time);
    } elseif ($meeting['start_time']) {
        $date = date('Y-m-d');
        $db_date = $meeting['meeting_date'];
        $time = date('H:i:s');
        $db_start_time = $meeting['start_time'];
        $temp_end_time = strtotime("$db_start_time + 30min" );
        $cal_end_time = date('H:i:s', $temp_end_time);
        $email = $meeting['email'];
    }
    if(in_array($meeting['codename'], ['pending'])) {
        $sql = "SELECT id,`meeting_date`, `start_time` FROM `meeting_selected_slots` WHERE meetings_id='".$meeting_id."' ORDER BY addedon DESC";
        $slots = mysqli_query($con, $sql);
    }
?>

    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center" style="margin-top:5px">

                <div class="tab-content" id="nav-tabContent">


                    <div class="tab-pane fade show active" id="pop1" role="tabpanel" aria-labelledby="pop1-tab">

                        <!-- Scheduled start -->

                        <div class="row">

                            <!---/col-->
                            <div class="col-md-12 col-sm-12  ">

                                <div class="store-btns">

                                
                                        
                                    <div class="row review-box">
                                             <!-- Canceled :: Section :: Set -->
                                             <?php if (in_array($meeting['codename'], ['mentor cancelled', 'admin cancelled', 'mentee cancelled'])): ?>
                                                <div class="col-md-12 canceled_section text-left">
                                                    <h4>Your Schedule has been Canceled</h4>
                                                    <h5><?php echo date_format(date_create($db_date . ' '. $db_start_time)," l, j F Y H:ia"); ?></h5>
                                                </div>
                                            <?php endif; ?>
                                       
                                        <!-- Canceled :: Section :: End -->
                                       
                                        <div class="custom_mmt col-md-3 col-sm-12">
                                            <div class="app-image">
                                                <img src="../images/mentor/<?php echo $meeting['profile_pic'] ?>" class="img-fluid rev-mentor-pic">
                                            </div>
                                            <h5 class="text-center"><?php echo $meeting['mentor'] ?> </h5>
                                            <!-- <h6 class="text-center">College of science Engineer</h6> -->
                                        </div>
                                        <!---/col-->
                                        <div class="col-md-9 col-sm-12 mb-5 mb-btm mb-md-0 order-md-0">
                                            <div class="title_discription app-nfo app-info mb-4">

                                                <h5 class="subject-title">Title</h5>
                                                <p class="topic"><?php echo $meeting['topic'] ?>: <?php echo $meeting['title'] ?></p>
                                                <h5 class="">Discription </h5>
                                                <p><?php echo $meeting['comments'] ?></p>
                                                
                                            </div>

                                        </div>

                                        <div class="schedulde_solt col-md-4 text-center"> 
                                                <?php if (in_array($meeting['codename'], ['scheduled', 'completed','rescheduled'])): ?>
                                                <h5>Schedulde Solt:</h5>
                                                <h6><?php echo date_format(date_create($db_date . ' '. $db_start_time)," l, j F Y H:ia"); ?></h6>
                                                <?php endif; ?>
                                                <?php if(in_array($meeting['codename'], ['rescheduled'])): ?>
                                                    <h6>Looking for new time slot please call us.</h6>
                                                <?php endif; ?>
                                                <?php if (in_array($meeting['codename'], ['pending'])):  ?>
                                                <h5>Requested Solt:</h5>
                                                <?php foreach($slots as $slot):?>
                                                    <h6><?php echo date_format(date_create($slot['meeting_date'] . ' '. $slot['start_time']),"l, j F Y h:ia"); ?></h6>
                                                <?php endforeach;?>
                                                <?php endif;?>
                                            </div>
                                        
                                        <!--  Requested  :: Set -->
                                        <div class="schedulde_solt col-md-4 text-center"> 
                                               
                                            </div>
                                        <div class="schedulde_solt col-md-2 text-right">
                                            <?php if ( $meeting['codename'] == 'scheduled' && $meeting['hangout_link']): ?>
                                                <a target="_blank" class="confirm connect_with_hangout" href="<?php echo $meeting['hangout_link'] ?>" data-start_time="<?php echo $db_start_time ?>" data-end_time="<?php echo $cal_end_time ?>" data-email="<?php echo $email ?>">Connect</a>
                                            <?php endif;?>
                                            <?php if ($meeting['codename'] == 'rescheduled'):?>
                                            <form
                                                class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill "
                                                title="Confirm Meeting" id="mentee-confirmed"><input type="hidden" name="meeting_id"
                                                    id="result" value="" />
                                                <button type="button" data-id="<?php echo $meeting['id'] ?>" class="confirm reschedule-confirm-from-mentee-button" data-toggle="modal" data-target="#reschedule-confirm-from-mentee">Confirm</button>
                                            </form>
                                            <?php endif;?>
                                        </div>

                                        <!--  Requested  :: End -->
                                    </div>
                                </div>
                                <!---/col-->

                            </div>

                            <!-- Scheduled END -->

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


<style>
.dtp_modal-content {
	z-index: 99999 !important ;
}
</style>

