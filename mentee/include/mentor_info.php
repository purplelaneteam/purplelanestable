<?php 
    // $settings_price = getTotalSettingsPrice($con);

    // $total_price = $settings_price + $mentor['rate'];
?>
<div class="row review-box">
    <div class="col-md-3 col-sm-12">
        <div class="app-image">
            <img src="<?php echo $mentor['profile']; ?>" class="img-fluid rev-mentor-pic">
        </div>
        <div style="text-align:center" id="book_mentor">
            <a data-id="<?php echo $mentor['id'] ?>" href="schedule_appointment.php?mentor_id=<?php echo $mentor['id'] ?>&topic_id=<?php echo $mentor['topic_id']; ?>"
                class="btn btn-alpha mr-lg-3 mr-2 cnect book-meeting-button"> Schedule Meeting <i class="fa fa-angle-right ml-3"></i></a>
        </div>
    </div>
    <div class="col-md-9 col-sm-12 mb-5 mb-md-0 order-first order-md-0 reviews-view">
        <div class="app-nfo app-info mb-4">
        <a class="mentor-name-div" href="about_mentor.php?id=<?php  echo $mentor['id']; ?>"><h5 class="" style="padding-left:0px; color:black;"><?php echo $mentor['name']; ?></h5></a><h5>
                <!-- <label class="pull-right hor-rate small-caption"> -->
                <!-- <?php //echo CURRENCY_SYMBOL .$total_price; ?>/hr -->
                <!-- </label> -->
            </h5>
            <p class="experience">Total experience <?php echo $mentor['experience'] ?>yrs.</p>
            <!-- <p class="contact"><?php //echo $mentor['email'] ?> <span class="phone"><?php //echo $mentor['mobile'] ?></span></p> -->

            <div class="row">
                <div class="star-ratings-sprite"><span style="width:<?php echo $mentor['rating'] ?>%" class="star-ratings-sprite-rating"></span></div>
            </div>
            <?php if (!isset($admin_mentor_info) || !$admin_mentor_info): ?>
                <p class="spcev"><?php echo $mentor['description'] ?></p>
            <?php endif; ?>
        </div>
    </div>
</div>