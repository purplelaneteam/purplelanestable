<?php
    $meeting_id = $meeting['id'];
    $get_rating_query = 'SELECT AVG(ratings) as rating FROM mentor_ratings WHERE meetings_id IN (SELECT id FROM meetings WHERE mentor_id = '.$meeting['mentor_id'].')';
    $get_rating_result = mysqli_query($con, $get_rating_query);
    $get_rating_row = mysqli_fetch_array($get_rating_result);
    $rating_val = $get_rating_row['rating'];
    $rating_val = $rating_val * 20;

    if($meeting['codename'] == 'rescheduled'){
        $get_rtime_sql = "SELECT id,`meeting_date`, `start_time` FROM `meeting_selected_slots` WHERE meetings_id='".$meeting_id."' ORDER BY addedon DESC LIMIT 1";
        $get_rtime_result = mysqli_query($con, $get_rtime_sql);
        $get_rtime_row = mysqli_fetch_array($get_rtime_result);
        $db_date = $get_rtime_row['meeting_date'];
        $db_start_time = $get_rtime_row['start_time'];
    } elseif ($meeting['start_time']) {
        $date = date('Y-m-d');
        $db_date = $meeting['meeting_date'];
        $time = date('H:i:s');
        $db_start_time = $meeting['start_time'];
        $temp_end_time = strtotime("$db_start_time + 30min" );
        $cal_end_time = date('H:i:s', $temp_end_time);
        $email = $meeting['email'];
    }
?>

    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center" style="margin-top:5px">

                <div class="tab-content" id="nav-tabContent">


                    <div class="tab-pane fade show active" id="pop1" role="tabpanel" aria-labelledby="pop1-tab">

                        <!-- Scheduled start -->

                        <div class="row">

                            <!---/col-->
                            <div class="col-md-12 col-sm-12  ">

                                <div class="store-btns">
                                    <div class="row review-box">
                                        <div class="col-md-3 col-sm-12 order-first">
                                            <div class="app-image">
                                                <img src="../images/mentor/<?php echo $meeting['profile_pic'] ?>" class="img-fluid rev-mentor-pic">
                                            </div>
                                            <h5 class="text-center"><?php echo $meeting['mentor'] ?> </h5>
                                        </div>
                                        <!---/col-->
                                        <div class="col-md-6 col-sm-12 mb-5 mb-btm mb-md-0 order-md-0">
                                            <div class="app-nfo app-info mb-4">

                                                <h5 class="subject-title">Subject:  <span class="subject"><?php echo $meeting['topic'] ?></span> </h5>
                                                <hr />
                                                <h5 class="">Summary </h5>
                                                <p class="spcev">
                                                    <?php echo $meeting['comments'] ?>
                                                </p>
                                                
                                            </div>
                                            <?php if($meeting['codename'] == 'rescheduled'): ?>
                                            <form
                                                class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill mentee-confirmed-form"
                                                title="Confirm Meeting" id="mentee-confirmed"><input type="hidden" name="meeting_id"
                                                    id="result" value="<?php echo $meeting['id'] ?>" /><a href="#"  class="btn btn-alpha mr-lg-3 mr-2 cnect"> Confirm <i
                                                    class="fa fa-angle-right ml-3"></i></a>
                                            </form>
                                            
                                            <!-- <a href="#" class="btn btn-alpha mr-lg-3 mr-2 cnect"> Reshedule <i
                                                    class="fa fa-angle-right ml-3"></i></a> -->
                                            <!-- <a href="#" class="btn btn-alpha mr-lg-3 mr-2 cnect"> Cancel <i
                                                    class="fa fa-angle-right ml-3"></i></a> -->
                                            <?php elseif ($meeting['codename'] == 'scheduled'):?>
                                            <h5 style="font-weight:normal;">Hangout:
                                                <!-- <button type="button" class="btn btn-info btn-sm connect_with_hangout" data-start_time="<?php echo $db_start_time ?>" data-end_time="<?php echo $cal_end_time ?>" data-email="<?php echo $email ?>">Connect</button> -->
                                                <a target="_blank" href="<?php echo $meeting['hangout_link'] ?>" class="btn btn-info btn-sm connect_with_hangout" data-start_time="<?php echo $db_start_time ?>" data-end_time="<?php echo $cal_end_time ?>" data-email="<?php echo $email ?>">Connect</a>
                                            </h5>
                                            <?php endif;?>

                                        </div>

                                        <div class="col-md-3 col-sm-12 ">
                                            <!---/col-->

                                            <div class="app-nfo app-info mb-4">
                                                <?php if (in_array($meeting['codename'], ['scheduled', 'completed','rescheduled'])): ?>
                                                <div class="chiller_cb2">
                                                    <p class="pull-right hor-rate small-caption">
                                                        
                                                        <label for="myCheckbox"><i class="fa fa-calendar"
                                                        aria-hidden="true"></i></label>
                                                        <?php echo date_format(date_create($db_date . ' '. $db_start_time),"d/m/Y h:i:s a"); ?>
                                                    </p>
                                                </div>
                                                <?php else: ?>

                                                <div class="rating-div">
                                                    <h4>
                                                        Rating
                                                    </h4>
                                                    <div class="star-ratings-sprite"><span style="width:<?php echo $rating_val ?>%"
                                                            class="star-ratings-sprite-rating"></span>
                                                    </div>
                                                </div>
                                                <?php endif; ?>

                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <!---/col-->

                            </div>

                            <!-- Scheduled END -->

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>