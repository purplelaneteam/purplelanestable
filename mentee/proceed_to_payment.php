<?php
require_once "../includes/initialize.php";
require_once "logincheck.php";
$id = $_SESSION['id'];
require_once "header.php";
?>
<?php require_once '../header.php' ?>
<?php require_once 'navbar.php' ?>
<?php 
    $mentor = selectedMentorInfoArray($con, $_GET['mentor_id'], $_GET['topic_id']);
    $meeting = unserialize(getMeetingByOrder($con, $_SESSION['order_id']));
    $rates = getSettingsPrice($con);
    $total = $mentor['rate'];
    
	if(isset($_GET['coupon']) && !empty($_GET['coupon']))
	$discount = getDiscount($_GET['coupon'], $mentor['rate']);
	else
	$discount = 0;
    $total = $total - $discount;
    $amountAfterDiscount = $total;

    $total += calculateSettingsPrice($rates, $total, $discount);

	$discountMessage = '';
	$discountMessageType = 'alert-danger';
	if (isset($_SESSION['coupon']) && !empty($_SESSION['coupon'])) {
		$discountMessage = $_SESSION['coupon'];
		unset($_SESSION["coupon"]);

		if (isset($_SESSION['coupon_message_type']) && !empty($_SESSION['coupon_message_type'])) {
			$discountMessageType = $_SESSION['coupon_message_type'];
			unset($_SESSION["coupon_message_type"]);
		}
	}

?>

<section class="burger2 app" id="app">
    <div class="container">
        <!-- container-start -->
        <div class="row justify-content-center">
            <div class="col-lg-4 col-sm-12">
                <h5 class="heading checkout-font checkout-padding">Proceed To Payment</h5>
            </div>
		</div>

		<?php if($discountMessage): ?>
			<div class="message">
				<div style="" id="alert_div_basic" class="alert <?php echo $discountMessageType; ?> alert-dismissible fade show" role="alert">
					<p><?php echo $discountMessage; ?></p>
					<button style="margin-top: -30px;" type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true"style="font-size: 31px;line-height: 0;">&times;</span>
					</button>
				</div>
			</div>
		<?php endif; ?>

        <div class="bs-stepper">
            <div class="bs-stepper-header checkout-padding" role="tablist">
                <!-- your steps here -->
                <div class="step" data-index="1">
                    <button type="button" class="step-trigger"
                        id="details-part-trigger">
                        <span class="bs-stepper-circle">1</span>
                    </button>
                    <p class="stepper-title checkout-font">Appointment Details</p>
                </div>
                <div class="line"></div>
                <div class="step"  data-index="2">
                    <button type="button" class="step-trigger"
                        id="confirm-part-trigger">
                        <span class="bs-stepper-circle">2</span>
                    </button>
                    <p class="stepper-title checkout-font">Confirm Appointment</p>
                </div>
                <div class="line"></div>
                <div class="step" data-index="3">
                    <button type="button" class="step-trigger"
                        id="checkout-part-trigger">
                        <span class="bs-stepper-circle">3</span>
                    </button>
                    <p class="stepper-title checkout-font">Checkout</p>
                </div>
			</div>
            <div class="bs-stepper-content container">
                <!-- your steps content here -->
                <div id="proceed-to-pay" class="content fade active" role="tabpanel" aria-labelledby="details-part-trigger">
                    <h5 class="title checkout-font">Review Appointment Detail</h5>
                    <div class="card container">
                        <div class="logos show-3">
                            <img class="image" src="/images/paytm.png" width="100%" alt="">
                        </div>
                        <div class="mentor-details checkout-padding-right show-1 show-2">
                            <h5 class="title">Mentor Details</h5>
                            <div class="mentor-basic-detail">
                                <img class="profile" src="<?php echo $mentor['profile']?>" alt="" srcset="">
                                <div class="name checkout-font">
                                    First Name
                                    <p><?php echo $mentor['name'] ?></p>
                                </div>
                            </div>
                            <h5 class="title checkout-font">Proposed Slots</h5>
                            <h5 class="name checkout-font"><span class="detail"><?php echo date('l d, F Y H:i a', strtotime($meeting['slot1']))." to ".date('H:i a',strtotime('+3 hour',strtotime($meeting['slot1']))) ?></span></h5>
                            <h5 class="name checkout-font"><span class="detail"><?php echo date('l d, F Y H:i a', strtotime($meeting['slot2']))." to ".date('H:i a',strtotime('+3 hour',strtotime($meeting['slot2']))) ?></span></h5>
                        </div>
                        <div class="meeting-details checkout-font checkout-padding-left ">
                            <h5 class="show-1 show-2 title checkout-font">Meeting Details</h5>
                            <h5 class="show-1 show-2 name checkout-font">Title: <span class="detail"><?php echo $meeting['title'] ?></span></h5>
                            <h5 class="show-1 show-2 name checkout-font">Description: <span class="detail"><?php echo $meeting['comment']; ?></span></h5>
                            <div class="charges show-2 show-3">
                                <h5 class="title">Consultation Charges</h5>
                                <div class="price">
                                    <span class="price-name">Meeting Fees</span><span class="price-charge">
                                    <?php echo CURRENCY_SYMBOL.' '.$mentor['rate']; ?>
                                    </span>
                                </div>
                                <?php foreach($rates as $rate): ?>
                                    <div class="price">
                                        <span class="price-name"><?php echo $rate['name'] ?></span><span class="price-charge"><?php echo CURRENCY_SYMBOL.' '.number_format(calculateRate($rate['price'], $amountAfterDiscount, ($rate['codename'] == 'platform_charges')? $discount: 0), 2, '.', '') ?></span>
                                    </div>
								<?php endforeach; ?>
								<?php if($discount): ?>
									<div class="price">
                                        <span class="price-name">Discount</span><span class="price-charge"><?php echo CURRENCY_SYMBOL.' '.number_format((float)$discount, 2, '.', '') ?></span>
                                    </div>
								<?php endif; ?>
                                <div class="price total">
                                    <span class="price-name">Total </span><span class="price-charge"><?php echo CURRENCY_SYMBOL.' '.number_format((float)$total, 2, '.', '') ?></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
            $customer_id = $_SESSION['id'];
            $transaction_amount = $total;
            include './include/paytm.php'
		?>
		<div class="apply-coupon">
			<div class="button-container">
				<div class="field">
					<input type="text" name="coupon" id="coupon" placeholder="COUPON" class="form-control"
					<?php echo (isset($_GET['coupon']) && !empty($_GET['coupon']) ) ? "value=\" ".$_GET['coupon']."\" disabled": ''  ?>
					>
				</div>
				<div class="buttons">
					<button class="check-coupon">Apply</button>
					<?php if((isset($_GET['coupon']) && !empty($_GET['coupon']) )): ?>
						<button class="clear-coupon">Clear</button>
					<?php endif; ?>
				</div>
			</div>
		</div>
        <div class="button-container">
            <div class="buttons">
                <button class="back">Back</button>
                <button class="next">Continue</button>
            </div>
        </div>
    </div>
    <!-- container end -->
</section>
<?php 
if(!(strstr(strtolower($_SERVER['HTTP_USER_AGENT']), 'mobile') || strstr(strtolower($_SERVER['HTTP_USER_AGENT']), 'android'))){
require_once('../footer.php');
}
require_once('../footer_tags.php');
?>
<script>
	<?php if(isset($_GET['coupon']) && !empty($_GET['coupon'])): ?>
		$('.button-container .buttons .clear-coupon').click(function () {
			const queryParams = <?php print(json_encode($_GET)) ?>;
			location.href = location.pathname + `?mentor_id=${queryParams.mentor_id}&topic_id=${queryParams.topic_id}`
		})
	<?php endif; ?>
	<?php if(!isset($_GET['coupon']) || empty($_GET['coupon'])): ?>
		$('.button-container .buttons .check-coupon').click(function () {
			const coupon = $('#coupon')
			const queryParams = <?php print(json_encode($_GET)) ?>;
			location.href = location.pathname + `?mentor_id=${queryParams.mentor_id}&topic_id=${queryParams.topic_id}&coupon=${coupon.val()}`
		})
	<?php endif; ?>
</script>
</body>

</html>