<?php
require_once "../includes/initialize.php";
require_once "logincheck.php";
$id = $_SESSION['id'];
require_once "header.php";
?>
<link rel="stylesheet" href="<?php echo SITE_URL;?>/css/mm.css" rel="stylesheet">

<style>
.select2-container .select2-selection--single {
    box-sizing: border-box;
    cursor: pointer;
    display: block;
    height: 42px;
    user-select: none;
    -webkit-user-select: none;
}
.select2-container--default .select2-selection--single .select2-selection__rendered {
    color: #444;
    line-height: 41px;
}
.select2-container--default .select2-selection--single .select2-selection__arrow b {
    border-color: #888 transparent transparent transparent;
    border-style: solid;
    border-width: 5px 4px 0 4px;
    height: 0;
    left: 50%;
    margin-left: -4px;
    margin-top: 4px;
    position: absolute;
    top: 50%;
    width: 0;
}
</style>
<?php require_once '../header.php' ?>
<?php require_once 'navbar.php' ?>
	<section class="burger2 app" id="app">
        <div class="container">
            <!-- container-start -->
			<div class="row">
				<div class="col-lg-9 col-sm-12">
					<h5 class="section-heading">Expert Details</h5>
				</div>
			</div>
			<div class="container">
				   <!-- completed start -->
					<div class="row">
						<div class="col-md-12 col-sm-12  ">
							<div class="store-btns">
								<h2 class="section-heading review-head"></h2>
								<?php	
									echo selectedMentorInfo($con,$_GET['mentor_id'], $topic_id);
								?>		
							</div>
						</div>
						<!---/col-->
					</div>
                    <!-- completed END -->
			</div>
        </div>
        <!-- container end -->
    </section>
	
	
	<section class="app" id="app">
        <div class="container">
            <!-- container-start -->
			<div class="row">
				<div class="col-lg-9 col-sm-12">
					<h5 class="section-heading">Schedule Details</h5>
				</div>
			</div>
			<div class="form-group">
				<div style="display:none;" id="alert_div_basic" class="alert alert-danger alert-dismissible fade show" role="alert">
					<p>Danger</p>
					<button style="margin-top: -30px;" type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true"style="font-size: 31px;line-height: 0;">&times;</span>
					</button>
				</div>
			</div>
			<div class="container">
				
				    <!-- completed start -->
					<div class="row">
						<div class="col-md-12 col-sm-12  ">
							<div class="store-btns">
								<h2 class="section-heading review-head"></h2>
										
								<div class="row review-box">
								<form id='schedule_appointment_form' action="./processreq/proc_place_appointment_req.php" method='post' style='width:100%'>
									<!---/col-->
									<div class="col-md-12 col-sm-12 mb-5 mb-btm mb-md-0 order-md-0">
										<div class="app-nfo app-info mb-4">
											<h6 class="" style="padding-left: 0px">Choose Timings (Select slot of 3 Hr to book 30 minute meeting) </h6>
											<p class="spcev">
											<div class="row">
											    <b style="margin-top:10px;margin-left:20px"> Slot 1 :</b>
												<div class="col-md-4 col-sm-12">
                                                    <div> 
                                                        <div id="picker"> Slot 1</div>
                                                        <input type="hidden" name="slot1" id="slot1" value="" />
                                                        <input type="hidden" name='mentor_id' id="mentor_id" value="<?php echo $_GET['mentor_id']?>" />
                                                        <input type="hidden" name='mentee_id' id="mentee_id" value="<?php echo $id?>" />
                                                        <input type="hidden" name='topic_id' id="topic_id" value="<?php echo $_GET['topic_id']?>" />
                                                       
                                                    </div>
												</div>
												<b style="margin-top:10px;"> Slot 2 :</b>
												<div class="col-md-4 col-sm-12">
                                                    <div>
                                                        <div id="picker2"> </div>
                                                        <input type="hidden" name="slot2" id="slot2" value="" />
                                                    </div>
												</div>
											<!---/col-->
                                            </div>
											</p>
											<hr/>
											<h6 class="" style="padding-left: 0px">A brief overview of meeting </h6>
											<div class="field">
                                                <input type="text" name="title" id="title" placeholder="Title for your meeting" class="form-control">
                                            </div>
											<div class="row">
											<div class="col-md-12 col-sm-12">
												<textarea class="form-control" rows="5" name="comment" id="comment" placeholder="Describe your meeting here" style="border:1px solid #ccc"></textarea>
											</div>
											</div>
											<button type='submit' class="btn btn-alpha mr-lg-3 mr-2 cnect"  >
											Place appointment request
											</button>
										</div>
									</div>
								</form>
								</div>
											
							</div>
						</div>
						<!---/col-->
					</div>
                    <!-- completed END -->
				
            </div>
        </div>
        <!-- container end -->
    </section>
	<?php 
require_once('../footer.php');
require_once('../footer_tags.php');

?>


<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
   
   <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.0/moment-with-locales.min.js"></script>
   
   <script type="text/javascript" src="/js/datetimepicker.js?<?php echo '0.0.1' ?>"></script>
    

	<script type="text/javascript">
	
    $(document).ready( function () {
		// moment().format("YYYY-MM-DD HH:mm")
		minute = moment().minute()
		if (minute >= 30) minute = 30
		else minute = 0;
		const tomorrow = moment().add(1, 'days').minute(minute)
		$('#slot1').val(moment(tomorrow).format("YYYY-MM-DD HH:mm"))
		$('#slot2').val(moment(tomorrow).format("YYYY-MM-DD HH:mm"))

        $('#picker').dateTimePicker({selectDate: tomorrow, startDate: tomorrow, minuteIncrement: 30});
		$("#book_mentor").hide();

		$('#picker2').dateTimePicker({selectDate: tomorrow, startDate: tomorrow, minuteIncrement: 30});

    })
	
	$(function($){
  
   $( '.toggle' ).click(function(){
     var target = $( this ).data( 'menu' );
     
     $( '#' + target ).toggleClass( 'menu--open' );
   });
  
});
    </script>

    
</body>

</html>