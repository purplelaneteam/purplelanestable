
<nav class="nav-fixed navbar navbar-expand-lg navbar-togglable navbar-dark" style="">
        <div class="container">
            <!-- Brand/ logo -->
            <a class="navbar-brand" href="index.php"><img src="images/logo.png" alt="logo image" class="img-fluid-logo"></a>
            <!-- Toggler -->
            <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <span></span><span></span><span></span>
            </button>
            <!-- Collapse -->
            <div class="collapse navbar-collapse" id="navbarCollapse">
                <!-- Links -->
                <ul class="navbar-nav ml-auto">
                    	<li class="nav-item">
                        <a class="btn btn-alpha ml-lg-3 mt-3 mt-lg-0">Book Appointment</a>
                    </li>
                   
                    <li class="nav-item">
                        <a class="nav-link" href="">My Appointments
                    </a>
                    </li>
                   
					
                </ul>
				
					<div class="">
						<div class="card-header msg_head">
							<div class="d-flex bd-highlight">
								<div class="img_cont">
									<img src="<?php echo $profile_pic ?>" class="rounded-circle user_img">
									
								</div>
								<div class="user_info">
									<span><?php echo $fname." ".$lname ?></span>
									
								</div>
							
							</div>
							<div class="d-flex bd-highlight">
						        <i class="fa fa-ellipsis-v dropbtn" id="action_menu_btn" onclick="myFunction()"></i>

								<div id="myDropdown" class="dropdown-content">
									<a href=""><i class="fa fa-user-o"></i> Edit profile</a>
									<a href=""><i class="fa fa-cog"></i> Account Settings</a>
									<a href=""><i class="fa fa-file-text"></i> My Appointments</a>
									<a href=""><i class="fa fa-info-circle"></i> Help Center</a>
									<a href=""><i class="fa fa-sign-out"></i> Logout</a>
								</div>
                            </div>
						</div>
					
					</div>
            </div>
            <!-- / .navbar-collapse -->
        </div>
        <!-- / .container -->
    </nav>