
<footer class="pb-2 footer" id="footer">
    <div class="container">
        <!-- container-start -->
        <div class="row text-white text-center burger">
            <div class="col-lg-10 offset-lg-1">
                <div class="newsletter mb-5">
                    <!-- <h3 class="mb-3">Don't miss out, Stay updated</h3> -->
                    <form class="form form-newsletter input-group  mx-auto position-relative" method="post" action="">
                        <!-- <div id="mail-messages" class="notification subscribe"></div>
                        <input class="form-control mr-md-2 mr-1" placeholder="Enter@email.com" type="email" name="subscribe_email">
                        <button type="submit" class="input-group-append btn btn-alpha " name="button">Subscribe <i class="fa fa-angle-right ml-3"></i></button> -->
                        <ul>
                            <li class="text">Subscribe for news letter</li>
                            <li><input type="email"><button type="submit" class="sss1"><img src="images/goair.png" alt="img"></button></li>
                        </ul>
                    </form>
                </div>
                
            </div>
            <!---/column-->
        </div>
        <!---/row-->
        <div class="card-body footer-card-body">
            <div class="col-lg-12 col-12 col-sm-12 sm-offset-2">
                <div class="row">
                    <div class="col-md-4">
                        <h6 class="foot-head">PurpleLane</h6>
                        <div class="spce">

                        </div>
                        <p>102, Alekya bliss, Camelot layout, 
                            Botanical gardens, Kondapur, 
                            Hyderabad Telangana, India-500084</p>
                        <p><a href="tel:+91 99999 99999">+91 99999 99999</a>
                        <p><a href="mailto:Info@Purplelane.com">Info@Purplelane.com</a></p>
                        
                    </div>
                    <div class="col-md-4">
                        <h6 class="foot-head">Our Services</h6>
                        <div class="spce"></div>
                        <ul>
                            <li class="ser-list"><a href="#">Mobile App Development</a></li>
                            <li class="ser-list"><a href="#">Web App Development</a></li>
                            <li class="ser-list"><a href="#">UI/UX Design</a></li>
                            <li class="ser-list"><a href="#">Offshore Development</a></li>
                            <li class="ser-list"><a href="#">Software Development</a></li>
                        </ul>
                    </div>
                    <div class="col-md-4">
                        <h6 class="foot-head">About Office</h6>
                        <div class="spce"></div>
                        <ul>
                            <li class="ser-list"><a href="#">Company</a></li>
                            <li class="ser-list"><a href="#">Portfolio</a></li>
                            <li class="ser-list"><a href="#">Client Stories</a></li>
                            <li class="ser-list"><a href="#">Career</a></li>
                        </ul>
                    </div>
                </div>                  
            </div>
            <!---/col-->              
        </div>
        <div class="container">
            
        </div>
        <div class="footer_copyright row text-white text-center">
            <div class="copyright social col-md-4">
                <ul>
                    <li><a href="http://" target="_blank" rel="noopener noreferrer"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                    <li><a href="http://" target="_blank" rel="noopener noreferrer"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                    <li><a href="http://" target="_blank" rel="noopener noreferrer"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                    <li><a href="http://" target="_blank" rel="noopener noreferrer"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                    <li><a href="http://" target="_blank" rel="noopener noreferrer"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                </ul> 
            </div>
            <div class="copyright col-md-4"> 
                <p class="text-left">© 2018 PurpleLane. All rights reserved.</p>
            </div>
            <div class="copyright text-right col-md-4"> 
                <ul>
                    <li><a href="http://" target="_blank" rel="noopener noreferrer">Privacy Policy</a></li>
                    <li><a href="http://" target="_blank" rel="noopener noreferrer">Terms of Service</a></li>
                </ul>
            </div>
        </div>
        <!---/row-->
    </div>
<!---/container-->
</footer>


