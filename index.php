<?php
require 'includes/initialize.php';


$error = $_SESSION['error'];
$success = $_SESSION['success'];

if ($success) {
    unset($_SESSION['success']);
}
if ($error) {
    unset($_SESSION['error']);
}
// dd([$error || $success, $error, $success]);
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <title><?php echo SITE_NAME; ?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <?php include 'head.php' ?>
</head>

<body data-spy="scroll" data-target=".navbar" data-offset="82">
    <div class="site-loader">
        <!---======Preloader===========-->
        <div class="loader-dots">
            <div class="circle circle-1"></div>
            <div class="circle circle-2"></div>
        </div>
    </div>
    <div class="site__layer"></div>
    <?php
    include 'header.php';
    ?>
    <section class="banner banner-full ">
        <!-- <div id="particles-js"></div> -->
        <div id="myCarousel" class="carousel slide carousel-fade" data-ride="carousel">
            <?php if ($error || $success) : ?>
                <div class="message">
                    <div style="" id="alert_div_basic" class="alert alert-<?php echo ($error) ? 'danger' : 'success' ?> alert-dismissible fade show" role="alert">
                        <p><?php echo ($error) ? $error : $success ?></p>
                        <button style="margin-top: -30px;" type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true" style="font-size: 31px;line-height: 0;">&times;</span>
                        </button>
                    </div>
                </div>
            <?php endif; ?>
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <div class="mask flex-center">
                        <div class="container">
                            <div class="row align-items-center">
                                <div class="col-md-7 col-12 order-md-1 order-1">
                                    <h4 class="text-white">Welcome to PurpleLane</h4>
                                    <p>Unlimited access to the world’s best. Learn and succeed faster with 1-on-1 mentoring from an expert.</p>

                                    <span class="" id="find-join">
                                        <?php if ($_SESSION['type'] === 'mentee') : ?>
                                            <a href="mentee/browse_mentor.php" class="btn btn-white find-a-mentor"> Find a
                                                Mentor <i class="fa fa-angle-right ml-3"></i></a>
                                        <?php elseif (!$_SESSION['id']) : ?>
                                            <a href="#." class="btn btn-alpha white find-a-mentor" data-toggle="modal" data-target="#login"> Find a MENTOR </a>
                                            <a href="#." class="btn btn-alpha join-purplelane" data-toggle="modal" data-target="#signup"> Join Purplelane </a>
                                        <?php endif; ?>
                                    </span>

                                </div>

                                <!-- <div class="col-md-5 col-12 order-md-2 order-2"><img src="images/classroom.svg" class="mx-auto"
                                        alt="slide"></div> -->
                            </div>
                        </div>
                    </div>
                </div>

                <!-- <div class="carousel-item">
                    <div class="mask flex-center">
                        <div class="container">
                            <div class="row align-items-center">
                                <div class="col-md-7 col-12 order-md-1 order-1">
                                    <h4 class="text-white">Welcome to Purplelane</h4>
                                    <p>Mentorship is a relationship in which a more experienced or more knowledgeable
                                        person helps to guide a less experienced or less knowledgeable person.</p>

                                    <span class="">
                                        <?php if ($_SESSION['type'] === 'mentee') : ?>
                                                                                                                            <a href="mentee/browse_mentor.php" class="btn btn-white"> Find a Mentor <i class="fa fa-angle-right ml-3"></i></a>
                                        <?php elseif (!$_SESSION['id']) : ?>

                                                                                                                            <a href="#." class="btn btn-alpha " data-toggle="modal" data-target="#signup"> Join Purplelane <i class="fa fa-angle-right ml-3"></i></a>
                                        <?php endif; ?>
                                    </span>

                                </div>

                                <div class="col-md-5 col-12 order-md-2 order-2"><img src="images/classroom.svg" class="mx-auto"
                                        alt="slide"></div>
                            </div>
                        </div>
                    </div>
                </div> -->

            </div>
            <!-- <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span> <span class="sr-only">Previous</span>
            </a>

            <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next"> <span class="carousel-control-next-icon"
                    aria-hidden="true"></span> <span class="sr-only">Next</span> </a> -->
        </div>
        <!--slide end-->

    </section>


    <section class="burger app" id="app">
        <div class="container">
            <!-- container-start -->

            <div class="row about_us">
                <div class="col-lg-12 col-sm-12 text-center">
                    <h3 class="mb-0 mb-lg-3 lh-1">What is PurpleLane</h3>
                </div>
            </div>

            <div class="row about_us">
                <div class="col-md-6 col-sm-12">
                    <div class="app-image">
                        <!--<iframe width="100%" height="315" src="https://www.youtube.com/embed/OGdeBxk1iRk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>-->
                        
                        <img src="images/home-page.jpg" width="100%" alt="images">
                        
                    </div>
                </div>
                <!---/col-->
                <div class="col-md-6 col-sm-12  mb-md-0 order-first order-md-0">
                    <div class="app-info mb-4">
                        <h5 class="small-heading"></h5>
                        <!--<h4 class="section-heading">Online Mentoring Platform</h4>-->

                        <ul>
                            <li>An online platform for students/professionals to connect with experts to seek guidance and boost their career and life.  </li>
                            
                            <li>PurpleLane enables you to choose a mentor, from a large pool of mentors from a variety of backgrounds. </li>
                            
                            <li>You can make video calls, share files, share screen with a mentor upon scheduling a meeting.</li>
                        </ul>
                        <a href="about_us.php" class="more">More</a>
                    </div>
                    <!-- <div class="store-btns">
                        <a href="#" class="mr-3 store-btn">
                            <img src="images/googlplay.png" alt="Google Play">
                        </a>
                        <a href="#" class="store-btn">
                            <img src="images/appstore.png" alt="App Store">
                        </a>
                    </div> -->
                </div>
                <!---/col-->

            </div>
            <!-- /row  -->
        </div>
        <!-- container end -->
    </section>


    <section>
        <div class="want_to_get_homeSlider">
            <h4 class="text-center center"><b>How Can Purple Lane Help You</b></h4>
            <!--<div class="col-lg-12 col-sm-12 text-center">-->
            <!--        <h3 class="about_us">How Can Purple Lane Help You</h3>-->
            <!--    </div>-->
            <div class="aaalll">
                <div class="all_slider">
                    <div class="all_sld">
                        <div class="container">

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="vh_cnter">
                                        <h5>Talk to our mentors from top companies who
                                            can help you crack interviews across various
                                            domains.</h5>
                                        <a href="#" data-target="#login" class="find_mntor" data-toggle="modal">Find Mentor</a>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="right">
                                        <h4>Want to get hired?</h4>
                                        <img src="images/Want_to_get _hired.svg" alt="images">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="all_slider">
                    <div class="all_sld">
                        <div class="container">

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="vh_cnter">
                                        <h5>Get advise from GATE toppers and tips to crack</h5>
                                        <a href="#" data-target="#login" class="find_mntor" data-toggle="modal">Find Mentor</a>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="right">
                                        <h4>Not Sure How To Crack GATE?</h4>
                                        <img src="images/ss_1.svg" alt="images">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="all_slider">
                    <div class="all_sld">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="vh_cnter">
                                        <h5>Speak to the alumni and current students from top universities across the world</h5>
                                        <a href="#" data-target="#login" class="find_mntor" data-toggle="modal">Find Mentor</a>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="right">
                                        <h4>Want to pursue masters
                                            from US/UK?</h4>
                                        <img src="images/ss_4.svg" alt="images">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="all_slider">
                    <div class="all_sld">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="vh_cnter">
                                        <h5>PurpleLane mentors work at government organizations like Central Excise department, Banking sector and have cracked most of the Competitive Exams. </h5>
                                        <a href="#" data-target="#login" class="find_mntor" data-toggle="modal">Find Mentor</a>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="right">
                                        <h4>Want to Crack Government Job ?</h4>
                                        <img src="images/Asset_2@4x.png" alt="images">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="all_slider">
                    <div class="all_sld">
                        <div class="container">

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="vh_cnter">
                                        <h5>Talk to our mentors across various fields who can help you make the right decisions</h5>
                                        <a href="#" data-target="#login" class="find_mntor" data-toggle="modal">Find Mentor</a>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="right">
                                        <h4>Want to boost
                                            your career and life?</h4>
                                        <img src="images/ss_3.svg" alt="images">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>



    <section id="roadmap" class="roadmap burger gray-light-bg">
        <!---======Roadmap start ===========-->
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-sm-12 text-center">
                    <h3 class="mb-0 mb-lg-3 lh-1">How It Works</h3>
                </div>
            </div>


            <div class="row n-d-flex">
                <div class="col-lg-12 offset-lg-0">
                    <ul class="nav nav-pills mb-5" id="pills-tab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-mentee" role="tab" aria-controls="pills-home" aria-selected="true"> Mentee</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-mentor" role="tab" aria-controls="pills-profile" aria-selected="false">Mentor</a>
                        </li>

                    </ul>
                    <div class="tab-content" id="pills-tabContent">
                        <div class="tab-pane fade show active" id="pills-mentee" role="tabpanel" aria-labelledby="pills-home-tab">
                            <div class="accordion" id="accordion1">
                                <div class="card">

                                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion1">
                                        <div class="card-body">
                                            <div class="col-lg-10 offset-lg-1 col-12 col-sm-8 sm-offset-2">
                                                <ul class="timeline timeline-centered">
                                                    <?php
                                                  // to hide after before lines in mobile view 
                                                   if(strstr(strtolower($_SERVER['HTTP_USER_AGENT']), 'mobile') || strstr(strtolower($_SERVER['HTTP_USER_AGENT']), 'android')){
                                                    echo '<style>#roadmap .timeline.timeline-centered .even:after { background-color:initial; }</style>';
                                                    echo '<style>#roadmap .timeline.timeline-centered .odd:before { background-color: initial; }</style>';
                                                    echo '<style>.timeline-marker:after{ content: none; }</style>';
                                                    echo '<style>.timeline-marker:before{ content: none; }</style>';
                                                     }
                                                   ?>
                                                   
                                                    <li class="timeline-item even">
                                                        <div class="timeline-info">
                                                            <span></span>
                                                        </div>
                                                        <div class="timeline-marker"></div>
                                                        <div class="timeline-content">
                                                            <img src="images/how-it-works/Find Perfect Mentor.svg" width="60%" />

                                                            <h5 class="timeline-title">Find Perfect Mentor</h5>
                                                            <p>PurpleLane mentors worked at places like Amazon, Microsoft,
                                                                Oracle, Qualcomm, and have graduated from institutes
                                                                like IISc, IIT,
                                                                IIM etc</p>
                                                        </div>
                                                    </li>
                                                    <li class="timeline-item odd">
                                                        <div class="timeline-info">
                                                            <span></span>
                                                        </div>
                                                        <div class="timeline-marker"></div>
                                                        <div class="timeline-content">
                                                            <img src="images/how-it-works/Get Guidance.svg" width="60%" />
                                                            <h5 class="timeline-title">Get Guidance</h5>
                                                            <p>With PurpleLane, you can make video calls, share
                                                                files, share screen with a mentor upon scheduling a
                                                                meeting and get 1-on-1 mentorship</p>
                                                        </div>
                                                    </li>
                                                    <li class="timeline-item even">
                                                        <div class="timeline-info">
                                                            <span></span>
                                                        </div>
                                                        <div class="timeline-marker"></div>
                                                        <div class="timeline-content">
                                                            <img src="images/how-it-works/Boost your career and life.svg" width="60%" />
                                                            <h5 class="timeline-title">Boost your career and life</h5>
                                                            <p>Your mentor imparts knowledge, provide insights; answer
                                                                your questions; curate the best resources to boost your
                                                                career.</p>
                                                        </div>
                                                    </li>

                                                </ul>
                                            </div>
                                            <!---/col-->

                                        </div>
                                    </div>
                                </div>

                            </div>
                            <!---/accordian-->
                        </div>
                        <!---tab end-->
                        <!--============================ FIRST TAB FAQ END HERE ====================================-->
                        <div class="tab-pane fade" id="pills-mentor" role="tabpanel" aria-labelledby="pills-profile-tab">
                            <div class="accordion" id="accordion2">
                                <div class="card">

                                    <div id="collapseFour" class="collapse show" aria-labelledby="headingFour" data-parent="#accordion2">
                                        <div class="card-body">
                                            <div class="col-lg-10 offset-lg-1 col-12 col-sm-8 sm-offset-2">
                                                <ul class="timeline timeline-centered">
                                                    <li class="timeline-item odd">
                                                        <div class="timeline-info">
                                                            <span></span>
                                                        </div>
                                                        <div class="timeline-marker"></div>
                                                        <div class="timeline-content">
                                                            <img src="images/how-it-works/Accept Meeting Invite.svg" width="60%" />

                                                            <h5 class="timeline-title">Accept Meeting Invite</h5>
                                                            <p>Accept the meeting invite proposed by the mentee.</p>
                                                        </div>
                                                    </li>
                                                    <li class="timeline-item even">
                                                        <div class="timeline-info">
                                                            <span></span>
                                                        </div>
                                                        <div class="timeline-marker"></div>
                                                        <div class="timeline-content">
                                                            <img src="images/how-it-works/Share Your expertise.svg" width="60%" />
                                                            <h5 class="timeline-title">Share Your Expertise</h5>
                                                            <p>As a mentor, you are a role model. Impart your knowledge
                                                                and experience, help your mentees get to new heights.
                                                            </p>
                                                        </div>
                                                    </li>
                                                    <li class="timeline-item odd">
                                                        <div class="timeline-info">
                                                            <span></span>
                                                        </div>
                                                        <div class="timeline-marker"></div>
                                                        <div class="timeline-content">
                                                            <img src="images/how-it-works/Get Rewarded.svg" width="60%" />
                                                            <h5 class="timeline-title">Get Rewarded</h5>
                                                            <p>Purple Lane lets you set your fees for sharing your
                                                                knowledge. Get your fees credited directly in your bank
                                                                account.</p>
                                                        </div>
                                                    </li>

                                                </ul>
                                            </div>
                                            <!---/col-->

                                        </div>
                                    </div>
                                </div>

                            </div>
                            <!---/accordian-->
                        </div>
                        <!---/tab end-->
                    </div>
                    <!--/tab-content-->
                </div>
                <!---/col-->
            </div>

        </div>
        <!--/container-->
    </section>

    <!--<section>-->
    <!--    <div class="testimonials">-->
    <!--        <h2 class="text-center">Testimonials</h2>-->
    <!--        <div class="container">-->
    <!--            <div class="slider">-->
    <!--                <div class="all_slider">-->
    <!--                    <div class="pik">-->
    <!--                        <img src="images/tem_2.png" alt="images">-->
    <!--                    </div>-->
    <!--                    <div class="text">-->
    <!--                        <h4>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</h4>-->
    <!--                        <h5><i>Name Details.</i></h5>-->
    <!--                        <h5><i>City, COUNTRY</i></h5>-->
    <!--                    </div>-->
    <!--                </div>-->
    <!--                <div class="all_slider">-->
    <!--                    <div class="pik">-->
    <!--                        <img src="images/tem_1.png" alt="images">-->
    <!--                    </div>-->
    <!--                    <div class="text">-->
    <!--                        <h4>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</h4>-->
    <!--                        <h5><i>Name Details.</i></h5>-->
    <!--                        <h5><i>City, COUNTRY</i></h5>-->
    <!--                    </div>-->
    <!--                </div>-->
    <!--                <div class="all_slider">-->
    <!--                    <div class="pik">-->
    <!--                        <img src="images/tem_2.png" alt="images">-->
    <!--                    </div>-->
    <!--                    <div class="text">-->
    <!--                        <h4>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</h4>-->
    <!--                        <h5><i>Name Details.</i></h5>-->
    <!--                        <h5><i>City, COUNTRY</i></h5>-->
    <!--                    </div>-->
    <!--                </div>-->
    <!--                <div class="all_slider">-->
    <!--                    <div class="pik">-->
    <!--                        <img src="images/tem_1.png" alt="images">-->
    <!--                    </div>-->
    <!--                    <div class="text">-->
    <!--                        <h4>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</h4>-->
    <!--                        <h5><i>Name Details.</i></h5>-->
    <!--                        <h5><i>City, COUNTRY</i></h5>-->
    <!--                    </div>-->
    <!--                </div>-->
    <!--            </div>-->
    <!--        </div>-->
    <!--    </div>-->
    <!--</section>-->

    <div class="modal" id="forgot_password" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Modal body text goes here.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary">Save changes</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Forgot Password End-->
    <!-- foooter started -->
    <?php
    include("footer.php");
    include("footer_tags.php");
    ?>