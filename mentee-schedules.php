<?php require_once "includes/initialize.php"; ?>
<body data-spy="scroll" data-target=".navbar" data-offset="82">
    <div class="site-loader">
        <!---======Preloader===========-->
        <div class="loader-dots">
            <div class="circle circle-1"></div>
            <div class="circle circle-2"></div>
        </div>
    </div>
    <div class="site__layer"></div>
    <nav class="nav-fixed navbar navbar-expand-lg navbar-togglable navbar-dark" style="">
        <div class="container">
            <!-- Brand/ logo -->
            <a class="navbar-brand" href="index.php"><img src="images/logo.png" alt="logo image" class="img-fluid-logo"></a>
            <!-- Toggler -->
            <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <span></span><span></span><span></span>
            </button>
            <!-- Collapse -->
            <div class="collapse navbar-collapse" id="navbarCollapse">
                <!-- Links -->
                <ul class="navbar-nav ml-auto">
                    	<li class="nav-item">
                        <a class="btn btn-alpha ml-lg-3 mt-3 mt-lg-0" href="choose-mentor.php">Book Appointment</a>
                    </li>
                   
                    <li class="nav-item">
                        <a class="nav-link" href="mentee-schedules.php">My Appointments
                    </a>
                    </li>
                   
					
                </ul>
				
					<div class="">
						<div class="card-header msg_head">
							<div class="d-flex bd-highlight">
								<div class="img_cont">
									<img src="images/mentor.jpg" class="rounded-circle user_img">
									
								</div>
								<div class="user_info">
									<span>Maryam Naz</span>
									
								</div>
							
							</div>
							<div class="d-flex bd-highlight">
						        <i class="fa fa-ellipsis-v dropbtn" id="action_menu_btn" onclick="myFunction()"></i>

								<div id="myDropdown" class="dropdown-content">
									<a href=""><i class="fa fa-user-o"></i> Edit profile</a>
									<a href=""><i class="fa fa-cog"></i> Account Settings</a>
									<a href=""><i class="fa fa-file-text"></i> My Appointments</a>
									<a href=""><i class="fa fa-info-circle"></i> Help Center</a>
									<a href=""><i class="fa fa-sign-out"></i> Logout</a>
								</div>
                            </div>
						</div>
					
					</div>
            </div>
            <!-- / .navbar-collapse -->
        </div>
        <!-- / .container -->
    </nav>
	
	
   
	<section class="burger2 app" id="app">
        <div class="container">
            <!-- container-start -->
			<div class="row">
				<div class="col-lg-9 col-sm-12">
										
					<h5 class="section-heading">My Scheduled Appointments</h5>
									
						<p class="spcev">
									Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the
                                  1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.

						</p></div>
			</div>
			<div class="container">
				<div class="row">
					<div class="col-md-12 text-center" style="margin-top:40px">
						
						<div class="tab-content" id="nav-tabContent">
						    
							
						    <div class="tab-pane fade show active" id="pop1" role="tabpanel" aria-labelledby="pop1-tab">
							   <div class="pt-3"></div>
							   
								<!-- Scheduled start -->
								
                                <div class="row">
								   
									<!---/col-->
									<div class="col-md-12 col-sm-12  ">
									   
										<div class="store-btns">
											 <h2 class="section-heading review-head"></h2>
										
												<div class="row review-box">
													<div class="col-md-3 col-sm-12 order-first">
															<div class="app-image">
																<img src="images/mentee.jpg"  class="img-fluid rev-mentor-pic">
															</div>
															<h5 class="">Mentor Name </h5>
													</div>
													<!---/col-->
													<div class="col-md-7 col-sm-12 mb-5 mb-btm mb-md-0 order-md-0">
														<div class="app-nfo app-info mb-4">
															
															<h5 class="">Subject </h5>
														<div class="row">	
															<fieldset class="rating">
															<input type="radio" id="star5" name="rating" value="5" />
															<label class = "full" for="star5"></label>
															<input type="radio" id="star4half" name="rating" value="4 and a half" />
															<label class="half" for="star4half" ></label>
															<input type="radio" id="star4" name="rating" value="4" />
															<label class = "full" for="star4" ></label>
															<input type="radio" id="star3half" name="rating" value="3 and a half" />
															<label class="half" for="star3half" ></label>
															<input type="radio" id="star3" name="rating" value="3" />
															<label class = "full" for="star3" ></label>
															<input type="radio" id="star2half" name="rating" value="2 and a half" />
															<label class="half" for="star2half" ></label>
															<input type="radio" id="star2" name="rating" value="2" />
															<label class = "full" for="star2" ></label>
															<input type="radio" id="star1half" name="rating" value="1 and a half" />
															<label class="half" for="star1half" ></label>
															<input type="radio" id="star1" name="rating" value="1" />
															<label class = "full" for="star1" ></label>
															<input type="radio" id="starhalf" name="rating" value="half" />
															<label class="half" for="starhalf" ></label>
															</fieldset>
														</div>	
															<p>
																Mentorship is a relationship in which a more experienced or more knowledgeable person helps to guide a less experienced or less knowledgeable person.
															</p>
															<p class="spcev">
																Mentorship is a relationship in which a more experienced or more knowledgeable person helps to guide a less experienced or less knowledgeable person.
															</p>
															<hr/>
															<h5 class="">Summary </h5>
															<p class="spcev">
																Mentorship is a relationship in which a more experienced or more knowledgeable person helps to guide a less experienced or less knowledgeable person.
															</p>
														</div>
														
														
														<a href="mentor-schedules.php" class="btn btn-alpha mr-lg-3 mr-2 cnect"> Reshedule <i class="fa fa-angle-right ml-3"></i></a>
														<a href="mentor-cancelled.php" class="btn btn-alpha mr-lg-3 mr-2 cnect"> Cancel <i class="fa fa-angle-right ml-3"></i></a>
													   
													</div>
													
													<div class="col-md-2 col-sm-12 ">
													   <!---/col-->
													
													<div class="app-nfo app-info mb-4">
															
														<h5 class="">Rating </h5>
														<div class="row">	
															<fieldset class="rating">
															<input type="radio" id="star5" name="rating" value="5" />
															<label class = "full" for="star5"></label>
															<input type="radio" id="star4half" name="rating" value="4 and a half" />
															<label class="half" for="star4half" ></label>
															<input type="radio" id="star4" name="rating" value="4" />
															<label class = "full" for="star4" ></label>
															<input type="radio" id="star3half" name="rating" value="3 and a half" />
															<label class="half" for="star3half" ></label>
															<input type="radio" id="star3" name="rating" value="3" />
															<label class = "full" for="star3" ></label>
															<input type="radio" id="star2half" name="rating" value="2 and a half" />
															<label class="half" for="star2half" ></label>
															<input type="radio" id="star2" name="rating" value="2" />
															<label class = "full" for="star2" ></label>
															<input type="radio" id="star1half" name="rating" value="1 and a half" />
															<label class="half" for="star1half" ></label>
															<input type="radio" id="star1" name="rating" value="1" />
															<label class = "full" for="star1" ></label>
															<input type="radio" id="starhalf" name="rating" value="half" />
															<label class="half" for="starhalf" ></label>
															</fieldset>
														</div>	
														
													 
														<div class="chiller_cb2">
														<p class="pull-right hor-rate small-caption">
														
														    <label for="myCheckbox"><i class="fa fa-calendar" aria-hidden="true"></i></label>
																12/12/2018
															</p>
														</div>
															
													</div>
												</div>
											
										</div>
									</div>
									<!---/col-->
								   
								</div>
                                 
								<!-- Scheduled END -->
							
						    </div>
						</div>
					</div>
				</div>
            </div>
           
          
			
			
        </div>
        <!-- container end -->
    </section>
	
    <footer class="pb-2 footer" id="footer">
        <div class="container">
            <!-- container-start -->
            <div class="row text-white text-center burger">
                <div class="col-lg-10 offset-lg-1">
                    <div class="newsletter mb-5">
                        <h4 class="mb-3">Don't miss out, Stay updated</h4>
                        <form class="form form-newsletter input-group w-50 mx-auto position-relative" method="post" action="">
                            <div id="mail-messages" class="notification subscribe"></div>
                            <input class="form-control mr-md-2 mr-1" placeholder="Enter@email.com" type="email" name="subscribe_email">
                            <button type="submit" class="input-group-append btn btn-alpha " name="button">Subscribe <i class="fa fa-angle-right ml-3"></i></button>
                        </form>
                    </div>
                    
                </div>
                <!---/column-->
            </div>
            <!---/row-->
			<div class="card-body footer-card-body">
                <div class="col-lg-12 col-12 col-sm-12 sm-offset-2">
						
				<div class="row">
					<div class="col-sm-6 col-lg-3 res-margin-xs">
						<h6 class="foot-head">About Purplelane</h6>
						<div class="spce"></div>
						<a href=""><p>Mentorship is a relationship in which a more experienced or <br/><br/>more knowledgeable person helps to guide a less experienced or less knowledgeable person.
							</p></a>
						<div class="spce"></div>
						
					</div>
					<div class="col-sm-6 col-lg-3 col-sm-offset-1 res-margin-xs">
						<h6 class="foot-head">Our Services</h6>
						<div class="spce"></div>
						<ul>
							<li class="ser-list"><a href="#">Mobile App Development</a></li>
							<li class="ser-list"><a href="#">Web App Development</a></li>
							<li class="ser-list"><a href="#">UI/UX Design</a></li>
							<li class="ser-list"><a href="#">Offshore Development</a></li>
							<li class="ser-list"><a href="#">Software Development</a></li>
						</ul>
					</div>
					<div class="col-sm-6 col-lg-3 col-sm-offset-1">
						<h6 class="foot-head">Main Menu</h6>
						<div class="spce"></div>
						<ul>
							<li class="ser-list"><a href="#">General Help</a></li>
							<li class="ser-list"><a href="#">Contact</a></li>
							<li class="ser-list"><a href="#">Portfolio</a></li>
							<li class="ser-list"><a href="#">Terms &amp; Conditions</a></li>
							<li class="ser-list"><a href="#">Privacy &amp; Policy</a></li>
						</ul>
					</div>
					<div class="col-sm-6 col-lg-3 res-margin-xs">
						<h6 class="foot-head">Contact Us</h6>
						<div class="spce"></div>
						<a href=""><p>102, Alekya bliss, Camelot layout,
							Botanical gardens, Kondapur,
							Hyderabad Telangana, India-500084.
							<br/><br/>
							<i class="fa fa-phone-square" aria-hidden="true"></i> +91 99999 99999.<br/>
							<i class="fa fa-envelope-o" aria-hidden="true"></i> Info@Purplelane.com
							</p></a>
						<div class="spce"></div>
						<div class="social-holder">
							<a href="#"><i class="fa fa-facebook social-icon"></i></a>
							<a href="#"><i class="fa fa-twitter social-icon"></i></a>
							<a href="#"><i class="fa fa-linkedin social-icon"></i></a>
							<a href="#"><i class="fa fa-google social-icon"></i></a>
						</div>
					</div>
				</div>
											
                </div>
                <!---/col-->
                                          
            </div>
			<div class="container">
				
		</div>
			
			
            <div class="row text-white text-center">
                <div class="copyright col-lg-12">
				
                    <p class="">  Copyright © 2018 <a href="" target="_blank" class="alt-color">Purplelane</a>. All rights reserved</p>
                </div>
            </div>
            <!---/row-->
        </div>
        <!---/container-->
    </footer>
  
  
  
  



    <script src="js/jquery3.2.1.min.js"></script>
    <!-- JQUERY LIBRARY -->
    <script src="js/particles.min.js"></script>
    <!-- Particles JS -->
    <script src="js/bootstrap.min.js"></script>
    <!-- Bootstrap JS -->
    <script src="js/main.js"></script>
    <!-- Main JS -->
    
</body>

</html>