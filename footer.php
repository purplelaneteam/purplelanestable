
<footer class="pb-2 footer mentor" id="footer">
    <div class="container">
        <!-- container-start -->
       
       
        <div class="footer_copyright row text-white text-center">
            <div class="copyright social col-md-4">
                <ul>
                    <li><a href="https://www.facebook.com/purplelanementors/" target="_blank" rel="noopener noreferrer"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                    <!--<li><a href="http://" target="_blank" rel="noopener noreferrer"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>-->
                    <li><a href="https://www.youtube.com/channel/UC_XCmRHOcY7yUJWHzPExjzQ" target="_blank" rel="noopener noreferrer"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                    <!--<li><a href="http://" target="_blank" rel="noopener noreferrer"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>-->
                    <!--<li><a href="http://" target="_blank" rel="noopener noreferrer"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>-->
                </ul> 
            </div>
            <div class="copyright col-md-4"> 
                <p class="text-left">© 2019 PurpleLane. All rights reserved.</p>
            </div>
            <div class="copyright text-right col-md-4"> 
                <ul>
                    <li><a href="http://" target="_blank" rel="noopener noreferrer">Privacy Policy</a></li>
                    <li><a href="/termsofuse.php" target="_blank" rel="noopener noreferrer">Terms of Use</a></li>
                </ul>
            </div>
        </div>
        <!---/row-->
    </div>
<!---/container-->
</footer>


