<link href="<?php echo SITE_URL;?>/assets/fontawesome/css/fontawesome.css" rel="stylesheet">
<link href="<?php echo SITE_URL;?>/assets/fontawesome/css/brands.css" rel="stylesheet">
<link href="<?php echo SITE_URL;?>/assets/fontawesome/css/solid.css" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="<?php echo SITE_URL;?>/css/design.css" id="dynamic-style">
<link rel="stylesheet" href="<?php echo SITE_URL;?>/css/responsive.css">
<link rel="stylesheet" href="<?php echo SITE_URL;?>/css/select2.css">
<link rel="stylesheet" href="<?php echo SITE_URL;?>/css/sweetalert.css">
<link href="<?php echo SITE_URL;?>/css/datatable/jquery.dataTables.min.css" rel="stylesheet" />
<link href="<?php echo SITE_URL;?>/css/datatable/buttons.dataTables.min.css" rel="stylesheet" />
<link href="<?php echo SITE_URL;?>/css/bs-stepper.css" rel="stylesheet" />
<link href="<?php echo SITE_URL;?>/css/slick.css" rel="stylesheet" />
<link href="<?php echo SITE_URL;?>/css/slick-theme.css" rel="stylesheet" />

<link href="<?php echo SITE_URL;?>/css/datetimepicker.css" rel="stylesheet" />
<link href="<?php echo SITE_URL;?>/css/slick-theme.css" rel="stylesheet" />

<link rel="stylesheet" href="<?php echo SITE_URL;?>/css/main.css">

<link rel="icon" type="image/png" href="/images/favicon.png" />

<meta name="description" content="PurpleLane is an online mentoring platform for students/professionals to connect with experts to seek guidance and boost their career and life. Find a Mentor and Book meeting today." />
<meta name="keywords" content="Online mentoring platform,Computer Science Mentors, Gate Mentors, Central Govertment Job Mentors,1 to 1 mentorship,online mentors ssc, Civil Service mentors,Banking job mentors,best online mentors,online mentors book , CAT online mentors, MBA mentors, career mentors " /> 
<link rel="canonical" href="https://www.purplelane.in" />

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-144649132-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-144649132-1');
</script>
