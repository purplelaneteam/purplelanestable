var SnippetLogin = function () {
    var e = $("#m_login"),
        i = function (e, i, a) {
            var l = $('<div class="m-alert m-alert--outline alert alert-' + i + ' alert-dismissible" role="alert">\t\t\t<button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>\t\t\t<span></span>\t\t</div>');
            e.find(".alert").remove(), l.prependTo(e), mUtil.animateClass(l[0], "fadeIn animated"), l.find("span").html(a)
        },
        a = function () {
            e.removeClass("m-login--forget-password"), e.removeClass("m-login--signup"), e.addClass("m-login--signin"), mUtil.animateClass(e.find(".m-login__signin")[0], "flipInX animated")
        },
        l = function () {
            $("#m_login_forget_password").click(function (i) {
                i.preventDefault(), e.removeClass("m-login--signin"), e.removeClass("m-login--signup"), e.addClass("m-login--forget-password"), mUtil.animateClass(e.find(".m-login__forget-password")[0], "flipInX animated")
            }), $("#m_login_forget_password_cancel").click(function (e) {
                e.preventDefault(), a()
            }), $("#m_login_signup").click(function (i) {
                i.preventDefault(), e.removeClass("m-login--forget-password"), e.removeClass("m-login--signin"), e.addClass("m-login--signup"), mUtil.animateClass(e.find(".m-login__signup")[0], "flipInX animated")
            }), $("#m_login_signup_cancel").click(function (e) {
                e.preventDefault(), a()
            })
        };
    return {
        init: function () {
            l(), $("#m_login_signin_submit").click(function (e) {
                e.preventDefault();
                var a = $(this),
                    l = $(this).closest("form");
                l.validate({
                    rules: {
                        email: {
                            required: !0,
                            email: !0
                        },
                        password: {
                            required: !0
                        }
                    }
                }), l.valid() && (a.addClass("m-loader m-loader--right m-loader--light").attr("disabled", !0), l.ajaxSubmit({
                    type: "POST",
                    dataType: 'json',
                    url: member_url_path + "/login/processreq/login.php",
                    success: function (e, t, r, s) {
                        if (e.code == '0') {
                            location.reload(1)
                        }
                        else if (e != '1') {
                            a.removeClass("m-loader m-loader--right m-loader--light").attr("disabled", !1), i(l, "danger", e.msg)
                        }
                    }
                }))
            }), $("#m_login_signup_submit").click(function (l) {
                l.preventDefault();
                var t = $(this),
                    r = $(this).closest("form");
                var position = r.position();
                r.validate({
                    rules: {
                    }
                }), r.valid() && (t.addClass("m-loader m-loader--right m-loader--light").attr("disabled", !0), r.ajaxSubmit({
                    url: member_url_path + "processreq/register.php",
                    type: 'post',
                    datatype: 'json',
                    success: function (l, s, n, o) {
                        var response = $.parseJSON(l)
                        if (response.code) {
                            scrollTo(position.left, position.top)
                            t.removeClass("m-loader m-loader--right m-loader--light").removeAttr("disabled")
                            var l = e.find(".m-login__signup form");
                            i(l, "danger", response.msg)
                        }
                        else {
                            t.removeClass("m-loader m-loader--right m-loader--light").attr("disabled", !0), r.clearForm(), r.validate().resetForm(), a();
                            var l = e.find(".m-login__signin form");
                            l.clearForm(), l.validate().resetForm(), i(l, "success", response.msg)
                        }
                    }
                }))
            }), $("#m_login_forget_password_submit").click(function (l) {
                l.preventDefault();
                var t = $(this),
                    r = $(this).closest("form");
                var position = r.position();
                r.validate({
                    rules: {
                        email: {
                            required: !0,
                            email: !0
                        }
                    }
                }), r.valid() && (t.addClass("m-loader m-loader--right m-loader--light").attr("disabled", !0), r.ajaxSubmit({
                    type: "POST",
                    dataType: 'json',
                    url: member_url_path + "/login/processreq/forgot_password.php",
                    success: function (l, s, n, o) {
                        var msg = l.msg
                        if (l.code == 1) {
                            scrollTo(position.left, position.top)
                            t.removeClass("m-loader m-loader--right m-loader--light").removeAttr("disabled")
                            var l = e.find(".m-login__forget-password form");
                            i(l, "danger", msg)
                        } else {
                            t.removeClass("m-loader m-loader--right m-loader--light").attr("disabled", !1), r.clearForm(), r.validate().resetForm(), a();
                            var l = e.find(".m-login__signin form");
                            l.clearForm(), l.validate().resetForm(), i(l, "success", msg)
                        }
                    }
                }))
            })
        }
    }
}();
jQuery(document).ready(function () {
    SnippetLogin.init()
});