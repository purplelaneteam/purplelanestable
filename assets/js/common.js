var hostname = window.location.hostname
var full_url = window.location.href
var protocol = window.location.protocol
var port_number = window.location.port
var segments = full_url.split('/');
if (hostname=="localhost")
{
    if (port_number != "") 
    {
        var url_path = protocol + "//" + hostname + ":" + port_number+"/"
    } 
    else 
    {
        var url_path = protocol + "//" + hostname + "/"
    }
}
else
{
    var url_path = protocol + "//" + hostname + "/"
}
var member_url_path = url_path + segments[3] + "/" + segments[4]

function submit_closest_form(element) 
{
    $(element).closest('form').submit()
}