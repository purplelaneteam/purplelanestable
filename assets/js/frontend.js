$(document).ready(function () {
    $("input[name='login_type']").click(function () {
        var login_type = $(this).val();
        if(login_type == 'mentee')
        {
            $("#google_sign_in_element").show();
        }
        else
        {
            $("#google_sign_in_element").hide();
        }  
    });

    $('#company').select2({
        placeholder: "Select Company",
        tags:true,
    });
    $('#skill').select2({
        placeholder: "Select a skill",
        tags:true,
    });

    $('#login_form_submit').click(function () {
        $("#error_div").removeClass("alert alert-danger");
        $("#error_div").html();
        $.ajax({
            type: 'POST',
            url: url_path + 'processreq/proc_login.php',
            data: $("#login_form").serialize(),
            dataType: "json",
            success: function (response) 
            {
                if (response.code) 
                {
                    $("#error_div").addClass("alert alert-danger");
                    $("#error_div").show();
                    $("#error_div").html(response.msg);
                    $("#error_div").fadeOut(3000);
                } 
                else
                {
                    if(response.type=='mentor')
                    window.location.href = url_path + response.type + "/my_appointment.php";
                    else
                    window.location.href = url_path + response.type + "/my_appointments.php";
                }
            },
            error: function (response) {

                $("#error_div").addClass("alert alert-danger");
                $("#error_div").show();
                $("#error_div").html(response.msg);
                $("#error_div").fadeOut(3000);
            }
        });
    });

    $('#mentor_update_form').submit(function (e) {
        e.preventDefault();
        $("#alert_div_basic").removeClass("alert alert-danger");
        $("#alert_div_basic").html();
        var formData = new FormData(this);
        $.ajax({
            type: 'POST',
            url: url_path + 'mentor/processreq/proc_edit_mentor_profile.php',
            data: formData,
            dataType: "json",
            cache: false,
            contentType: false,
            processData: false,
            success: function (response) {
                if (response.code) {
                    $("#alert_div_basic").addClass("alert alert-danger");
                    $("#alert_div_basic").show();
                    $("#alert_div_basic").html(response.msg);
                    $("#alert_div_basic").fadeOut(3000);
                } else {
                    $(window).scrollTop(0);
                    $("#alert_div_basic").addClass("alert alert-success");
                    $("#alert_div_basic").show();
                    $("#alert_div_basic").html(response.msg);
                    setTimeout(location.reload.bind(location), 3000);
                }
            },
            error: function (response) {
                $("#alert_div_basic").addClass("alert alert-danger");
                    $("#alert_div_basic").show();
                    $("#alert_div_basic").html(response.msg);
                    $("#alert_div_basic").fadeOut(3000);
            }
        });
    })

    $('#mentor_password_form').submit(function (e) {
        e.preventDefault();
        $("#alert_div_password").removeClass("alert alert-danger");
        $("#alert_div_password").html();
        var formData = new FormData(this);
        $.ajax({
            type: 'POST',
            url: url_path + 'mentor/processreq/proc_change_password_mentor.php',
            data: formData,
            dataType: "json",
            cache: false,
            contentType: false,
            processData: false,
            success: function (response) {
                if (response.code) {
                    $("#alert_div_password").addClass("alert alert-danger");
                    $("#alert_div_password").show();
                    $("#alert_div_password").html(response.msg);
                    $("#alert_div_password").fadeOut(3000);
                } else {
                    $(window).scrollTop(0);
                    $("#alert_div_password").addClass("alert alert-success");
                    $("#alert_div_password").show();
                    $("#alert_div_password").html(response.msg);
                    setTimeout(location.reload.bind(location), 3000);
                }
            },
            error: function (response) {
                $("#alert_div_password").addClass("alert alert-danger"); // changes
                    $("#alert_div_password").show();
                    $("#alert_div_password").html(response.msg);
                    $("#alert_div_password").fadeOut(3000);
            }
        });
    });

    $('#mentee_update_form').submit(function (e) {
        e.preventDefault();
        $("#alert_div_basic").removeClass("alert alert-danger");
        $("#alert_div_basic").html();
        var formData = new FormData(this);
        $.ajax({
            type: 'POST',
            url: url_path + 'mentee/processreq/proc_edit_mentee_profile.php',
            data: formData,
            dataType: "json",
            cache: false,
            contentType: false,
            processData: false,
            success: function (response) {
                if (response.code) {
                    $("#alert_div_basic").addClass("alert alert-danger");
                    $("#alert_div_basic").show();
                    $("#alert_div_basic").html(response.msg);
                    $("#alert_div_basic").fadeOut(3000);
                } else {
                    $(window).scrollTop(0);
                    $("#alert_div_basic").addClass("alert alert-success");
                    $("#alert_div_basic").show();
                    $("#alert_div_basic").html(response.msg);
                    setTimeout(location.reload.bind(location), 3000);
                }
            },
            error: function (response) {
                $("#alert_div_basic").addClass("alert alert-danger");
                    $("#alert_div_basic").show();
                    $("#alert_div_basic").html(response.msg);
                    $("#alert_div_basic").fadeOut(3000);
            }
        });
    })
    $('#mentee_password_form').submit(function (e) {
        e.preventDefault();
        $("#alert_div_password").removeClass("alert alert-danger");
        $("#alert_div_password").html();
        var formData = new FormData(this);
        $.ajax({
            type: 'POST',
            url: url_path + 'mentee/processreq/proc_change_password_mentee.php',
            data: formData,
            dataType: "json",
            cache: false,
            contentType: false,
            processData: false,
            success: function (response) {
                if (response.code) {
                    $("#alert_div_password").addClass("alert alert-danger");
                    $("#alert_div_password").show();
                    $("#alert_div_password").html(response.msg);
                    $("#alert_div_password").fadeOut(3000);
                } else {
                    $(window).scrollTop(0);
                    $("#alert_div_password").addClass("alert alert-success");
                    $("#alert_div_password").show();
                    $("#alert_div_password").html(response.msg);
                    setTimeout(location.reload.bind(location), 3000);
                }
            },
            error: function (response) {
                $("#alert_div_password").addClass("alert alert-danger"); // changes
                    $("#alert_div_password").show();
                    $("#alert_div_password").html(response.msg);
                    $("#alert_div_password").fadeOut(3000);
            }
        });
    });
    $('#forgot_pass_form_add').submit(function (e) {
        e.preventDefault();
        $("#alert_div_forgot").removeClass("alert alert-danger");
        $("#alert_div_forgot").html();
        var formData = new FormData(this);
        
        $.ajax({
            type: 'POST',
            url: url_path + 'processreq/forgot_password.php',
            data: formData,
            dataType: "json",
            cache: false,
            contentType: false,
            processData: false,
            success: function (response) {
                
                if (response.code) {
                    $("#alert_div_forgot").addClass("alert alert-danger");
                    $("#alert_div_forgot").show();
                    $("#alert_div_forgot").html(response.msg);
                    $("#alert_div_forgot").fadeOut(3000);
                } else {
                    $(window).scrollTop(0);
                    $("#alert_div_forgot").addClass("alert alert-success");
                    $("#alert_div_forgot").show();
                    $("#alert_div_forgot").html(response.msg);
                    setTimeout(location.reload.bind(location), 3000);
                }
            },
            error: function (response) {
                
                $("#alert_div_forgot").addClass("alert alert-danger"); // changes
                    $("#alert_div_forgot").show();
                    $("#alert_div_forgot").html(response.msg);
                    $("#alert_div_forgot").fadeOut(3000);
            }
        });
    })
    $('.category').select2({
        placeholder: "Select Category",
        allowClear: true
    });
    $('.subcategory').select2({
        placeholder: "Select Subcategory",
        allowClear: true
    });
    $('.topics').select2({
        placeholder: "Select Subject",
        allowClear: true
    });
    $(document).on("change",'.category',function(){
        $.ajax({
            type: 'GET',
            url: url_path + 'mentee/processreq/proc_get_subcategory.php?id='+$(this).val(),
            dataType: "json",
            cache: false,
            contentType: false,
            processData: false,
            success: function (response) {
                $('.subcategory').html(response.all_sub_cat);
                $('.topics').html(response.all_topics);

            },
            error: function (response) {
                
            }
        });
    });
    $(document).on("change",'.subcategory',function(){
        $.ajax({
            type: 'GET',
            url: url_path + 'mentee/processreq/proc_get_topics.php?id='+$(this).val(),
            dataType: "json",
            cache: false,
            contentType: false,
            processData: false,
            success: function (response) {
                $('.topics').html(response.all_topics);
            },
            error: function (response) {
                
            }
        });
    });
    $('#search_mentor').on("submit",function(e){
        e.preventDefault();
        var formData = new FormData(this);
        $.ajax({
            type : 'POST',
            url : url_path + 'mentee/processreq/proc_get_mentor_by_category.php',
            dataType : 'json',
            data : formData,
            cache: false,
            contentType: false,
            processData: false,
            success : function(response)
            {
                window.location.href =  url_path + 'mentee/select_mentor.php?cat='+response.cat+'&subcat='+response.subcat+'&subject='+response.topic;
            },
            error : function(response)
            {

            }
        });
    });
    
 });

 $(".check").change(function() {
    var id=$(this).attr('id');
    if($('#'+id).is(':checked'))
    {
        getMentor();
    }
    else
    {
        getMentor();
    }
});

function getMentor()
{
    var subcategory=$("#subcategory").val();
    var category=$("#category").val();
    var topics=$("#topics").val();
    var companies=[];
    var skills=[];
    $('.check').each(function(){
        if($('#'+this.id).is(':checked'))
        {
            var id=this.id;
            var data=id.split('_');
            if(data[0]=='companies')
            {
                companies.push(data[1]);
            }
            if(data[0]=='skills')
            {
                skills.push(data[1]);
            }
        }
    });

    $.ajax({
        type : 'POST',
        url : url_path + 'mentee/processreq/proc_get_mentor_on_filter.php',
        dataType : 'json',
        data : {cat:category,subcat:subcategory,topics:topics,companies:companies,skills:skills},
        cache: false,
        success : function(response)
        {
            $('#mentor_info').html('');
            $('#mentor_info').html(response.mentor_list);
        },
        error : function()
        {
            $('#mentor_info').html('');
            $('#mentor_info').html("ERROR");
        }
    });
    
   
}

$('#schedule_appointment_form').on("submit",function(e){
    e.preventDefault();
    var formData = new FormData(this);
    var mentor_id = this.mentor_id.value
    var topic_id = this.topic_id.value
    var slot1 = this.slot1.value;
    var slot2 = this.slot2.value;
    
    if (slot1 !== slot2)
    {
        $.ajax({
            type : 'POST',
            url : url_path + 'mentee/processreq/proc_place_appointment_req.php',
            dataType : 'json',
            data : formData,
            cache: false,
            contentType: false,
            processData: false,
            success : function(response)
            {
                if(response.code==0){
                    window.location.href =  url_path + `mentee/proceed_to_payment.php?mentor_id=${mentor_id}&topic_id=${topic_id}`;
                } else {
                    $('#mymsg').html(response.msg);
                }
            },
            error : function(response)
            {
    
            }
        });
    }
    else
    {
        $('#alert_div_basic p').html('Both time slots cannot be same');
        $('#alert_div_basic').fadeIn();
        
    }
    
    
});

$(document).ready(function() {
    $('#menteeCTransaction').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdfHtml5'
        ]
    } );
    $('#requested_meeting_tbl').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdfHtml5'
        ]
    } );
    $('#rescheduled_meeting_tbl').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdfHtml5'
        ]
    } );
    $('#scheduled_meeting_tbl').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdfHtml5'
        ]
    } );
    $('#menteeDTransaction').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdfHtml5'
        ]
    } );
    $('#completed_meeting_tbl').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdfHtml5'
        ]
    } );
    $('#cancelled_meeting_tbl').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdfHtml5'
        ]
    } );
    
} );

function cancel_meeting(status, mentor_id) {
    swal({
        title: "Are you sure you want to " + status + "?",
        type: "warning",
        showCancelButton: !0,
        confirmButtonText: "Yes"
    }).then(function (e) {
        if (e.value) {
            $.ajax({
                url: member_url_path + "/processreq/activate_deactivate_mentor.php",
                type: 'post',
                dataType: "json",
                data: "mentor_id=" + mentor_id + "&status=" + status,
                success: function (response) {
                    if (response.code) {
                        swal("Error!", response.msg, "error")
                    } else {
                        swal({
                            title: "Success!",
                            text: response.msg,
                            type: "success",
                        }).then(function () {
                            location.reload();
                        });
                    }
                }
            });
        }
    })
}

$(document).on('submit','#reschedule_appointment_form',function (e) {
    e.preventDefault();
    var formData=$(this).serialize();
    $.ajax({
        type: 'POST',
        url: url_path + 'mentor/processreq/proc_reschedule_by_mentor.php',
        data: formData,
        dataType: "json",
        success: function (response) {
            if (response.code) {
                $("#alert_div_basic").addClass("alert alert-dansubger");
                $("#alert_div_basic").show();
                $("#alert_div_basic").html(response.msg);
                $("#alert_div_basic").fadeOut(3000);
            } else {
                $(window).scrollTop(0);
                $("#alert_div_basic").addClass("alert alert-success");
                $("#alert_div_basic").show();
                $("#alert_div_basic").html(response.msg);
                setTimeout(function() {
                    location.href = '/mentor/my_appointment.php'
                }, 1000);
            }
        },
        error: function (response) {
           
        }
    });
});

$(document).on('submit','#confirm_appointment_form',function (e) {
    e.preventDefault();
    var formData=$(this).serialize();

    
    const slot = this.selected_slot.value
    if (!slot)
    {
        $("#alert_div_basic").addClass("alert alert-dansubger");
        $("#alert_div_basic").show();
        $("#alert_div_basic").html("Slelect a time to continue");
        $("#alert_div_basic").fadeOut(3000);
    }
    
    
    var time = moment(this[slot].value)
    var beforeTime = moment(this[`${slot}_prev`].value)
    var afterTime = moment(moment(this[`${slot}_prev`].value).add({hours: 3}).toString())
    
    
    
    if (time.isBetween(beforeTime, afterTime) || time.isSame(beforeTime) || time.isSame(afterTime))
    {
        $('.site-loader').show()
        
        $.ajax({
            type: 'POST',
            url: url_path + 'mentor/processreq/proc_confirm_by_mentor.php',
            data: formData,
            dataType: "json",
            success: function (response) {
                $('.site-loader').hide()
                
                if (response.code) {
                    $("#alert_div_basic").addClass("alert alert-success");
                    $("#alert_div_basic").show();
                    $("#alert_div_basic").html(response.msg);
                    $("#alert_div_basic").fadeOut(3000);
                    window.location.href = url_path + 'mentor' + "/my_appointment.php";
                } else {
                    $(window).scrollTop(0);
                    $("#alert_div_basic").addClass("alert alert-dansubger");
                    $("#alert_div_basic").show();
                    $("#alert_div_basic").html(response.msg);
                    $("#alert_div_basic").fadeOut(3000);
                }
            },
            error: function (response) {
                $('#alert_div_basic p').html('Please select time slot.');
                $('#alert_div_basic').fadeIn();
            }
        });

    }
    else
    {
        $("#alert_div_basic").addClass("alert alert-dansubger");
        $("#alert_div_basic").show();
        $("#alert_div_basic").html("Time Should be within 3hrs of the given time");
        $("#alert_div_basic").fadeOut(3000);
    }
    
    
    
});

$(document).on('click','#cancelled_appointment_by_mentor',function (e) {
    e.preventDefault();
    var cancel_meeting=$('#cancelled_appointment_by_mentor').attr("data-id");
    var ids=cancel_meeting.split('_');
    let cancelled = false
    swal({
        title: "Are you sure ??",
        text: "You will not be able to schedule meeting!", 
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
    .then((willDelete) => {
        if (willDelete) {
            $.ajax({
                url:  "/mentor/processreq/proc_cancel_meeting_by_mentor.php?meeting_id="+ids[0]+"&mentee_id="+ids[1],
                type: "GET",
                success: function(data) {
                    swal("Success", "Meeting Cancelled", "success")
                    .then(function() {
                        window.location.href =  url_path + 'mentor/my_appointment.php'
                    });
                    
                },
                error: function(data) {
                    swal("Oops", "Something Went Wrong", "error");
                }
                
            })
          } else {
            swal("Your Meeting status is pending!");
          }
    });
});
    /* Cancel mentor meeting */
    $(document).on('click','#cancelled_appointment',function (e) {
        e.preventDefault();
        var formData=$(this).serialize();
        $.ajax({
            type: 'POST',
            url: url_path + 'mentor/processreq/proc_cancel_meeting_by_mentor.php',
            data: formData,
            dataType: "json",
            success: function (response) {
                if (response.code) {
                    $("#alert_div_basic").addClass("alert alert-dansubger");
                    $("#alert_div_basic").show();
                    $("#alert_div_basic").html(response.msg);
                    $("#alert_div_basic").fadeOut(3000);
                } else {
                    $(window).scrollTop(0);
                    $("#alert_div_basic").addClass("alert alert-success");
                    $("#alert_div_basic").show();
                    $("#alert_div_basic").html(response.msg);
                    setTimeout(location.reload.bind(location), 3000);
                }
            },
            error: function (response) {
            
            }
        });
});
$(function() {
    $('.reschedule-confirm-from-mentee-button').click(function(e) {
        const id = $(this).attr('data-id')
        const input = $('#reschedule-confirm-from-mentee input[name="meeting_id"]')
        input.val(id)
    })

    $('.confirm-form-by-mentor-button').click(function(e) {
        id = $(this).attr('data-id')
        console.log(id);
        
        $(`#confirm-form-by-mentor-${id}`).submit()
    })
    $('.cancel-meeting').click(function(e) {
        id = $(this).attr('data-id')
        mentee = $(this).attr('data-mentee')
        console.log(id, mentee);

        const modal = $('#cancel-modal')
        $('#cancel-modal #cancel-meeting').val(id)
        $('#cancel-modal #cancel-mentee').val(mentee)
        
        // modal.show()
    })

    $('.new-time-slot').click(function(e) {
        id = $(this).attr('data-id')

        const modal = $('#reschedule-modal')
        $('#reschedule-modal #reschedule-meeting').val(id)
        var mentee_id = $('.cancel-meeting').attr('data-mentee');
        $('#res_mentee_id').val(mentee_id);
        
        // modal.show()
    })

    $('.select-the-slot-on-click').click(function(e) {
        
        const id = $(this).attr('data-id')
        const name = $(this).attr('name')
        const notSlot = $(this).attr('data-not-slot')
        
        $(this).removeClass('not-selected')
        $(`#slot${notSlot}[data-id="${id}"]`).addClass('not-selected')
        
        $(`#selected-slot-${id}`).val(name)
    })

    $('#mentor_education_form').hide();
    $('#mentor_experience_form').hide();
    $('.mentee-confirmed-form').click(function(e) {
        e.preventDefault();
        
        var formData = $(this).serialize();
        swal({
            title: "Are you sure?",   
            text: "Appointment will be confirmed",   
            icon: "warning",
            buttons: {
                cancel: {
                    text: "Cancel",
                    value: null,
                    visible: true,
                    className: "",
                    closeModal: true,
                  },
                  confirm: {
                    text: "Yes",
                    value: true,
                    visible: true,
                    className: "",
                    closeModal: true
                  }
            },
        })
        .then(function(result) {
            
            if (result) {
                swal({
                    title: 'Rescheduling',
                    closeOnEsc: true,

                    onOpen: () => {
                      swal.showLoading();
                    }
                }).then(function() {
                    location.reload();
                })

                $.ajax({
                    type: 'POST',
                    url: url_path + 'mentee/processreq/proc_confirm_meeting_by_mentee.php',
                    data: formData,
                    dataType: "json",
                })
                .done(function (response) {
                    // swal.close()
                    
                    // location.reload();
                    if (response.code) {
                        $("#alert_div_basic").addClass("alert alert-dansubger");
                        $("#alert_div_basic").show();
                        $("#alert_div_basic").html(response.msg);
                        $("#alert_div_basic").fadeOut(3000);
                    } else {
                        $(window).scrollTop(0);
                        $("#alert_div_basic").addClass("alert alert-success");
                        $("#alert_div_basic").show();
                        $("#alert_div_basic").html(response.msg);
                        // setTimeout(location.reload.bind(location), 3000);
                    }
                })
                .catch(function () {
                    swal("Oops", "Something Went Wrong", "error");
                })
            }
        })
        
    });

    $('#meeting_feedback').submit(function(e) {
        e.preventDefault();
        var formData = $(this).serialize();
        console.log(this.rating.value);
        
        swal({
            title: "Sending", 
            type: "success",
            onOpen: () => {
                swal.showLoading();
            }
        });
        $.ajax({
            type: 'POST',
            url: url_path + 'mentee/processreq/proc_meeting_feedback.php',
            data: formData,
            dataType: "json",
        })
        .done(function (response) {
            swal.close()
            if(response.code)
            {
                $("#alert_div_basic").addClass("alert alert-dansubger");
                $("#alert_div_basic").show();
                $("#alert_div_basic").html(response.msg);
                $("#alert_div_basic").fadeOut(3000);
            }
            else
            {
                swal({
                    title: "Received", 
                    text: "Feedback Sent",
                    type: "success",
                }).then(function () {
                    window.location.href = "../index.php"
                });
            }
        })
        .catch(function () {
            swal("Oops", "Something Went Wrong", "error");
        })
    });

    $('#mentor_bank_account').submit(function(e) {
        e.preventDefault();
        var formData = $(this).serialize();
        
        swal({
            title: "Seending", 
            icon: "success",
            onOpen: () => {
                swal.showLoading();
            }
        });
        $.ajax({
            type: 'POST',
            url: url_path + 'mentor/processreq/proc_save_bank_account.php',
            data: formData,
            dataType: "json",
        })
        .done(function (response) {
            swal.close()
            if(response.code)
            {
                $("#alert_div_basic").addClass("alert alert-dansubger");
                $("#alert_div_basic").show();
                $("#alert_div_basic").html(response.msg);
                $("#alert_div_basic").fadeOut(3000);
            }
            else
            {
                swal({
                    title: "Received", 
                    text: response.msg,
                    icon: "success"
                });
                setTimeout(function(){
                    location.href = url_path + 'mentor/edit_profile.php'
                }, 2000)
            }
        })
        .catch(function () {
            swal("Oops", "Something Went Wrong", "error");
        })
    });

    $('#mentor_education_form').submit(function(e) {
        e.preventDefault();
        var formData = $(this).serialize();
        
        swal({
            title: "Seending", 
            icon: "success",
            onOpen: () => {
                swal.showLoading();
            }
        });
        $.ajax({
            type: 'POST',
            url: url_path + 'mentor/processreq/proc_save_mentor_education.php',
            data: formData,
            dataType: "json",
        })
        .done(function (response) {
            swal.close()
            if(response.code)
            {
                $("#alert_div_basic").addClass("alert alert-dansubger");
                $("#alert_div_basic").show();
                $("#alert_div_basic").html(response.msg);
                $("#alert_div_basic").fadeOut(3000);
            }
            else
            {
                swal({
                    title: "Success", 
                    text: response.msg,
                    icon: "success"
                });
                setTimeout(function(){
                    location.href = url_path + 'mentor/edit_profile.php'
                }, 2000)
            }
        })
        .catch(function () {
            swal("Oops", "Something Went Wrong", "error");
        })
    });

    $('#mentor_experience_form').submit(function(e) {
        e.preventDefault();
        var formData = $(this).serialize();
        
        swal({
            title: "Seending", 
            icon: "success",
            onOpen: () => {
                swal.showLoading();
            }
        });
        $.ajax({
            type: 'POST',
            url: url_path + 'mentor/processreq/proc_save_mentor_experience.php',
            data: formData,
            dataType: "json",
        })
        .done(function (response) {
            swal.close()
            if(response.code)
            {
                $("#alert_div_basic").addClass("alert alert-dansubger");
                $("#alert_div_basic").show();
                $("#alert_div_basic").html(response.msg);
                $("#alert_div_basic").fadeOut(3000);
            }
            else
            {
                swal({
                    title: "Success", 
                    text: response.msg,
                    icon: "success"
                });
                setTimeout(function(){
                    location.href = url_path + 'mentor/edit_profile.php'
                }, 2000)
            }
        })
        .catch(function () {
            swal("Oops", "Something Went Wrong", "error");
        })
    });

    $('.edit-education').click(function () {
        $('#mentor_education_form').toggle();
    })

    $('.edit-experience').click(function () {
        $('#mentor_experience_form').toggle();
    })
    
    var current_step = 1;
    $('.step').click(function (e) {
        current_step = $(this).attr('data-index');
        stepperControl(e, $(this))
    });

    $('.bs-stepper-header .step').each(function(i, value) {
        if(i>0)
        {
            $(value).addClass('deactive');
        }
    })

    showHideElementsOfProceedToPay()

    function showHideElementsOfProceedToPay() {
        if (current_step == 1)
        {
            $('#proceed-to-pay .show-2').hide();
            $('#proceed-to-pay .show-3').hide();
            $('#proceed-to-pay .show-1').show();
            $('#proceed-to-pay .show-3').removeClass('checkout-detail');
        }
        if (current_step == 2)
        {
            $('#proceed-to-pay .show-1').hide();
            $('#proceed-to-pay .show-3').hide();
            $('#proceed-to-pay .show-2').show();
            $('#proceed-to-pay .show-3').removeClass('checkout-detail');
        }
        if (current_step == 3)
        {
            $('#proceed-to-pay .show-1').hide();
            $('#proceed-to-pay .show-2').hide();
            $('#proceed-to-pay .show-3').show();
            $('#proceed-to-pay .show-3').addClass('checkout-detail');
        }
    }

    function stepperControl(e, button) {

        $('.bs-stepper-header .step').each(function(i, value) {
            if(i * 2 > button.index())
            {
                $(value).addClass('deactive');
            }
            else
            {
                $(value).removeClass('deactive');
            }
        })
        showHideElementsOfProceedToPay()
        
	}

    $('.button-container .buttons .back').click(function() {

        if ($('.bs-stepper-header .step:not(.deactive)').length == 1)
        {
            history.back()
        }
        var deactive = $('.bs-stepper-header .step.deactive')
        
        if (deactive[0])
        {
            if(deactive[0].previousElementSibling.previousElementSibling.previousElementSibling)
            {
                deactive[0].previousElementSibling.previousElementSibling.previousElementSibling.previousElementSibling.click()
            }
            
        }
        else
        {
            $('.bs-stepper-header .step')[1].click()
        }        
        
    })
    $('.button-container .buttons .next').click(function() {
        if (current_step == 3)
        {
            $('.site-loader').show();

            setTimeout(() => $('#checkout-to-payment').submit(), 1000)
        }
        else
        {
            var deactive = $('.bs-stepper-header .step.deactive')
            deactive[0].click()
        }
    })

    $('#topics-modal').hide();
    $('.category-item .sub-categories .item').click(function (e) {
        const sub_id = $(this).attr('data-sub-id')
        const cat_id = $(this).attr('data-cat-id')
        const title = $(this).attr('data-title')
        $('#topics-modal .modal-container .sub-title').text(`Which kind of ${title} mentor do you need?`)
        $('#topics-modal').show();
        
        fetch(`/mentee/processreq/proc_get_topics_by_sub_category.php?id=${sub_id}`)
        .then(response => response.json())
        .then(data => {
            let topic_html = `
                
                    <input name="cat" value="${cat_id}" hidden>
                    <input name="subcat" value="${sub_id}" hidden>
                `;
            for (i = 0; i < data.topics.length; i++) {
                const topic = data.topics[i]
                topic_html += `
                    <label class="topics-select" for="topic_id${i}">
                        <input type="radio" required name="subject" id="topic_id${i}" value="${topic.id}">
                        ${topic.name}
                    </label>`
            }
            $('#topics-modal .body #specific-div').html(topic_html);

        })  
    })

    $('#topics-modal #next').click(function(e) {
        const activeDiv = $('.questions.active');
        const current = activeDiv.attr('data-current');
        if(current == 3) {
            
            if($('#submit_select_mentor input[name="subject"]:checked').val() && $('#submit_select_mentor input[name="starting_point"]:checked').val() && $('#submit_select_mentor input[name="goal"]:checked').val()) {
                $('#submit_select_mentor').submit()
            }
            return;
        }

        activeDiv.removeClass('active')
        activeDiv.addClass('prev')
        activeDiv.next().addClass('active')
    })
    $('#topics-modal #back').click(function(e) {
        const activeDiv = $('.questions.active');
        const current = activeDiv.attr('data-current');
        if(current == 1) {
            return;
        }

        activeDiv.removeClass('active')
        activeDiv.prev().removeClass('prev')
        activeDiv.prev().addClass('active')
    })

    $('#topics-modal').mouseup(function(e) 
    {
        var container = $('#topics-modal .modal-container');

        // if the target of the click isn't the container nor a descendant of the container
        if (!container.is(e.target) && container.has(e.target).length === 0) 
        {
            // container.hide();
            $('#topics-modal').hide()
        }
    });

    $('#find-join .join-purplelane').mouseenter(function (e) {
        $('#find-join .find-a-mentor').addClass('hover-join')
    })
    $('#find-join .join-purplelane').mouseleave(function (e) {
        $('#find-join .find-a-mentor').removeClass('hover-join')
    })
    
    
});

function createTopicsDiv() {
    
}

function getCurrentTime(input_time) {
    var stt = new Date("November 13, 2013 " + input_time);
	return stt.getTime();
}