$(document).ready(function () {
    $('#profile').submit(function (e) {
        e.preventDefault();
    }).validate({
        focusInvalid: true,
        invalidHandler: function (form, validator) {
            var errors = validator.numberOfInvalids();
            if (errors) {
                $("html, body").animate({
                    scrollTop: 0
                }, "fast");
            }
        },

        submitHandler: function (form) {
            var formData = new FormData(form);
            var button = $(form).find('button')
            var loading_class = 'm-loader m-loader--light m-loader--right'
            var button_text = $(button).text()
            $(button).html(`Processing`).addClass(loading_class).prop('disabled', true)
            $.ajax({
                type: 'POST',
                url: member_url_path + '/processreq/profile.php',
                data: formData,
                dataType: "json",
                cache: false,
                contentType: false,
                processData: false,
                success: function (response) {
                    $(button).prop('disabled', false).html(button_text).removeClass(loading_class)
                    if (response.code) {
                        swal('Error!', response.msg, 'error')
                    } else {
                        swal({
                            title: "Success!",
                            text: response.msg,
                            type: "success",
                        });
                    }
                },
                error: function (response) {
                    $(button).prop('disabled', false).html(button_text).removeClass(loading_class)
                    swal('Error!', "something went wrong", 'error')
                }
            });
        }
    })
});