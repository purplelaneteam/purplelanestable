// load_category();

function load_category(element) {
   
    var accordion = $(element).closest('#category_div')
    $(element).closest('.form-group').nextAll('.form-group').remove()
   //console.log(accordion);
    var element_val = $(element).val()
    if (element_val != '') {
        // $('#category_id').val($(element).val());
        $.ajax({
            type: 'post',
            url: "processreq/load_category_subcat.php",
            data: "cat_id=" + element_val,
            success: function (response) {
                if (response != "") {
                    $(accordion).append(response);
                     $('.category').select2({
                         placeholder: "Select Category",
                         allowClear: true
                     });
                }
            }
        });
    }
}

function activate_deactivate_mentor(status, mentor_id) {
    swal({
        title: "Are you sure you want to " + status + "?",
        type: "warning",
        showCancelButton: !0,
        confirmButtonText: "Yes"
    }).then(function (e) {
        if (e.value) {
            $.ajax({
                url: member_url_path + "/processreq/activate_deactivate_mentor.php",
                type: 'post',
                dataType: "json",
                data: "mentor_id=" + mentor_id + "&status=" + status,
                success: function (response) {
                    if (response.code) {
                        swal("Error!", response.msg, "error")
                    } else {
                        swal({
                            title: "Success!",
                            text: response.msg,
                            type: "success",
                        }).then(function () {
                            location.reload();
                        });
                    }
                }
            });
        }
    })
}

$(document).ready(function () {

    $('#add_setting_form').submit(function (e) {
        e.preventDefault();
    }).validate({
        focusInvalid: true,
        invalidHandler: function (form, validator) {
            var errors = validator.numberOfInvalids();
            if (errors) {
                $("html, body").animate({
                    scrollTop: 0
                }, "fast");
            }
        },

        submitHandler: function (form) {
            var formData = new FormData(form);
            var button = $(form).find('button')
            var loading_class = 'm-loader m-loader--light m-loader--right'
            var button_text = $(button).text()
            $(button).html(`Processing`).addClass(loading_class).prop('disabled', true)
            $.ajax({
                type: 'POST',
                url: member_url_path + '/processreq/proc_add_setting_form.php',
                data: formData,
                dataType: "json",
                cache: false,
                contentType: false,
                processData: false,
                success: function (response) {
                    $(button).prop('disabled', false).html(button_text).removeClass(loading_class)
                    if (response.code==1) {
                        swal('Error!', response.msg, 'error')
                    } else {
                        swal({
                            title: "Success!",
                            text: response.msg,
                            type: "success",
                        }).then(function () {
                            location.reload();
                        });
                    }
                },
                error: function (response) {
                    $(button).prop('disabled', false).html(button_text).removeClass(loading_class)
                    swal('Error!', response.msg, 'error');
                }
            });
        }
    });

    $('#add_category_form').submit(function (e) {
        e.preventDefault();
    }).validate({
        focusInvalid: true,
        invalidHandler: function (form, validator) {
            var errors = validator.numberOfInvalids();
            if (errors) {
                $("html, body").animate({
                    scrollTop: 0
                }, "fast");
            }
        },

        submitHandler: function (form) {
            var formData = new FormData(form);
            var button = $(form).find('button')
            var loading_class = 'm-loader m-loader--light m-loader--right'
            var button_text = $(button).text()
            $(button).html(`Processing`).addClass(loading_class).prop('disabled', true)
            $.ajax({
                type: 'POST',
                url: member_url_path + '/processreq/proc_manage_category.php',
                data: formData,
                dataType: "json",
                cache: false,
                contentType: false,
                processData: false,
                success: function (response) {
                    $(button).prop('disabled', false).html(button_text).removeClass(loading_class)
                    if (response.code) {
                        swal('Error!', response.msg, 'error')
                    } else {
                        swal({
                            title: "Success!",
                            text: response.msg,
                            type: "success",
                        }).then(function () {
                            window.location.href = "list_category.php";
                        });
                    }
                },
                error: function (response) {
                    $(button).prop('disabled', false).html(button_text).removeClass(loading_class)
                    swal('Error!', response.msg, 'error');
                }
            });
        }
    })

    $('#edit_category_form').submit(function (e) {
        e.preventDefault();
    }).validate({
        focusInvalid: true,
        invalidHandler: function (form, validator) {
            var errors = validator.numberOfInvalids();
            if (errors) {
                $("html, body").animate({
                    scrollTop: 0
                }, "fast");
            }
        },

        submitHandler: function (form) {
            var formData = new FormData(form);
            var button = $(form).find('button')
            var loading_class = 'm-loader m-loader--light m-loader--right'
            var button_text = $(button).text()
            $(button).html(`Processing`).addClass(loading_class).prop('disabled', true)
            $.ajax({
                type: 'POST',
                url: member_url_path + '/processreq/proc_manage_category.php',
                data: formData,
                dataType: "json",
                cache: false,
                contentType: false,
                processData: false,
                success: function (response) {
                    $(button).prop('disabled', false).html(button_text).removeClass(loading_class)
                    if (response.code) {
                        swal('Error!', response.msg, 'error')
                    } else {
                        swal({
                            title: "Success!",
                            text: response.msg,
                            type: "success",
                        }).then(function () {
                            // window.location.href = "list_category.php";
                        });
                    }
                },
                error: function (response) {
                    $(button).prop('disabled', false).html(button_text).removeClass(loading_class)
                    swal('Error!', response.msg, 'error');
                }
            });
        }
    })

    $('.delete').click(function () {
        var id=$(this).attr("data-id");
        var status=$(this).attr("data-status");
        var action="delete";
        $.ajax({
            type: 'POST',
            url: member_url_path + '/processreq/proc_manage_category.php',
            data: {id:id,status:status,action:action},
            dataType: "json",
            success: function (response) {
                if (response.code) {
                    swal('Error!', response.msg, 'error')
                } else {
                    swal({
                        title: "Success!",
                        text: response.msg,
                        type: "success",
                    }).then(function () {
                        window.location.href = "list_category.php";
                    });
                }
            },
            error: function (response) {
            
                swal('Error!', response.msg, 'error');
            }
        });
    });

    $('#mentor_form').submit(function (e) {
            e.preventDefault();
    }).validate({
        focusInvalid: true,
        invalidHandler: function (form, validator) {
            var errors = validator.numberOfInvalids();
            if (errors) {
                $("html, body").animate({
                    scrollTop: 0
                }, "fast");
            }
        },

        submitHandler: function (form) {
            var formData = new FormData(form);
            var button = $(form).find('button')
            var loading_class = 'm-loader m-loader--light m-loader--right'
            var button_text = $(button).text()
            $(button).html(`Processing`).addClass(loading_class).prop('disabled', true)
            $.ajax({
                type: 'POST',
                url: member_url_path + '/processreq/proc_add_mentor.php',
                data: formData,
                dataType: "json",
                cache: false,
                contentType: false,
                processData: false,
                success: function (response) {
                    $(button).prop('disabled', false).html(button_text).removeClass(loading_class)
                    if (response.code) {
                        swal('Error!', response.msg, 'error')
                    } else {
                        swal({
                            title: "Success!",
                            text: response.msg,
                            type: "success",
                        }).then(function () {
                            window.location.href = "list_mentor.php";
                        });
                    }
                },
                error: function (response) {
                    $(button).prop('disabled', false).html(button_text).removeClass(loading_class)
                    swal('Error!', response.msg, 'error');
                }
            });
        }
    })

    var dataTable = $('#mentee_transc_list').DataTable({
        "processing": true,
        "serverSide": true,
        "columnDefs": [
			{
				"targets": 4,
				"orderable": false,
				"searchable": false
			},
			{
				"targets": 5,
				"orderable": false,
				"searchable": false
			},
			{
				"targets": 6,
				"orderable": false,
				"searchable": false
			},
		],
        "scrollX": true,
        "order": [
            [10, "desc"]
        ],
        "ajax": {
            url: member_url_path + "/processreq/proc_list_mentee_trans.php", // json datasource
            type: "POST", // method  , by default get
            error: function () { // error handling
                $(".list_mentor_table_error").html("");
                $("#list_mentor_table").append('<tbody class="list_mentor_table_error"><tr><th colspan="10">No data found </th></tr></tbody>');
                $("#list_mentor_table").css("display", "none");

            }
        }
    });

    var dataTable = $('#admin_transc_list').DataTable({
        "processing": true,
        "serverSide": true,
        "columnDefs": [{
            "targets": 4,
            "orderable": false,
            "searchable": false
        }],
        "scrollX": true,
        "order": [
            [0, "asc"]
        ],
        "ajax": {
            url: member_url_path + "/processreq/proc_list_admin_trans.php", // json datasource
            type: "POST", // method  , by default get
            error: function () { // error handling
                $(".list_mentor_table_error").html("");
                $("#list_mentor_table").append('<tbody class="list_mentor_table_error"><tr><th colspan="10">No data found </th></tr></tbody>');
                $("#list_mentor_table").css("display", "none");

            }
        }
    });

    var dataTable = $('#mentor_transc_list').DataTable({
        "processing": true,
        "serverSide": true,
        "columnDefs": [],
        "scrollX": true,
        "order": [
            [5, "desc"]
        ],
        "ajax": {
            url: member_url_path + "/processreq/proc_list_mentor_trans.php", // json datasource
            type: "POST", // method  , by default get
            error: function () { // error handling
                $(".list_mentor_table_error").html("");
                $("#list_mentor_table").append('<tbody class="list_mentor_table_error"><tr><th colspan="10">No data found </th></tr></tbody>');
                $("#list_mentor_table").css("display", "none");

            }
        }
    });

    var dataTable = $('#taxes_transc_list').DataTable({
        "processing": true,
        "serverSide": true,
        "columnDefs": [{
            "targets": 4,
            "orderable": false,
            "searchable": false
        }],
        "scrollX": true,
        "order": [
            [0, "asc"]
        ],
        "ajax": {
            url: member_url_path + "/processreq/proc_list_taxes_trans.php", // json datasource
            type: "POST", // method  , by default get
            error: function () { // error handling
                $(".list_mentor_table_error").html("");
                $("#list_mentor_table").append('<tbody class="list_mentor_table_error"><tr><th colspan="10">No data found </th></tr></tbody>');
                $("#list_mentor_table").css("display", "none");

            }
        }
    });

    var dataTable = $('#order_trans_list').DataTable({
        "processing": true,
        "serverSide": true,
        "columnDefs": [
			{
				"targets": 4,
				"orderable": false,
				"searchable": false
			},
			{
				"targets": 5,
				"orderable": false,
				"searchable": false
			},
		],
        "scrollX": true,
        "order": [
            [7, "desc"]
        ],
        "ajax": {
            url: member_url_path + "/processreq/proc_list_orders_trans.php", // json datasource
            type: "POST", // method  , by default get
            error: function () { // error handling
                $(".list_mentor_table_error").html("");
                $("#list_mentor_table").append('<tbody class="list_mentor_table_error"><tr><th colspan="10">No data found </th></tr></tbody>');
                $("#list_mentor_table").css("display", "none");

            }
        }
    });

   var status=$('#status').val();
   

    var DatatablesSearchOptionsAdvancedSearch = function () {
        $.fn.dataTable.Api.register("column().title()", function () {
            return $(this.header()).text().trim()
        });
        return {
            init: function () {
                var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                var a;
                if (status === 'pending' || status === 'completed' || status === 'scheduled' || status === 'rescheduled')
                {
                    var columnsDef = ["Actions", "id", "mentor", "mentee", "topic", "mentee_slots", "mentor_slots","status",]
                    var columns = [
                        {
                            data: "Actions"
                        },
                        {
                            data: "id"
                        },
                        {
                            data: "mentor"
                        },
                        {
                            data: "mentee"
                        },
                        {
                            data: "topic"
                        },
                        {
                            data: "mentee_slots"
                        },
                        {
                            data: "mentor_slots"
                        },
                        {
                            data: "status"
                        },
                    ];
                    var columnDefs= [
                        {
                            targets: 0,
                            title: "Actions",
                            orderable: !1,
                            searchable: false
                        },
                        {
                            targets: 5,
                            title: "Mentee Slots",
                            orderable: !1,
                            searchable: false
                        },
                        {
                            targets: 6,
                            title: "Mentor Slots",
                            orderable: !1,
                            searchable: false
                        },
                        {
                            targets: 7,
                            title: "Status",
                            orderable: !1,
                            searchable: false
                        },
                    ];
                }
                else
                {
                    var columnsDef = ["id", "mentor", "mentee", "topic", "mentee_slots", "mentor_slots", "status",]
                    var columns = [
                        {
                            data: "id"
                        },
                        {
                            data: "mentor"
                        },
                        {
                            data: "mentee"
                        },
                        {
                            data: "topic"
                        },
                        {
                            data: "mentee_slots"
                        },
                        {
                            data: "mentor_slots"
                        },
                        {
                            data: "status"
                        },
                    ];
                    var columnDefs=[
                        {
                            targets: 4,
                            title: "Mentee Slots",
                            orderable: !1,
                            searchable: false
                        },
                        {
                            targets: 5,
                            title: "Mentor Slots",
                            orderable: !1,
                            searchable: false
                        },
                        {
                            targets: 6,
                            title: "Status",
                            orderable: !1,
                            searchable: false
                        },
                    ]
                }
                a = $("#meeting_list").DataTable({
                    responsive: !0,
                    dom: "<'row'<'col-sm-12'tr>>\n\t\t\t<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>",
                    lengthMenu: [5, 10, 25, 50],
                    pageLength: 10,
					scrollX: "100%",
                    language: {
                        lengthMenu: "Display _MENU_"
					},
                    searchDelay: 500,
                    processing: !0,
                    serverSide: !0,
                    ajax: {
                        headers: {
                            'X-CSRF-TOKEN': CSRF_TOKEN
                        },
                        url: member_url_path + "/processreq/proc_list_meeting.php",
                        type: "POST",
                        data: {
                            status,
                            columnsDef,
                        }
                    },
    
                    columns,
                   
                    initComplete: function () {
    
                    },
                    columnDefs,
                    order: [
                        [1, "asc"]
                    ]
                }), $("#m_search").on("click", function (t) {
                    t.preventDefault();
                    var e = {};
                    $(".m-input").each(function () {
                        var a = $(this).data("col-index");
                        e[a] ? e[a] += "|" + $(this).val() : e[a] = $(this).val()
                    }), $.each(e, function (t, e) {
                        a.column(t).search(e || "", !1, !1)
                    }), a.table().draw()
                }), $("#m_reset").on("click", function (t) {
                    t.preventDefault(), $(".m-input").each(function () {
                        $(this).val(""), a.column($(this).data("col-index")).search("", !1, !1)
                    }), a.table().draw()
                }), $("#m_datepicker").datepicker({
                    todayHighlight: !0,
                    templates: {
                        leftArrow: '<i class="la la-angle-left"></i>',
                        rightArrow: '<i class="la la-angle-right"></i>'
                    }
                })
            }
        }
    }();
    jQuery(document).ready(function () {
        DatatablesSearchOptionsAdvancedSearch.init()
    });
    

    var dataTable = $('#list_mentor_table').DataTable({
        "processing": true,
        "serverSide": true,
        "columnDefs": [{
            "targets": 8,
            "orderable": false,
            "searchable": false
        }],
        "scrollX": true,
        "order": [
            [0, "asc"]
        ],
        "ajax": {
            url: member_url_path + "/processreq/proc_list_mentor.php", // json datasource
            type: "POST", // method  , by default get
            error: function () { // error handling
                $(".list_mentor_table_error").html("");
                $("#list_mentor_table").append('<tbody class="list_mentor_table_error"><tr><th colspan="10">No data found </th></tr></tbody>');
                $("#list_mentor_table").css("display", "none");

            }
        }
    });
});

$('.topic_delete').click(function () {
    var id=$(this).attr("data-id");
    var status=$(this).attr("data-status");
    var action="delete";
    $.ajax({
        type: 'POST',
        url: member_url_path + '/processreq/proc_manage_topic.php',
        data: {id:id,status:status,action:action},
        dataType: "json",
        success: function (response) {
            if (response.code) {
                swal('Error!', response.msg, 'error')
            } else {
                swal({
                    title: "Success!",
                    text: response.msg,
                    type: "success",
                }).then(function () {
                    window.location.href = "list_topic.php";
                });
            }
        },
        error: function (response) {
        
            swal('Error!', response.msg, 'error');
        }
    });
});


$('#add_edit_topic_form').submit(function (e) {
    e.preventDefault();
}).validate({
    focusInvalid: true,
    invalidHandler: function (form, validator) {
        var errors = validator.numberOfInvalids();
        if (errors) {
            $("html, body").animate({
                scrollTop: 0
            }, "fast");
        }
    },

    submitHandler: function (form) {
        var formData = new FormData(form);
        var button = $(form).find('button')
        var loading_class = 'm-loader m-loader--light m-loader--right'
        var button_text = $(button).text()
        $(button).html(`Processing`).addClass(loading_class).prop('disabled', true)
        $.ajax({
            type: 'POST',
            url: member_url_path + '/processreq/proc_manage_topic.php',
            data: formData,
            dataType: "json",
            cache: false,
            contentType: false,
            processData: false,
            success: function (response) {
                $(button).prop('disabled', false).html(button_text).removeClass(loading_class)
                if (response.code) {
                    swal('Error!', response.msg, 'error')
                } else {
                    swal({
                        title: "Success!",
                        text: response.msg,
                        type: "success",
                    }).then(function () {
                        window.location.href = "list_topic.php";
                    });
                }
            },
            error: function (response) {
                $(button).prop('disabled', false).html(button_text).removeClass(loading_class)
                swal('Error!', response.msg, 'error');
            }
        });
    }

    
})

$('.category_delete').click(function () {
        var id = $(this).attr("data-id");
        var status = $(this).attr("data-status");
        var action = "delete";
        $.ajax({
        type: 'POST',
        url: member_url_path + '/processreq/proc_manage_category.php',
        data: {
        id: id,
        status: status,
        action: action
        },
        dataType: "json",
        success: function (response) {
            if (response.code) {
                swal('Error!', response.msg, 'error')
            } else {
                swal({
                    title: "Success!",
                    text: response.msg,
                    type: "success",
                }).then(function () {
                    window.location.href = "list_category.php";
                });
            }
        }
    });
});

$(document).on('submit','#reschedule_appointment_form',function (e) {
    e.preventDefault();
    const typeOfAppointment = $('#reschedule_type').val()
    var formData=$(this).serialize();
    swal({
        title: "Are you sure?",   
        text: "Amount will be distributed!",   
        type: "warning",
        showCancelButton: true,   
        confirmButtonColor: "#DD6B55",   
        confirmButtonText: "Yes",
    })
    .then(function(result) {
        swal({
            title: 'Rescheduling',
            allowEscapeKey: false,
            allowOutsideClick: false,
            
            onOpen: () => {
              swal.showLoading();
            }
        })

        $.ajax({
            type: 'POST',
            url: url_path + '/members/admin/processreq/proc_reschedule_by_admin.php',
            data: formData,
            dataType: "json",
            success: function (response) {
                if (response.code) {
                    swal({
                        title: "Success!",
                        text: response.msg,
                        type: "success",
                    }).then(function () {
                        if(typeOfAppointment) {
                            location.href = "/members/admin/list_meeting.php?status=rescheduled";
                        }
                        else {
                            location.href = "/members/admin/list_meeting.php?status=pending";
                        }
                    });
                    // $("#alert_div_basic").addClass("alert alert-dansubger");
                    // $("#alert_div_basic").show();
                    // $("#alert_div_basic").html(response.msg);
                    // $("#alert_div_basic").fadeOut(3000);
                } else {
                    $(window).scrollTop(0);
                    swal({
                        title: "Done", 
                        text: response.msg, 
                        type: "success"
                    });
                    if(typeOfAppointment) {
                        location.href = "/members/admin/list_meeting.php?status=rescheduled";
                    }
                    else {
                        location.href = "/members/admin/list_meeting.php?status=pending";
                    }
                }
            },
            error: function (response) {
                swal("Oops", "Something Went Wrong", "error");
            }
        });
    })
    
});

$(document).on('click','.distribute_profits',function (e) {
    e.preventDefault();
    var meeting_id = $(this).attr('data-meeting_id');

    swal({
        title: "Are you sure?",   
        text: "Amount will be distributed!",   
        type: "warning",
        showCancelButton: true,   
        confirmButtonColor: "#DD6B55",   
        confirmButtonText: "Yes",
    })
    .then(function(result) {
        
        if (result.value)
        {
            swal({
                title: 'Waiting...',
                allowEscapeKey: false,
                allowOutsideClick: false,
                
                onOpen: () => {
                  swal.showLoading();
                }
            })
            $.ajax({
                url:  url_path + "members/admin/processreq/proc_distribute_profit.php",
                type: "POST",
                data: {meeting_id:meeting_id},
                dataType: "json",
            })
            .done(function(data) {
                swal.close()
                swal({
                    title: "Deleted", 
                    text: "Meeting Cancelled successfully", 
                    type: "success"
                });
                location.reload();
            })
            .catch(function(data) {
                swal("Oops", "Something Went Wrong", "error");
            });
        }
    })
});


$(document).on('click','.complete_appointment',function (e) {
    e.preventDefault();
    var meeting_id = $(this).attr('data-meeting_id');

    swal({
        title: "Are you sure?",   
        text: "Meeting was completed.",   
        type: "warning",
        showCancelButton: true,   
        confirmButtonColor: "#DD6B55",   
        confirmButtonText: "Yes",
    })
    .then(function(result) {
        
        if (result.value)
        {
            swal({
                title: 'Completing',
                allowEscapeKey: false,
                allowOutsideClick: false,
                
                onOpen: () => {
                  swal.showLoading();
                }
            })
            $.ajax({
                url:  url_path + "members/admin/processreq/proc_complete_appointment.php",
                type: "POST",
                data: {meeting_id},
                dataType: "json",
            })
            .done(function(response) {
                swal.close()
                swal({
                    title: "Done", 
                    text: response.msg, 
                    type: "success"
                });
                location.reload();
            })
            .catch(function() {
                swal("Oops", "Something Went Wrong", "error");
            });
        }
    })
});

$(document).on('click','#cancel_appointment',function (e) {
    e.preventDefault();
    var formData=$(this).serialize();

    swal({
        title: "Are you sure?",   
        text: "Appointment will be cancelled",   
        type: "warning",
        showCancelButton: true,   
        confirmButtonColor: "#DD6B55",   
        confirmButtonText: "Yes",
    })
    .then(function(result) {
        
        if (result.value)
        {
            swal({
                title: 'Cancelling',
                allowEscapeKey: false,
                allowOutsideClick: false,
                
                onOpen: () => {
                  swal.showLoading();
                }
            })
            $.ajax({
                url:  url_path + "members/admin/processreq/proc_cancel_meeting_by_admin.php",
                type: "POST",
                data: formData,
                dataType: "json",
            })
            .done(function(data) {
                swal.close()
                swal({
                    title: "Deleted", 
                    text: "Meeting Cancelled successfully", 
                    type: "success"
                });
                location.reload();
            })
            .catch(function(data) {
                swal("Oops", "Something Went Wrong", "error");
            });
        }
    })
});

$(function() {
    /* Initial */
    minute = moment().minute()
    if (minute >= 30) minute = 30
    else minute = 0;
    const tomorrow = moment().add(1, 'days').minute(minute)
    
    $('#picker1').dateTimePicker({selectDate: tomorrow, startDate: tomorrow, minuteIncrement: 30});;
    $('#picker2').dateTimePicker({selectDate: tomorrow, startDate: tomorrow, minuteIncrement: 30});
   

    $('#slot1').val(moment().format("YYYY-MM-DD HH:mm"))
    $('#slot2').val(moment().format("YYYY-MM-DD HH:mm"))

    $('#mentor_bank_account_admin').submit(function(e) {
        e.preventDefault();
        mentor_id = this.mentor_id.value
        var formData = $(this).serialize();
        
        swal({
            title: "Seending", 
            icon: "success",
            onOpen: () => {
                swal.showLoading();
            }
        });
        $.ajax({
            type: 'POST',
            url: url_path + 'members/admin/processreq/proc_save_mentor_bank_details.php',
            data: formData,
            dataType: "json",
        })
        .done(function (response) {
            swal.close()
            if(response.code)
            {
                $("#alert_div_basic").addClass("alert alert-dansubger");
                $("#alert_div_basic").show();
                $("#alert_div_basic").html(response.msg);
                $("#alert_div_basic").fadeOut(3000);
            }
            else
            {
                swal({
                    title: "Success", 
                    text: response.msg,
                    icon: "success"
                });
                setTimeout(function(){
                    location.href = url_path + `members/admin/edit_mentor.php?mentor_id=${mentor_id}`
                }, 2000)
            }
        })
        .catch(function () {
            swal("Oops", "Something Went Wrong", "error");
        })
    });

    $('#mentor_education_admin').submit(function(e) {
        e.preventDefault();
        mentor_id = this.mentor_id.value
        var formData = $(this).serialize();
        
        swal({
            title: "Seending", 
            icon: "success",
            onOpen: () => {
                swal.showLoading();
            }
        });
        $.ajax({
            type: 'POST',
            url: url_path + 'members/admin/processreq/proc_save_mentor_education.php',
            data: formData,
            dataType: "json",
        })
        .done(function (response) {
            swal.close()
            if(response.code)
            {
                $("#alert_div_basic").addClass("alert alert-dansubger");
                $("#alert_div_basic").show();
                $("#alert_div_basic").html(response.msg);
                $("#alert_div_basic").fadeOut(3000);
            }
            else
            {
                swal({
                    title: "Success", 
                    text: response.msg,
                    icon: "success"
                });
                setTimeout(function(){
                    location.href = url_path + `members/admin/edit_mentor.php?mentor_id=${mentor_id}`
                }, 2000)
            }
        })
        .catch(function () {
            swal("Oops", "Something Went Wrong", "error");
        })
    });
    $('#mentor_experience_admin').submit(function(e) {
        e.preventDefault();
        mentor_id = this.mentor_id.value
        var formData = $(this).serialize();
        
        swal({
            title: "Seending", 
            icon: "success",
            onOpen: () => {
                swal.showLoading();
            }
        });
        $.ajax({
            type: 'POST',
            url: url_path + 'members/admin/processreq/proc_save_mentor_experience.php',
            data: formData,
            dataType: "json",
        })
        .done(function (response) {
            swal.close()
            if(response.code)
            {
                $("#alert_div_basic").addClass("alert alert-dansubger");
                $("#alert_div_basic").show();
                $("#alert_div_basic").html(response.msg);
                $("#alert_div_basic").fadeOut(3000);
            }
            else
            {
                swal({
                    title: "Success", 
                    text: response.msg,
                    icon: "success"
                });
                setTimeout(function(){
                    location.href = url_path + `members/admin/edit_mentor.php?mentor_id=${mentor_id}`
                }, 2000)
            }
        })
        .catch(function () {
            swal("Oops", "Something Went Wrong", "error");
        })
    });

    $('#hangout-link-submit').click(function(e) {
        e.preventDefault();
        var formData= $('#set_hangout_link').serialize();

        swal({
            title: 'Cancelling',
            allowEscapeKey: false,
            allowOutsideClick: false,
            
            onOpen: () => {
                swal.showLoading();
            }
        })
        $.ajax({
            url:  url_path + "members/admin/processreq/proc_set_hangout_link.php",
            type: "POST",
            data: formData,
            dataType: "json",
        })
        .done(function(data) {
            swal.close()
            swal({
                title: "Success", 
                text: "Hangout Link saved successfully", 
                type: "success"
            });
            location.reload();
        })
        .catch(function(data) {
            swal("Oops", "Something Went Wrong", "error");
        });

    })

    $(document).on('click','.insert-hangout-link',function (e) {
        e.preventDefault();
        var formData=$(this).serialize();
        $('#hangout-link-modal').show()
        const meeting_id = $(this).attr('data-meeting_id')
        $('#meeting_id_for_link').val(meeting_id)

        const link = $(this).attr('title')
        $('#hangout-link').val(link)
    });
    
})
