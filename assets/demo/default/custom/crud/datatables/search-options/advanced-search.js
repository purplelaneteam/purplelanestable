var DatatablesSearchOptionsAdvancedSearch = function () {
    $.fn.dataTable.Api.register("column().title()", function () {
        return $(this.header()).text().trim()
    });
    return {
        init: function () {
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            var a;
            a = $("#m_table_1").DataTable({
                responsive: !0,
                dom: "<'row'<'col-sm-12'tr>>\n\t\t\t<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>",
                lengthMenu: [5, 10, 25, 50],
                pageLength: 10,
                language: {
                    lengthMenu: "Display _MENU_"
                },
                searchDelay: 500,
                processing: !0,
                serverSide: !0,
                ajax: {
                    headers: {
                        'X-CSRF-TOKEN': CSRF_TOKEN
                    },
                    url: member_url_path + "/admin/member/get-users",
                    type: "POST",
                    data: {
                        columnsDef: ["name", "email", "phone_number", "Actions"]
                    }
                },
                columns: [{
                    data: "name"
                }, {
                    data: "email"
                }, {
                    data: "phone_number"
                }, {
                    data: "Actions"
                }],
                initComplete: function () {
                    
                },
                columnDefs: [{
                    targets: -1,
                    title: "Actions",
                    orderable: !1,
                    searchable: false
                }]
            }), $("#m_search").on("click", function (t) {
                t.preventDefault();
                var e = {};
                $(".m-input").each(function () {
                    var a = $(this).data("col-index");
                    e[a] ? e[a] += "|" + $(this).val() : e[a] = $(this).val()
                }), $.each(e, function (t, e) {
                    a.column(t).search(e || "", !1, !1)
                }), a.table().draw()
            }), $("#m_reset").on("click", function (t) {
                t.preventDefault(), $(".m-input").each(function () {
                    $(this).val(""), a.column($(this).data("col-index")).search("", !1, !1)
                }), a.table().draw()
            }), $("#m_datepicker").datepicker({
                todayHighlight: !0,
                templates: {
                    leftArrow: '<i class="la la-angle-left"></i>',
                    rightArrow: '<i class="la la-angle-right"></i>'
                }
            })
        }
    }
}();
jQuery(document).ready(function () {
    DatatablesSearchOptionsAdvancedSearch.init()
});