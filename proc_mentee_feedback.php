<?php
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: POST');
header('Access-Control-Allow-Headers: Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods, Authorization, X-Requested-With');

require_once("includes/initialize.php");

$sql = "START TRANSACTION";
    $result = mysqli_query($con, $sql);
    if(!$result)
    {
        $error = "Something went wrong. Please try again later.";
        $_SESSION['error'] = $error;
    }

$request_data = json_decode(file_get_contents("php://input"));

if((isset($_SESSION['feedback_id'])) && (isset($_SESSION['participant_id'])))
    {
        $feedback_id = $_SESSION['feedback_id'];
        $participant_id = $_SESSION['participant_id'];

        $sql = "SELECT feedback_id,participant_id,participant_type,meetings_id 
        FROM feedback_participants WHERE feedback_id = '".$feedback_id."' AND participant_id =  '".$participant_id."' AND status = 'inprog'";
        $result = mysqli_query($con, $sql);
        if(!($result && $pcpn_row = mysqli_fetch_array($result)))
        {
            $error = "Invalud Request or Feedback already completed.";
            $_SESSION['error'] = $error;
            exit();
        }
    }
$feedback_data =  $request_data->data;
// read survey data and insert rows
foreach($feedback_data as $question => $answer) 
{
// $tt = $tt . 'Your key is: '.$question.' and the value of the key is:'.$answer;
$sql_mentee_insert = "INSERT INTO participants_responses SET meetings_id = $request_data->meetings_id, participant_id = $participant_id, feedback_id=$feedback_id, question='$question', answer='$answer'";

    $result_insert = mysqli_query($con, $sql_mentee_insert);
    if(!$result_insert) 
    {   
        $error = "Something went wrong while adding mentee responses. Please try again later.";
        ajax_error($error);
    }
}

// chagne the status to compl 
$sql_update = "UPDATE feedback_participants set status='comp' where feedback_id=$feedback_id";
mysqli_query($con, $sql_update);

$sql = "COMMIT";
    $result = mysqli_query($con, $sql);
    if(!$result)
    {
        $error = "commit: Something went wrong while adding mentee responses. Please try again later.";
        ajax_error($error);
    }
    else
    {
        $jsonarray["code"] = 0;
        $jsonarray["msg"] = "Feedback Responses Stored Successfully.";
        echo json_encode($jsonarray);
    }
?>