<?php
require 'includes/initialize.php';


$error = $_SESSION['error'];
$success = $_SESSION['success'];

if ($success) {
    unset($_SESSION['success']);
}
if ($error) {
    unset($_SESSION['error']);
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <title>Contact Us | PurpleLane</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <?php include 'head.php' ?>
</head>
<style>
    nav.navbar.navbar-expand-lg.navbar-togglable.navbar-dark.navbarall {
        background: inherit !important;
        background-color: #2a0653 !important;
    }
</style>


<body>
    <?php
    include 'header.php';
    ?>
    <div class="container">
        
        <div class="new_contact_us">
            <?php if ($error || $success) : ?>
            <div class="message">
                <div style="" id="alert_div_basic"
                    class="alert alert-<?php echo ($error) ? 'danger' : 'success' ?> alert-dismissible fade show"
                    role="alert">
                    <p><?php echo ($error) ? $error : $success ?></p>
                    <button style="margin-top: -30px;" type="button" class="close" data-dismiss="alert"
                        aria-label="Close">
                        <span aria-hidden="true" style="font-size: 31px;line-height: 0;">&times;</span>
                    </button>
                </div>
            </div>
            <?php endif; ?>
            <h4 class="text-center">Contact Us</h4>
            <form action="processreq/proc_contact_us.php" method="POST">
                <ul class="clearfix">
                    <li>
                        <div class="input-group">
                            <label for="contact_name">Name</label>
                            <input type="text" name="name" id="contact_name" required />
                        </div>
                    </li>
                    <li>
                        <div class="input-group">
                            <label for="contact_email">Email</label>
                            <input type="email" name="email" id="contact_email" required />
                        </div>
                    </li>
                    <li class="placeholdder">
                        <div class="input-group">
                            <label for="message">Message</label>
                            <textarea id="message" name="message" placeholder="Your message is here..." ></textarea>
                        </div>
                    </li>
                </ul>
                <div class="text-center">
                    <button class="submit">Submit</button>
                </div>
            </form>
        </div>
    </div>
    </div>
</body>


<!-- Forgot Password End-->
<!-- foooter started -->
<?php
include("footer.php");
include("footer_tags.php");
?>