<?php
include('includes/initialize.php');

    $mobile="";
    $profile_pic="";
    $fname="";
    $lname="";
    if(isset($_SESSION['id']) && isset($_SESSION['type']))
    {
     if($_SESSION['type']=="mentee")
     {
         $sql="SELECT mobile,profile_pic FROM mentee WHERE id='".$_SESSION['id']."' AND is_active='1'";
         $result=mysqli_query($con,$sql);
        
         if(mysqli_num_rows($result)>0)
         {
             $myrow=mysqli_fetch_array($result);
             $mobile=$myrow['mobile'];
             $profile_pic=SITE_URL."/".MENTEE_IMAGE_UPLOAD_FOLDER."/".$myrow['profile_pic'];
             $fname=$_SESSION['fname'];
             $lname=$_SESSION['lname'];
         }

     }   
    }

?>
<!DOCTYPE html>
<html lang="en">


<head>
    <title>Purple Lane</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta name="author" content="" />
   
    <?php include 'head.php' ?>
<style>
.nav{
   
display: block;
    flex-wrap: wrap;
    padding-left: 0;
    margin-bottom: 0;
    list-style: none;
}


</style>

  <link rel="stylesheet" type="text/css" href="css/jquery.datetimepicker.css"/>
   
<script>
$(document).ready(function() {
    $("#show_hide_password a").on('click', function(event) {
        event.preventDefault();
        if($('#show_hide_password input').attr("type") == "text"){
            $('#show_hide_password input').attr('type', 'password');
            $('#show_hide_password i').addClass( "fa-eye-slash" );
            $('#show_hide_password i').removeClass( "fa-eye" );
        }else if($('#show_hide_password input').attr("type") == "password"){
            $('#show_hide_password input').attr('type', 'text');
            $('#show_hide_password i').removeClass( "fa-eye-slash" );
            $('#show_hide_password i').addClass( "fa-eye" );
        }
    });
});
</script>
	
</head>
<body data-spy="scroll" data-target=".navbar" data-offset="82">
    <div class="site-loader">
        <!---======Preloader===========-->
        <div class="loader-dots">
            <div class="circle circle-1"></div>
            <div class="circle circle-2"></div>
        </div>
    </div>
    <div class="site__layer"></div>
    <?php include 'header1.php' ?>
	<section class="burger app" id="app">
        <div class="container">
            <!-- container-start -->
			
			<div class="container">
				<div class="row">
					<div class="col-md-12 text-center" style="margin-top:40px">
						
						<div class="tab-content" id="nav-tabContent">
						   
							  
						  <div class="tab-pane fade show active" id="pop2" role="tabpanel" aria-labelledby="pop2-tab">
							   
								<!-- Choose a Mentor start -->
								
								
								<div class="row">
									
									<div class="col-md-12 col-sm-12  ">
									   
										<div class="store-btns">
											
												<div class=" review-box">
													
													<div class="tabs">
													
													
													
			<div class="container">
				<div class="row">
				
				    <div class="col-md-3">
					    <nav class="nav-justified ">
						  <div class="nav nav-tabs " id="nav-tab" role="tablist">
							<a class="nav-item nav-link active" id="pop1-tab" data-toggle="tab" href="#pop1" role="tab" aria-controls="pop1" aria-selected="true"> <b>Basic Information</b></a>
							
							<a class="nav-item nav-link" id="pop2a-tab" data-toggle="tab" href="#pop2a" role="tab" aria-controls="pop2a" aria-selected="false">  <b>Engagement Preferences</b></a>
							
							<a class="nav-item nav-link" id="pop3-tab" data-toggle="tab" href="#pop3" role="tab" aria-controls="pop3" aria-selected="false">  <b>Bank Account Details</b></a>
							
							<a class="nav-item nav-link" id="pop4-tab" data-toggle="tab" href="#pop4" role="tab" aria-controls="pop4" aria-selected="false">  <b>Change password</b></a>
						  </div>
						</nav>
					</div>
				
					<div class="col-md-9" >
						
						<div class="tab-content" >
						    
						    
						    <div class="tab-pane fade show active" id="pop1" role="tabpanel" aria-labelledby="pop1-tab">
							  
							    <p>
							        <div class="card border-primary rounded-0">
                            <div class="card-header p-0">
                                <div class="bg-info text-white text-center py-2">
                                    <h6> Basic Information 
									<span class="edt"><i class="fa fa-pencil-square"></i></span>
									</h6>
                                   
                                </div>
                            </div>
                            <div class="card-body p-3">

                                <!--Body-->
                                <div class="form-group">
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text"><i class="fa fa-user text-info"></i></div>
                                        </div>
                                        <input type="text" class="form-control" id="nombre" name="nombre" placeholder="User Name" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text"><i class="fa fa-envelope text-info"></i></div>
                                        </div>
                                        <input type="email" class="form-control" id="nombre" name="email" placeholder="Email" required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text"><i class="fa fa-phone text-info"></i></div>
                                        </div>
                                        <input type="text" class="form-control" id="nombre" name="text" placeholder="Phone" required>
                                    </div>
                                </div>

                              <a href="#" class="btn btn-alpha mr-lg-3 mr-2 cnect"> Save Changes <i class="fa fa-angle-right ml-3"></i></a>
                            </div>

                        </div>
                   
							    </p>
							
							</div>
							
							<div class="tab-pane fade" id="pop2a" role="tabpanel" aria-labelledby="pop2a-tab">
							   
							    <div class="row">
							   
									<div class="col-md-5">
								   
										 
									<p>
										   
										<div class="card border-primary rounded-0">
											<div class="card-header p-0">
									<div class="bg-info text-white text-center py-2">
										<h6> Availability
										<span class="edt"><i class="fa fa-pencil-square"></i></span>
										</h6>
									   
									</div>
								</div>
											<div class="card-body p-3">

									<!--Body-->
									
 <div id="demo2"></div>

								</div>

										</div>
									</p>
								  
									</div>
								   
							   
								   <div class="col-md-7">
								   
									<p>
										   
										<div class="card border-primary rounded-0">
											<div class="card-header p-0">
									<div class="bg-info text-white text-center py-2">
										<h6> Price Range
										<span class="edt"><i class="fa fa-pencil-square"></i></span>
										</h6>
									   
									</div>
								</div>
											<div class="card-body p-3">

									<!--Body-->
									<div class="form-group">
									<lable>Set call rate( Hourly)</lable>
										<div class="input-group mb-2">
											<div class="input-group-prepend">
												<div class="input-group-text"><i class="fa fa-inr text-info"></i></div>
											</div>
											<input type="text" class="form-control" id="nombre" name="nombre" placeholder="Hourly" required>
											
											
										</div>
									</div>
									<span>Once the password is changed, you'll be redirected to the login page.</span>

									<a href="#" class="btn btn-alpha mr-lg-3 mr-2 cnect"> Save Changes <i class="fa fa-angle-right ml-3"></i></a>
								</div>

										</div>
									</p>
								  
								  </div>
							  </div>

						    </div>
							
							<div class="tab-pane fade" id="pop3" role="tabpanel" aria-labelledby="pop3-tab">
							   
								<p>
							           
                                    <div class="card border-primary rounded-0">
                                        <div class="card-header p-0">
                                <div class="bg-info text-white text-center py-2">
                                    <h6> Bank Account Details 
									<span class="edt"><i class="fa fa-pencil-square"></i></span>
									</h6>
                                   
                                </div>
                            </div>
                                        <div class="card-body p-3">

                                <!--Body-->
                                <div class="form-group">
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text"><i class="fa fa-user text-info"></i></div>
                                        </div>
                                        <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Bank Name" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text"><i class="fa fa-check-square text-info"></i></div>
                                        </div>
                                        <input type="text" class="form-control" id="nombre" name="email" placeholder="Account Number" required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text"><i class="fa fa-check-square text-info"></i></div>
                                        </div>
                                        <input type="text" class="form-control" id="nombre" name="text" placeholder="IFS Code" required>
                                    </div>
                                </div>

                                <a href="#" class="btn btn-alpha mr-lg-3 mr-2 cnect"> Save Changes <i class="fa fa-angle-right ml-3"></i></a>
                            </div>

                                    </div>
                                </p>
							  
						    </div>
							
							<div class="tab-pane fade" id="pop4" role="tabpanel" aria-labelledby="pop4-tab">
							  
								<p>
							           
                                    <div class="card border-primary rounded-0">
                            <div class="card-header p-0">
                                <div class="bg-info text-white text-center py-2">
                                    <h6> Change Password
									<span class="edt"><i class="fa fa-pencil-square"></i></span>
									</h6>
                                   
                                </div>
                            </div>
                            <div class="card-body p-3">

                                <!--Body-->
                                <div class="form-group">
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text"><i class="fa fa-lock text-info"></i></div>
                                        </div>
                                        <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Current Password" required>
										<div class="input-group-addon">
										     <a href=""><i class="fa fa-eye-slash" aria-hidden="true"></i></a>
									    </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text"><i class="fa fa-key text-info"></i></div>
                                        </div>
                                        <input type="text" class="form-control" id="nombre" name="email" placeholder="New Password" required> 
										<div class="input-group-addon">
										     <a href=""><i class="fa fa-eye-slash" aria-hidden="true"></i></a>
									    </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text"><i class="fa fa-key text-info"></i></div>
                                        </div>
                                        <input type="text" class="form-control" id="nombre" name="text" placeholder="Retype New Password" required> <div class="input-group-addon">
        <a href=""><i class="fa fa-eye-slash" aria-hidden="true"></i></a>
      </div>
                                    </div>
									
                                </div>

                                <a href="#" class="btn btn-alpha mr-lg-3 mr-2 cnect"> Save Changes <i class="fa fa-angle-right ml-3"></i></a>
                            </div>

                        </div>
                   
							    </p>
							  
						    </div>
						  
						</div>
					
					</div>
				
				    </div>
				
				</div>
            </div>
           
          
			
													
													
													</div>
												</div>
												 <!---/col-->
								        </div>
										   <!-- /row  -->
									</div>
											  
											  
									<div class="tab-pane fade" id="pop3" role="tabpanel" aria-labelledby="pop3-tab">
											<div class="pt-3"></div>
											<p>3. There's another way to do this for layouts that doesn't have to put the navbar inside the container, and which doesn't require any CSS or Bootstrap overrides.

											Simply place a div with the Bootstrap container class around the navbar. This will center the links inside the navbar:
												  
									</div>
											  
								</div>
					
					</div>
				
				    </div>
				
				</div>
            </div>
           
          
			
			
        </div>
        <!-- container end -->
    </section>
	
    <footer class="pb-2 footer" id="footer">
        <div class="container">
            <!-- container-start -->
            <div class="row text-white text-center burger">
                <div class="col-lg-10 offset-lg-1">
                    <div class="newsletter mb-5">
                        <h3 class="mb-3">Don't miss out, Stay updated</h3>
                        <form class="form form-newsletter input-group w-50 mx-auto position-relative" method="post" action="">
                            <div id="mail-messages" class="notification subscribe"></div>
                            <input class="form-control mr-md-2 mr-1" placeholder="Enter@email.com" type="email" name="subscribe_email">
                            <button type="submit" class="input-group-append btn btn-alpha " name="button">Subscribe <i class="fa fa-angle-right ml-3"></i></button>
                        </form>
                    </div>
                    
                </div>
                <!---/column-->
            </div>
            <!---/row-->
			<div class="card-body footer-card-body">
                <div class="col-lg-12 col-12 col-sm-12 sm-offset-2">
						
				<div class="row">
					<div class="col-sm-6 col-lg-3 res-margin-xs">
						<h6 class="foot-head">About Purplelane</h6>
						<div class="spce"></div>
						<a href=""><p>Mentorship is a relationship in which a more experienced or <br/><br/>more knowledgeable person helps to guide a less experienced or less knowledgeable person.
							</p></a>
						<div class="spce"></div>
						
					</div>
					<div class="col-sm-6 col-lg-3 col-sm-offset-1 res-margin-xs">
						<h6 class="foot-head">Our Services</h6>
						<div class="spce"></div>
						<ul>
							<li class="ser-list"><a href="#">Mobile App Development</a></li>
							<li class="ser-list"><a href="#">Web App Development</a></li>
							<li class="ser-list"><a href="#">UI/UX Design</a></li>
							<li class="ser-list"><a href="#">Offshore Development</a></li>
							<li class="ser-list"><a href="#">Software Development</a></li>
						</ul>
					</div>
					<div class="col-sm-6 col-lg-3 col-sm-offset-1">
						<h6 class="foot-head">Main Menu</h6>
						<div class="spce"></div>
						<ul>
							<li class="ser-list"><a href="#">General Help</a></li>
							<li class="ser-list"><a href="#">Contact</a></li>
							<li class="ser-list"><a href="#">Portfolio</a></li>
							<li class="ser-list"><a href="#">Terms &amp; Conditions</a></li>
							<li class="ser-list"><a href="#">Privacy &amp; Policy</a></li>
						</ul>
					</div>
					<div class="col-sm-6 col-lg-3 res-margin-xs">
						<h6 class="foot-head">Contact Us</h6>
						<div class="spce"></div>
						<a href=""><p>102, Alekya bliss, Camelot layout,
							Botanical gardens, Kondapur,
							Hyderabad Telangana, India-500084.
							<br/><br/>
							<i class="fa fa-phone-square" aria-hidden="true"></i> +91 99999 99999.<br/>
							<i class="fa fa-envelope-o" aria-hidden="true"></i> Info@Purplelane.com
							</p></a>
						<div class="spce"></div>
						<div class="social-holder">
							<a href="#"><i class="fa fa-facebook social-icon"></i></a>
							<a href="#"><i class="fa fa-twitter social-icon"></i></a>
							<a href="#"><i class="fa fa-linkedin social-icon"></i></a>
							<a href="#"><i class="fa fa-google social-icon"></i></a>
						</div>
					</div>
				</div>
											
                </div>
                <!---/col-->
                                          
            </div>
			<div class="container">
				
		</div>
			
			
            <div class="row text-white text-center">
                <div class="copyright col-lg-12">
				
                    <p class="">  Copyright © 2018 <a href="" target="_blank" class="alt-color">Purplelane</a>. All rights reserved</p>
                </div>
            </div>
            <!---/row-->
        </div>
        <!---/container-->
    </footer>
  
  
  
  
    <script type="text/javascript" src="js/jquery-1.11.1.js"></script>
    <script type="text/javascript" src="js/jquery.datetimepicker.js"></script>
  
    <script type="text/javascript">
        $(document).ready(function(){
            function logEvent(type, date) {
                $("<div class='log__entry'/>").hide().html("<strong>"+type + "</strong>: "+date).prependTo($('#eventlog')).show(200);
            }
            $('#clearlog').click(function() {
                $('#eventlog').html('');
            });

            $('#demo2').datetimepicker({
                date: new Date(),
                viewMode: 'YMD',
                onDateChange: function(){
                    $('#date-text2').text(this.getText());
                    $('#date-text-ymd2').text(this.getText('YYYY-MM-DD'));
                    $('#date-value2').text(this.getValue());
                }
            });
           
        });
    </script>

    <!-- <script src="js/jquery3.2.1.min.js"></script>-->
    <!-- JQUERY LIBRARY -->
    <script src="js/particles.min.js"></script>
    <!-- Particles JS -->
    <script src="js/bootstrap.min.js"></script>
    <!-- Bootstrap JS -->
    <script src="js/main.js"></script>
    <!-- Main JS -->
    
</body>

</html>