<?php
    require_once "../includes/initialize.php";

    $email = sanitize_input($_REQUEST['email']);
    $password = sanitize_input($_REQUEST['password']);
    $login_types = ['mentee', 'mentor'];

    if(!$email || !$password)
    {
        $error = "Please enter email and password";
        ajax_error($error);
    }

    foreach ($login_types as $key => $login_type) {
        $sql_login = "SELECT id, fname, lname, email FROM  $login_type WHERE email = '".$email."' AND password = '".md5($password)."' AND is_active = 1";
        $result_login = mysqli_query($con,$sql_login); 
        if(!$result_login)
        {
            $error = "Something went wrong";
            ajax_error($error);exit;
        }
        $result_count = mysqli_num_rows($result_login);
        if($result_count > 0)
        {
            if($myrow_login = mysqli_fetch_array($result_login))
            {
                $id = $myrow_login['id'];
                $fname = $myrow_login['fname'];
                $lname = $myrow_login['lname'];
                $email = $myrow_login['email'];
                $_SESSION['id'] = $id;
                $_SESSION['fname'] = $fname;
                $_SESSION['lname'] = $lname;
                $_SESSION['email'] = $email;
                $_SESSION['type'] = $login_type;

                $jsonarray["code"] = 0;
                $jsonarray["msg"] = "";
                $jsonarray["type"] = $login_type;
                echo json_encode($jsonarray);exit;
            }
        }
        else 
        {
            if ($key + 1 < count($login_types))
            {
                continue;
            }
            $error = "Invalid login details";
            ajax_error($error);exit;
        }
    }

    
    
?>