<?php
    require_once "../includes/initialize.php";
    require_once "../includes/class.phpmailer.php";
    require_once "../includes/class.smtp.php";

    $jsonarray = array();
    $fname ="";
    $lname ="";
    $email ="";
    $password = "";
    $mobile = "";
    $user_type = "";
    $name="";
    $register_type="";
    $google_id="";
    $profile_pic="";
    $addedon=date("Y-m-d H:i:s");
    $verify_token="";

    /* Google Login Api Start */
    class GoogleLoginApi
    {
        public function GetAccessToken($client_id, $redirect_uri, $client_secret, $code) {	
            $url = 'https://www.googleapis.com/oauth2/v4/token';			
            
            $curlPost = 'client_id=' . $client_id . '&redirect_uri=' . $redirect_uri . '&client_secret=' . $client_secret . '&code='. $code . '&grant_type=authorization_code';
            $ch = curl_init();		
            curl_setopt($ch, CURLOPT_URL, $url);		
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);		
            curl_setopt($ch, CURLOPT_POST, 1);		
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $curlPost);	
            $data = json_decode(curl_exec($ch), true);
            $http_code = curl_getinfo($ch,CURLINFO_HTTP_CODE);		
            if($http_code != 200) 
                throw new Exception('Error : Failed to receieve access token');
                
            return $data;
        }

        public function GetUserProfileInfo($access_token) {	
            $url = 'https://www.googleapis.com/oauth2/v2/userinfo?fields=name,email,gender,id,picture,verified_email';			
            
            $ch = curl_init();		
            curl_setopt($ch, CURLOPT_URL, $url);		
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: Bearer '. $access_token));
            $data = json_decode(curl_exec($ch), true);
            $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);		
            if($http_code != 200) 
                throw new Exception('Error : Failed to get user information');
                
            return $data;
        }
    }
    function randomString($length =10) {
        $str = "";
        $characters = array_merge(range('A','Z'), range('a','z'), range('0','9'));
        $max = count($characters) - 1;
        for ($i = 0; $i < $length; $i++) {
            $rand = mt_rand(0, $max);
            $str .= $characters[$rand];
        }
        return $str;
    }
    /* Google Login Api End*/
    if(isset($_POST) && count($_POST)>0)
    {
        $fname = mysqli_real_escape_string($con, $_REQUEST['firstname']);
        $lname = mysqli_real_escape_string($con, $_REQUEST['lastname']);
        $email = mysqli_real_escape_string($con, $_REQUEST['email']);
        $password = mysqli_real_escape_string($con, $_REQUEST['password']);
        $mobile = mysqli_real_escape_string($con, $_REQUEST['mobile']);
        $user_type = 'mentee';
        $name=ucwords($fname." ".$lname);
        $register_type="manual";
        $profile_pic="mentee.jpg";
        $verify_token=randomString($length = 6);
    }
    if(isset($_GET['code'])) 
    {
        try {
            $gapi = new GoogleLoginApi();
            
            // Get the access token 
            $data = $gapi->GetAccessToken(CLIENT_ID, CLIENT_REDIRECT_URL, CLIENT_SECRET, $_GET['code']);
            
            // Get user information
            $user_info = $gapi->GetUserProfileInfo($data['access_token']);
        }
        catch(Exception $e) {
            echo $e->getMessage();
            exit();
        }
        $email =$user_info['email'];
        $password = $user_info['email'];
        $mobile = '';
        $name= (explode(" ",$user_info['name']));
        $fname =$name[0];
        $lname =$name[1];
        $user_type = "mentee";
        $register_type="google";
        $google_id=$user_info['id'];
        $profile_pic="mentee_".rand().".jpg";
        copy($user_info['picture'], $_SERVER["DOCUMENT_ROOT"]."/".MENTEE_IMAGE_UPLOAD_FOLDER."/".$profile_pic);

    }
    if (filter_var($email, FILTER_VALIDATE_EMAIL)) 
    {
        $sql = "SELECT email FROM mentor WHERE email = '$email' LIMIT 1";
        $result = mysqli_query($con, $sql);
        if (mysqli_num_rows($result) > 0)
        {
            // dd($result);
            $jsonarray["code"] = 0;
            $jsonarray["msg"] =  "You are signed up as a mentor Please sign in to continue";
            // echo json_encode($jsonarray);exit;

            $_SESSION['error'] = "You are signed up as a mentor Please sign in to continue";
            header('Location:'.SITE_URL);exit;
        }

        if($register_type=="manual")
        {
            if($name=="" || $email=="" || $mobile=="" || $password=="")
            {
                $jsonarray["code"] = 0;
                $jsonarray["msg"] = "All Field Mandatory.";
                echo json_encode($jsonarray);exit;
            }
            else 
            {
                $sql = "SELECT id FROM mentee WHERE email='".$email."'";
                $result = mysqli_query($con, $sql);
                if(mysqli_num_rows($result)>0)
                {
                    $jsonarray["code"] = 0;
                    $jsonarray["msg"] = "Email Already Exists.";
                    echo json_encode($jsonarray);exit;
                }
                else 
                {
                    $sql_insert_mentee="INSERT INTO mentee(fname,lname, email, mobile, profile_pic, password, register_type, is_active,verify_token, addedon) VALUES ('".$fname."','".$lname."','".$email."','".$mobile."','mentee.jpg',md5('".$password."'),'".$register_type."','0','".$verify_token."','".$addedon."')";
                    
                    $result_insert_mentee = mysqli_query($con, $sql_insert_mentee);

                    if($result_insert_mentee)
                    {
                        $user_id = mysqli_insert_id($con);
                        $email_subject = "Verify Email Address";
                        $verification_link = EMAIL_VERIFICATION_LINK."?purplelaneid=".$user_id."&purplelanetoken=".$verify_token;
                        $mailbody = "
                            <head>
                                <title></title>
                                <meta http-equiv='X-UA-Compatible' content='IE=edge'>
                                <meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>
                                <style type='text/css'>
                                #outlook a { padding: 0; }
                                .ReadMsgBody { width: 100%; }
                                .ExternalClass { width: 100%; }
                                .ExternalClass * { line-height:100%; }
                                body { margin: 0; padding: 0; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; }
                                table, td { border-collapse:collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; }
                                img { border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; }
                                p { display: block; margin: 13px 0; }
                                </style>
                                <style type='text/css'>
                                @media only screen and (max-width:480px) {
                                    @-ms-viewport { width:320px; }
                                    @viewport { width:320px; }
                                }
                                </style>
                            
                                <link href='https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700' rel='stylesheet' type='text/css'>
                                <style type='text/css'>
                            
                                    @import url(https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700);
                            
                                </style>
                                <style type='text/css'>
                                @media only screen and (min-width:480px) {
                                    .mj-column-per-100, * [aria-labelledby='mj-column-per-100'] { width:100%!important; }
                                }
                                </style>
                            </head>
                            <body style='background: #F9F9F9;'>
                                <div style='background-color:#F9F9F9;'>
                                <style type='text/css'>
                                    html, body, * {
                                    -webkit-text-size-adjust: none;
                                    text-size-adjust: none;
                                    }
                                    a {
                                    color:#1EB0F4;
                                    text-decoration:none;
                                    }
                                    a:hover {
                                    text-decoration:underline;
                                    }
                                </style>
                                <div style='margin:0px auto;max-width:640px;background:transparent;'><table role='presentation' cellpadding='0' cellspacing='0' style='font-size:0px;width:100%;background:transparent;' align='center' border='0'><tbody><tr><td style='text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:40px 0px;'><div aria-labelledby='mj-column-per-100' class='mj-column-per-100 outlook-group-fix' style='vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;'><table role='presentation' cellpadding='0' cellspacing='0' width='100%' border='0'><tbody><tr><td style='word-break:break-word;font-size:0px;padding:0px;' align='center'><table role='presentation' cellpadding='0' cellspacing='0' style='border-collapse:collapse;border-spacing:0px;' align='center' border='0'><tbody><tr><td style='width:138px;'><a href=".SITE_URL." target='_blank'><img alt='' title='' height='38px' src=".SITE_URL."/images/logo.png style='border:none;border-radius:;display:block;outline:none;text-decoration:none;width:100%;height:38px;' width='138'></a></td></tr></tbody></table></td></tr></tbody></table></div></td></tr></tbody></table></div><div style='max-width:640px;margin:0 auto;box-shadow:0px 1px 5px rgba(0,0,0,0.1);border-radius:4px;overflow:hidden'><div style='margin:0px auto;max-width:640px;background:#87238c url(".SITE_URL."/images/email_back.png) top center / cover no-repeat;'><table role='presentation' cellpadding='0' cellspacing='0' style='font-size:0px;width:100%;background:#87238c url(".SITE_URL."/images/email_back.png) top center / cover no-repeat;' align='center' border='0' background=".SITE_URL."/images/email_back.png><tbody><tr><td style='text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:57px;'><div style='cursor:auto;color:white;font-family:Whitney, Helvetica Neue, Helvetica, Arial, Lucida Grande, sans-serif;font-size:36px;font-weight:600;line-height:36px;text-align:center;'>Welcome to PurperLane</div></td></tr></tbody></table></div><div style='margin:0px auto;max-width:640px;background:#ffffff;'><table role='presentation' cellpadding='0' cellspacing='0' style='font-size:0px;width:100%;background:#ffffff;' align='center' border='0'><tbody><tr><td style='text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:40px 70px;'><div aria-labelledby='mj-column-per-100' class='mj-column-per-100 outlook-group-fix' style='vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;'><table role='presentation' cellpadding='0' cellspacing='0' width='100%' border='0'><tbody><tr><td style='word-break:break-word;font-size:0px;padding:0px 0px 20px;' align='left'><div style='cursor:auto;color:#737F8D;font-family:Whitney, Helvetica Neue, Helvetica, Arial, Lucida Grande, sans-serif;font-size:16px;line-height:24px;text-align:left;'>
                                <p><img src='https://purplelane.in/images/pl-logo.png' alt='Party Wumpus' title='None' width='150' height='150' style='height: auto;'></p>
                                <h2 style='font-family: Whitney, Helvetica Neue, Helvetica, Arial, Lucida Grande, sans-serif;font-weight: 500;font-size: 20px;color: #4F545C;letter-spacing: 0.27px;'>Hi ".$name.",</h2>
                                <p>Thanks for registering with us. Learn and succeed faster with 1-on-1 mentoring from an expert. </p>
                                <p>Before Accessing Your Acount, we'll need to verify your email.</p>
                                </div></td></tr><tr><td style='word-break:break-word;font-size:0px;padding:10px 25px;' align='center'><table role='presentation' cellpadding='0' cellspacing='0' style='border-collapse:separate;' align='center' border='0'><tbody><tr><td style='border:none;border-radius:3px;color:white;cursor:auto;padding:15px 19px;' align='center' valign='middle' bgcolor='#87238c'><a href='".$verification_link."' style='text-decoration:none;line-height:100%;background:#87238c;color:white;font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:15px;font-weight:normal;text-transform:none;margin:0px;' target='_blank'>
                                Verify Email
                                </a></td></tr>
                                 
                                 <tr>
                                    <td>
                                        <p> Please check out <a href='https://purplelane.in/guidelines_for_mentees.php' style='color:#1EB0F4;text-decoration:none;' target='_blank'>Guidelines For Mentees</a></p> 
                                    </td>
                                </tr>
                                
                                </tbody></table></td></tr></tbody></table></div>
                                        
                                </td></tr></tbody></table></div>
                                </div><div style='margin:0px auto;max-width:640px;background:transparent;'><table role='presentation' cellpadding='0' cellspacing='0' style='font-size:0px;width:100%;background:transparent;' align='center' border='0'><tbody><tr><td style='text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:0px;'>
                                    
                                <div aria-labelledby='mj-column-per-100' class='mj-column-per-100 outlook-group-fix' style='vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;'><table role='presentation' cellpadding='0' cellspacing='0' width='100%' border='0'><tbody><tr><td style='word-break:break-word;font-size:0px;'><div style='font-size:1px;line-height:12px;'>&nbsp;</div></td></tr></tbody></table></div>
                                </td></tr></tbody></table></div>
                                    
                                <div style='margin:0 auto;max-width:640px;background:#ffffff;box-shadow:0px 1px 5px rgba(0,0,0,0.1);border-radius:4px;overflow:hidden;'><table cellpadding='0' cellspacing='0' style='font-size:0px;width:100%;background:#ffffff;' align='center' border='0'><tbody><tr><td style='text-align:center;vertical-align:top;font-size:0px;padding:0px;'>
                                </td></tr></tbody></table></div>
                                    
                                <div style='margin:0px auto;max-width:640px;background:transparent;'><table role='presentation' cellpadding='0' cellspacing='0' style='font-size:0px;width:100%;background:transparent;' align='center' border='0'><tbody><tr><td style='text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:20px 0px;'>
                                    
                                <div aria-labelledby='mj-column-per-100' class='mj-column-per-100 outlook-group-fix' style='vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;'><table role='presentation' cellpadding='0' cellspacing='0' width='100%' border='0'><tbody><tr><td style='word-break:break-word;font-size:0px;padding:0px;' align='center'><div style='cursor:auto;color:#99AAB5;font-family:Whitney, Helvetica Neue, Helvetica, Arial, Lucida Grande, sans-serif;font-size:12px;line-height:24px;text-align:center;'>
                                • <a href=".SITE_URL." style='color:#1EB0F4;text-decoration:none;' target='_blank'>Sent by PurperLane</a> 
                              
                                </div></td></tr></tbody></table></div></div>
                            </body>
                            ";
                        $send_mail = send_mail($email_subject, $mailbody, $email);
                        if($send_mail)
                        {
                            $jsonarray["code"] = 1;
                            $jsonarray["msg"] = "You Have Register Successfully. Please Verify Your Email To Activate Your Account";
                            echo json_encode($jsonarray);exit;
                        }
                        else
                        {
                            $jsonarray["code"] = 0;
                            $jsonarray["msg"] = "Something went wrong. Please try again later.";
                            echo json_encode($jsonarray);exit;
                        }                           
                    } 
                    else 
                    {
                        $jsonarray["code"] = 0;
                        $jsonarray["msg"] =  "Something went wrong. Please try again later.";
                        echo json_encode($jsonarray);exit;
                    }
                }
            }
        }
        if($register_type=="google")
        {

            if($name=="" || $email=="" )
            {
                $error = "All Field Mandatory.";
                ajax_error($error);exit;
            }
            else 
            {
                $sql = "SELECT id,profile_pic,fname,lname,email FROM mentee WHERE email='".$email."'";
                $result = mysqli_query($con, $sql);
                if(mysqli_num_rows($result)>0)
                {
                    $sql_update="UPDATE mentee SET fname='".$fname."',lname='".$lname."',profile_pic='".$profile_pic."' WHERE email='".$email."'";
                    $result_update = mysqli_query($con, $sql_update);
                    if($result_update)
                    {
                        $myrow=mysqli_fetch_array($result);
                        unlink($_SERVER["DOCUMENT_ROOT"]."/".MENTEE_IMAGE_UPLOAD_FOLDER."/".$myrow['profile_pic']);

                        $_SESSION['id']    =  $myrow['id'];
                        $_SESSION['fname'] = $myrow['fname'];
                        $_SESSION['lname'] = $myrow['lname'];
                        $_SESSION['email'] = $myrow['email'];
                        $_SESSION['type']  = "mentee";
                        $jsonarray["code"] = 1;
                        $jsonarray["msg"] = "Register Successfully .";
                        header('Location:'.SITE_URL.'/mentee/my_appointments.php?');

                    }
                }
                else 
                {
                    $sql_insert_mentee="INSERT INTO mentee(fname,lname, email, profile_pic, register_type, is_active, addedon) VALUES ('$fname','$lname','$email','$profile_pic','$register_type','1','$addedon')";
                    $result_insert_mentee = mysqli_query($con, $sql_insert_mentee);
                    if($result)
                    {
                        $user_id = mysqli_insert_id($con);
                        $_SESSION['id']    =  $myrow['user_id'];
                        $_SESSION['fname'] = $fname;
                        $_SESSION['lname'] = $lname;
                        $_SESSION['email'] = $email;
                        $_SESSION['type']  = "mentee";
                        $jsonarray["code"] =1;
                        $jsonarray["msg"] = "Register Successfully .";
                        header('Location:'.SITE_URL.'/mentee/my_appointments.php?');
                    } 
                    else 
                    {
                        $error = "Something went wrong. Please try again later.";
                        ajax_error($error);exit;
                    }
                }
            }
        }
        
    } else {
        $jsonarray["code"] = 0;
        $jsonarray["msg"] =  "Please enter valid data.";
        echo json_encode($jsonarray);exit;
    }
?>