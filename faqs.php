<?php 
  require 'includes/initialize.php';
?>
<!DOCTYPE html>
<html lang="en">


<head>
    <title>Frequently Asked Questions | PurpleLane</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <?php include 'head.php' ?>
	
	<style>
    nav.navbar.navbar-expand-lg.navbar-togglable.navbar-dark.navbarall{
      background: inherit !important;
      background-color: #2a0653 !important;
    }

.accordion a {
  position: relative;
  display: -webkit-box;
  display: -webkit-flex;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-orient: vertical;
  -webkit-box-direction: normal;
  -webkit-flex-direction: column;
  -ms-flex-direction: column;
  flex-direction: column;
  width: 100%;
  padding: 1rem 3rem 1rem 1rem;
  color: #7288a2;
  font-size: 1.15rem;
  font-weight: 400;
  border-bottom: 1px solid #e5e5e5;
}

.accordion a:hover,
.accordion a:hover::after {
  cursor: pointer;
  color: #2c0754;
}

.accordion a:hover::after {
  border: 1px solid #2c0754;
}

.accordion a.active {
  color: #2c0754;
  border-bottom: 1px solid #2c0754;
}

.accordion a::after {
  font-family: 'FontAwesome';
  content: '\f067';
  position: absolute;
  float: right;
  right: 1rem;
  font-size: 1rem;
  color: #7288a2;
  padding: 4px;
  width: 30px;
  height: 30px;
  -webkit-border-radius: 50%;
  -moz-border-radius: 50%;
  border-radius: 50%;
  border: 1px solid #7288a2;
  text-align: center;
}

.accordion a.active::after {
  font-family: 'FontAwesome';
  content: '\f068';
  color: #2c0754;
  border: 1px solid #2c0754;
}

.accordion .content {
  opacity: 0;
  padding: 0 1rem;
  max-height: 0;
  border-bottom: 1px solid #e5e5e5;
  overflow: hidden;
  clear: both;
  -webkit-transition: all 0.2s ease 0.15s;
  -o-transition: all 0.2s ease 0.15s;
  transition: all 0.2s ease 0.15s;
}

.accordion .content p {
  font-size: 1rem;
  font-weight: 300;
}

.accordion .content.active {
  opacity: 1;
  padding: 1rem;
  max-height: 100%;
  -webkit-transition: all 0.35s ease 0.15s;
  -o-transition: all 0.35s ease 0.15s;
  transition: all 0.35s ease 0.15s;
}

.alt-color {
    color: #491066;
}
	</style>
	

	
</head>
<body data-spy="scroll" data-target=".navbar" data-offset="82">
    <div class="site-loader">
        <!---======Preloader===========-->
        <div class="loader-dots">
            <div class="circle circle-1"></div>
            <div class="circle circle-2"></div>
        </div>
    </div>
    <div class="site__layer"></div>
    <?php $link = '/'; ?>
    <?php include 'header.php' ?>
	
	
   
	<section class="burger2 app" id="app">
        <div class="container">
            <!-- container-start -->
			<div class="row">
									<div class="col-lg-12 col-sm-12">
										
										<h5 class="section-heading">Frequently Asked Questions</h5>
									
									<p class="spcev">
									We have complied a list of questions frequently asked by our customers, If there is a question that hasn’t been fully answered here, please check the Contact Us page, or send an email to <a href="mailto:team@purplelane.in" class="alt-color">team@purplelane.in</a>

									</p></div>
						<!-- <div class="col-lg-3 col-sm-12 hidden-xs hidden-sm"> -->
							<!-- <label class="pull-right"> -->
							    <!-- <div class="d-flex justify-content-center h-100"> -->
									<!-- <div class="searchbar"> -->
									    <!-- <input class="search_input" type="text" name="" placeholder="Search..."> -->
									    <!-- <a href="#" class="search_icon"><i class="fa fa-search"></i></a> -->
									<!-- </div> -->
								<!-- </div> -->
							<!-- </label> -->
						<!-- </div> -->
									
								</div>
			<div class="container">
				<div class="row">
				
				    <div class="container">

 
  <div class="accordion">
    <div class="accordion-item">
      <a>What is Purplelane?</a>
      <div class="content">
        <p>PurpleLane is an
		online platform for students/professionals to connect with experts
		to seek guidance and boost their career and life.</p>
      </div>
    </div>
    <div class="accordion-item">
      <a>How can PurpleLane help me?</a>
      <div class="content">
        <p>PurpleLane helps
		you to choose a mentor from a large pool of mentors with a variety
		of backgrounds. We handpick the
		mentors who would add value to our mentees in a way that enables
		the mentee to choose the right path, which would lead them towards
		their goal.</p>
      </div>
    </div>
    <div class="accordion-item">
      <a>How PurpleLane works?</a>
      <div class="content">
        <p><uL>
		<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in">Find a perfect
		mentor</P>
		<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in">Schedule a meeting</P>
		<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in">Get mentorship</P>
	</OL></p>
      </div>
    </div>
    
    <div class="accordion-item">
      <a>Any tips for finding the perfect mentor who can help me boost my career?</a>
      <div class="content">
      <P>Ask yourself - what are you
		looking for help with respect to specific aspects of your career or
		life, whether is is a current challenge or a future goal?</P> <br/>
    
    <P>You can have more than one mentor
		– one to help you with the current challenges and one to help
		guide you for the long term direction of your career – but make
		sure you understand how each mentor is able to help you.</P> 
    <br/>
    <OL>
	<UL>
		<LI><P STYLE="margin-bottom: 0in">Keep in mind these questions when
		picking your mentor</P>
		<UL>
			<LI><P STYLE="margin-bottom: 0in">Do you need support for specific
			challenges you are facing now, personally or in your role or
			career?</P>
			<LI><P STYLE="margin-bottom: 0in">Are you looking to develop your
			own strengths, skills and experience for the next five years?</P>
			<LI><P STYLE="margin-bottom: 0in">Are you trying to take your
			career or leadership to the next level?</P> 
		</UL> <br/>
		<LI><P STYLE="margin-bottom: 0in">Once you have an idea of what
		sort of guidance and advice you need – whether it is for a
		current challenge, or with a future goal in mind, it is important
		to start asking the right questions about your mentor. Start with:</P> 
		<UL>
			<LI><P STYLE="margin-bottom: 0in">What is your mentor’s career
			experience?</P>
			<LI><P STYLE="margin-bottom: 0in">Have they done what you are
			trying to do?</P>
			<LI><P STYLE="margin-bottom: 0in">Does your mentor have the
			experience and skillset to provide guidance on how to tackle your
			existing problem or achieve your goal?</P>
			<LI><P STYLE="margin-bottom: 0in">Can your mentor help position
			you for the next career step? Do they have the experience and
			network to open doors for you?</P>
		</UL>
	</UL>
</OL>
<OL>
<br/> 
	<UL>
		<LI><P STYLE="margin-bottom: 0in">And don’t forget – you can
		learn a lot from a mentor – not just off their successes, but
		also from where they had difficulties – as the mentor who faced
		the hurdles and experienced difficulties was able to share with the
		mentee how to avoid common pitfalls and what to focus on.</P>
	</UL>
</OL>

      </div>
    </div>
    
    <div class="accordion-item">
      <a>Okay, now that I have found a mentor. How to schedule a meeting with him?</a>
      <div class="content">
        <p><UL>
		<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in">Mentee Proposes:
		After choosing a mentor, you need to propose <I><U>two</U></I>
		slots where each slot is for a duration of 3 hours.</P>
		<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in">Mentor Accepts:
		From the proposed slots, mentor selects a half-an-hour slot in
		which the meeting takes place and accepts your meeting request. 
		</P>
		<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in">You will be
		communicated with the details of meeting once mentor accepts your
		request.</P>
	</OL></p>
      </div>
    </div>
    
    <div class="accordion-item">
      <a>How to get the best out of mentorship relationship?</a>
      <div class="content">
        <p>Follow the guidelines given <a style="all: initial;" href="guidelines_for_mentees.php">here</a></p>
      </div>
    </div>
    
   
	 <div class="accordion-item">
      <a>What type of
	guidance would I be provided?</a>
      <div class="content">
        <p>
        <UL>
		<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in">If you have any
		questions regarding your career opportunities, 
		</P>
		<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in">For example,</P>
		<OL TYPE=i>
			<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in">You may want to
			pursue PG or PhD and want to prepare for the same. We show you a
			list of appropriate mentors who can help you in clearing the
			admission test, interviews, etc. We provide an option to set up
			mock interviews with the mentors.</P>
			<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in">You are currently
			pursuing B.Tech and have taken the GRE/TOEFL for getting admission
			in MS abroad. But you’re not sure about the colleges you need to
			apply. PurpleLane enables you to connect with a mentor who’s
			pursuing the same degree in that university (preferably the mentor
			can be from the mentee’s home town/district/state).</P>
			<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in">You just finished
			your 12th class and want to join a graduate school. 
			</P>
			<OL>
				<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in">You can find
				mentors who can show some light on each degree and the colleges,
				their faculty, fee structure, curriculum, facilities, placements,
				etc.</P>
				<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in">If you want to
				know more about a particular degree/department from a university,
				PurpleLane can connect you to a mentor who is pursuing the same
				degree in that university so that you will get a clear picture
				which helps you decide whether to choose the university.</P>
			</OL>
			<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in">There can be many
			situations like these, you can find a perfect mentor who can clear
			your dilemmas and choose the right path.</P>
		</OL>
	</UL> 
        </p>
      </div>
    </div>
    
    	<div class="accordion-item">
      <a>Is mentorship really
	important for achieving my goal?</a>
      <div class="content">
        <p>
        In order to be
		successful in any field, aspiring people are going to need guidance
		and role models. Getting mentored from the experienced folks is
		quite vital in a lot of different fields, some of which include
		education, career, healthcare, business management, fitness
		programs, music, etc.</P> <BR/>
		<P>Getting
		the guidance, encouragement and support from an experienced mentor
		provides you with a broad range of personal and professional
		benefits, which ultimately lead to improved performance in the
		career
		</P>
        </p>
      </div>
    </div>
    
    <div class="accordion-item">
      <a>Is my internet connection good enough for video calls?</a>
      <div class="content">
        <p>We recommend Internet connections of at least 1mbps for better and uninterrupted video calls.</p>
      </div>
    </div>
    
     <div class="accordion-item">
      <a>Any software do I
	need to install for using video call?</a>
      <div class="content">
        <p>No need to install
		any software or app for using our video call service. Our platform
		is completely web-based.</p>
      </div>
    </div>
    


    

    <div class="accordion-item">
      <a>Can I get a refund?</a>
      <div class="content">
        <p>Yes, we have a
		refund policy. If the mentor rejects your proposal and could not
		find a time slot for your meeting, we provide you with a full refund. </p>
      </div>
    </div>
  </div>
  
</div>
				
				</div>
            </div>
           
          
			
			
        </div>
        <!-- container end -->
    </section>

<!-- Forgot Password End-->
<!-- foooter started -->
<?php
    include("footer.php");
    include("footer_tags.php");
?>

  
  
  	<Script>
	    const items = document.querySelectorAll(".accordion a");

function toggleAccordion(){
  this.classList.toggle('active');
  this.nextElementSibling.classList.toggle('active');
}

items.forEach(item => item.addEventListener('click', toggleAccordion));
	</script>



    <!--<script src="js/jquery3.2.1.min.js"></script>-->
    <!-- JQUERY LIBRARY -->
    <!--<script src="js/particles.min.js"></script>-->
    <!-- Particles JS -->
    <!--<script src="js/bootstrap.min.js"></script>-->
    <!-- Bootstrap JS -->
    <!--<script src="js/main.js"></script>-->
    <!-- Main JS -->
    
</body>

</html>