<?php require_once "includes/initialize.php"; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Purple Lane</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <?php include 'head.php' ?>
	
	<style>
.navbarall{
    background: linear-gradient(45deg, #23044f 0%, #87238c 100%);
}
.accordion a {
  position: relative;
  display: -webkit-box;
  display: -webkit-flex;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-orient: vertical;
  -webkit-box-direction: normal;
  -webkit-flex-direction: column;
  -ms-flex-direction: column;
  flex-direction: column;
  width: 100%;
  padding: 1rem 3rem 1rem 1rem;
  color: #7288a2;
  font-size: 1.15rem;
  font-weight: 400;
  border-bottom: 1px solid #e5e5e5;
}

.accordion a:hover,
.accordion a:hover::after {
  cursor: pointer;
  color: #2c0754;
}

.accordion a:hover::after {
  border: 1px solid #2c0754;
}

.accordion a.active {
  color: #2c0754;
  border-bottom: 1px solid #2c0754;
}

.accordion a::after {
  font-family: 'FontAwesome';
  content: '\f067';
  position: absolute;
  float: right;
  right: 1rem;
  font-size: 1rem;
  color: #7288a2;
  padding: 4px;
  width: 30px;
  height: 30px;
  -webkit-border-radius: 50%;
  -moz-border-radius: 50%;
  border-radius: 50%;
  border: 1px solid #7288a2;
  text-align: center;
}

.accordion a.active::after {
  font-family: 'FontAwesome';
  content: '\f068';
  color: #2c0754;
  border: 1px solid #2c0754;
}

.accordion .content {
  opacity: 0;
  padding: 0 1rem;
  max-height: 0;
  border-bottom: 1px solid #e5e5e5;
  overflow: hidden;
  clear: both;
  -webkit-transition: all 0.2s ease 0.15s;
  -o-transition: all 0.2s ease 0.15s;
  transition: all 0.2s ease 0.15s;
}

.accordion .content p {
  font-size: 1rem;
  font-weight: 300;
}

.accordion .content.active {
  opacity: 1;
  padding: 1rem;
  max-height: 100%;
  -webkit-transition: all 0.35s ease 0.15s;
  -o-transition: all 0.35s ease 0.15s;
  transition: all 0.35s ease 0.15s;
}

.alt-color {
    color: #491066;
}
	</style>
	

	
</head>
<body data-spy="scroll" data-target=".navbar" data-offset="82">
    <div class="site-loader">
        <!---======Preloader===========-->
        <div class="loader-dots">
            <div class="circle circle-1"></div>
            <div class="circle circle-2"></div>
        </div>
    </div>
    <div class="site__layer"></div>
    <?php include 'header.php' ?>
	
	
    <section class="burger2 app" id="app">
        <div class="container">
            <!-- container-start -->
			
			<div class="container">
				<div class="row">
                    <div class="content">
                        <?php
                        if(isset($_GET['purplelaneid']) && isset($_GET['purplelanetoken']) && !empty($_GET['purplelaneid']) && !empty($_GET['purplelanetoken']) )
                        {
                            $user_id=mysqli_real_escape_string($con, $_REQUEST['purplelaneid']);
                            $token=mysqli_real_escape_string($con, $_REQUEST['purplelanetoken']);
                            $sql = "SELECT id FROM mentee WHERE id='".$user_id."' AND verify_token='". $token."'";
                            $result = mysqli_query($con, $sql);
                            if(mysqli_num_rows($result)>0)
                            {
                              $sql_update="UPDATE mentee SET is_active='1'
                              WHERE id= '".$user_id."'";
                              $result_update = mysqli_query($con, $sql_update);
                              if($result_update)
                              {?>
                                <p>Thanks For verifying Email! Your Account Has Been Activate. </p>
                              <?}
                            }
                            else 
                            {?>
                                <p>Your Email Was Not Verify. </p>
                            <?}
                        } 
                         ?>
                    </div>
                </div>
                                
            </div>
        </div>
    </section>
    <?php 
      require_once('footer.php');
      require_once('footer_tags.php');
    ?>
    
</body>

</html>