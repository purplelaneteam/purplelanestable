<?php 
    require_once("../../includes/initialize.php");
    require_once("logincheck.php");
    require_once("header.php"); 
    require_once("sidebar.php");
?>
<div class="m-grid__item m-grid__item--fluid m-wrapper">    			    
    <div class="m-subheader">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">Add Category</h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__item">
                        <a href="#" class="m-nav__link">
                            <span class="m-nav__link-text">Manage Category</span>
                        </a>
                    </li>
                    <li class="m-nav__separator">-</li>
                    <li class="m-nav__item">
                        <a href="javascript:void(0)" class="m-nav__link">
                            <span class="m-nav__link-text">Add Category</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="m-content" style="padding-top:10px;">
        <form class="m-form m-form--fit m-form--label-align-right" id="add_category_form" method="POST" enctype=multipart/form-data>
            <div class="m-portlet m-portlet--tabs">
                <div class="m-portlet__body">
                <div class="form-row" >
                        <div class="form-group col-lg-6 m-form__group-sub col-lg-cust ">
                            <label for="category">Category Name<span class="m--font-danger">*</span></label>
                            <input class="form-control" type="text" name="name" id="name" placeholder="Category Name" value="" >
                            
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-lg-6 m-form__group-sub col-lg-cust" style="margin-bottom:0px;">
                        <label class="m-checkbox m-checkbox--state-primary">
                                <input type="checkbox" name="is_category" id="is_category" value="yes">Is Sub Category ?
                                <span></span>
                            </label>
                        </div>
                    </div>
                    <div class="form-row" id="category_div" style="display:none">
                        <div class="form-group col-lg-6 m-form__group-sub col-lg-cust ">
                            <label for="category">Category<span class="m--font-danger">*</span></label>
                            <select class="form-control category " data-select2-id="category" name="category" id="category" >
                                <option value="">Select Category</option>
                                <?php
                                    $sql_category_list = "SELECT id, name FROM category WHERE parent_id IS NULL AND active = 1 ORDER BY name ASC";
                                    
                                    $result_category_list = mysqli_query($con, $sql_category_list);
                                    if($result_category_list && $myrow_category_list = mysqli_fetch_array($result_category_list))
                                    {
                                        do
                                        {
                                            $id = $myrow_category_list["id"];
                                        ?>
                                            <option value="<?php echo $id;?>">
                                                <?php echo ucwords($myrow_category_list["name"]);?>
                                            </option>
                                            <?php if($category_id == $id)
                                            {
                                            ?>
                                                <script>
                                                    category.value = '<?php echo $id;?>';
                                                </script>
                                            <?php
                                            }
                                        }
                                        while($myrow_category_list = mysqli_fetch_array($result_category_list));
                                    }
                                ?>
                                </select>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-lg-cust">
                            <input type="hidden" name="action"  id="action" value="add"/>
                            <input type="hidden" name="category_id"  id="category_id" value="category_id"/>
                            <button class="btn btn-success">Submit</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<?php require('footer.php');?>
<script>
// required

$('.category').change(function(){
    $('#category_id').val($(this).val());
})  

$('#is_category').click(function(){
    var element=document.getElementsByClassName("category");
    var x = document.getElementById("category");
  
    if($(this). prop("checked") == true)
{
    for(var i=0;i<element.length;i++)
    $("#category_div").show();
    $('.category').attr("required", true);
    
}
else
{
    $("#category_div").hide();
    $('.category').attr("required", false);
}
});
      
</script>