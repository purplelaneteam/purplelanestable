<?php 
    require_once("../../includes/initialize.php");
    require_once("logincheck.php");
    require_once("header.php");
    require_once("sidebar.php");
   
?>

<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title ">Dashboard</h3>
            </div>
        </div>
    </div>
    <div class="m-content">
        <div class="m-portlet">
            <div class="m-portlet__body  m-portlet__body--no-padding">
                <div class="row m-row--no-padding m-row--col-separator-xl">
                    <!-- <div class="col-md-12 col-lg-6 col-xl-3">
                        <div class="m-widget24">
                            <div class="m-widget24__item">
                                <h4 class="m-widget24__title">
                                    Total Packages
                                </h4>
                                
                                <span class="m-widget24__stats m--font-brand" style="margin-top: 36.57px;">
                                <?php echo $total_packages;?>
                                </span>
                                <div class="m--space-10"></div>
                                <div class="progress m-progress--sm">
                                <div class="progress-bar m--bg-brand" role="progressbar" style="width: <?php echo $total_packages?>%;" aria-valuenow="<?php echo $total_packages?>" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <a class="m-menu__link"  href="list_packages.php"> 
                                    <span class="m-widget24__change">
                                    View Details
                                    </span>
                                </a>
                                <span class="m-widget24__number">
                                <i class="fa fa-arrow-right"></i>
                                </span>
                            </div>
                        </div>
                    </div> -->
                  
                </div>
            </div>
        </div>
    </div>
</div>
<?php require('footer.php');?>