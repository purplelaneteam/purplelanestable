        </div>
        <footer class="m-grid__item		m-footer ">
            <div class="m-container m-container--fluid m-container--full-height m-page__container">
                <div class="m-stack m-stack--flex-tablet-and-mobile m-stack--ver m-stack--desktop">
                    <div class="m-stack__item m-stack__item--left m-stack__item--middle m-stack__item--last">
                        <span class="m-footer__copyright">
                        <?php echo  date('Y');?> &copy; Developed by
                            <a href="http://www.spryox.com" class="m-link">SpryOX</a>
                        </span>
                    </div>
                </div>
            </div>
        </footer>
    </div>
    <div id="m_scroll_top" class="m-scroll-top">
        <i class="la la-arrow-up"></i>
    </div>
    <script src="/assets/vendors/base/vendors.bundle.js"></script>
    <script src="/assets/demo/default/base/scripts.bundle.js"></script>
    <script src="/assets/demo/default/custom/crud/forms/widgets/bootstrap-datepicker.js" type="text/javascript"></script>

    <script src="/assets/vendors/custom/datatables/datatables.bundle.js"></script>
    <script src="/js/datetimepicker.js"></script>
   
    <script src="/assets/js/common.js"></script>
    <script src="/assets/js/admin/common.js"></script>
    
            
</body>

</html>