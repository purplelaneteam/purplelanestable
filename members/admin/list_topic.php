<?php 
    require_once("../../includes/initialize.php");
    require_once("logincheck.php");
    $category = sanitize_input($_POST['category']);
    $date =  date('Y-m-d H:i:s');
    $success = "";
    $error = "";
    require_once("header.php");
    require_once("sidebar.php");
?>

<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">List Topic</h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__item m-nav__item--home">
                        <a href="" class="m-nav__link">
                            <span class="m-nav__link-text">Manage Topic</span>
                        </a>
                    </li>
                   <li class="m-nav__separator">-</li>
                     <li class="m-nav__item">
                        <a href="" class="m-nav__link">
                            <span class="m-nav__link-text">List Topic</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="m-content">
        <div class="m-portlet m-portlet--mobile">
          
                         
            <div class="m-portlet__body">

                <table class="table table-striped- table-bordered table-hover table-checkable" id="table_category_list"> 
                    <thead>
                        <tr>
                            <th>Sr No.</th>
                            <th>Category </th>
                            <th>Topic Name </th>
                            <th>Added on</th>
                            <th>Modified on</th>
                            <th>Active</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                     <?php

                    $sql_get_topic = "SELECT t.id, c.id as cid,t.name,c.name as category,t.addedon,t.active,t.modifiedon  FROM topic t inner join category c on t.category_id=c.id";
                    // print_r($sql_get_category);exit;
                    $result_get_topic = mysqli_query($con, $sql_get_topic);
                        $i = 1;
                        while($result_get_topic && $myrow_get_topic = mysqli_fetch_array($result_get_topic))
                        {
                          
                            $id = $myrow_get_topic["id"];
                            $cid = $myrow_get_topic["cid"];
                            $topic_name = $myrow_get_topic["name"];
                            $topic_category = $myrow_get_topic["category"];
                            $topic_active= $myrow_get_topic["active"];
                            $addedon = $myrow_get_topic["addedon"];
                            $modifiedon = $myrow_get_topic["modifiedon"];
                            $active_status="";
                            if($topic_active == 1)
                            {
                                $active_status = '<span style="width: 100px;"><span class="m-badge  m-badge--success m-badge--wide">Active</span></span>';
                            }
                            else
                            {
                                $active_status = '<span style="width: 100px;"><span class="m-badge  m-badge--danger m-badge--wide">Inactive</span></span>';
                            }   
                           
                    ?>
                        <tr>
                            <td><?php echo $i;?></td>
                            <td>
                                <?php $rtn_str=''; get_category_list($cid,$topic_category,$rtn_str); echo $rtn_str;?>
                            </td>
                            <td>
                                <?php echo $topic_name;?>
                            </td>
                            <td><?php echo $addedon;?></td>
                            <td><?php echo $modifiedon;?></td>
                            <td><?php echo $active_status;?></td>
                            <td>
                                <?php
                                    $actionoption = '';
                                    if($topic_active == 1)
                                    {
                                        $status = "'Deactivate'";
                                        $actionoption .= '<input type="hidden" name="active_deactive" value="'.$topic_active.'"/>
                                        <a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill topic_delete" title="Deactivate" data-id="'.$id.'" data-status="0">
                                        <i class="fas fa-thumbs-down"></i>
                                        </a>';
                                    }
                                    else
                                    {
                                        $status = "'Activate'";
                                        $actionoption .='<input type="hidden" name="active_deactive" value="'.$topic_active.'"/>
                                        <a href="javascript:void(0)" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill topic_delete" title="Activate" data-id="'.$id.'" data-status="1">
                                        <i class="fas fa-thumbs-up"></i> 
                                        </a>';
                                    }
                                    $actionoption .='<a href="add_edit_topic.php?id='.$id.'" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="Edit Topic" > <i class="fas fa-edit"></i>
                                    </a>';
                                ?>
                                   <?php echo $actionoption;?>
                                </td>
                          
                          
                        </tr>
                    <?php
                        $i++;
                        }
                    ?> 
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<?php require('footer.php');?>

<script>
    $('#table_category_list').DataTable({
        "order": [[ 3, "desc" ]],
        "columnDefs": [
            { "orderable": false, "targets": 6 },
        ]
    });
</script>