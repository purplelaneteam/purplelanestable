<?php
    include '../../includes/initialize.php';
    
require_once __DIR__.'/../../vendor/autoload.php';

session_start();

$client = new Google_Client();
$client->setAuthConfig('client_secrets.json');
$client->setScopes(Google_Service_Calendar::CALENDAR_EVENTS);
// $client->setAuthConfig('credentials.json');
$client->setAccessType('offline');
$client->setPrompt('select_account consent');

if (isset($_SESSION['access_token']) && $_SESSION['access_token']) {
  $client->setAccessToken($_SESSION['access_token']);
  $service = new Google_Service_Calendar($client);

// // Print the next 10 events on the user's calendar.
    // $calendarId = 'primary';
    // $optParams = array(
    // 'maxResults' => 10,
    // 'orderBy' => 'startTime',
    // 'singleEvents' => true,
    // 'timeMin' => date('c'),
    // );
    // // var_dump($service->events);exit;
    // $results = $service->events->listEvents($calendarId, $optParams);
    // $events = $results->getItems();
    // $events_array = json_decode(json_encode($events), true);
    // foreach ($events_array as $key => $event) {
    //     echo json_encode($event);
    // }

    $event = new Google_Service_Calendar_Event();
    $event->setSummary('Interview');
    $event->setLocation('Dell');
    $event->sendNotifications=true;

    $start = new Google_Service_Calendar_EventDateTime();
    $start->setDateTime('2019-04-17T17:30:00.000-07:00');
    $event->setStart($start);
    $end = new Google_Service_Calendar_EventDateTime();
    $end->setDateTime('2019-04-17T18:00:00.000-07:00');
    $event->setEnd($end);
    $attendee1 = new Google_Service_Calendar_EventAttendee();
    $attendee1->setEmail('sharif.spryox@gmail.com');


    // ...
    $attendees = array($attendee1);
    $event->attendees = $attendees;
    $sendNotifications = array('sendNotifications' => true);
    $createdEvent = $service->events->insert('hkf71jqfnc9ocp9l9httg6qm60@group.calendar.google.com', $event, $sendNotifications);

    echo $createdEvent->getId();exit;
} else {
  $redirect_uri = 'http://' . $_SERVER['HTTP_HOST'] . '/members/admin/calendar-callback.php';
  header('Location: ' . filter_var($redirect_uri, FILTER_SANITIZE_URL));
}