<?php 
    require_once("../../includes/initialize.php");
    require_once("logincheck.php");
    $erroroccured = 0;
    $errormsg = "";
    $action="add";
    $button_name="Add";
    require_once("header.php"); 
    require_once("sidebar.php");

    $cost_array = [];
    $sql_get_cost = "SELECT id,name,codename,price,addedon,modifiedon FROM cost_card_type";
                    $result_get_cost = mysqli_query($con, $sql_get_cost);
                    while($myrow_get_cost=mysqli_fetch_array($result_get_cost))
                    {
                        $cost_array[]=array("id"=>$myrow_get_cost["id"],
                                                "name"=>$myrow_get_cost["name"],
                                                "codename"=>$myrow_get_cost["codename"],
                                                "price"=>$myrow_get_cost["price"],
                                                "Addedon"=>$myrow_get_cost["addedon"],
                                                "Modifiedon"=>$myrow_get_cost["modifiedon"]
                        );
                    }

    if(isset($_GET['edit']))
    {
        $edit = sanitize_input($_GET['edit']);
        $edit_query="SELECT name, price FROM cost_card_type WHERE id='$edit'";
       
        if(!$edit_cost=mysqli_query($con,$edit_query))
        {
            $erroroccured = 1;
            $errormsg = "ERROR_CUST_00:Something went wrong";
        } else{
            if($result_edit_cost=mysqli_fetch_array($edit_cost))
            {
                $cus_id=$result_edit_cost['id'];
                $cost_card_name=$result_edit_cost['name'];
                $cost_card_price=$result_edit_cost['price'];
            }
        }
        $action="edit";$button_name="Update";
    }    

?>
<?php
if($erroroccured==1)
{
?>
<div class="alert alert-danger" role="alert">
    <?php echo $errormsg;?>
</div>
<?php
}
?><div class="m-grid__item m-grid__item--fluid m-wrapper">    			    
<div class="m-subheader">
    <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">Add Content</h3>
            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                <li class="m-nav__item m-nav__item--home">
                    <a href="#" class="m-nav__link m-nav__link--icon">
                        <i class="m-nav__link-icon la la-home"></i>
                    </a>
                </li>
                <li class="m-nav__item">
                    <a href="#" class="m-nav__link">
                        <span class="m-nav__link-text">Manage Homepage</span>
                    </a>
                </li>
                <li class="m-nav__separator">-</li>
                <li class="m-nav__item">
                    <a href="javascript:void(0)" class="m-nav__link">
                        <span class="m-nav__link-text">Add Content</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>
<div class="m-content" style="padding-top:10px;">
<form class="m-form m-form--fit m-form--label-align-right" action="processreq/proc_add_homepage_attribute.php" id="add_homepage_attribute_form" name="add_homepage_attribute_form" method="POST" enctype=multipart/form-data>
        <div class="m-portlet m-portlet--tabs">
            <div class="m-portlet__body">
                <div class="form-row" id="homepage_view_select_div" >
                    <div class="form-group col-lg-6 m-form__group-sub col-lg-cust ">
                    <label for="homepage_view_select">Select View<span class="m--font-danger">*</span></label>
                    <br>
                    <select class="form-control m-select2 category" style="width:300px" name="homepage_view_select" id="homepage_view_select">
                        <option value="" selected>Select Category</option>
                        <?php
                        $sql = "SELECT `id`, `name` FROM homepage_setting_attribute WHERE is_active=1";
                        $result = mysqli_query($con, $sql);
                        if(mysqli_num_rows($result) > 0){
                            while($row = mysqli_fetch_assoc($result)){
                                $id = $row['id'];
                                $name = $row['name'];
                                echo "
                                <option value=$id>$name</option>
                                ";
                            }
                        }
                        ?>    
                    </select>
                    </div>
                </div>
                <div class="form-row" id="">
                    <div class="form-group col-lg-2 m-form__group-sub col-lg-cust">
                        <label for="content_order">Content Order</label>
                        <input class="form-control" name="content_order[]" id="content_order[]" placeholder="Enter Order" required>
                    </div>
                    <div class="form-group col-lg-3 m-form__group-sub col-lg-cust">
                        <label for="content_title">Content Title</label>
                        <input class="form-control" name="content_title[]" id="content_title[]" placeholder="Enter Title">
                    </div>
                    <div id="more_data"></div>
                </div>
                <div class="form-row" id="add_more_div"></div>
                <div class="form-row">
                    <div class="form-group col-lg-cust">
                        <input class="btn btn-primary" type="button" name="add_more" id="add_more" value="Add More">
                    </div>
                    <div class="form-group col-lg-cust">
                        <button class="btn btn-success" type="submit">Submit</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
</div>

<?php require('footer.php');?>
<script>
var dropdown='';
var new_dropdown = '';
$("#homepage_view_select").change(function () {
var homepage_view_id = this.value;
$.ajax({
    type: 'post',
    url: "processreq/load_homepage_attributes.php",
    data: "id=" + homepage_view_id,
    success: function (response) {
        if(dropdown==''){
            dropdown = response;
            $("#more_data").html(dropdown);
        } else {
            new_dropdown = response;
            $("#more_data").html(new_dropdown);
        }
        if(new_dropdown!='' && dropdown!=new_dropdown){
            $("#add_more_div").empty();
            dropdown = new_dropdown;
        }
    }
});
});
$("#add_more").click(function(){
if(dropdown!=''){
    $("#add_more_div").append(`
    <div class="form-row more_views">
        <div class="form-group col-lg-2 m-form__group-sub col-lg-cust">
            <label for="content_order">Content Order</label>
            <input class="form-control" name="content_order[]" id="content_order[]" placeholder="Enter Order" required>
        </div>
        <div class="form-group col-lg-3 m-form__group-sub col-lg-cust">
            <label for="content_title">Content Title</label>
            <input class="form-control" name="content_title[]" id="content_title[]" placeholder="Enter Title">
        </div>
        `+dropdown+`
        <div class="col-md-1">
            <input type="button" class="btn m-btn--pill m-btn--air btn-danger" onClick="remove(this)" value="Remove">
        </div>
    </div>
    <br>
    `);
}
});
function remove(element)
{
$(element).closest('.more_views').remove()
}
</script>
