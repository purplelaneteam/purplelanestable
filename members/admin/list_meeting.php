<?php 
    require_once("../../includes/initialize.php");
    require_once("logincheck.php");
    $category = sanitize_input($_POST['category']);
    $date =  date('Y-m-d H:i:s');
    $success = "";
    $error = "";
    $status="activate";
    $status="deactivate";
    require_once("header.php");
    require_once("sidebar.php");
    $title = '';
    if($_GET['status'] == 'pending') {
        $title = 'Requested';
    }
    if($_GET['status'] == 'completed') {
        $title = 'Completed';
    }
    if($_GET['status'] == 'scheduled') {
        $title = 'Scheduled';
    }
    if($_GET['status'] == 'rescheduled') {
        $title = 'Rescheduled';
    }
    if($_GET['status'] == 'cancelled') {
        $title = 'Rejected';
    }
?>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">List Meetings</h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__item m-nav__item--home">
                        <a href="" class="m-nav__link">
                            <span class="m-nav__link-text">Manage Meetings</span>
                        </a>
                    </li>
                   <li class="m-nav__separator">-</li>
                     <li class="m-nav__item">
                        <a href="" class="m-nav__link">
                            <span class="m-nav__link-text"><?php echo $title ?> Meetings</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="m-content">
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__body">
                <?php 
                if($success!="")
                {
                ?>
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    </button>
                    <strong>Success!</strong> <?php echo $success; ?>
                </div>
                <?php 
                }
                ?>
                 <?php 
                if($error!="")
                {
                ?>
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    </button>
                    <strong>Error!</strong> <?php echo $error; ?>
                </div>
                <?php 
                }
                $startCol = ($_GET['status'] === 'pending' || $_GET['status'] === 'completed' || $_GET['status'] === 'scheduled' || $_GET['status'] === 'rescheduled') ? 1: 0;
                // $startCol =  0;
                ?>
                <input type='hidden' name='status' id='status' value='<?php echo $_GET['status'];?>'>
                    <div class="m-accordion__item-content">
                        <form class="m-form m-form--fit m--margin-bottom-20">
                            <div class="row m--margin-bottom-20">
                                <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
                                    <label>Meeting ID</label>
                                    <input class="form-control m-input" data-col-index="<?php echo $startCol + 0 ?>" placeholder="Meeting ID">
                                </div>
                                <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
                                    <label>Mentor</label>
                                    <input class="form-control m-input" data-col-index="<?php echo $startCol + 1 ?>" placeholder="Mentor">
                                </div>
                                <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
                                <label>Mentee</label>
                                    <input class="form-control m-input" data-col-index="<?php echo $startCol + 2 ?>" placeholder="Mentee">
                                </div>
                                <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
                                    <label>Topics</label>
                                    <input class="form-control m-input" data-col-index="<?php echo $startCol + 3 ?>" placeholder="Topic">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <button class="btn btn-brand m-btn m-btn--icon" id="m_search">
                                        <span>
                                            <i class="la la-search"></i>
                                            <span>Search</span>
                                        </span>
                                    </button>
                                    &nbsp;&nbsp;
                                    <button class="btn btn-secondary m-btn m-btn--icon" id="m_reset">
                                        <span>
                                            <i class="la la-close"></i>
                                            <span>Reset</span>
                                        </span>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                
                <table class="table table-striped- table-bordered table-hover table-checkable" id="meeting_list"> 
                    <thead>
                        <tr>
                            <?php
                                if ($_GET['status'] === 'pending' || $_GET['status'] === 'completed' || $_GET['status'] === 'scheduled' || $_GET['status'] === 'rescheduled')
                                {
                                    echo '<th>Actions</th>';
                                }
                            ?>
                            <th>Meeting ID</th>
                            <th>Mentor</th>
                            <th>Mentee</td>
                            <th>Topic</td>
                            <th>Mentee Slots</td>
                            <th>Mentor Slots</td>
                            <th>Status</td>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="modal" id="package-details-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Reason</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <form class="m-form m-form--fit m-form--label-align-right"  id="package_update_form" method="POST" action="javascript:void(0)">
                <div class="modal-body">
                    <div class="form-row">
                        <input type="hidden" name="package_id" id="package_id">
                        <div class="form-group col-lg-12 m-form__group-sub col-lg-cust">
                            <input type="file" class="form-control" name="package_featured_image" id="package_featured_image" placeholder="icon" required>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-success cancel" id="">Submit</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal" id="hangout-link-modal" style="display:none">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Hangout Link</h4>
                <button type="button" class="close" data-dismiss="modal" onclick="$('#hangout-link-modal').hide()">&times;</button>
            </div>
            <form class="m-form m-form--fit m-form--label-align-right"  id="set_hangout_link" method="POST" action="javascript:void(0)">
                <div class="modal-body">
                    <div class="form-row">
                        <input type="hidden" name="meeting_id" id="meeting_id_for_link">
                        <div class="form-group col-lg-12 m-form__group-sub col-lg-cust">
                            <input type="text" class="form-control" name="hangout_link" id="hangout-link" placeholder="Hangout Link" required>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-success cancel" id="hangout-link-submit">Submit</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="$('#hangout-link-modal').hide()">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
<?php require('footer.php');?>