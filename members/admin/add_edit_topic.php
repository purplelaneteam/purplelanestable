<?php 
    require_once("../../includes/initialize.php");
    require_once("logincheck.php");
    require_once("header.php");
    require_once("sidebar.php");
    $action="";
    $topic_id="";
    $name="";
    $category_id="";
 
    if(isset($_GET['id']) && !empty($_GET['id']))
    {
        $action="edit";
        $topic_id=sanitize_input($_GET["id"]);
        $sql_select="SELECT id,name,category_id FROM topic WHERE id='$topic_id'";
        $sql_result=mysqli_query($con,$sql_select);
        if($sql_result)
        {
            while($myrow=mysqli_fetch_array($sql_result))
            {
                $name=$myrow['name'];
                $category_id=$myrow['category_id'];
            }
        }
        else 
        {
            header("Location:list_topic.php");
        }
    }
    else 
    {
        $action="add";
        $topic_id="";
    }
?>
<div class="m-grid__item m-grid__item--fluid m-wrapper">    			    
    <div class="m-subheader">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">Topic</h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__item">
                        <a href="#" class="m-nav__link">
                            <span class="m-nav__link-text">Manage Topic</span>
                        </a>
                    </li>
                    <li class="m-nav__separator">-</li>
                    <li class="m-nav__item">
                        <a href="javascript:void(0)" class="m-nav__link">
                            <span class="m-nav__link-text">Add/Edit Topic</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="m-content" style="padding-top:10px;">
        <form class="m-form m-form--fit m-form--label-align-right" id="add_edit_topic_form" method="POST" enctype=multipart/form-data>
            <div class="m-portlet m-portlet--tabs">
                <div class="m-portlet__body">
                <div class="form-row" >
                    <div class="form-group col-lg-6 m-form__group-sub col-lg-cust ">
                        <label for="topic">Topic Name<span class="m--font-danger">*</span></label>
                        <input class="form-control" type="text" name="name" id="name" required value="<?php echo $name; ?>" placeholder="Topic Name"  >
                    </div>
                       
                    </div>
                    
                    <div class="form-row" id="category_div" >
                     <div class="form-group col-lg-6 m-form__group-sub col-lg-cust ">
                        <label for="category">Category<span class="m--font-danger">*</span></label>
                        <select class="form-control category_id" required name="category_id" id="category_id" >
                            <option value="">Select Category</option>
                            <?php
                                $sql_category_list = "SELECT id, name, parent_id FROM category WHERE  active = 1 AND parent_id IS NOT NULL ORDER BY name ASC";
                                $result_category_list = mysqli_query($con, $sql_category_list);
                                if($result_category_list && $myrow_category_list = mysqli_fetch_array($result_category_list))
                                {
                                    $return_str = '';
                                    do
                                    {
                                        $category_name = '';
                                        $id = $myrow_category_list["id"];
                                        $parentid = $myrow_category_list["parent_id"];
                                        // if($myrow_category_list["parent_id"]!=NULL)
                                        // {
                                        //     get_category_list($id,  $myrow_category_list["name"], $return_str);
                                        //     $category_name =  $return_str;
                                         
                                        // }
                                        // else
                                        // {
                                        //     $category_name = $myrow_category_list["name"];
                                        // }
                                            $category_name = $myrow_category_list["name"];
                                      
                                    ?>
                                   
                                        <option value="<?php echo $id;?>">
                                            <?php echo $category_name?>
                                        </option>
                                        <?php
                                    }
                                    while($myrow_category_list = mysqli_fetch_array($result_category_list));
                                }
                            ?>
                            </select>
                            <script>
                            document.getElementById("category_id").value="<?php echo $category_id ?>";
                            </script>
                        </div> 
                    </div>
                   
                    <div class="form-group col-lg-cust">
                    <input type="hidden" name="id"  id="id" value="<?php echo $topic_id ?>"/>
                    <input type="hidden" name="action"  id="action" value="<?php echo $action ?>"/>
                        <button class="btn btn-success">Submit</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>


<?php require('footer.php');?>
<script>
  

$('#is_category').click(function(){
   
    var element=document.getElementsByClassName("category");
    if($(this). prop("checked") == true)
{
    $('#category_div').show();
    for(var i=0;i<element.length;i++)
    $('.category').attr("required", true);
    
}
else
{
    $('#category_div').hide();
    $('.category').attr("required", false);
}
});
$('.category').change(function(){
    $('#category_id').val($(this).val());
})   
</script>