<?php 
    require_once("../../includes/initialize.php");
    require_once("logincheck.php");
    $category = sanitize_input($_POST['category']);
    $date =  date('Y-m-d H:i:s');
    $success = "";
    $error = "";
    $status="activate";
    $status="deactivate";
    require_once("header.php");
    require_once("sidebar.php");
?>

<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">List Transaction</h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__item m-nav__item--home">
                        <a href="" class="m-nav__link">
                            <span class="m-nav__link-text">Manage Transaction</span>
                        </a>
                    </li>
                   <li class="m-nav__separator">-</li>
                     <li class="m-nav__item">
                        <a href="" class="m-nav__link">
                            <span class="m-nav__link-text">List Mentor Transaction</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="m-content">
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__body">
                <?php 
                if($success!="")
                {
                ?>
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    </button>
                    <strong>Success!</strong> <?php echo $success; ?>
                </div>
                <?php 
                }
                ?>
                 <?php 
                if($error!="")
                {
                ?>
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    </button>
                    <strong>Error!</strong> <?php echo $error; ?>
                </div>
                <?php 
                }
                ?>
                <table class="table table-striped- table-bordered table-hover table-checkable" id="mentor_transc_list"> 
                    <thead>
                        <tr>
                            <th>Meeting Id</th>
                            <th>Amount</td>
                            <th>Type</td>
                            <th>Transcation Type</td>
                            <th>Status</td>
                            <th>Added on</th>
                        </tr>
                    </thead>
                    <tbody>
                  
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="modal" id="package-details-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Reason</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <form class="m-form m-form--fit m-form--label-align-right"  id="package_update_form" method="POST" action="javascript:void(0)">
                <div class="modal-body">
                    <div class="form-row">
                        <input type="hidden" name="package_id" id="package_id">
                        <div class="form-group col-lg-12 m-form__group-sub col-lg-cust">
                            <input type="file" class="form-control" name="package_featured_image" id="package_featured_image" placeholder="icon" required>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-success cancel" id="">Submit</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
<?php require('footer.php');?>
<script>
 function activate_deactivate_package(empid,delid){  
    var confirmation = confirm("are you sure you want to Activate this?");
    if (confirmation) {
    var action="Activate";
     $.ajax({
          url: "processreq/proc_add_mentor.php",
          type: "POST",
          data: {action:action,delid:delid},
          success: function(){
            setTimeout(location.reload.bind(location), 5000);
          }
      });
       alert('Mentor Activated');
    }
}

function activate_deactivate_package(empid,delid){  
    var confirmation = confirm("are you sure you want to Deactivate this?");
    if (confirmation) {
    var action="Deactivate";
     $.ajax({
          url: "processreq/proc_add_mentor.php",
          type: "POST",
          data: {action:action,delid:delid},
          success: function(){
            // setTimeout(location.reload.bind(location), 5000);
          }
      });
       alert('Mentor Deactivate');
    }
}
</script>
