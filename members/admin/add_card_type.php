<?php 
    require_once("../../includes/initialize.php");
    require_once("logincheck.php");
    $erroroccured = 0;
    $errormsg = "";
    $action="add";
    $button_name="Add";
    require_once("header.php"); 
    require_once("sidebar.php");

    $cost_array = [];
    $sql_get_cost = "SELECT id,name,codename,price,addedon,modifiedon FROM cost_card_type";
                    $result_get_cost = mysqli_query($con, $sql_get_cost);
                    while($myrow_get_cost=mysqli_fetch_array($result_get_cost))
                    {
                        $cost_array[]=array("id"=>$myrow_get_cost["id"],
                                                "name"=>$myrow_get_cost["name"],
                                                "codename"=>$myrow_get_cost["codename"],
                                                "price"=>$myrow_get_cost["price"],
                                                "Addedon"=>$myrow_get_cost["addedon"],
                                                "Modifiedon"=>$myrow_get_cost["modifiedon"]
                        );
                    }

    if(isset($_GET['edit']))
    {
        $edit = sanitize_input($_GET['edit']);
        $edit_query="SELECT name, price FROM cost_card_type WHERE id='$edit'";
       
        if(!$edit_cost=mysqli_query($con,$edit_query))
        {
            $erroroccured = 1;
            $errormsg = "ERROR_CUST_00:Something went wrong";
        } else{
            if($result_edit_cost=mysqli_fetch_array($edit_cost))
            {
                $cus_id=$result_edit_cost['id'];
                $cost_card_name=$result_edit_cost['name'];
                $cost_card_price=$result_edit_cost['price'];
            }
        }
        $action="edit";$button_name="Update";
    }    

?>
<?php
if($erroroccured==1)
{
?>
<div class="alert alert-danger" role="alert">
    <?php echo $errormsg;?>
</div>
<?php
}
?>
<div class="m-grid__item m-grid__item--fluid m-wrapper">    			    
    <div class="m-subheader">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">Add Card Name</h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__item">
                        <a href="#" class="m-nav__link">
                            <span class="m-nav__link-text">Manage Card Type</span>
                        </a>
                    </li>
                    <li class="m-nav__separator">-</li>
                    <li class="m-nav__item">
                        <a href="javascript:void(0)" class="m-nav__link">
                            <span class="m-nav__link-text">Add Card Type</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="m-content" >
        <form class="m-form m-form--fit m-form--label-align-right" id="add_cardtype_form" method="POST" value="<?php  ?>" enctype=multipart/form-data>
            <div class="m-portlet m-portlet--tabs">
                <div class="m-portlet__body">
                    <div class="form-row" id="category_div" >
                    </div>
                    <div class="form-row">
                        <input type="hidden" class="form-control" name="id" id="id" value="<?php echo $edit ?>" placeholder="id">
                        <div class="form-group col-lg-4 m-form__group-sub col-lg-cust">
                            <label for="name">Name<span class="m--font-danger">*</span></label>
                            <input class="form-control" name="name" id="name" placeholder="Name" value="<?php echo $cost_card_name ?>" required>
                        </div>
                        <div class="form-group col-lg-4 m-form__group-sub col-lg-cust">
                            <label for="name">Price<span class="m--font-danger">*</span></label>
                            <input class="form-control" name="price" type="number" min="0" id="price" placeholder="Price" value="<?php echo $cost_card_price ?>" required>
                        </div>
                        <div class="form-group col-lg-4 m-form__group-sub col-lg-cust">
                            <input type="hidden" name="action" id="action" value="<?php echo $action ?>">
                            <button class="btn btn-success" type="submit" style="margin-top: 25px;"><?php echo $button_name; ?></button>
                            <a class="btn btn-secondary" style="margin-top: 25px;" href="add_card_type.php">Cancel</a>
                        </div>
                    </div>
                    <div class="col-xs-12" style="margin-bottom: 30px;"><hr></div>
                    <table class="table table-striped- table-bordered table-hover table-checkable" id="add_cardtype_table"> 
                        <thead>
                            <tr>
                                
                                <th>Name </th>
                                <th>CodeName</th>
                                <th>Price</th>
                                <th>Addedon</th>
                                <th>Modifiedon</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($cost_array as $cost_key => $cost_data) {
                            ?>
                            <tr>
                                
                                <td><?php echo $cost_data["name"];?></td>
                                <td><?php echo $cost_data["codename"];?></td>
                                <td><?php echo $cost_data["price"];?></td>
                                <td><?php echo $cost_data["Addedon"];?></td>
                                <td><?php echo $cost_data["Modifiedon"];?></td>
                                <td><a href="add_card_type.php?edit=<?php echo $cost_data['id'];?>">   <i class="fas fa-edit"></i></a>
                                    <a href="javascript:void(0);" onclick="deleteCardType(<?php echo $cost_data['id'];?>)"><i class="fas fa-trash-alt"></i></a>
                                </td> 
                            </tr>
                            <?php
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </form>
    </div>
</div>

<?php require('footer.php');?>

<script>

    function deleteCardType(card_type_id)
    {

        swal({
            title: "Are you sure?",
            text: "You want to delete this entry!",
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Yes",
            cancelButtonText: "No",
            reverseButtons: !0
        }).then(function(e) {

            if(e.value)
            {
                var action="delete";
                $.ajax({
                    url: "processreq/proc_add_cost_card_type.php",
                    type: "POST",
                    data: {action:action,delid:card_type_id},
                    success: function(response){
                        if (response.code) {
                            swal('Error!', response.msg, 'error')
                        } else {
                            swal({
                                title: "Success!",
                                text: response.msg,
                                type: "success",
                            }).then(function () {
                                window.location.href = "add_card_type.php";
                            });
                        }
                    },
                    error: function (response) {
                        swal('Error!', response.msg, 'error');
                    }
                });
            }
        })
    }

    $('#add_cardtype_table').DataTable({
        "order": [[ 3, "desc" ]],
        "columnDefs": [
            { "orderable": false, "targets": 5 },
        ]
    });
    $('#add_cardtype_form').submit(function (e) {
        e.preventDefault();
    }).validate({
        focusInvalid: true,
        invalidHandler: function (form, validator) {
            var errors = validator.numberOfInvalids();
            if (errors) {
                $("html, body").animate({
                    scrollTop: 0
                }, "fast");
            }
        },

        submitHandler: function (form) {
            var formData = new FormData(form);
            var button = $(form).find('button')
            var loading_class = 'm-loader m-loader--light m-loader--right'
            var button_text = $(button).text()
            $(button).html(`Processing`).addClass(loading_class).prop('disabled', true)
            $.ajax({
                type: 'POST',
                url:'processreq/proc_add_cost_card_type.php',
                data: formData,
                dataType: "json",
                cache: false,
                contentType: false,
                processData: false,
                success: function (response) {
                    setTimeout(location.reload.bind(location), 5000);
                    $(button).prop('disabled', false).html(button_text).removeClass(loading_class)
                    if (response.code) {
                        swal('Error!', response.msg, 'error')
                    } else {
                        swal({
                            title: "Success!",
                            text: response.msg,
                            type: "success",
                        }).then(function () {
                            window.location.href = "add_card_type.php";
                        });
                    }
                },
                error: function (response) {
                    $(button).prop('disabled', false).html(button_text).removeClass(loading_class)
                    swal('Error!', response.msg, 'error');
                }
            });
            $('#add_cardtype_form')[0].reset();
        }
    })

</script>