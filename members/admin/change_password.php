<?php 
    require_once("../../includes/initialize.php");
    require_once("logincheck.php");
    if(count($_POST)>0)
    {
        $new_password = sanitize_input($_POST['new_password']);
        $confirm_password = sanitize_input($_POST['confirm_password']);
        $old_password = sanitize_input($_POST['old_password']);
        $date =  date('Y-m-d H:i:s');
        if($old_password != "" && $new_password != "" && $confirm_password != "")
        {
            if($new_password == $confirm_password)
            {
                $sql_check = "SELECT email FROM employee WHERE id = '".USER_ID."' AND `password` = '".md5($old_password)."'";
                $result_check = mysqli_query($con, $sql_check);
                if($myrow_check = mysqli_fetch_array($result_check))
                {
                    $email = $myrow_check["email"];
                    $new_password = md5($_POST["new_password"]);
                    $token = md5($email.$new_password);

                    $sqlpass = "UPDATE employee SET `password` ='".$new_password."', token = '".$token."', modifiedon = '".$date."' WHERE id = '".USER_ID."'";
                    
                    if(mysqli_query($con, $sqlpass))
                    {
                        $success = "Password Successfully Updated.";
                        header("Refresh:3; url=/members/logout.php");
                    }
                    else
                    {
                        $error = "Error in updating Password";
                    }
                }
                else
                {
                    $error = "Incorrect old Password";
                }
            }
            else
            {
                $error = "New Password and Confirm Password doesn't match";
            }
        }
        else
        {
            $error = "Please fill all the criteria.";
        }
    }
    require_once("header.php");
    require_once("sidebar.php");
?>

<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-subheader">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">Password</h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__item">
                        <a href="" class="m-nav__link">
                            <span class="m-nav__link-text">Change Password</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="m-content" style="padding-top:10px;">
        <div class="m-portlet">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            CHANGE PASSWORD
                        </h3>
                    </div>
                </div>
            </div>
            <div class="m-portlet">
                <div class="m-portlet__body">
                    <form class="m-form m-form--fit m-form--label-align-right" action="" method="post">
                        <?php 
                        if(isset($success))
                        {
                            echo '<div class="alert alert-success" role="alert"> '.$success.'</div>';
                        }

                        if(isset($error))
                        {
                            echo '<div class="alert alert-danger" role="alert"> '.$error.' </div>';
                        }
                        ?>
                        <div class="form-group m-form__group row">
                            <label class="col-form-label col-lg-3 col-sm-12 m--font-danger">Old Password <span aria-required="true"> * </span> </label>
                            <div class="col-lg-6 col-md-9 col-sm-12">
                                <input type="password" class="form-control m-input" name="old_password" placeholder="Enter Old Password" required>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-form-label col-lg-3 col-sm-12 m--font-danger">New Password <span aria-required="true"> * </span> </label>
                            <div class="col-lg-6 col-md-9 col-sm-12">
                                <input type="password" class="form-control m-input" name="new_password" required placeholder="Enter New Password">
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-form-label col-lg-3 col-sm-12 m--font-danger">Confirm Password</label>
                            <div class="col-lg-6 col-md-9 col-sm-12">
                                <input type="password" class="form-control m-input" name="confirm_password" required placeholder="Enter Confirm Password">
                            </div>
                        </div>
                        <div class="m-portlet__foot m-portlet__foot--fit">
                            <div class="m-form__actions m-form__actions">
                                <div class="row">
                                    <div class="col-lg-9 ml-lg-auto">
                                        <button type="submit" id="edit_button" class="btn btn-brand">Submit</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php require('footer.php');?>