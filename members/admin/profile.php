<?php 
    require_once("../../includes/initialize.php");
    require_once("logincheck.php");

    if($_SERVER['REQUEST_METHOD'] == 'POST') {
        $fname = sanitize_input($_POST['fname']);
        $lname = sanitize_input($_POST['lname']);
        $email = sanitize_input($_POST['email']);
        $phone = sanitize_input($_POST['phone']);
        $user_id = USER_ID;

        $sql_update = "UPDATE employee SET 
            fname = '$fname',
            lname = '$lname',
            email = '$email',
            phone = '$phone'
        WHERE id = $user_id
        ";

        if(mysqli_query($con, $sql_update))
        {
            $_SESSION['success'] = 'Data updated succesfully';
        }
        else
        {
            $_SESSION['error'] = 'Data not updated';
        }
    }
    
    $sql_user = "SELECT fname, lname, phone, email FROM employee WHERE id = '".USER_ID."'";
    $result_user = mysqli_query($con, $sql_user);
    if($myrow_user = mysqli_fetch_array($result_user))
    {
        $fname = $myrow_user['fname'];
        $lname = $myrow_user['lname'];
        $email = $myrow_user['email'];
        $phone = $myrow_user['phone'];
    }


    $error = $_SESSION['error'];
    $success = $_SESSION['success'];

    if($success) {
        unset($_SESSION['success']);
    }
    if($error) {
        unset($_SESSION['error']);
    }
    
    require_once("header.php");
    require_once("sidebar.php");
?>

<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-subheader">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">Profile</h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__item">
                        <a href="" class="m-nav__link">
                            <span class="m-nav__link-text">Profile</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="m-content" style="padding-top:10px;">
        <?php 
            if(isset($success))
            {
                echo '<div class="alert alert-success" role="alert"> '.$success.'</div>';
            }

            if(isset($error))
            {
                echo '<div class="alert alert-danger" role="alert"> '.$error.' </div>';
            }
        ?>
        <form class="m-form m-form--fit m-form--label-align-right" method="POST" id="profile">
            <div class="m-portlet m-portlet--tabs">
                <div class="m-portlet__body">
                    <div class="form-row">
                        <div class="form-group col-lg-4 m-form__group-sub col-lg-cust">
                            <label>First Name<span class="m--font-danger">*</span></label>
                            <input class="form-control" name="fname" placeholder="First Name" value="<?php echo $fname; ?>" required>
                        </div>
                        <div class="form-group col-lg-4 m-form__group-sub col-lg-cust">
                            <label>Last Name<span class="m--font-danger">*</span></label>
                            <input class="form-control" name="lname" value="<?php echo $lname; ?>" placeholder="Last Name" required>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-lg-4 m-form__group-sub col-lg-cust">
                            <label>Email<span class="m--font-danger">*</span></label>
                            <input class="form-control" name="email" placeholder="Email" value="<?php echo $email; ?>" required>
                        </div>
                        <div class="form-group col-lg-4 m-form__group-sub col-lg-cust">
                            <label>Phone<span class="m--font-danger">*</span></label>
                            <input class="form-control" name="phone" value="<?php echo $phone; ?>" placeholder="Phone" required>
                        </div>
                    </div>
                    <div class="form-group col-lg-cust">
                        <button class="btn btn-success">Update</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<?php require('footer.php');?>