<?php 
    require_once("../../includes/initialize.php");
    require_once("logincheck.php");
    $category = sanitize_input($_POST['category']);
    $date =  date('Y-m-d H:i:s');
    $success = "";
    $error = "";
    $status="activate";
    $status="deactivate";
    require_once("header.php");
    require_once("sidebar.php");
?>

<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">List Transaction</h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__item m-nav__item--home">
                        <a href="" class="m-nav__link">
                            <span class="m-nav__link-text">Manage Transaction</span>
                        </a>
                    </li>
                   <li class="m-nav__separator">-</li>
                     <li class="m-nav__item">
                        <a href="" class="m-nav__link">
                            <span class="m-nav__link-text">List Order Transaction</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
   
    <div class="m-content">
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__body">
    <table class="table table-striped- table-bordered table-hover table-checkable" id="order_trans_list"> 
                    <thead>
                        <tr>
                            <th>Order ID</th>
                            <th>Transaction id</td>
                            <th>Amount</td>
                            <th>Coupon</td>
                            <th>Coupon Amount</th>
                            <th>Total Amount</th>
                            <th>Status</th>
                            <th>Date</th>
                        </tr>
                    </thead>
                    <tbody>
                  
                    </tbody>
                </table>
</div>
</div>
        </div>
    </div>

<?php require('footer.php');?>


