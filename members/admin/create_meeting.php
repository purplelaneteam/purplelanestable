<?php 
    require_once("../../includes/initialize.php");
    require_once("logincheck.php");
    require_once("header.php");
    require_once("sidebar.php");
    $action="";
    $topic_id="";
    $name="";
    $category_id="";
 
    if(isset($_GET['id']) && !empty($_GET['id']))
    {
        $action="edit";
        $topic_id=sanitize_input($_GET["id"]);
        $sql_select="SELECT id,name,category_id FROM topic WHERE id='$topic_id'";
        $sql_result=mysqli_query($con,$sql_select);
        if($sql_result)
        {
            while($myrow=mysqli_fetch_array($sql_result))
            {
                $name=$myrow['name'];
                $category_id=$myrow['category_id'];
            }
        }
        else 
        {
            header("Location:list_topic.php");
        }
    }
    else 
    {
        $action="add";
        $topic_id="";
    }
    $mentors = getMentorByTopics($con,'2');
?>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-subheader">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">Meetings</h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__item">
                        <a href="#" class="m-nav__link">
                            <span class="m-nav__link-text">Meeting</span>
                        </a>
                    </li>
                    <li class="m-nav__separator">-</li>
                    <li class="m-nav__item">
                        <a href="javascript:void(0)" class="m-nav__link">
                            <span class="m-nav__link-text">Create Meeting</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div style="display:none;" id="alert_div_basic" class="alert alert-danger alert-dismissible fade show" role="alert">
        <p>Danger</p>
        <button style="margin-top: -30px;" type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true" style="font-size: 31px;line-height: 0;">&times;</span>
        </button>
    </div>
    <div class="m-content" style="padding-top:10px;">
        <form class="m-form m-form--fit m-form--label-align-right" id="create_appointment_form" method="POST"
            enctype=multipart/form-data> <div class="m-portlet m-portlet--tabs">
                <input type="text" id="mentor_id" name="mentor_id" hidden value>
            <div class="m-portlet__body">
                <div class="form-row" id="category_div">
                    <div class="form-group col-lg-4 m-form__group-sub col-lg-cust" id="category_id_div">
                        <label for="category">Category<span class="m--font-danger">*</span></label>
                        <select class="form-control category_id" required name="category_id" id="category_id">
                            <option disabled selected>Select Category</option>
                            <?php
                                $sql_category_list = "SELECT id, name, parent_id FROM category WHERE parent_id IS NULL AND active = 1 ORDER BY name ASC";
                                $result_category_list = mysqli_query($con, $sql_category_list);
                                if($result_category_list && $myrow_category_list = mysqli_fetch_array($result_category_list))
                                {
                                    do
                                    {
                                      
                                    ?>

                            <option value="<?php echo $myrow_category_list['id'];?>">
                                <?php echo $myrow_category_list["name"]?>
                            </option>
                            <?php
                                    }
                                    while($myrow_category_list = mysqli_fetch_array($result_category_list));
                                }
                            ?>
                        </select>
                    </div>
                    <div class="form-group col-lg-4 m-form__group-sub col-lg-cust " style="display:none;"
                        id="sub_category_id_div">
                        <label for="category">Sub Category<span class="m--font-danger">*</span></label>
                        <select class="form-control sub_category_id" required name="sub_category_id"
                            id="sub_category_id">
                            <option value="" disabled selected>Select Sub Category</option>

                        </select>
                    </div>
                    <div class="form-group col-lg-4 m-form__group-sub col-lg-cust " style="display:none;"
                        id="topic_id_div">
                        <label for="category">Topic<span class="m--font-danger">*</span></label>
                        <select class="form-control category_id" required name="topic_id" id="topic_id">
                            <option disabled selected>Select Topic</option>

                        </select>
                    </div>
                </div>

                <div class="col-md-9 col-sm-12" id="mentor_info_div" style="display:none">
                    <div class="row mb-5">
                        <div class="col-md-5 col-sm-5  ">
                            <div id="picker1"> </div>
                            <input type="hidden" name="slot1" id="slot1" value="" />
                        </div>
                        <div class="col-md-5 col-sm-5  ">
                            <div id="picker2"> </div>
                            <input type="hidden" name="slot2" id="slot2" value="" />
                        </div>
                    </div>
                    <div class="store-btns" id='mentor_info'><br><br>

                    </div>
                    <div class="row">
                        <div class="form-group col-lg-6 m-form__group-sub col-lg-cust ">
                            <label for="email">Mentee Email<span class="m--font-danger">*</span></label>
                            <input id="email" class="form-control" type="text" name="email" placeholder="Email..." value="" >
                            
                        </div>
                        <div class="form-group col-lg-6 m-form__group-sub col-lg-cust ">
                            <label for="title">Title<span class="m--font-danger">*</span></label>
                            <input id="title" class="form-control" type="text" name="name" placeholder="Title" value="" >
                            
                        </div>
                        <div class="form-group col-lg-12 m-form__group-sub col-lg-cust">
                            <label for="comments">Comments<span class="m--font-danger">*</span></label>
                            <textarea class="form-control" name="comment" id="comment" required rows="3"></textarea>
                        </div>
                    </div>
                </div>

                <!-- <div class="form-group col-lg-cust">
                    <input type="hidden" name="id" id="id" value="<?php echo $topic_id ?>" />
                    <input type="hidden" name="action" id="action" value="<?php echo $action ?>" />
                    <button class="btn btn-success">Submit</button>
                </div> -->
            </div>
    </div>
    </form>
</div>
</div>


<?php require('footer.php');?>
<script>
    $('#is_category').click(function () {

        var element = document.getElementsByClassName("category");
        if ($(this).prop("checked") == true) {
            $('#category_div').show();
            for (var i = 0; i < element.length; i++)
                $('.category').attr("required", true);

        } else {
            $('#category_div').hide();
            $('.category').attr("required", false);
        }
    });
    $('.category').change(function () {
        $('#category_id').val($(this).val());
    })

    $(function () {
        /* Initial */
        $('#sub_category_id_div').hide()
        $('#topic_id_div').hide()
        $('#mentor_info_div').hide()


        $('#category_id').change(function () {
            const id = $(this).val()
            $('#sub_category_id').html('<option disabled selected>Select Sub Category</option>')

            fetch(`/members/admin/processreq/proc_get_sub_category.php?id=${id}`)
                .then(res => res.json())
                .then(res => {
                    $('#sub_category_id_div').show()
                    let options = res.sub_categories.map(sub_category => {
                        return `
                        <option value="${sub_category.id}">
                            ${sub_category.name}
                        </option>
                    `
                    })
                    $('#sub_category_id').append(options)
                })
        })
        $('#sub_category_id').change(function () {
            const id = $(this).val()
            $('#topic_id').html('<option disabled selected>Select Topic</option>')

            fetch(`/members/admin/processreq/proc_get_topics.php?id=${id}`)
                .then(res => res.json())
                .then(res => {
                    $('#topic_id_div').show()
                    let options = res.topics.map(topic => {
                        return `
                        <option value="${topic.id}">
                            ${topic.name}
                        </option>
                    `
                    })
                    $('#topic_id').append(options)
                })
        })
        $('#topic_id').change(function () {
            const id = $(this).val()
            
            fetch(`/members/admin/processreq/proc_get_mentor_by_topic.php?id=${id}`)
                .then(res => res.json())
                .then(res => {
                    $('#mentor_info_div').show()
                    $('#mentor_info').html(res.mentors);

                    $('.mentor-name-div').click(function (e) {
                        e.preventDefault()
                    })

                    $('.book-meeting-button').click(function (e) {
                        e.preventDefault()
                        const mentor_id = $(this).attr('data-id');
                        //console.log(mentor_id);
                        
                        $('#mentor_id').val(mentor_id)
                        $('#create_appointment_form').submit();
                    })
                    
                })

            $('#create_appointment_form').submit(function(e) {
                e.preventDefault();
                var formData = new FormData(this);
                var mentor_id = $('#mentor_id').val()
                var topic_id = $('#topic_id').val()

                var slot1 = this.slot1.value;
                var slot2 = this.slot2.value;
                console.log($(this).serialize());
                

                if (slot1 !== slot2) {
                    $.ajax({
                        type: 'POST',
                        url: url_path +
                            'members/admin/processreq/proc_create_appointment_req.php',
                        dataType: 'json',
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function (response) {
                            if (response.code == 0) {
                                swal({
                        title: "Success!",
                        text: response.msg,
                        type: "success",
                        
                    }).then(function(){
                        location.reload()
                        //location.href = "/members/admin/list_meeting.php?status=pending";
                    })
                                
                            } else {
                                $('#alert_div_basic p').html(response.msg);
                                $('#alert_div_basic').fadeIn();
                            }
                        },
                        error: function (response) {

                        }
                    });
                } else {
                    $('#alert_div_basic p').html('Both time slots cannot be same');
                    $('#alert_div_basic').fadeIn();

                }
            })
        })
    })
</script>