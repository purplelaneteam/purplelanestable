<div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-dark ">
    <div id="m_ver_menu" class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark " m-menu-vertical="1"
        m-menu-scrollable="1" m-menu-dropdown-timeout="500" style="position: relative;">
        <ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
            
            <li class="m-menu__item  m-menu__item--submenu <?php if(stristr($scriptname, "add_card_type.php") || stristr($scriptname, "cost_cardtype_price.php")  ): echo ' m-menu__item--open m-menu__item--expanded';  endif;?>"
                aria-haspopup="true" m-menu-submenu-toggle="hover">
                <a href="javascript:;" class="m-menu__link m-menu__toggle  active">
                    <i class="m-menu__link-icon flaticon-add"></i>
                    <span class="m-menu__link-text">Manage Cost Card</span>
                    <i class="m-menu__ver-arrow la la-angle-right"></i>
                </a>
                <div class="m-menu__submenu ">
                    <span class="m-menu__arrow"></span>
                    <ul class="m-menu__subnav">
                        <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true">
                            <span class="m-menu__link">
                                <span class="m-menu__link-text">Manage Cost Card</span>
                            </span>
                        </li>
                        <li class="m-menu__item <?php if(stristr($scriptname, "add_card_type.php")): echo 'm-menu__item--expanded m-menu__item--open  m-menu__item--active';  endif;?>"
                            aria-haspopup="true">
                            <a href="add_card_type.php" class="m-menu__link ">
                                <i class="m-menu__link-icon flaticon-plus">
                                    <span></span>
                                </i>
                                <span class="m-menu__link-text">Cost Card Type</span>
                            </a>
                        </li>
                        <!-- <li class="m-menu__item <?php if(stristr($scriptname, "cost_cardtype_price.php")  ): echo 'm-menu__item--expanded m-menu__item--open  m-menu__item--active';  endif;?>"
                            aria-haspopup="true" m-menu-link-redirect="1">
                            <a href="cost_cardtype_price.php" class="m-menu__link ">
                                <i class="m-menu__link-icon flaticon-list">
                                    <span></span>
                                </i>
                                <span class="m-menu__link-text">Cost Card Type Price</span>
                            </a>
                        </li> -->
                        
                    </ul>
                </div>
            </li>

            <li class="m-menu__item  m-menu__item--submenu <?php if(stristr($scriptname, "list_category.php") || stristr($scriptname, "add_category.php")  ): echo ' m-menu__item--open m-menu__item--expanded';  endif;?>"
                aria-haspopup="true" m-menu-submenu-toggle="hover">
                <a href="javascript:;" class="m-menu__link m-menu__toggle  active">
                    <i class="m-menu__link-icon fab fa-accusoft"></i>
                    <span class="m-menu__link-text">Manage Category</span>
                    <i class="m-menu__ver-arrow la la-angle-right"></i>
                </a>
                <div class="m-menu__submenu ">
                    <span class="m-menu__arrow"></span>
                    <ul class="m-menu__subnav">
                        <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true">
                            <span class="m-menu__link">
                                <span class="m-menu__link-text">Manage Category</span>
                            </span>
                        </li>
                        <li class="m-menu__item <?php if(stristr($scriptname, "add_category.php")): echo 'm-menu__item--expanded m-menu__item--open  m-menu__item--active';  endif;?>"
                            aria-haspopup="true">
                            <a href="add_category.php" class="m-menu__link ">
                                <i class="m-menu__link-icon flaticon-plus">
                                    <span></span>
                                </i>
                                <span class="m-menu__link-text">Add Category</span>
                            </a>
                        </li>
                        <li class="m-menu__item <?php if(stristr($scriptname, "list_category.php")  ): echo 'm-menu__item--expanded m-menu__item--open  m-menu__item--active';  endif;?>"
                            aria-haspopup="true" m-menu-link-redirect="1">
                            <a href="list_category.php" class="m-menu__link ">
                                <i class="m-menu__link-icon flaticon-list">
                                    <span></span>
                                </i>
                                <span class="m-menu__link-text">List Category</span>
                            </a>
                        </li>
                        
                    </ul>
                </div>
            </li>
            <li class="m-menu__item  m-menu__item--submenu <?php if(stristr($scriptname, "list_topic.php") || stristr($scriptname, "add_edit_topic.php")  ): echo ' m-menu__item--open m-menu__item--expanded';  endif;?>"
                aria-haspopup="true" m-menu-submenu-toggle="hover">
                <a href="javascript:;" class="m-menu__link m-menu__toggle  active">
                    <i class="m-menu__link-icon fab fa-affiliatetheme"></i>
                    <span class="m-menu__link-text">Manage Topic</span>
                    <i class="m-menu__ver-arrow la la-angle-right"></i>
                </a>
                <div class="m-menu__submenu ">
                    <span class="m-menu__arrow"></span>
                    <ul class="m-menu__subnav">
                        <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true">
                            <span class="m-menu__link">
                                <span class="m-menu__link-text">Add/Edit Topic</span>
                            </span>
                        </li>
                        <li class="m-menu__item <?php if(stristr($scriptname, "add_edit_topic.php")): echo 'm-menu__item--expanded m-menu__item--open  m-menu__item--active';  endif;?>"
                            aria-haspopup="true">
                            <a href="add_edit_topic.php" class="m-menu__link ">
                                <i class="m-menu__link-icon flaticon-plus">
                                    <span></span>
                                </i>
                                <span class="m-menu__link-text">Add/Edit Topic</span>
                            </a>
                        </li>
                        <li class="m-menu__item <?php if(stristr($scriptname, "list_topic.php")  ): echo 'm-menu__item--expanded m-menu__item--open  m-menu__item--active';  endif;?>"
                            aria-haspopup="true" m-menu-link-redirect="1">
                            <a href="list_topic.php" class="m-menu__link ">
                                <i class="m-menu__link-icon flaticon-list">
                                    <span></span>
                                </i>
                                <span class="m-menu__link-text">List Topic</span>
                            </a>
                        </li>
                        
                    </ul>
                </div>
            </li>
            <li class="m-menu__item  m-menu__item--submenu <?php if(stristr($scriptname, "list_mentor.php") || stristr($scriptname, "add_mentor.php") || stristr($scriptname, "edit_mentor.php") ): echo ' m-menu__item--open m-menu__item--expanded';  endif;?>"
                aria-haspopup="true" m-menu-submenu-toggle="hover">
                <a href="javascript:;" class="m-menu__link m-menu__toggle  active">
                    <i class="m-menu__link-icon flaticon-user"></i>
                    <span class="m-menu__link-text">Manage Mentor</span>
                    <i class="m-menu__ver-arrow la la-angle-right"></i>
                </a>
                <div class="m-menu__submenu ">
                    <span class="m-menu__arrow"></span>
                    <ul class="m-menu__subnav">
                        <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true">
                            <span class="m-menu__link">
                                <span class="m-menu__link-text">Manage Mentor</span>
                            </span>
                        </li>
                        <li class="m-menu__item <?php if(stristr($scriptname, "add_mentor.php")): echo 'm-menu__item--expanded m-menu__item--open  m-menu__item--active';  endif;?>"
                            aria-haspopup="true">
                            <a href="add_mentor.php" class="m-menu__link ">
                                <i class="m-menu__link-icon flaticon-plus">
                                    <span></span>
                                </i>
                                <span class="m-menu__link-text">Add Mentor</span>
                            </a>
                        </li>
                        <li class="m-menu__item <?php if(stristr($scriptname, "list_mentor.php")  ): echo 'm-menu__item--expanded m-menu__item--open  m-menu__item--active';  endif;?>"
                            aria-haspopup="true" m-menu-link-redirect="1">
                            <a href="list_mentor.php" class="m-menu__link ">
                                <i class="m-menu__link-icon flaticon-list">
                                    <span></span>
                                </i>
                                <span class="m-menu__link-text">List Mentor</span>
                            </a>
                        </li>
                        
                    </ul>
                </div>
            </li>
            <li class="m-menu__item  m-menu__item--submenu <?php if(stristr($scriptname, "list_meeting.php")  || stristr($scriptname, "create_meeting.php")): echo ' m-menu__item--open m-menu__item--expanded';  endif;?>"
                aria-haspopup="true" m-menu-submenu-toggle="hover">
                <a href="javascript:;" class="m-menu__link m-menu__toggle  active">
                    <i class="m-menu__link-icon fa fa-handshake"></i>
                    <span class="m-menu__link-text">Manage Meetings</span>
                    <i class="m-menu__ver-arrow la la-angle-right"></i>
                </a>
                <div class="m-menu__submenu ">
                    <span class="m-menu__arrow"></span>
                    <ul class="m-menu__subnav">
                        <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true">
                            <span class="m-menu__link">
                                <span class="m-menu__link-text">List Meetings</span>
                            </span>
                        </li>
                        <li class="m-menu__item <?php if(stristr($scriptname, "create_meeting.php")): echo 'm-menu__item--expanded m-menu__item--open  m-menu__item--active';  endif;?>"
                            aria-haspopup="true">
                            <a href="create_meeting.php" class="m-menu__link ">
                                <i class="m-menu__link-icon flaticon-plus">
                                    <span></span>
                                </i>
                                <span class="m-menu__link-text">Create Meeting</span>
                            </a>
                        </li>
                        <li class="m-menu__item <?php if(stristr($scriptname, "list_meeting.php?status=scheduled")): echo 'm-menu__item--expanded m-menu__item--open  m-menu__item--active';  endif;?>"
                            aria-haspopup="true">
                            <a href="list_meeting.php?status=scheduled" class="m-menu__link ">
                                <i class="m-menu__link-icon flaticon-signs-1">
                                    <span></span>
                                </i>
                                <span class="m-menu__link-text">Scheduled Meetings</span>
                            </a>
                        </li>
                        <li class="m-menu__item <?php if(stristr($scriptname, "list_meeting.php?status=pending")): echo 'm-menu__item--expanded m-menu__item--open  m-menu__item--active';  endif;?>"
                            aria-haspopup="true">
                            <a href="list_meeting.php?status=pending" class="m-menu__link ">
                                <i class="m-menu__link-icon flaticon-signs-1">
                                    <span></span>
                                </i>
                                <span class="m-menu__link-text">Requested Meetings</span>
                            </a>
                        </li>
                        <li class="m-menu__item <?php if(stristr($scriptname, "list_meeting.php?status=rescheduled")): echo 'm-menu__item--expanded m-menu__item--open  m-menu__item--active';  endif;?>"
                            aria-haspopup="true">
                            <a href="list_meeting.php?status=rescheduled" class="m-menu__link ">
                                <i class="m-menu__link-icon flaticon-signs-1">
                                    <span></span>
                                </i>
                                <span class="m-menu__link-text">Rescheduled Meetings</span>
                            </a>
                        </li>
                        <li class="m-menu__item <?php if(stristr($scriptname, "list_meeting.php?status=completed")): echo 'm-menu__item--expanded m-menu__item--open  m-menu__item--active';  endif;?>"
                            aria-haspopup="true">
                            <a href="list_meeting.php?status=completed" class="m-menu__link ">
                                <i class="m-menu__link-icon flaticon-signs-1">
                                    <span></span>
                                </i>
                                <span class="m-menu__link-text">Completed Meetings</span>
                            </a>
                        </li>
                       
                        <li class="m-menu__item <?php if(stristr($scriptname, "list_meeting.php?status=cancelled")): echo 'm-menu__item--expanded m-menu__item--open  m-menu__item--active';  endif;?>"
                            aria-haspopup="true">
                            <a href="list_meeting.php?status=cancelled" class="m-menu__link ">
                                <i class="m-menu__link-icon flaticon-signs-1">
                                    <span></span>
                                </i>
                                <span class="m-menu__link-text">Rejected Meetings</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            <li class="m-menu__item  m-menu__item--submenu <?php if(stristr($scriptname, "list_mentee_transc.php") || stristr($scriptname, "list_admin_transc.php") || stristr($scriptname, "list_taxes_transc.php") || stristr($scriptname, "list_mentor_transc.php") || stristr($scriptname, "list_ordes_transc.php")): echo ' m-menu__item--open m-menu__item--expanded';  endif;?>"
                aria-haspopup="true" m-menu-submenu-toggle="hover">
                <a href="javascript:;" class="m-menu__link m-menu__toggle  active">
                    <i class="m-menu__link-icon flaticon-coins"></i>
                    <span class="m-menu__link-text">List Transcations</span>
                    <i class="m-menu__ver-arrow la la-angle-right"></i>
                </a>
                <div class="m-menu__submenu ">
                    <span class="m-menu__arrow"></span>
                    <ul class="m-menu__subnav">
                        <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true">
                            <span class="m-menu__link">
                                <span class="m-menu__link-text">List Transcations</span>
                            </span>
                        </li>
                        <li class="m-menu__item <?php if(stristr($scriptname, "list_mentee_transc.php")): echo 'm-menu__item--expanded m-menu__item--open  m-menu__item--active';  endif;?>"
                            aria-haspopup="true">
                            <a href="list_mentee_transc.php" class="m-menu__link ">
                                <i class="m-menu__link-icon flaticon-signs-1">
                                    <span></span>
                                </i>
                                <span class="m-menu__link-text">Mentee Transcations</span>
                            </a>
                        </li>
                        <li class="m-menu__item <?php if(stristr($scriptname, "list_mentor_transc.php")): echo 'm-menu__item--expanded m-menu__item--open  m-menu__item--active';  endif;?>"
                            aria-haspopup="true">
                            <a href="list_mentor_transc.php" class="m-menu__link ">
                                <i class="m-menu__link-icon flaticon-signs-1">
                                    <span></span>
                                </i>
                                <span class="m-menu__link-text">Mentor Transcations</span>
                            </a>
                        </li>
                        <li class="m-menu__item <?php if(stristr($scriptname, "list_admin_transc.php")): echo 'm-menu__item--expanded m-menu__item--open  m-menu__item--active';  endif;?>"
                            aria-haspopup="true">
                            <a href="list_admin_transc.php" class="m-menu__link ">
                                <i class="m-menu__link-icon flaticon-signs-1">
                                    <span></span>
                                </i>
                                <span class="m-menu__link-text">Admin Transcations</span>
                            </a>
                        </li>
                        <li class="m-menu__item <?php if(stristr($scriptname, "list_taxes_transc.php")): echo 'm-menu__item--expanded m-menu__item--open  m-menu__item--active';  endif;?>"
                            aria-haspopup="true">
                            <a href="list_taxes_transc.php" class="m-menu__link ">
                                <i class="m-menu__link-icon flaticon-signs-1">
                                    <span></span>
                                </i>
                                <span class="m-menu__link-text">Taxes Transcations</span>
                            </a>
                        </li>

                        <li class="m-menu__item <?php if(stristr($scriptname, "list_ordes_transc.php")): echo 'm-menu__item--expanded m-menu__item--open  m-menu__item--active';  endif;?>"
                            aria-haspopup="true">
                            <a href="list_ordes_transc.php" class="m-menu__link ">
                                <i class="m-menu__link-icon flaticon-signs-1">
                                    <span></span>
                                </i>
                                <span class="m-menu__link-text">Orders Transcations</span>
                            </a>
                        </li>
                        
                    </ul>
                </div>
            </li>
            <li class="m-menu__item  m-menu__item--submenu <?php if(stristr($scriptname, "add_setting.php")): echo ' m-menu__item--open m-menu__item--expanded';  endif;?>"
                aria-haspopup="true" m-menu-submenu-toggle="hover">
                <a href="add_setting.php" class="m-menu__link m-menu__toggle  active">
                    <i class="m-menu__link-icon flaticon-coins"></i>
                    <span class="m-menu__link-text">Manage Settings</span>
                    <i class="m-menu__ver-arrow la la-angle-right"></i>
                </a>
                
            </li>
        </ul>
    </div>
</div>