<?php 
    require_once("../../includes/initialize.php");
    require_once("logincheck.php");
    $erroroccured = 0;
    $errormsg = "";
    $action="add";  
    $tab = $_GET['tab'];

    $categories_sql = "SELECT id,name FROM category WHERE parent_id IS NULL";
    $categories_result = mysqli_query($con, $categories_sql);
    $categories = [];
    foreach($categories_result as $category_result_value){
        $categories[] = $category_result_value;
    }

    $companies = [];
    $sql_get_companies = "SELECT id, name, codename FROM company";
    $result_get_companies = mysqli_query($con, $sql_get_companies);
    while($myrow_get_company = mysqli_fetch_array($result_get_companies))
    {
        $companies[] = array("id"=>$myrow_get_company["id"],
                            "name"=>$myrow_get_company["name"],
                            "codename"=>$myrow_get_company["codename"]
        );
    }

    $skills = [];
    $sql_get_skills = "SELECT id, name, codename FROM skill";
    $result_get_skills = mysqli_query($con, $sql_get_skills);
    while($myrow_get_skill = mysqli_fetch_array($result_get_skills))
    {
        $skills[] = array("id"=>$myrow_get_skill["id"],
                            "name"=>$myrow_get_skill["name"],
                            "codename"=>$myrow_get_skill["codename"]
        );
    }

    if(isset($_GET['mentor_id']))
    {
        $mentor_id = sanitize_input($_GET['mentor_id']);
        $edit_query="SELECT fname, description,lname ,email,mobile,paytm_mobile,total_experience,profile_pic,cost_card_type_id,overided_price FROM mentor WHERE id='$mentor_id'";
        //   echo $edit_query;exit;
        if(!$edit_mentor=mysqli_query($con,$edit_query))
        {
            $erroroccured = 1;
            $errormsg = "ERROR_CUST_00:Something went wrong";
        }
        else
        {
            if($result_edit_mentor=mysqli_fetch_array($edit_mentor))
            {
                
                $fname=$result_edit_mentor['fname'];
                $lname=$result_edit_mentor['lname'];
                $email=$result_edit_mentor['email'];
                $mobile=$result_edit_mentor['mobile'];
                $paytm_mobile=$result_edit_mentor['paytm_mobile'];
                $description=$result_edit_mentor['description'];
                $total_experience=$result_edit_mentor['total_experience'];
                $profile_pic= MENTOR_IMAGE_UPLOAD_FOLDER_LINK.$result_edit_mentor['profile_pic'];
                $cost_card_type_id=$result_edit_mentor['cost_card_type_id'];
                $overrided_price=$result_edit_mentor['overided_price'];
                $bank = getMentorBankDeatils($con, $mentor_id);
                $education = getMentorEducation($con, $mentor_id);
                $experience = getMentorExperience($con, $mentor_id);
                
            }
        }
        $action="edit";

        $query_company = "SELECT company_id FROM mentor_company WHERE mentor_id='".$mentor_id."' ORDER BY id";
        $result_company = mysqli_query($con, $query_company);
        $row_company = mysqli_fetch_assoc($result_company);

        if($row_company == ''){
            $company_id = '';

        }else{
            $company_id = $row_company['company_id'];
        }

        $skills_selected = [];
        $query_skills_selected = "SELECT skill_id FROM mentor_skill WHERE mentor_id='".$mentor_id."'";
        $result_get_skills_selected = mysqli_query($con, $query_skills_selected);
        while($myrow_get_skills_selected = mysqli_fetch_array($result_get_skills_selected))
        {
            $skills_selected[] = $myrow_get_skills_selected["skill_id"];
        }

        $topics_sql = "SELECT mc.id, t.name as topic_name, c.id as cat_id, c.name as cat_name, mc.addedon as addedon FROM mentor_categories mc INNER JOIN topic t ON mc.topic_id = t.id INNER JOIN category c ON t.category_id = c.id WHERE mentor_id = $mentor_id";
        $topics_result = mysqli_query($con, $topics_sql);
        $topics = [];
        foreach($topics_result as $topic_result_value){
            $topics[] = $topic_result_value;
        }
    }
    $levels=getLevel($con);
    require_once("header.php"); 
    require_once("sidebar.php");
?>
<?php
if($erroroccured==1)
{
?>
<div class="alert alert-danger" role="alert">
    <?php echo $errormsg;?>
</div>
<?php
exit;
}
?>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-subheader">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">Edit Mentor</h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__item">
                        <a href="#" class="m-nav__link">
                            <span class="m-nav__link-text">Manage Mentor</span>
                        </a>
                    </li>
                    <li class="m-nav__separator">-</li>
                    <li class="m-nav__item">
                        <a href="javascript:void(0)" class="m-nav__link">
                            <span class="m-nav__link-text">Edit Mentor</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="m-content" style="padding-top:10px;">
        <div class="m-portlet m-portlet--tabs">
            <div class="m-portlet__head">
                <div class="m-portlet__head-tools">
                    <ul class="nav nav-tabs m-tabs m-tabs-line m-tabs-line--brand m-tabs-line-danger" role="tablist">
                        <li class="nav-item m-tabs__item">
                            <a class="nav-link m-tabs__link <?php if($tab == '') echo 'active'; ?>" data-toggle="tab"
                                href="#m_portlet_tab_1_1" role="tab">
                                Basic Info
                            </a>
                        </li>
                        <li class="nav-item m-tabs__item">
                            <a class="nav-link m-tabs__link <?php if($tab == 'edit_tab') echo 'active'; ?>"
                                data-toggle="tab" href="#m_portlet_tab_1_2" role="tab">
                                Mentor Topics
                            </a>
                        </li>
                        <li class="nav-item m-tabs__item">
                            <a class="nav-link m-tabs__link <?php if($tab == 'edit_tab') echo 'active'; ?>"
                                data-toggle="tab" href="#bank_account" role="tab">
                                Bank account details
                            </a>
                        </li>
                        <li class="nav-item m-tabs__item">
                            <a class="nav-link m-tabs__link <?php if($tab == 'edit_tab') echo 'active'; ?>"
                                data-toggle="tab" href="#education" role="tab">
                                Education
                            </a>
                        </li>
                        <li class="nav-item m-tabs__item">
                            <a class="nav-link m-tabs__link <?php if($tab == 'edit_tab') echo 'active'; ?>"
                                data-toggle="tab" href="#experience" role="tab">
                                Experience
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="m-portlet__body">
                <div class="tab-content">
                    <div class="tab-pane <?php if($tab == '') echo 'active'; ?>" id="m_portlet_tab_1_1">
                        <form class="m-form m-form--fit m-form--label-align-right" id="mentor_form" method="POST"
                            enctype=multipart/form-data>
                            <div class="form-row">
                                <input type="hidden" class="form-control" name="mentor_id" id="mentor_id"
                                    value="<?php echo $mentor_id ?>" placeholder="mentor_id">
                                <div class="form-group col-lg-6 m-form__group-sub col-lg-cust">
                                    <label for="name">First Name<span class="m--font-danger">*</span></label>
                                    <input class="form-control" name="fname" id="fname" placeholder="First Name"
                                        value="<?php echo $fname ?>" required>
                                </div>
                                <div class="form-group col-lg-6 m-form__group-sub col-lg-cust">
                                    <label for="name">Last Name<span class="m--font-danger">*</span></label>
                                    <input class="form-control" name="lname" id="lname" placeholder="Last Name"
                                        value="<?php echo $lname ?>" required>
                                </div>
                                <div class="form-group col-lg-6 m-form__group-sub col-lg-cust">
                                    <label for="Email">Email <span class="m--font-danger">*</span></label>
                                    <input class="form-control" name="email" id="email" placeholder="Email"
                                        value="<?php echo $email ?>" required>
                                </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-lg-6 m-form__group-sub col-lg-cust">
                                <label for="Mobile">Mobile <span class="m--font-danger">*</span></label>
                                <input class="form-control" name="mobile" id="mobile" placeholder="Mobile Number"
                                    value="<?php echo $mobile ?>" required>
                            </div>
                            <div class="form-group col-lg-6 m-form__group-sub col-lg-cust">
                                <label for="Paytm_mobile">Paytm Mobile <span class="m--font-danger">*</span></label>
                                <input class="form-control" name="paytm_mobile" id="paytm_mobile" placeholder="Paytm Mobile"
                                    value="<?php echo $paytm_mobile ?>" required>
                            </div>
                            <div class="form-group col-lg-6 m-form__group-sub col-lg-cust">
                                <label for="Total_experience">Total Experience <span class="m--font-danger">*</span></label>
                                <input type="number" class="form-control" min="0" max="100" name="total_experience"
                                    id="total_experience" placeholder="Total Experience"
                                    value="<?php echo $total_experience ?>" required>
                            </div>
                            <div class="form-group col-lg-6 m-form__group-sub col-lg-cust">
                                <label for="Profile Pic">Profile Pic</label>
                                <input class="form-control" type="file" name="profile_pic" id="profile_pic"
                                    placeholder="Profile Pic">
                                <br />
                                <img style="width: 95px; height: 85px;" src="<?php echo $profile_pic ?>" />
                            </div>
                            <div class="form-group col-lg-6 m-form__group-sub col-lg-cust">
                                <label for="Total_experience">Cost Card Type<span class="m--font-danger">*</span></label>
                                <select class="form-control" name="cost_card_type" id="cost_card_type"
                                    placeholder="Add Cost Card Type" required>
                                    <?php
                                            foreach($levels as $level)
                                            {
                                                if($cost_card_type_id==$level['id'])
                                                {
                                                    $selected='selected';
                                                }
                                            ?>
                                    <option value="<?php echo $level['id'];?>" <?php if($selected == 'selected'){echo              'selected';$selected='';}
                                    ?>>
                                        <?php echo $level['name'];?> (<?php echo $level['price'];?>)</option>
                                    <?php
                                            }
                                            ?>
                                </select>
                            </div>
                            <div class="form-group col-lg-6 m-form__group-sub col-lg-cust">
                                <label for="Profile Pic">Give Him/Her price irrelent to cost card( in $)</label>
                                <input class="form-control" type="text" name="overided_price" id="overided_price"
                                    placeholder="Give Him/Her price irrelent to cost card"
                                    value="<?php echo $overrided_price ?>">
                            </div>
                            <div class="form-group col-lg-6 m-form__group-sub col-lg-cust">
                                <label for="Total_experience">Company <span class="m--font-danger">*</span></label>
                                <select class="form-control m-select2" name="company" id="company"
                                    placeholder="Add Cost Card Type" required>
                                    <option></option>
                                    <?php
                                            foreach($companies as $company)
                                            {
                                            ?>
                                    <option value="<?php echo $company['name'];?>"
                                        <?php echo checkSelected($company['id'], $company_id); ?>>
                                        <?php echo $company['name'];?></option>
                                    <?php
                                            }
                                            ?>
                                </select>
                            </div>
                            <div class="form-group col-lg-6 m-form__group-sub col-lg-cust">
                                <label for="Total_experience">Skills <span class="m--font-danger">*</span></label>
                                <select class="form-control m-select2" name="skill[]" id="skill" multiple="multiple">

                                    <?php
                                            foreach($skills as $skill)
                                            {
                                            ?>
                                    <option value="<?php echo $skill['name'];?>"
                                        <?php echo checkSelected($skill['id'], $skills_selected); ?>>
                                        <?php echo $skill['name'];?></option>
                                    <?php
                                            }
                                            ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group col-lg-12 m-form__group-sub col-lg-cust">
                            <label for="description">Description</label>
                            <textarea class="form-control" name="description" id="description" placeholder="Description about mentor."><?php echo $description; ?></textarea>
                        </div>

                        <div class="form-group col-lg-cust">
                            <input type="hidden" name="action" id="action" value="<?php echo $action ?>">
                            <button class="btn btn-success" type="submit">Submit</button>
                        </div>
                        </form>
                    </div>
                    <div class="tab-pane <?php if($tab == 'edit_tab') echo 'active'; ?>" id="m_portlet_tab_1_2">
                        <form class="m-form m-form--fit m-form--label-align-right" id="add_topic" method="POST"
                            enctype=multipart/form-data> <input type="hidden" name="mentor_id"
                            value="<?php echo $mentor_id;?>">
                            <div class="form-row" id="cat_topic_div">
                                <div class="form-group col-lg-4 m-form__group-sub col-lg-cust">
                                    <label for="name">Category <span class="m--font-danger">*</span></label>
                                    <select class="form-control category" required>
                                        <option value="">Select Category</option>
                                        <?php foreach ($categories as $category_key => $category_value) { ?>
                                        <option value="<?php echo $category_value['id']; ?>">
                                            <?php echo $category_value['name']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            
                            <div class="form-row">
                                <div class="form-group col-lg-cust">
                                    <a class="btn btn-secondary"
                                        href="edit_mentor.php?mentor_id=<?php echo $mentor_id;?>">Cancel</a>
                                    <input type="hidden" name="action" value="add">
                                    <button class="btn btn-success" type="submit">Add</button>
                                </div>
                            </div>
                        </form>

                        <div class="col-xs-12" style="margin-bottom: 30px;">
                            <hr>
                        </div>

                        <table class="table table-striped- table-bordered table-hover table-checkable" id="topic_listing">
                            <thead>
                                <tr>
                                    <th>Sr No. </th>
                                    <th>Topic</th>
                                    <th>Category</th>
                                    <th>Addedon</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                    foreach ($topics as $topic_key => $topic_data) {
                                    ?>
                                <tr>
                                    <td><?php echo ($topic_key + 1);?></td>
                                    <td><?php echo $topic_data["topic_name"];?></td>
                                    <td><?php $rtn_str=''; get_category_list($topic_data["cat_id"],$topic_data["cat_name"],$rtn_str); echo $rtn_str;?>
                                    </td>
                                    <td><?php echo date('d M Y', strtotime($topic_data["addedon"]));?></td>
                                    <td>
                                        <a href="javascript:void(0);" onclick="delemp(<?php echo $topic_data['id'];?>)"><i
                                                class="fas fa-trash-alt"></i></a>
                                    </td>
                                </tr>
                                <?php
                                    }
                                    ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="tab-pane" id="bank_account">
                        <form class="admin-edit-mentor" id="mentor_bank_account_admin">
                            <input type="hidden" name="mentor_id" value="<?php echo $mentor_id?>">
                            <div class="rounded-0">
                                    <div class="form-group">
                                        <label for="bank_name">Bank Name</label>
                                        <input type="text" class="input-field form-control" id="bank_name" name="name"
                                            placeholder="Bank Name" required
                                            value="<?php echo ($bank) ? $bank['bank_name']: '' ?>">
                                    </div>
                                    <div class="form-group">
                                        <label for="account_number">Account Number</label>
                                        <input type="text" class="input-field form-control" id="account_number" name="number"
                                            placeholder="Account Number" required
                                            value="<?php echo ($bank) ? $bank['account_number']: '' ?>">
                                    </div>
                                    <div class="form-group">
                                        <label for="ifsc">IFSC</label>
                                        <input type="text" class="input-field form-control" id="ifsc" name="ifsc"
                                            placeholder="IFS Code" required
                                            value="<?php echo ($bank) ? $bank['ifsc']: '' ?>">
                                    </div>
                                    <button type="submit" class="btn btn-alpha mr-lg-3 mr-2 cnect">
                                        Save Bank Account <i class="fa fa-angle-right ml-3"></i></button>
                            </div>
                        </form>
                    </div>
                    <div class="tab-pane" id="education">
                        <div class="jumbotron mentor-info">
                            <div class="info-container">
                                <div class="info-block">
                                    <div class="education">
                                        <?php foreach($education as $data): ?>
                                        <p class="title"><?php echo $data['name'] ?></p>
                                        <div class="description"><?php echo $data['description'] ?></div>
                                        <hr class="my-4">
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <form class="admin-edit-mentor" id="mentor_education_admin">
                            <input type="hidden" name="mentor_id" value="<?php echo $mentor_id?>">
                            <div class="rounded-0">
                                    <div class="form-group">
                                        <label for="education_name">Name</label>
                                        <input type="text" class="input-field form-control" id="education_name" name="name"
                                            placeholder="Education name" required
                                            value="">
                                    </div>
                                    <div class="form-group">
                                        <label for="education_description">Description</label>
                                        <textarea type="text" class="input-field form-control" id="education_description" name="description"
                                            placeholder="Description" row="4" col="4"></textarea>
                                    </div>
                                    <button type="submit" class="btn btn-alpha mr-lg-3 mr-2 cnect">
                                        Save Education <i class="fa fa-angle-right ml-3"></i></button>
                            </div>
                        </form>
                    </div>
                    <div class="tab-pane" id="experience">
                        <div class="jumbotron mentor-info">
                            <div class="info-container">
                                <div class="info-block">
                                    <div class="experience">
                                        <?php foreach($experience as $data): ?>
                                        <p class="title"><?php echo $data['name'] ?></p>
                                        <div class="description"><?php echo $data['description'] ?></div>
                                        <hr class="my-4">
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <form class="admin-edit-mentor" id="mentor_experience_admin">
                            <input type="hidden" name="mentor_id" value="<?php echo $mentor_id?>">
                            <div class="rounded-0">
                                    <div class="form-group">
                                        <label for="education_name">Name</label>
                                        <input type="text" class="input-field form-control" id="education_name" name="name"
                                            placeholder="Experience name" required
                                            value="">
                                    </div>
                                    <div class="form-group">
                                        <label for="education_description">Description</label>
                                        <textarea type="text" class="input-field form-control" id="education_description" name="description"
                                            placeholder="Description" row="4" col="4"></textarea>
                                    </div>
                                    <button type="submit" class="btn btn-alpha mr-lg-3 mr-2 cnect">
                                        Save Experience <i class="fa fa-angle-right ml-3"></i></button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php require('footer.php');?>

<script>
    $("#company").select2({
        placeholder: "Select a company",
        tags: true,
    })

    $("#skill").select2({
        placeholder: "Select skills",
        tags: true,
    })

    $('body').on('change', '.category', function () {
        var cat_id = $(this).val();
        console.log($(this).parent().nextAll('.sub_cat_div').remove())
        if (cat_id != '') {
            $.ajax({
                type: 'post',
                url: "processreq/load_category_subcat_topic.php",
                data: "cat_id=" + cat_id,
                success: function (response) {
                    if (response != "") {
                        $('#cat_topic_div').append(response);
                    }
                }
            });
        } else {
            $('.sub_cat_div').remove()
        }

    })

    $('#add_topic').submit(function (e) {
        e.preventDefault();
    }).validate({
        focusInvalid: true,
        invalidHandler: function (form, validator) {
            var errors = validator.numberOfInvalids();
            if (errors) {
                $("html, body").animate({
                    scrollTop: 0
                }, "fast");
            }
        },

        submitHandler: function (form) {
            var formData = new FormData(form);
            var button = $(form).find('button')
            var loading_class = 'm-loader m-loader--light m-loader--right'
            var button_text = $(button).text()
            $(button).html(`Processing`).addClass(loading_class).prop('disabled', true)
            $.ajax({
                type: 'POST',
                url: 'processreq/proc_add_mentor_topic.php',
                data: formData,
                dataType: "json",
                cache: false,
                contentType: false,
                processData: false,
                success: function (response) {
                    $(button).prop('disabled', false).html(button_text).removeClass(
                        loading_class)
                    if (response.code) {
                        swal('Error!', response.msg, 'error')
                    } else {
                        swal({
                            title: "Success!",
                            text: response.msg,
                            type: "success",
                        }).then(function () {
                            window.location.href = "edit_mentor.php?mentor_id=" +
                                '<?php echo $mentor_id;?>' + '&tab=edit_tab';
                        });
                    }
                },
                error: function (response) {
                    $(button).prop('disabled', false).html(button_text).removeClass(
                        loading_class)
                    swal('Error!', response.msg, 'error');
                }
            });
            $('#add_cardprice_form')[0].reset();
        }
    })

    $('#topic_listing').DataTable({
        "order": [
            [2, "asc"]
        ],
        "columnDefs": [{
            "orderable": false,
            "targets": 4
        }, ]
    });

    function delemp(empid) {
        swal({
            title: "Are you sure?",
            text: "You want to delete this entry!",
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Yes",
            cancelButtonText: "No",
            reverseButtons: !0
        }).then(function (e) {
            var action = "delete";
            $.ajax({
                url: "processreq/proc_add_mentor_topic.php",
                type: "POST",
                dataType: "json",
                data: {
                    action: action,
                    delid: empid
                },
                success: function (data) {
                    if (data.code) {
                        swal('Error!', data.msg, 'error')
                    } else {
                        swal({
                            title: "Success!",
                            text: data.msg,
                            type: "success",
                        }).then(function () {
                            window.location.href = "edit_mentor.php?mentor_id=" +
                                '<?php echo $mentor_id;?>' + '&tab=edit_tab';
                        });
                    }
                },
                error: function (response) {
                    swal('Error!', response.msg, 'error');
                }
            });
        })
    }
</script>
