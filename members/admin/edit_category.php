<?php 
    require_once("../../includes/initialize.php");
    require_once("logincheck.php");
    require_once("header.php");
    require_once("sidebar.php");
    $name="";  
    $id="";
    $category_id=""; 
    $parent_id=""; 
    if(isset($_GET['id']))
    {
       
        $category_id = sanitize_input($_GET['id']);
        $sql_get_category = "SELECT parent_id, id, name FROM category WHERE id = $category_id";
        $result_get_category = mysqli_query($con, $sql_get_category);
        $myrow_get_category = mysqli_fetch_array($result_get_category);
        $name = $myrow_get_category['name'];
        $parent_id = $myrow_get_category['parent_id'];       
        if($result_get_category)
        {
            $name = $myrow_get_category['name'];
            $id = $myrow_get_category['id'];
        }
  
    }
?>
<div class="m-grid__item m-grid__item--fluid m-wrapper">    			    
    <div class="m-subheader">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">Edit Category</h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__item">
                        <a href="#" class="m-nav__link">
                            <span class="m-nav__link-text">Manage Category</span>
                        </a>
                    </li>
                    <li class="m-nav__separator">-</li>
                    <li class="m-nav__item">
                        <a href="javascript:void(0)" class="m-nav__link">
                            <span class="m-nav__link-text">Edit Category</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="m-content" style="padding-top:10px;">
        <form class="m-form m-form--fit m-form--label-align-right" id="add_category_form" method="POST" enctype=multipart/form-data>
            <div class="m-portlet m-portlet--tabs">
                <div class="m-portlet__body">
                <div class="form-row" >
                    <div class="form-group col-lg-6 m-form__group-sub col-lg-cust ">
                        <label for="category">Category Name<span class="m--font-danger">*</span></label>
                        <input class="form-control" type="text" name="name" id="name" placeholder="Category Name" value="<?php echo $name?>" >
                    </div>
                       
                    </div>
                    <div class="form-row">
                        <div class="form-group col-lg-6 m-form__group-sub col-lg-cust" style="margin-bottom:0px;">
                            <label class="m-checkbox m-checkbox--state-primary">
                                <?php if(empty($parent_id))
                                {?>
                                    <input type="checkbox" name="is_category" id="is_category" value="yes">Is Sub Category ?
                                    <input type="hidden" name="category_id"  id="category_id" value=""/>
                                <?php
                                }
                                else 
                                {?>
                                    <input type="checkbox" name="is_category" id="is_category" checked value="yes">Is Sub Category ?
                                    <input type="hidden" name="category_id"  id="category_id" value="<?php echo $parent_id ?>"/>
                                <?php } ?>
                                <span></span>
                            </label>
                        </div>
                    </div>
                    
                    <div class="form-row" id="category_div" style="display:none" >
                     <div class="form-group col-lg-6 m-form__group-sub col-lg-cust ">
                        <label for="category">Category<span class="m--font-danger">*</span></label>
                        <select class="form-control category" name="category" id="category" >
                            <option value="">Select Category</option>
                            <?php
                                $sql_category_list = "SELECT id, name, parent_id FROM category WHERE parent_id IS NULL AND active = 1 ORDER BY name ASC";
                                $result_category_list = mysqli_query($con, $sql_category_list);
                                if($result_category_list && $myrow_category_list = mysqli_fetch_array($result_category_list))
                                {
                                    $return_str = '';
                                    do
                                    {
                                        $category_name = '';
                                        $id = $myrow_category_list["id"];
                                        $parentid = $myrow_category_list["parent_id"];
                                        if($myrow_category_list["parent_id"]!=NULL)
                                        {
                                            get_category_list($id,  $myrow_category_list["name"], $return_str);
                                            $category_name =  $return_str;
                                         
                                        }
                                        else
                                        {
                                            $category_name = $myrow_category_list["name"];
                                        }
                                        if($id!=$category_id)
                                        {
                                    ?>
                                   
                                        <option value="<?php echo $id;?>">
                                            <?php echo $category_name?>
                                        </option>
                                        <?php
                                        } 
                                        if(!empty($parent_id))
                                        {
                                        ?>
                                            <script>
                                                category.value = '<?php echo $parent_id ?>';
                                            
                                            </script>
                                        <?php
                                        }
                                        else 
                                        {
                                          ?>
                                          
                                          <script>
                                           document.getElementById('category').selectedIndex = -1;
                                         
                                            </script>
                                          <?php
                                        }
                                    }
                                    while($myrow_category_list = mysqli_fetch_array($result_category_list));
                                }
                            ?>
                            </select>
                        </div> 
                    </div>

                    <div class="form-row">
                        <div class="form-group col-lg-cust">
                            <input type="hidden" name="action"  id="action" value="edit"/>
                            <input type="hidden" name="id"  id="id" value="<?php echo  $category_id; ?>"/>
                            <button class="btn btn-success">Submit</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>


<?php require('footer.php');?>
<script>
   if($('#is_category'). prop("checked") == true)
{
    $('#category_div').show();
}
else
{
    $('#category_div').hide();
}

$('#is_category').click(function(){
   
    var element=document.getElementsByClassName("category");
    if($(this). prop("checked") == true)
{
    $('#category_div').show();
    for(var i=0;i<element.length;i++)
    $('.category').attr("required", true);
    
}
else
{
    $('#category_div').hide();
    $('.category').attr("required", false);
}
});
$('.category').change(function(){
    $('#category_id').val($(this).val());
})   
</script>