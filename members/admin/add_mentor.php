<?php 
    require_once("../../includes/initialize.php");
    require_once("logincheck.php");
    $erroroccured = 0;
    $errormsg = "";
    $action = "add";

    require_once("header.php"); 
    require_once("sidebar.php");
    $levels=getLevel($con);

    $companies = [];
    $sql_get_companies = "SELECT id, name, codename FROM company";
    $result_get_companies = mysqli_query($con, $sql_get_companies);
    while($myrow_get_company = mysqli_fetch_array($result_get_companies))
    {
        $companies[] = array("id"=>$myrow_get_company["id"],
                            "name"=>$myrow_get_company["name"],
                            "codename"=>$myrow_get_company["codename"]
        );
    }

    $skills = [];
    $sql_get_skills = "SELECT id, name, codename FROM skill";
    $result_get_skills = mysqli_query($con, $sql_get_skills);
    while($myrow_get_skill = mysqli_fetch_array($result_get_skills))
    {
        $skills[] = array("id"=>$myrow_get_skill["id"],
                            "name"=>$myrow_get_skill["name"],
                            "codename"=>$myrow_get_skill["codename"]
        );
    }


?>
<?php
if($erroroccured==1)
{
?>
    <div class="alert alert-danger" role="alert">
        <?php echo $errormsg;?>
    </div>
<?php
}
?>
<div class="m-grid__item m-grid__item--fluid m-wrapper">    			    
    <div class="m-subheader">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">Add Mentor</h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__item">
                        <a href="#" class="m-nav__link">
                            <span class="m-nav__link-text">Manage Mentor</span>
                        </a>
                    </li>
                    <li class="m-nav__separator">-</li>
                    <li class="m-nav__item">
                        <a href="javascript:void(0)" class="m-nav__link">
                            <span class="m-nav__link-text">Add Mentor</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="m-content" style="padding-top:10px;">
        <form autocomplete="for-form" class="m-form m-form--fit m-form--label-align-right" id="mentor_form" method="POST" enctype=multipart/form-data>
            <div class="m-portlet m-portlet--tabs">
                <div class="m-portlet__body">
                    <div class="form-row">
                        <input type="hidden" class="form-control" name="mentor_id" id="mentor_id" value="<?php echo $edit ?>" placeholder="mentor_id">
                        <div class="form-group col-lg-6 m-form__group-sub col-lg-cust">
                            <label for="fname">First name<span class="m--font-danger">*</span></label>
                            <input class="form-control" name="fname" id="fname" placeholder="First name" value="<?php echo $fname ?>" required>
                        </div>
                        <div class="form-group col-lg-6 m-form__group-sub col-lg-cust">
                            <label for="lname">Last name<span class="m--font-danger">*</span></label>
                            <input class="form-control" name="lname" id="lname" placeholder="Last name" value="<?php echo $lname ?>" required>
                        </div>
                        <div class="form-group col-lg-6 m-form__group-sub col-lg-cust">
                            <label for="Email">Email<span class="m--font-danger">*</span></label>
                            <input class="form-control" autocomplete="for-email" name="email" id="email" placeholder="Email" value="<?php echo $email ?>" required>  
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-lg-6 m-form__group-sub col-lg-cust">
                            <label for="Mobile">Mobile<span class="m--font-danger">*</span></label>
                            <input class="form-control" name="mobile" id="mobile" placeholder="Mobile Number" value="<?php echo $mobile ?>" required>
                        </div>
                        <div class="form-group col-lg-6 m-form__group-sub col-lg-cust">
                            <label for="Paytm_mobile">Paytm Mobile<span class="m--font-danger">*</span></label>
                            <input class="form-control" name="paytm_mobile" id="paytm_mobile" placeholder="Paytm Mobile" value="<?php echo $paytm_mobile ?>" required>
                        </div>
                        <div class="form-group col-lg-6 m-form__group-sub col-lg-cust">
                            <label for="Password">Password<span class="m--font-danger">*</span></label>
                            <input type="password" autocomplete="for-password" class="form-control" name="password" id="password" placeholder="Password" value="<?php echo $password ?>" required>
                        </div>
                        <div class="form-group col-lg-6 m-form__group-sub col-lg-cust">
                            <label for="confirm_password">Confirm Password<span class="m--font-danger">*</span></label>
                            <input type="password" autocomplete="for-cnf_pass" class="form-control" name="confirm_password" id="confirm_password" placeholder="Confirm Password" value="<?php echo $confirm_password ?>" required>
                        </div>
                        <div class="form-group col-lg-4 m-form__group-sub col-lg-cust">
                            <label for="Total_experience">Total Experience<span class="m--font-danger">*</span></label>
                            <input type="text" class="form-control" name="total_experience" id="total_experience" placeholder="Total Experience" value="<?php echo $total_experience ?>" required>
                        </div>
                        <div class="form-group col-lg-4 m-form__group-sub col-lg-cust">
                            <label for="Total_experience">Cost Card Type<span class="m--font-danger">*</span></label>
                            <select class="form-control" name="cost_card_type" id="cost_card_type" placeholder="Add Cost Card Type" required>
                            <option value="">Select cost card type</option>
                            <?php
                            foreach($levels as $level)
                            {
                            ?>
                                <option value="<?php echo $level['id'];?>"><?php echo $level['name'];?> (<?php echo $level['price']; ?>)</option>
                            <?php
                            }
                            ?>
                            </select>
                        </div>
                        <div class="form-group col-lg-4 m-form__group-sub col-lg-cust">
                            <label for="Profile Pic">Profile Pic</label>
                            <input class="form-control" type="file" name="profile_pic" id="profile_pic" placeholder="Profile Pic" value="" >
                        </div>
                        
                        <div class="form-group col-lg-6 m-form__group-sub col-lg-cust" hidden>
                            <label for="Profile Pic">Give Him/Her price irrelent to cost card( in $)</label>
                            <input class="form-control" type="text" name="overided_price" id="overided_price" placeholder="Give Him/Her price irrelent to cost card" value="<?php echo 0 ?>" >
                        </div>
                        <div class="form-group col-lg-6 m-form__group-sub col-lg-cust">
                            <label for="Total_experience">Company <span class="m--font-danger">*</span></label>
                            <select class="form-control m-select2" name="company" id="company" placeholder="Add Cost Card Type" required>
                            <option></option>
                            <?php
                            foreach($companies as $company)
                            {
                            ?>
                                <option value="<?php echo $company['name'];?>"><?php echo $company['name'];?></option>
                            <?php
                            }
                            ?>
                            </select>
                        </div>
                        <div class="form-group col-lg-6 m-form__group-sub col-lg-cust">
                            <label for="Total_experience">Skills <span class="m--font-danger">*</span></label>
                            <select class="form-control m-select2" name="skill[]" id="skill" multiple="multiple">
                            
                            <?php
                            foreach($skills as $skill)
                            {
                            ?>
                                <option value="<?php echo $skill['name'];?>"><?php echo $skill['name'];?></option>
                            <?php
                            }
                            ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-lg-12 m-form__group-sub col-lg-cust">
                        <label for="description">Description</label>
                        <textarea class="form-control" name="description" id="description" placeholder="Description about mentor." value="<?php echo $description ?>"></textarea>
                    </div>
                    <div class="form-group col-lg-cust">
                        <input type="hidden" name="action" id="action" value="<?php echo $action ?>">
                        <button class="btn btn-success" type="submit">Submit</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<?php require('footer.php');?>

<script>
    $("#company").select2({
        placeholder:"Select a company",
        tags:true,
    })

    $("#skill").select2({
        placeholder:"Select skills",
        tags:true,
    })
</script>