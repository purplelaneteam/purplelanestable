<?php
    require_once("../../../../includes/initialize.php");
    $jsonarray = array();
    $email = sanitize_input($_POST['email']);

    if($email == "")
    {
        $error = 'All fields are mandatory.';
        ajax_error($error);
    }

    if(!filter_var($email, FILTER_VALIDATE_EMAIL))
    {
        $error = "Email is not a valid email address.";
        ajax_error($error);
    }
    
    $sql = "SELECT fname, id FROM employee WHERE email = '$email'";
    $result = mysqli_query($con, $sql);
    if($result && $myrow = mysqli_fetch_array($result))
    {
        $usersid = $myrow["id"];
        $firstname = $myrow["fname"];
        $reset_code = md5(uniqid());

        $sql_updatereset_code = "UPDATE employee 
                        SET reset_code = '".$reset_code."'
                        WHERE id = '".$usersid."'";
        if(!mysqli_query($con, $sql_updatereset_code))
        {
            $error = "Something went wrong while updating user.";
            ajax_error($error);
        }
        $email_subject = "Your ".SITE_NAME." reset password link";
        $mailbody = "Hi ".ucwords($firstname).",<br/><br/>
                    <a href='".SITE_URL."/members/admin/login/resetpassword.php?reset_code=".$reset_code."'>Click this link to reset your password.</a><br/><br/>
                    Thanks<br/>
                    Regards<br/>
                    '".CONTACTUS_FROM_NAME."'";

        $mail_file = "../../../../includes/class.phpmailer.php";
        $send_mail = send_mail($email_subject, $mailbody, $email, $mail_file);
        if($send_mail)
        {
            $jsonarray["code"] = 0;
            $jsonarray["msg"] = "Cool! Password recovery link has been sent to your email.";
            echo json_encode($jsonarray);
            exit;
        }
        else 
        {
            // $error = $mail->ErrorInfo;
            $error = 'Something went wrong while sending mail.';
            ajax_error($error);
        }
    }
    else 
    {
        $error = "You are not register with us.";
        ajax_error($error);
    }
?>



