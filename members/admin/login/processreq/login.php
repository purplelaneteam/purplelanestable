<?php 
    require_once("../../../../includes/initialize.php");
    $jsonarray = array();
    $email = sanitize_input($_POST["email"]);
    $password = sanitize_input($_POST["password"]);
    $remember = sanitize_input($_POST["remember"]);

    if(!filter_var($email, FILTER_VALIDATE_EMAIL))
    {
        $error = "Email is not a valid email address.";
        ajax_error($error);
    }

    $sql = "SELECT id, role, fname, lname, token, email, concat(fname, ' ', lname) as name
            FROM employee
            WHERE email = '".$email."' AND password = '".md5($password)."' AND active = 1 AND is_deleted = 0";
    
    $result = mysqli_query($con, $sql);
    //print_r($result);exit;    
    if($result && $myrow = mysqli_fetch_array($result))
    {
        $_SESSION["USER"]["USER_ID"]= $myrow["id"];
        $_SESSION["USER"]["USER_FNAME"]= $myrow["fname"];
        $_SESSION["USER"]["USER_LNAME"]= $myrow["lname"];
        $_SESSION["USER"]["USER_NAME"]= $myrow["name"];
        $_SESSION["USER"]["USER_P_EMAIL"]= $myrow["email"];
        $_SESSION["USER"]["USER_TOKEN"]= $myrow["token"];
        $_SESSION["USER"]["USER_ROLE"]= $myrow["role"];
        setcookie("REMEMBER", "0", time()+24*3600*31, "/");
      
        if($remember)
        {
            setcookie("REMEMBER", "1", time()+24*3600*31, "/");
            setcookie("USER_EMAIL", $email, time()+24*3600*31, "/");
            setcookie("USER_PASSWORD", $password, time()+24*3600*31, "/");
        }
        $jsonarray["code"] = 0;
        echo json_encode($jsonarray);
        exit;
    }
    else
    {
        $error = "Invalid email or password.";
        ajax_error($error);
    }