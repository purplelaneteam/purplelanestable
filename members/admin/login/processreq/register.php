<?php
    require_once("../../includes/initialize.php");
    $jsonarray = array();

    $fname = sanitize_input($_POST['fname']);
    $lname = sanitize_input($_POST['lname']);
    $phone = sanitize_input($_POST['phone']);
    $email = sanitize_input($_POST['email']);
    $password = sanitize_input($_POST['password']);
    $rpassword = sanitize_input($_POST['rpassword']);
    $company = sanitize_input($_POST['company']);
    $address = sanitize_input($_POST['address']);
    $currency_id = sanitize_input($_POST['currency']);
    $website = sanitize_input($_POST['website']);
    $agree = sanitize_input($_POST['agree']);

    if($fname == "" || $lname == "" || $phone == "" || $email == "" || $password == "" || $address == ""  || $agree  == "")
    {
        $error = 'All fields are mandatory.';
        ajax_error($error);
    }

    
    $sql = "SELECT id FROM billing_cycles WHERE codename = 'per_job'";
    $result = mysqli_query($con, $sql);
    if($result && $myrow = mysqli_fetch_array($result))
    {
        $billing_cycle_id = $myrow['id'];
    }
    else 
    {
        $error = "Not found per job billing cycle.";
        ajax_error($error);   
    }
    
    if(!filter_var($email, FILTER_VALIDATE_EMAIL))
    {
        $error = "Email is not a valid email address";
        ajax_error($error);
    }

    if($password != $rpassword)
    {
        $error = "Password and confirmed password mismatched";
        ajax_error($error);
    }

    $sql = "SELECT id FROM users WHERE email = '".$email."'";
    $result = mysqli_query($con, $sql);
    if($result && $myrow = mysqli_fetch_array($result))
    {
        $error = "Email already exist.";
        ajax_error($error);
    }
    $sql_currency_id = "SELECT id FROM currencies WHERE codename = '".strtoupper('usd')."'";
    $result_currency_id = mysqli_query($con, $sql_currency_id);
    if($result_currency_id && $myrow_currency_id = mysqli_fetch_array($result_currency_id))
    {
        $default_currency_id = $myrow_currency_id['id'];
    }
    $select_client_id = "SELECT * FROM users ORDER BY id DESC LIMIT 1";
    $result_client_id = mysqli_query($con, $select_client_id);
    if($result_client_id && $myrow_client_id = mysqli_fetch_array($result_client_id))
    {
        $user_no = $myrow_client_id["user_no"];
    }
    if($user_no == "")
    { 
        $user_no = CLIENT_NO;
        $user_no++;
            
    }
    else
    {  
        $user_no = substr ($user_no ,6);
        $user_no = (int)$user_no;
        $user_no++;
    }
    $user_no = CLIENT_CODE_NO.$user_no;
    //echo $sql_currency_id;exit;
    $activation_code = uniqid();
    $now = date('Y-m-d H:i:s');
    $sql_insert = "INSERT INTO users (billing_cycles_id, currencies_id, fname, lname, phone, username, email, password, company, address, website, registered_or_added, role, activation_code, active, token, addedon,user_no) VALUES ('$billing_cycle_id','$default_currency_id', '$fname', '$lname', '$phone', '$email', '$email', '".md5($password)."', '$company', '$address', '$website', 'registered', 'client', '$activation_code', '1', '".uniqid()."', '".$now."', '".$user_no."')";
    
    if(mysqli_query($con, $sql_insert))
    {
        $email_subject = "Email Activation Link";

        $mailbody = "Hi ".ucwords($fname).",<br/><br/>
                    You can login with following credentials: 
                    Email : ".$email."
                    Password : ".$password."<br/><br/>
                    <a href='".SITE_URL."/login/index.php'>Click this link to login.</a><br/><br/>
                    Thanks<br/>
                    Regards<br/>
                    '".CONTACTUS_FROM_NAME."'";
        require_once('../../includes/class.phpmailer.php');
        $reply_address = CONTACTUS_REPLY_ADD;
        $reply_person_name = CONTACTUS_REPLY_NAME;
        $from_address = CONTACTUS_FROM_ADD;
        $from_name = CONTACTUS_FROM_NAME;
        $alt_body = "To view the message, please use an HTML compatible email viewer!";

        $mail = new PHPMailer(); // defaults to using php "mail()"

        if(USE_SMTP_SERVER==1)
        {
            $mail->IsSMTP();
            $mail->SMTPDebug  = SMTP_DEBUGGING;                     // enables SMTP debug information (for testing)
            $mail->SMTPAuth   = true;                  // enable SMTP authentication
            $mail->Host       = SMTP_HOST; // sets the SMTP server
            $mail->Port       = SMTP_HOST_PORT;                    // set the SMTP port for the GMAIL server
            $mail->Username   = SMTP_HOST_USERNAME; // SMTP account username
            $mail->Password   = SMTP_HOST_PASSWORD;        // SMTP account password                
        }                

        $body = $mailbody;
        $mail->SetFrom($from_address, $from_name);
        $mail->AddReplyTo($reply_address,$reply_person_name);

        $mail->AddAddress($email);

        $mail->Subject = $email_subject;
        $mail->AltBody = $alt_body; // optional, comment out and test
        $mail->MsgHTML($body);
        if(!$mail->Send())
        {
            $error = 'Something went wrong while sending activation mail.';
            ajax_error($error);
        }
    }
    else
    {
        $error = 'Something went wrong while inserting client.';
        ajax_error($error);
    }
    $jsonarray["code"] = 0;
    $jsonarray["msg"] = "Thank you. To complete your registration please check your email.";
    echo json_encode($jsonarray);
    exit;
?>