<?php 
    require_once("../../../includes/initialize.php");
    if(isset($_COOKIE["USER_ID"]) && isset($_COOKIE["USER_TOKEN"]))
    {
        header("Location: /index.php");
    }

    if(isset($_GET['reset_code']) && $_GET['reset_code']!="")
    {
        $reset_code = sanitize_input($_GET['reset_code']);
        $sql = "SELECT id FROM employee WHERE reset_code = '".$reset_code."'";
        $result = mysqli_query($con, $sql);
        if(!($result && $myrow = mysqli_fetch_array($result)))
        {
            $error = "Reset password link expired.";
        }
    }
    else 
    {
        header("Refresh:3; url=/index.php");
        exit;
    }	
    
    if(!isset($error))
    {
        if(count($_POST)>0)
        {
            if($_POST['newpassword'] == $_POST['confirmnewpassword'])
            {
                $newpassword = md5(sanitize_input($_POST['newpassword']));
                $newtoken = md5(uniqid());
                $sql_updatepassword = "UPDATE employee SET token = '".$newtoken."', password = '".$newpassword."', reset_code = '' WHERE reset_code = '".$reset_code."'";
                if(mysqli_query($con, $sql_updatepassword))
                {
                    header("Refresh:3; url=index.php");
                    $success = "Password changed successfully.";
                }
                else
                {
                    $error = "Something went wrong while updating password.";
                }
            }
            else
            {
                $error = "Password and confirm password mismatched.";
            }
        }
    }

?>
<?php require('header.php');?>
<body  class="m--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >   
    <div class="m-grid m-grid--hor m-grid--root m-page">
        <div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor m-login m-login--singin m-login--2 m-login-2--skin-1" id="m_login" style="background-image: url(/assets/app/media/img//bg/bg-1.jpg)">
            <div class="m-grid__item m-grid__item--fluid m-login__wrapper">
                <div class="m-login__container">
                    <div class="m-login__logo">
                        <a href="#">
                        
                        </a>
                    </div>
                    <div class="m-login__signin">
                        <div class="m-login__head">
                            <h3 class="m-login__title">Reset Password</h3>
                        </div>
                        <form class="m-login__form m-form" action="" method="post">
                            <?php if(isset($error)): ?>
                            <div class="m-alert m-alert--outline alert alert-danger alert-dismissible animated fadeIn" role="alert">			
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
                                <span><?php echo  $error; ?></span>
                            </div>
                            <?php endif ?>
                            <?php if(isset($success)): ?>
                            <div class="m-alert m-alert--outline alert alert-success alert-dismissible animated fadeIn" role="alert">			
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
                                <span><?php echo  $success; ?></span>
                            </div>
                            <?php endif ?>
                            <div class="form-group m-form__group">
                                <input class="form-control m-input" type="password" placeholder="New Password" name="newpassword" autocomplete="off">
                            </div>
                            <div class="form-group m-form__group">
                                <input class="form-control m-input m-login__form-input--last" type="password" placeholder="Confirm Password" name="confirmnewpassword">
                            </div>
                            <div class="m-login__form-action">
                                <button class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air  m-login__btn m-login__btn--primary" id="m_reset_submit">Change Password</button>
                                <button class="btn m-btn m-btn--pill m-btn--custom m-btn--air m-login__btn" type="button" onclick="window.location.href='index.php'">Cancel</button>
                            </div>
                        </form>
                    </div>
                </div>  
            </div>
        </div>          
    </div> 
<?php require('footer.php'); ?>