<?php 
    require_once("../../includes/initialize.php");
    require_once("logincheck.php");
    require_once("header.php");
    require_once("sidebar.php");

    $erroroccured = 0;
    $errormsg = "";
    $action="add";

    $cost_price_array = [];
    $sql_get_cost_prices = "SELECT cost_card_type_prices.id as id, name,
        cost_card_type_id, cost_card_type_price, cost_card_type_prices.addedon, cost_card_type_prices.modifiedon 
        FROM cost_card_type_prices INNER JOIN cost_card_type ON cost_card_type_prices.cost_card_type_id = cost_card_type.id";
    $result_get_cost = mysqli_query($con, $sql_get_cost_prices);
    while($myrow_get_cost = mysqli_fetch_array($result_get_cost))
    {
        $cost_price_array[] = array("id"=>$myrow_get_cost["id"],
                                "cost_card_type_id"=>$myrow_get_cost["cost_card_type_id"],
                                "name"=>$myrow_get_cost["name"],
                                "cost_card_type_price"=>$myrow_get_cost["cost_card_type_price"],
                                "addedon"=>$myrow_get_cost["addedon"],
                                "modifiedon"=>$myrow_get_cost["modifiedon"]
        );
    }

    $cost_card_array = [];
    $sql_get_cost_cards = "SELECT id, name, codename FROM cost_card_type";
    $result_get_cost_cards = mysqli_query($con, $sql_get_cost_cards);
    while($myrow_get_cost_card = mysqli_fetch_array($result_get_cost_cards))
    {
        $cost_card_array[] = array("id"=>$myrow_get_cost_card["id"],
                            "name"=>$myrow_get_cost_card["name"],
                            "codename"=>$myrow_get_cost_card["codename"]
        );
    }

    $cost_card_type_id = '';
    $cost_card_type_price = '';

    if(isset($_GET['edit']))
    {
        $edit = sanitize_input($_GET['edit']);
        $edit_query="SELECT cost_card_type_id, cost_card_type_price FROM cost_card_type_prices WHERE id='$edit'";
       
        if(!$edit_cost=mysqli_query($con,$edit_query))
        {
            $erroroccured = 1;
            $errormsg = "ERROR_CUST_00:Something went wrong";
        } else{
            if($result_edit_cost=mysqli_fetch_array($edit_cost))
            {
                $cost_card_type_id = $result_edit_cost['cost_card_type_id'];
                $cost_card_type_price = $result_edit_cost['cost_card_type_price'];
            }
        }
        $action="edit";
    }
?>
<?php
    if($erroroccured==1)
    { ?>
        <div class="alert alert-danger" role="alert">
            <?php echo $errormsg;?>
        </div>
<?php } ?>

<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">Manage Card</h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__item m-nav__item--home">
                        <a href="" class="m-nav__link">
                            <span class="m-nav__link-text">Manage Card Type</span>
                        </a>
                    </li>
                   <li class="m-nav__separator">-</li>
                     <li class="m-nav__item">
                        <a href="" class="m-nav__link">
                            <span class="m-nav__link-text">Cost Card Type Price</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="m-content">
        <form class="m-form m-form--fit m-form--label-align-right" id="add_cardprice_form" method="POST" value="<?php  ?>" enctype=multipart/form-data>
            <input type="hidden" class="form-control" name="id" id="id" value="<?php echo $edit ?>">
            <div class="m-portlet m-portlet--tabs">
                <div class="m-portlet__body">
                    <div class="form-row">
                        <div class="form-group col-lg-6 m-form__group-sub col-lg-cust">
                            <label for="name">Type<span class="m--font-danger">*</span></label>
                            <select class="form-control" id="cost_card_id" name="cost_card_id">
                                <option value="">Select Card Type</option>
                                <?php foreach ($cost_card_array as $cost_card_key => $cost_card_value) { ?>
                                    <option value="<?php echo $cost_card_value['id']; ?>" <?php if($cost_card_type_id == $cost_card_value['id']) echo 'selected'; else echo ''; ?>><?php echo $cost_card_value['name']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-lg-6 m-form__group-sub col-lg-cust">
                            <label for="name">Price<span class="m--font-danger">*</span></label>
                            <input class="form-control" type="number" name="price" id="price" placeholder="eg. 5000" min="0" value="<?php echo $cost_card_type_price; ?>" required>
                        </div>
                    </div>
                    <div class="form-group col-lg-cust">
                        <input type="hidden" name="action" id="action" value="<?php echo $action ?>">
                        <a class="btn btn-secondary" href="cost_cardtype_price.php">Cancel</a>
                        <button class="btn btn-success" type="submit">Submit</button>
                    </div>
                    <div class="col-xs-12" style="margin-bottom: 30px;"><hr></div>
                    <table class="table table-striped- table-bordered table-hover table-checkable" id="add_cardtype_table"> 
                        <thead>
                            <tr>
                                
                                <th>Name </th>
                                <th>CodeName</th>
                                <th>Addedon</th>
                                <th>Modifiedon</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($cost_price_array as $cost_key => $cost_data) {
                            ?>
                            <tr>
                                
                                <td><?php echo $cost_data["name"];?></td>
                                <td><?php echo $cost_data["cost_card_type_price"];?></td>
                                <td><?php echo $cost_data["addedon"];?></td>
                                <td><?php echo $cost_data["modifiedon"];?></td>
                                <td><a href="cost_cardtype_price.php?edit=<?php echo $cost_data['id'];?>">   <i class="fas fa-edit"></i></a>
                                    <a href="javascript:void(0);" onclick="delemp(<?php echo $cost_data['id'];?>)"><i class="fas fa-trash-alt"></i></a>
                                </td> 
                            </tr>
                            <?php
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </form>
    </div>
</div>
<?php require('footer.php');?>
<script>

    function delemp(empid)
    {
        var action="delete";
            $.ajax({
                url: "processreq/proc_add_customer.php",
                type: "POST",
                data: {action:action,delid:empid},
                success: function(data){
                //$("#ajaxresponse").html(result);
                //$("#ajaxresponse").show();
                setTimeout(location.reload.bind(location), 5000);
                alert(data);
                }
            });
    }

    $('#add_cardtype_table').DataTable({
        "order": [[ 2, "desc" ]]
    });
    $('#add_cardprice_form').submit(function (e) {
        e.preventDefault();
    }).validate({
        focusInvalid: true,
        invalidHandler: function (form, validator) {
            var errors = validator.numberOfInvalids();
            if (errors) {
                $("html, body").animate({
                    scrollTop: 0
                }, "fast");
            }
        },

        submitHandler: function (form) {
            var formData = new FormData(form);
            var button = $(form).find('button')
            var loading_class = 'm-loader m-loader--light m-loader--right'
            var button_text = $(button).text()
            $(button).html(`Processing`).addClass(loading_class).prop('disabled', true)
            $.ajax({
                type: 'POST',
                url:'processreq/proc_manage_cardprice.php',
                data: formData,
                dataType: "json",
                cache: false,
                contentType: false,
                processData: false,
                success: function (response) {
                    setTimeout(location.reload.bind(location), 5000);
                    $(button).prop('disabled', false).html(button_text).removeClass(loading_class)
                    if (response.code) {
                        swal('Error!', response.msg, 'error')
                    } else {
                        swal({
                            title: "Success!",
                            text: response.msg,
                            type: "success",
                        }).then(function () {
                            window.location.href = "cost_cardtype_price.php";
                        });
                    }
                },
                error: function (response) {
                    $(button).prop('disabled', false).html(button_text).removeClass(loading_class)
                    swal('Error!', response.msg, 'error');
                }
            });
            $('#add_cardprice_form')[0].reset();
        }
    })

</script>


