<?php
    require_once("../../../includes/initialize.php");
    require("../logincheck.php");

    $jsonarray = array();

    $sql = "START TRANSACTION";
    $result = mysqli_query($con, $sql);
    if(!$result)
    {
        $error = "Something went wrong. Please try again later.";
        ajax_error($error);
    }
    $meeting_id = sanitize_input($_POST["meeting_id"]);
    $hangout_link = sanitize_input($_POST["hangout_link"]);

    $sql_insert = "UPDATE meetings set hangout_link = '$hangout_link' WHERE id=$meeting_id";
    
    $result_update = mysqli_query($con, $sql_insert);
    if(!$result_update) 
    {
        $error = "Something went wrong while cancelling meeting. Please try again later.";
        ajax_error($error);
    }

    $sql = "SELECT * FROM  meetings WHERE id = $meeting_id";
    $result = mysqli_query($con, $sql);
    $meeting = mysqli_fetch_assoc($result);

    if (SEND_EMAIL_HANGOUT_LINK_MENTOR)
    {
        $sql = "SELECT email, fname, lname FROM mentor WHERE id = ".$meeting['mentor_id'];
        $result = mysqli_query($con, $sql);
        $mentor = mysqli_fetch_assoc($result);
        if (!$mentor)
        {
            $error = "Unable To send email";
            ajax_error($error);
        }
        else
        {
            $email_subject = "Your ".SITE_NAME." Meeting Hangout created";
            $mailbody = "Hi ".ucwords($mentor['fname']. " " . $mentor['lname']).",<br/><br/>
                        <p>Your meeting link has been created</p>
                        <a href=\"$hangout_link\">This is your hangout link</a><br/><br/>
                        Thanks<br/>
                        Regards<br/>
                        '".CONTACTUS_FROM_NAME."'";

            // $send_mail = send_mail($email_subject, $mailbody, $mentor['email']);
        }
    }

    if (SEND_EMAIL_HANGOUT_LINK_MENTEE)
    {
        $sql = "SELECT email, fname, lname FROM mentee WHERE id = ".$meeting['mentee_id'];
        $result = mysqli_query($con, $sql);
        $mentee = mysqli_fetch_assoc($result);
        if (!$mentee)
        {
            $error = "Unable To send email";
            ajax_error($error);
        }
        else
        {
            $email_subject = "Your ".SITE_NAME." Meeting Hangout created";
            $mailbody = "Hi ".ucwords($mentee['fname']. " " . $mentee['lname']).",<br/><br/>
                        <p>Your meeting link has been created</p>
                        <a href=\"$hangout_link\">This is your hangout link.</a><br/><br/>
                        Thanks<br/>
                        Regards<br/>
                        '".CONTACTUS_FROM_NAME."'";

            // $send_mail = send_mail($email_subject, $mailbody, $mentee['email']);
        }
    }
    
    $sql = "COMMIT";
    $result = mysqli_query($con, $sql);
    if(!$result)
    {
        $error = "commit: Something went wrong. Please try again later.";
        ajax_error($error);
    }
    else
    {
        $jsonarray["code"] = 0;
        $jsonarray["msg"] = "Link Saved Successfully.";
        echo json_encode($jsonarray);
    }