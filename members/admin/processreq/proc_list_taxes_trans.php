<?php
	require_once("../../../includes/initialize.php");
    require("../logincheck.php");
	 
	// initilize all variable
    $params = array(); 
    $columns = array();
    $totalRecords = array();
    $data = array();
    $table = 'taxes_transactions tt, meetings m, meeting_status ms';
	$params = $_REQUEST;

	//define index of column
	$columns = array(
		0 => 'tt.meetings_id', 
		1 => 'tt.amount',
        2 => 'tt.type',
        3 => 'ms.name',
	);

    $where = "WHERE tt.meetings_id = m.id AND m.meeting_status_id = ms.id";
    $sqlTot = "";
    $sqlRec = "";

	// check search value exist
	if(!empty($params['search']['value']) ) {   
		$where .=" AND ";
		$where .=" ( tt.meetings_id LIKE '".$params['search']['value']."%' ";    
		$where .=" OR tt.amount LIKE '".$params['search']['value']."%' ";    
		$where .=" OR tt.type LIKE '".$params['search']['value']."%' )";
	}

	// getting total number records without any search
	$sql = "SELECT tt.meetings_id, tt.amount, tt.type, ms.name, tt.addedon
    FROM $table ";
 
	$sqlTot .= $sql;
    $sqlRec .= $sql;
     
	//concatenate search sql if value exist
	if(isset($where) && $where != '') {

		$sqlTot .= $where;
		$sqlRec .= $where;
	}
    $column = "";
    //print_r($columns[$params['order'][0]['column']]);
    
    $column .= $columns[$params['order'][0]['column']]; 
    

 	$sqlRec .=  " ORDER BY ". $column." ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
//    echo $sqlRec;exit;
	$queryTot = mysqli_query($con, $sqlTot);


	$totalRecords = mysqli_num_rows($queryTot);

	$queryRecords = mysqli_query($con, $sqlRec) or die("error to fetch employees data");

    //iterate on results row and create new index array of data
    $i = 1;
	while( $row = mysqli_fetch_array($queryRecords) ) { 
       // print_r($row);exit;
        $mentor_id = $row['mentor_id'] ;
        $actionoption = '';
        $active_status = '';
        $profile_pic = '';
        $addedon = date('d M Y', (strtotime($row['addedon'])));
        
        $img = MENTOR_IMAGE_UPLOAD_FOLDER_LINK.$row["profile_pic"];
        $profile_pic .= '<img src="'.$img.'" width="50px" height="50px">';
        if($row['is_active'] == 1)
        {
            $active_status = '<span style="width: 100px;"><span class="m-badge  m-badge--success m-badge--wide">Active</span></span>';
        }   
        else
        {
            $active_status = '<span style="width: 100px;"><span class="m-badge  m-badge--danger m-badge--wide">Inactive</span></span>';
        }   
        
       $actionoption = '';
        if($row['is_active'] == 1)
        {
            $status = "'Deactivate'";
            $actionoption .= '<input type="hidden" name="active_deactive" value="'.$row['is_active'].'"/>
            <a href="javascript:void(0)" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="Deactivate" onclick="activate_deactivate_mentor('.$status.', '.$mentor_id.')">
            <i class="fas fa-thumbs-down" ></i>
            </a>';
        }
        else
        {
            $status = "'Activate'";
            $actionoption .='<input type="hidden" name="active_deactive" value="'.$row['active'].'"/>
            <a href="javascript:void(0)" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="Activate" onclick="activate_deactivate_mentor('.$status.','.$mentor_id.')">
            <i class="fas fa-thumbs-up"></i> 
            </a>';
        }
        $actionoption .='<a href="edit_mentor.php?mentor_id='.$mentor_id.'" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="Edit Mentor" > <i class="fas fa-edit"></i>
        </a>';

        $data[] = array(
              $row['meetings_id'],
              $row['amount'],
              ucwords($row['type']),
              ucwords($row['name']),
              $addedon
        );
        $i ++;
	}	

	$json_data = array(
			"draw"            => intval( $params['draw'] ),   
			"recordsTotal"    => intval( $totalRecords ),  
			"recordsFiltered" => intval($totalRecords),
			"data"            => $data   // total data array
			);

	echo json_encode($json_data);  // send data as json format
?>
	