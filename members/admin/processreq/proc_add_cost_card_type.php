<?php
    require_once("../../../includes/initialize.php");
    require("../logincheck.php");
   
    if(count($_POST)>0)
    {
       
       if(isset($_POST['action']) && $_POST['action']=="add")
       {
            $name = ucwords(sanitize_input($_POST["name"]));
            $codename = getCodename($_POST["name"]);
            $price = $_POST["price"];
            $addedon=date("Y-m-d H:i:s");

            if ($price < 0) {
                $error = "Price cannot be negative!";
                ajax_error($error);
            }


            if(!empty($_POST['name']) && !empty($_POST["price"]))
            {
                $sql_topic_insert = "INSERT INTO 
                    cost_card_type(name, codename, price, addedon)
                    VALUES('".$name."','".$codename."','".$price."','".$addedon."')";
                $result_topic_insert = mysqli_query($con, $sql_topic_insert);
                $lastid_topic_insert = mysqli_insert_id($con); 
                if(mysqli_affected_rows($con)<=0) 
                {
                    $error = "Something went wrong while adding Card type. Plese make sure card is not added previously.";
                    ajax_error($error);
                }
                else 
                {
                    $jsonarray["code"] = 0;
                    $jsonarray["msg"] = "Card type Successfully added.";
                    echo json_encode($jsonarray);
                }
            }
            else 
            {
                $error = "All Field Mandatory";
                ajax_error($error);
            }
          
       }
       elseif(isset($_POST['action']) && $_POST['action']=="edit")
       {
            $id = sanitize_input($_POST["id"]);
            $name = ucwords(sanitize_input($_POST["name"]));
            $codename = getCodename($_POST["name"]);
            $price = $_POST["price"];
            $modifiedon=date("Y-m-d H:i:s");

            if(!empty($_POST['name']) && !empty($_POST["price"]))
            {
                $sql_topic_update = "UPDATE cost_card_type SET
                    name ='".$name."',
                    codename='".$codename."', 
                    price='".$price."',
                    modifiedon='".$modifiedon."' 
                    WHERE id='$id'";
                $result_topic_update = mysqli_query($con, $sql_topic_update);
              
                if(!$result_topic_update) 
                {
                    $error = "Something went wrong while update Topic. Please try again later.";
                    ajax_error($error);
                }
                else 
                {
                    $jsonarray["code"] = 0;
                    $jsonarray["msg"] = "Topic Successfully Update.";
                    echo json_encode($jsonarray);
                }
            }
            else 
            {
                $error = "All Field Mandatory";
                ajax_error($error);
            }
       }
       elseif(isset($_POST['action']) && $_POST['action']=="delete")
       {
            $id = $_POST['delid'];

            $sql_get_cost_prices = "SELECT id FROM mentor WHERE cost_card_type_id = $id";
            $result_get_cost = mysqli_query($con, $sql_get_cost_prices); 
            $data_array = mysqli_fetch_array($result_get_cost);

            if(!empty($_POST['delid']))
            {
                if (!$data_array) {
                    $sql_topic_update = "DELETE FROM cost_card_type WHERE id='$id'";
                    $result_topic_update = mysqli_query($con, $sql_topic_update);
                    if(!$result_topic_update) 
                    {
                        $error = "Something went wrong while delete Topic. Please try again later.";
                        ajax_error($error);
                    }
                    else 
                    {
                        $jsonarray["code"] = 0;
                        $jsonarray["msg"] = "Topic Successfully Delete.";
                        echo json_encode($jsonarray);
                    }
                }else {
                    $error = "This audit card has been added to members. Therefore cannot be deleted.";
                    ajax_error($error);
                }
            }
            else 
            {
                $error = "All Field Mandatory";
                ajax_error($error);
            }
       }
       else 
        {
            $error = "Something went wrong . Please try again later.";
            ajax_error($error);
        }
    }
    else 
    {
        $error = "Something went wrong . Please try again later.";
        ajax_error($error);
    }
   