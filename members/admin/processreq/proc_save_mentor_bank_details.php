<?php
    require_once("../../../includes/initialize.php");
    require("../logincheck.php");
    $jsonarray = array();

    $sql = "START TRANSACTION";
    $result = mysqli_query($con, $sql);
    if(!$result)
    {
        $error = "Something went wrong. Please try again later.";
        ajax_error($error);
    }
    
    $mentor_id = sanitize_input($_POST['mentor_id']);
    $account = sanitize_input($_POST['number']);
    $bank = sanitize_input($_POST['name']);
    $ifsc = sanitize_input($_POST['ifsc']);

    if(!$mentor_id || !$account || !$bank || !$ifsc)
    {
        $jsonarray['code']="0";
        $jsonarray['msg'] = "All field is mandatory";
        echo json_encode($jsonarray);exit;
    }

    $sql = "SELECT id, mentor_id, bank_name, account_number, ifsc FROM mentor_bank_account WHERE mentor_id = $mentor_id";
    $result=mysqli_query($con,$sql);
    if($row = mysqli_fetch_assoc($result))
    {
        $id = $row['id'];
        $sql_update_bank="UPDATE mentor_bank_account SET bank_name = '$bank', account_number = '$account', ifsc = '$ifsc' WHERE id = $id";
        $result_put_new_slot=mysqli_query($con,$sql_update_bank);
        /* When no data changes This code gives error */
        // if(mysqli_affected_rows($con) <= 0)
        // {
        //     $error = "ERROR 1: Something went wrong. Please try again later. ";
        //     ajax_error($error);
        // }
    }
    else
    {
        $sql_insert_bank="INSERT INTO mentor_bank_account (mentor_id, bank_name, account_number, ifsc) VALUES ($mentor_id, '$bank', '$account', '$ifsc')";
        $result_put_new_slot=mysqli_query($con,$sql_insert_bank);
        if($result_put_new_slot)
        {
            $bank_account_id = $con->insert_id;
        }
        else
        {
            $error = "ERROR 2: Something went wrong. Please try again later. ";
            ajax_error($error);
        }
    }

    $sql = "COMMIT";
    $result = mysqli_query($con, $sql);
    if(!$result)
    {
        $error = "commit: Something went wrong. Please try again later.";
        ajax_error($error);
    }
    else
    {
        $jsonarray["code"] = 0;
        $jsonarray["msg"] = " Bank Account Saved Successfully.";
        echo json_encode($jsonarray);
    }