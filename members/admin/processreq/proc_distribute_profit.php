<?php
    require_once("../../../includes/initialize.php");
    require("../logincheck.php");

    $jsonarray = array();

    $date=date('Y-m-d H:i:s');
    $sql = "START TRANSACTION";
    $result = mysqli_query($con, $sql);
    if(!$result)
    {
        $error = "Something went wrong. Please try again later.";
        ajax_error($error);
    }
    $meeting_id = sanitize_input($_POST["meeting_id"]);

    $sql = "SELECT cost_card_price, service_tax_price, platform_charges, mentor_id FROM meetings WHERE id = $meeting_id";
    $result = mysqli_query($con, $sql);
    $status_id = mysqli_fetch_assoc($result);
    if (!$status_id)
    {
        $error = "Something went wrong. Please try again later.";
        ajax_error($error);
    }

    $cost_card_price = $status_id['cost_card_price'];
    $service_tax_price = $status_id['service_tax_price'];
    $platform_charges = $status_id['platform_charges'];
    $mentor_id = $status_id['mentor_id'];

    $sql = "SELECT concat(fname,' ',lname) as name, m.id, m.fname, m.lname, m.email, m.cost_card_type_id, c.price FROM mentor m LEFT JOIN cost_card_type c ON c.id = m.cost_card_type_id WHERE m.id = $mentor_id";
    
    $result = mysqli_query($con, $sql);
    $mentor = mysqli_fetch_assoc($result);
    if (!$mentor)
    {
        $error = "Something went wrong. Please try again later.";
        ajax_error($error);
    }
    $mentor_cost_card_price = $mentor['price'];



    $sql_insert_men_comp = "UPDATE mentee_transactions SET transaction_status = 'closed', addedon = '$date' WHERE meetings_id = '$meeting_id'";
    $result_insert_men_comp = mysqli_query($con, $sql_insert_men_comp);
    $men_comp_id = mysqli_insert_id($con);
    


    $sql_insert_men_comp = "INSERT INTO mentor_transactions (meetings_id, amount, type, transaction_type, addedon) VALUES ('$meeting_id','$mentor_cost_card_price','credit','close','$date')";
    $result_insert_men_comp = mysqli_query($con, $sql_insert_men_comp);
    $men_comp_id = mysqli_insert_id($con);


    if(mysqli_affected_rows($con)<=0) 
    {
        $error = "Something went wrong while adding mentor company. Please try again later.";
        ajax_error($error);
    }

    $sql_insert_men_comp = "INSERT INTO taxes_transactions (meetings_id, amount, type, addedon) VALUES ('$meeting_id','$service_tax_price','debit','$date')";
    $result_insert_men_comp = mysqli_query($con, $sql_insert_men_comp);
    $men_comp_id = mysqli_insert_id($con);

    if(mysqli_affected_rows($con)<=0) 
    {
        $error = "Something went wrong while adding mentor company. Please try again later.";
        ajax_error($error);
    }

    $sql_insert_men_comp = "INSERT INTO admin_transactions (meetings_id, amount, type, addedon) VALUES ('$meeting_id','$platform_charges','debit','$date')";
    $result_insert_men_comp = mysqli_query($con, $sql_insert_men_comp);
    $men_comp_id = mysqli_insert_id($con);

    if(mysqli_affected_rows($con)<=0) 
    {
        $error = "Something went wrong while adding mentor company. Please try again later.";
        ajax_error($error);
    }

    $sql_insert = "update meetings set distribution_status=1 WHERE id=$meeting_id";

    $result_update = mysqli_query($con, $sql_insert);
    if(!$result_update) 
    {
        $error = "Something went wrong while cancelling meeting. Please try again later.";
        ajax_error($error);
    }
    
    $sql = "COMMIT";
    $result = mysqli_query($con, $sql);
    if(!$result)
    {
        $error = "commit: Something went wrong. Please try again later.";
        ajax_error($error);
    }
    else
    {
        
        // send email to mentor about settlement
        $sql_get_mentor_email="SELECT email FROM mentor WHERE id=$mentor_id";
                $result_get_mentor_email=mysqli_query($con,$sql_get_mentor_email);
                if($row=mysqli_fetch_assoc($result_get_mentor_email))
                {
                    $email=$row['email'];
                }

                $email_subject = "Your Meeting was Settled";
                $mailbody = "Hi,<br/><br/>
                            Distributed Profit Settlement <br>
                            Amount : ".$mentor_cost_card_price."<br>
                            Meeting ID : ".$meeting_id."<br>
                            Thanks<br/></br>
                            Regards<br/>
                           ";

                $mail_file = "../../../../../includes/class.phpmailer.php";
                $send_mail = send_mail($email_subject, $mailbody, $email, $mail_file);

                if(!$send_mail)
                {
                    $error = "ERROR 3: Something went wrong. Please try again later.";
                    $_SESSION['error'] = $error;
                }
                
        $jsonarray["code"] = 0;
        $jsonarray["msg"] = "Settled Successfully.";
        echo json_encode($jsonarray);
        
    }