<?php
    require_once("../../../includes/initialize.php");
    require("../logincheck.php");
    // $jsonarray = array();
    // $jsonarray["code"] = 1;
    // $jsonarray["last_id"] =$_POST;
   
    if(count($_POST)>0){
        $trans_id = sanitize_input($_POST["trans_id"]);
        $closing_remark = ucwords(sanitize_input($_POST["closing_remark"]));
        $transaction_status = sanitize_input($_POST["transaction_status"]);
        $date = date("Y-m-d H:i:s");

    $newmeeting_id="select meetings_id from mentee_transactions where id=$trans_id";
    $meeting_query=mysqli_query($con,$newmeeting_id);
    $meeting_fetch=mysqli_fetch_array($meeting_query);
    $newMeeting_value=$meeting_fetch['meetings_id'];
    
        if ($closing_remark) {
            $sql_update_men_comp = "UPDATE mentor_transactions SET transaction_type = 'close' WHERE meetings_id = $newMeeting_value ";
            $result_update_men_comp = mysqli_query($con, $sql_update_men_comp);
            if ($transaction_status == 'close')
            {
                $sql_transaction_update = "UPDATE mentee_transactions SET
                transaction_status_remark='$closing_remark', 
                transaction_status='closed', 
                modifiedon='$date'
                WHERE id='$trans_id'";
                $result_transaction_update = mysqli_query($con, $sql_transaction_update);
                
                $meetings="UPDATE meetings set meeting_status_id=(SELECT id FROM meeting_status WHERE codename = 'completed') WHERE id=$newMeeting_value";
                $meeting_result=mysqli_query($con,$meetings);

                if(!$result_transaction_update) 
                {
                    $error = "Something went wrong while update. Please try again later.";
                    ajax_error($error);
                }
                else 
                {
    
                    $jsonarray["code"] = 0;
                    $jsonarray["msg"] = "Transaction closed successfully.";
                    echo json_encode($jsonarray);
    
                }
            }

            elseif ($transaction_status == 'refund')
            {
                $sql = "SELECT meetings_id, amount, transaction_status FROM mentee_transactions WHERE id = $trans_id";
                $result = mysqli_query($con, $sql);
                $transaction = mysqli_fetch_assoc($result);
                if (!$transaction)
                {
                    $error = "Something went wrong. Please try again later.";
                    ajax_error($error);
                }
                $meeting_id = $transaction['meetings_id'];
                $cost_card_price = $transaction['amount'];

                $sql = "SELECT codename FROM meeting_status WHERE id = (SELECT meeting_status_id FROM meetings WHERE id = $meeting_id)";
                
                $result = mysqli_query($con, $sql);
                $meeting_status = mysqli_fetch_assoc($result);
                if (in_array($meeting_status['codename'], ['scheduled', 'completed']) )
                {
                    $error = "Scheduled and completed meeting cannot be refunded";
                    ajax_error($error);
                }

                $sql_transaction_update = "UPDATE mentee_transactions SET
                transaction_status_remark='Refunded', 
                transaction_status='closed', 
                modifiedon='$date'
                WHERE id='$trans_id'";
    
                $result_transaction_update = mysqli_query($con, $sql_transaction_update);
                
                // NOTE Should not update
                // $meetings_refund="UPDATE meetings set meeting_status_id=(SELECT id FROM meeting_status WHERE codename = 'completed') where id=$newMeeting_value";
                // $meeting_result=mysqli_query($con,$meetings_refund);

                if(!$result_transaction_update) 
                {
                    $error = "Something went wrong while update. Please try again later.";
                    ajax_error($error);
                }

                $sql_insert = "INSERT INTO mentee_transactions (meetings_id, amount, type, transaction_status,transaction_status_remark, addedon) VALUES ('$meeting_id','$cost_card_price','credit','refund', '$closing_remark','$date')";

                $result_transaction_update = mysqli_query($con, $sql_insert);
                
                if(!$result_transaction_update) 
                {
                    $error = "Something went wrong while update Topic. Please try again later.";
                    ajax_error($error);
                }
                else 
                {
                       // send email to mentee
                        $mentee_id ="SELECT mentee_id from meetings where id=$newMeeting_value";
            $mentee_result=mysqli_query($con,$mentee_id);
            $mentee_fetch=mysqli_fetch_array($mentee_result);

                $sql_get_mentee_email="SELECT email FROM mentee WHERE id='".$mentee_fetch['mentee_id']."'";
                $result_get_mentee_email=mysqli_query($con,$sql_get_mentee_email);
                if($row=mysqli_fetch_assoc($result_get_mentee_email))
                {
                    $email=$row['email'];
                }

                $email_subject = "Your Amount Refunded";
                $mailbody = "Hi,<br/><br/>
                             your settlement is as follows<br>
                            Meeting ID : ".$meeting_id."<br>
                            Amount : ".$cost_card_price."<br>
                            <br/>
                           
                            Thanks<br/>
                            Regards<br/>
                          ";

                $mail_file = "../../../../../includes/class.phpmailer.php";
                $send_mail = send_mail($email_subject, $mailbody, $email, $mail_file);

                if(!$send_mail)
                {
                    $error = "ERROR 3: Something went wrong. Please try again later.";
                    $_SESSION['error'] = $error;
                }
    
                    
                    $jsonarray["code"] = 0;
                    $jsonarray["msg"] = "Transaction refunded successfully.";
                    echo json_encode($jsonarray);
    
                }
            }


        }else{
            $error = "Please add transaction closing remark.";
            ajax_error($error);
        }


    }
    else 
    {
        $error = "Something went wrong . Please try again later.";
        ajax_error($error);
    }