<?php
    require_once("../../../includes/initialize.php");
    require("../logincheck.php");
    // $jsonarray = array();
    // $jsonarray["code"] = 1;
    // $jsonarray["last_id"] =$_POST;
   
    if(count($_POST)>0)
    {
       
       if(isset($_POST['action']) && $_POST['action']=="add")
       {
            $category = sanitize_input($_POST["category_id"]);
            $name = ucwords(sanitize_input($_POST["name"]));
            $codename = str_replace(" ","_",strtolower($name));
            $addedon=date("Y-m-d");
            if(!isset($_POST['is_category']))
            {
                $sql_category_insert = "INSERT INTO 
                category(name, codename, active, addedon)
                VALUES('".$name."','".$codename."', '1', '".$addedon."')";
                // print_r($sql_category_insert);exit;
                $result_category_insert = mysqli_query($con, $sql_category_insert);
                $lastid_category_insert = mysqli_insert_id($con); 
                if(mysqli_affected_rows($con)<=0) 
                {
                    $error = "Something went wrong while adding category. Please try again later.";
                    ajax_error($error);
                }
                else 
                {
                    $jsonarray["code"] = 0;
                    $jsonarray["msg"] = "Category Successfully added.";
                    echo json_encode($jsonarray);
                }
            }
            else 
            {
                if(isset($_POST['is_category']) && isset($_POST['name']) && !empty($_POST['name']))
                {
                    $sql_category_insert = "INSERT INTO 
                    category(name, codename, parent_id,active, addedon)
                    VALUES('".$name."','".$codename."','".$category."', '1', '".$addedon."')";
                    // print_r($sql_category_insert);exit;
                    $result_category_insert = mysqli_query($con, $sql_category_insert);
                    $lastid_category_insert = mysqli_insert_id($con); 
                    if(mysqli_affected_rows($con)<=0) 
                    {
                        $error = "Something went wrong while adding category. Please try again later.";
                        ajax_error($error);
                    }
                    else 
                    {
                        $jsonarray["code"] = 0;
                        $jsonarray["msg"] = "Category Successfully added.";
                        echo json_encode($jsonarray);
                    }
                }
                else 
                {
                    $error = "Something went wrong while adding category. Please try again later.";
                    ajax_error($error);
                }
            }
       }
       if(isset($_POST['action']) && $_POST['action']=="edit")
       {
        $category_id = sanitize_input($_POST["category_id"]);
        $name = ucwords(sanitize_input($_POST["name"]));
        $codename = str_replace(" ","_",strtolower($name));
        $id = sanitize_input($_POST["category_id"]);
        $modifiedon=date("Y-m-d");
        
            if(!isset($_POST['is_category']))
            {
                
                $category = sanitize_input($_POST["categoryid"]);
                $id = sanitize_input($_POST["id"]);
                $sql_category_update = "UPDATE category SET
                name='".$name."', 
                codename='".$codename."',
                modifiedon='".$modifiedon."',
                parent_id=NULL
                WHERE id='".$id."'";
                
                $result_category_update = mysqli_query($con, $sql_category_update);
                if(!$result_category_update) 
                {
                    $error = "Something went wrong while updating category. Please try again later.";
                    ajax_error($error);
                }
                else 
                {
                    $jsonarray["code"] = 0;
                    $jsonarray["msg"] = "Category Successfully Updated.";
                    echo json_encode($jsonarray);
                }
            }
              
            else 
            {
                $category = sanitize_input($_POST["categoryid"]);
                $id = sanitize_input($_POST["id"]);
                $sql_category_update = "UPDATE category SET
                name='".$name."', 
                codename='".$codename."',
                modifiedon='".$modifiedon."',
                parent_id='".$category_id."'
                WHERE id='".$id."'";
                
                $result_category_update = mysqli_query($con, $sql_category_update);
                if(!$result_category_update) 
                {
                    $error = "Something went wrong while updating category. Please try again later.";
                    ajax_error($error);
                }
                else 
                {
                    $jsonarray["code"] = 0;
                    $jsonarray["msg"] = "Category Successfully Updated.";
                    echo json_encode($jsonarray);
                }
            }
       }
       if(isset($_POST['action']) && $_POST['action']=="delete")
       {
        $id=$_POST['id'];
        $status=$_POST['status'];
        $sql_update="UPDATE category SET active=$status WHERE id=$id";
        $result_update=mysqli_query($con,$sql_update);
        if(!$result_update)
        {
            $error = "Something went wrong while deleting category. Please try again later.";
            ajax_error($error);
        }
        else 
        {
            $jsonarray["code"] = 0;
            $jsonarray["msg"] = "Success.";
            echo json_encode($jsonarray);
        }
       }
    }
   