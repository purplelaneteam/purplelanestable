<?php
   require_once("../../../includes/initialize.php");
   require("../logincheck.php"); 

   $all_sub_cat = [];
   $cat_id = mysqli_real_escape_string($con, $_GET["id"]);
   $sql_sub_cat = "SELECT id, name FROM category WHERE parent_id = '$cat_id' ORDER BY name ASC";
   $result_sub_cat = mysqli_query($con, $sql_sub_cat);
   if($result_sub_cat)
   {
       foreach ($result_sub_cat as $key => $myrow_sub_cat) {
           
           $all_sub_cat[] = [
               'id' => $myrow_sub_cat['id'],
               'name' => $myrow_sub_cat['name'],
           ];
       }
   }
   $jsonarray["sub_categories"] = $all_sub_cat;
   
   echo json_encode($jsonarray);
?>