<?php
	require_once("../../../includes/initialize.php");
    require("../logincheck.php");
	 
	// initilize all variable
    $params = array(); 
    $columns = array();
    $totalRecords = array();
    $data = array();
    $table = 'mentee_transactions mt, meetings m, meeting_status ms, orders o';
	$params = $_REQUEST;

	//define index of column
	$columns = array(
		0 => 'mt.meetings_id', 
        1 => 'o.order_id',
        2 => 'mt.type',
        3 => 'o.coupon',
		4 => 'mt.amount',
		5 => 'mt.amount',
		6 => 'mt.amount',
		7 => 'mt.amount',
        8 => 'ms.name',
        9 => 'mt.transaction_status',
        10 => 'mt.transaction_status_remark',
        11 => 'mt.addedon',
	);

    $where = "WHERE mt.meetings_id = m.id AND m.meeting_status_id = ms.id AND o.order_id = m.order_id";
    $sqlTot = "";
    $sqlRec = "";

	// check search value exist
	if(!empty($params['search']['value']) ) {   
		$where .=" AND ";
		$where .=" ( mt.meetings_id LIKE '".$params['search']['value']."%' ";    
        $where .=" OR mt.amount LIKE '".$params['search']['value']."%' ";    
        $where .=" OR mt.transaction_status LIKE '".$params['search']['value']."%' "; 
        $where .=" OR mt.transaction_status_remark LIKE '".$params['search']['value']."%' "; 
		$where .=" OR mt.type LIKE '".$params['search']['value']."%' )";
	}

	// getting total number records without any search
	$sql = "SELECT o.order_id, mt.id as trans_id, mt.meetings_id, o.total, o.discount, mt.amount, mt.type, ms.name, mt.transaction_status, mt.transaction_status_remark, mt.addedon, m.mentor_id
    FROM $table ";
 //echo $sql;
	$sqlTot .= $sql;
    $sqlRec .= $sql;
     
	//concatenate search sql if value exist
	if(isset($where) && $where != '') {

		$sqlTot .= $where;
		$sqlRec .= $where;
	}
    $column = "";
    //print_r($columns[$params['order'][0]['column']]);
    
    $column .= $columns[$params['order'][0]['column']]; 
    

 	$sqlRec .=  " ORDER BY ". $column." ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
//   echo $sqlRec;exit;
    $queryTot = mysqli_query($con, $sqlTot);

    $totalRecords = mysqli_num_rows($queryTot);

	$queryRecords = mysqli_query($con, $sqlRec) /* or die("error to fetch employees data") */;

    //iterate on results row and create new index array of data
    $i = 1;
	while( $row = mysqli_fetch_array($queryRecords) ) { 
       // print_r($row);exit;
        $mentor_id = $row['mentor_id'] ;
        $actionoption = '';
        $active_status = '';
        $profile_pic = '';
        $addedon = date('d M Y', (strtotime($row['addedon'])));


        $coupon = getCoupon($row['meetings_id']);
        $coupon = $coupon['coupon'];

        // $discountAmt = getDiscountOnly($coupon, $row['amount']);
        $discountAmt = $row['discount'];
        $totalAmountBeforeDiscount = $row['amount'] + $discountAmt;
        $mentorAmount=getMentorRate($con,$row['mentor_id']);
        
        $img = MENTOR_IMAGE_UPLOAD_FOLDER_LINK.$row["profile_pic"];
        $profile_pic .= '<img src="'.$img.'" width="50px" height="50px">';
        if($row['is_active'] == 1)
        {
            $active_status = '<span style="width: 100px;"><span class="m-badge  m-badge--success m-badge--wide">Active</span></span>';
        }   
        else
        {
            $active_status = '<span style="width: 100px;"><span class="m-badge  m-badge--danger m-badge--wide">Inactive</span></span>';
        }   
        
        $view_edit = '';
        if($row['type'] == 'debit' && $row['transaction_status'] == 'open')
        {
            $actionoption .= '<a href="javascript:void(0)" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill update_status" data-trans_id="'.$row['trans_id'].'" data-trans_remark="'.$row['transaction_status_remark'].'" data-refund="'.$row['amount'].'" data-close="'.$mentorAmount.'" title="Edit Transaction Status")">
            <i class="fas fa-edit" ></i>
            </a>';
        }
        else
        {
            $actionoption .='<a href="javascript:void(0)" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill view_status" data-trans_id="'.$row['trans_id'].'" data-trans_remark="'.$row['transaction_status_remark'].'" title="View")">
            <i class="fas fa-eye"></i> 
            </a>';
        }
        $data[] = array(
              $row['meetings_id'],
              $row['order_id'],
              ucwords($row['type']),
              $coupon,
              $row['amount'],
              $discountAmt,
              $totalAmountBeforeDiscount,
              $mentorAmount,
              ucwords($row['name']),
              ucwords($row['transaction_status']),
              $row['transaction_status_remark'] != '' ? ucwords($row['transaction_status_remark']) : '-',
              $addedon,
              $actionoption
        );
        $i ++;
	}	

	$json_data = array(
			"draw"            => intval( $params['draw'] ),   
			"recordsTotal"    => intval( $totalRecords ),  
			"recordsFiltered" => intval($totalRecords),
			"data"            => $data   // total data array
			);

	echo json_encode($json_data);  // send data as json format
?>
	