<?php
    require_once("../../../includes/initialize.php");
    require("../logincheck.php");
    // $jsonarray = array();
    // $jsonarray["code"] = 1;
    // $jsonarray["last_id"] =$_POST;
   
    if(count($_POST)>0)
    {
       
       if(isset($_POST['action']) && $_POST['action']=="add")
       {
            $category_id = sanitize_input($_POST["category_id"]);
            $name = ucwords(sanitize_input($_POST["name"]));
            $addedon=date("Y-m-d H:i:s");
            if(!empty($_POST['category_id']) && !empty($_POST['name']) )
            {
                $sql_topic_insert = "INSERT INTO 
                topic(name, category_id, active, addedon)
                VALUES('".$name."','".$category_id."', '1', '".$addedon."')";
                $result_topic_insert = mysqli_query($con, $sql_topic_insert);
                $lastid_topic_insert = mysqli_insert_id($con); 
                if(mysqli_affected_rows($con)<=0) 
                {
                    $error = "Something went wrong while adding Topic. Please try again later.";
                    ajax_error($error);
                }
                else 
                {
                    $jsonarray["code"] = 0;
                    $jsonarray["msg"] = "Topic Successfully added.";
                    echo json_encode($jsonarray);
                }
            }
            else 
            {
                $error = "All Field Mandatory";
                ajax_error($error);
            }
          
       }
       elseif(isset($_POST['action']) && $_POST['action']=="edit")
       {
          
            $id = sanitize_input($_POST["id"]);
            $category_id = sanitize_input($_POST["category_id"]);
            $name = ucwords(sanitize_input($_POST["name"]));
            $modifiedon=date("Y-m-d H:i:s");
            if(!empty($_POST['category_id']) && !empty($_POST['name']) )
            {
                $sql_topic_update = "UPDATE topic SET
                name='".$name."',
                category_id='".$category_id."', 
                modifiedon='".$modifiedon."' WHERE id='$id'";
                $result_topic_update = mysqli_query($con, $sql_topic_update);
              
                if(!$result_topic_update) 
                {
                    $error = "Something went wrong while update Topic. Please try again later.";
                    ajax_error($error);
                }
                else 
                {
                    $jsonarray["code"] = 0;
                    $jsonarray["msg"] = "Topic Successfully Update.";
                    echo json_encode($jsonarray);
                }
            }
            else 
            {
                $error = "All Field Mandatory";
                ajax_error($error);
            }
       }
       elseif(isset($_POST['action']) && $_POST['action']=="delete")
       {
            $id=$_POST['id'];
            $status=$_POST['status'];
            if(!empty($_POST['id']))
            {
                $sql_topic_update = "UPDATE topic SET
                active='$status' WHERE id='$id'";
                $result_topic_update = mysqli_query($con, $sql_topic_update);
                if(!$result_topic_update) 
                {
                    $error = "Something went wrong while delete Topic. Please try again later.";
                    ajax_error($error);
                }
                else 
                {
                    $jsonarray["code"] = 0;
                    $jsonarray["msg"] = "Topic Successfully Delete.";
                    echo json_encode($jsonarray);
                }
            }
            else 
            {
                $error = "All Field Mandatory";
                ajax_error($error);
            }
       }
       else 
        {
            $error = "Something went wrong . Please try again later.";
            ajax_error($error);
        }
    }
    else 
    {
        $error = "Something went wrong . Please try again later.";
        ajax_error($error);
    }
   