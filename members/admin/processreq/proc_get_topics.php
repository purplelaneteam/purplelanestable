<?php
   require_once("../../../includes/initialize.php");
   require("../logincheck.php"); 

                  
   $sub_cat_id = mysqli_real_escape_string($con, $_GET["id"]);
   $all_topics = [];
  
   $sql_topics = "SELECT id, name FROM topic WHERE category_id = '".$sub_cat_id."' ORDER BY name ASC";
   $result_topics = mysqli_query($con, $sql_topics);
   if($result_topics)
   {
       foreach ($result_topics as $key => $myrow_topics)
       {
            $all_topics[] = [
                'id' => $myrow_topics['id'],
                'name' => $myrow_topics['name'],
            ];
       }
   }

   $jsonarray["topics"] = $all_topics;
   echo json_encode($jsonarray);