<?php
    require_once("../../../includes/initialize.php");
    require("../logincheck.php");
    $jsonarray = array();

    $sql = "START TRANSACTION";
    $result = mysqli_query($con, $sql);
    if(!$result)
    {
        $error = "Something went wrong. Please try again later.";
        ajax_error($error);
    }
    
    $mentor_id = sanitize_input($_POST['mentor_id']);
    $name = sanitize_input($_POST['name']);
    $description = sanitize_input($_POST['description']);

    if(!$mentor_id || !$name || !$description)
    {
        $jsonarray['code']="0";
        $jsonarray['msg'] = "All field is mandatory";
        echo json_encode($jsonarray);exit;
    }

    
    $sql_insert_education="INSERT INTO mentor_education (mentor_id, name, description) VALUES ($mentor_id, '$name', '$description')";
    $result_put_new_slot=mysqli_query($con,$sql_insert_education);
    if($result_put_new_slot)
    {
        $bank_account_id = $con->insert_id;
    }
    else
    {
        $error = "ERROR 2: Something went wrong. Please try again later. ";
        ajax_error($error);
    }

    $sql = "COMMIT";
    $result = mysqli_query($con, $sql);
    if(!$result)
    {
        $error = "commit: Something went wrong. Please try again later.";
        ajax_error($error);
    }
    else
    {
        $jsonarray["code"] = 0;
        $jsonarray["msg"] = "Qualification Saved Successfully.";
        echo json_encode($jsonarray);
    }