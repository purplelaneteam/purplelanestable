<?php
    require_once("../../../includes/initialize.php");
    require("../logincheck.php");
    
    $arraycols = array(
        'Actions',
		'mentor',
        'mentee',
        'topic',
        'status',
	);

    $limit = "";

    if (isset($_REQUEST['start']) && $_REQUEST['start'] != '-1')
    {
        $limit = $_REQUEST['length'];
        $offset = $_REQUEST['start'];
        $limit = "LIMIT ".sanitize_input($offset).", ".sanitize_input($limit);
    }

    $order_column = sanitize_input($_REQUEST['order'][0]['column']);
    $order_column_dir = sanitize_input($_REQUEST['order'][0]['dir']);


    $tables = ' meetings m
    LEFT JOIN mentor mn ON mn.id = m.mentor_id
    LEFT JOIN mentee mt ON mt.id = m.mentee_id
    LEFT JOIN meeting_status ms ON ms.id = m.meeting_status_id
    LEFT JOIN meeting_selected_slots msl ON msl.id = m.meeting_selected_slots_id
    LEFT JOIN topic t ON t.id = m.topic_id ';

    $status = '';
    if ($_POST['status'] == 'cancelled')
    {
        $status = " ms.codename IN ('mentee cancelled', 'mentor cancelled', 'admin cancelled')";
    }
    else
    {
        $status = " ms.codename = '". $_POST['status']. "'";
    }
    
    $where = "WHERE $status";

    for($i=0;$i<count($_REQUEST['columns']);$i++)
    {
        $column = sanitize_input($_REQUEST['columns'][$i]['data']);
        $searchable = sanitize_input($_REQUEST['columns'][$i]['searchable']);
        $value = sanitize_input($_REQUEST['columns'][$i]['search']['value']);
        
        if($column == 'id')
        {
            if($value !="")
            {
                $where .= " AND m.id LIKE '%$value%' ";
            }
        }
        else if($column == 'mentor')
        {
            if($value !="")
            {
                $where .= " AND mn.fname LIKE '%$value%' ";
            }
        }
        else if($column == 'mentee')
        {
            if($value !="")
            {
                $where .= " AND mt.fname LIKE '%$value%' ";
            }
        }
        else if($column == 'topic')
        {
            if($value !="")
            {
                $where .= " AND t.name LIKE '%$value%' ";
            }
        }
        else if($column == 'status')
        {
            if($value !="")
            {
                $where .= " AND ms.name LIKE '%$value%' ";
            }
        }
        else if($column == 'addedon')
        {
            if($value !="")
            {
                $where .= " AND ms.addedon LIKE '%$value%' ";
            }
        }
        else if($searchable=='true')
        { 
            $column;  
            if($value !="")
            {
                $where .= " AND a.$column like '%$value%' ";
            }
        }
    }
    // $where .= " AND a.application_status_id IN (select id FROM application_status where codename IN('application_added') )";

    $where .= " ORDER BY ";
    
    //ORDER BY
    if($arraycols[$order_column] == "id")
    {
        $where .= "m.id $order_column_dir, ";
    }

    else if($arraycols[$order_column] == "mentor")
    {
        $where .= "mn.fname $order_column_dir, ";
    }
    else if($arraycols[$order_column] == "mentee")
    {
        $where .= "mt.fname $order_column_dir, ";
    }
    else if($arraycols[$order_column] == "topic")
    {
        $where .= "t.name $order_column_dir, ";
    }
    else if($arraycols[$order_column] == "status")
    {
        $where .= "ms.name $order_column_dir, ";
    }
    else
    {
        $where .= " a.$arraycols[$order_column] $order_column_dir, ";
    }

    $where = substr_replace($where, "", -2);
   

    $sql =  "SELECT SQL_CALC_FOUND_ROWS m.id, mn.email as mnemail, mt.email as mtemail, msl.meeting_date, msl.start_time, m.id, mn.fname as mentor, mt.fname as mentee, t.name as topic, ms.name as status, ms.codename, m.distribution_status, m.addedon, m.hangout_link
            FROM  $tables  $where
            $limit";
//   echo $sql;exit;

   $result = mysqli_query($con, $sql);
    
    /* Data set length after filtering */
    $sqlcnt = "SELECT FOUND_ROWS()";
    $resultcnt = mysqli_query($con, $sqlcnt);
    $myrowcnt = mysqli_fetch_array($resultcnt);
    $filteredtotal = $myrowcnt[0];

    /* Total data set length */
    $sqlcnt = "SELECT COUNT(*) FROM $tables $where";
    $resultcnt = mysqli_query($con, $sqlcnt);
    $myrowcnt = mysqli_fetch_array($resultcnt);
    $total_records = $myrowcnt[0];
    
    if($result && $myrow = mysqli_fetch_array($result))
    {
        $sr_no = 1;
        do
        {
			$id = $myrow["id"];
			$menteeSlots = "" ;
			$sql = "SELECT meeting_date, start_time FROM meeting_selected_slots WHERE meetings_id = $id ORDER BY addedon ASC LIMIT 2";
			// dd($sql);
			$data = mysqli_query($con, $sql);
			if ($data && mysqli_num_rows($data) === 2) {
				$data1 = mysqli_fetch_assoc($data);
				$data2 = mysqli_fetch_assoc($data);
				$menteeSlots = $data1['meeting_date'] ." ". $data1['start_time'] ."<br>". $data2['meeting_date'] ." ". $data2['start_time'];
			}

            $mentor = $myrow['mentor'] . " &lt;".$myrow['mnemail']."&gt;";
            $mentee = $myrow['mentee'] . " &lt;".$myrow['mtemail']."&gt;";
            $topic = $myrow['topic'] ;
            $mentorSlot = $myrow['meeting_date'] . " " . $myrow['start_time'] ;
            $status = $myrow['status'] ;
            $codename = $myrow['codename'] ;
			$addedon = date('d M Y', strtotime($myrow["addedon"]));
			$action_dom = '';
			$actionsArray = [];
            if ($codename === 'pending')
            {
                $action_dom = '<span> 
                <a href="reschedule_meeting.php?meeting_id='.$id.'" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="Reschedule Meeting" > <i class="fa fa-calendar-plus"></i>
                </a>
                <form class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="Cancel Meeting" id="cancel_appointment" ><input type="hidden" name="meeting_id" id="result" value="'.$id.'" /><i class="fa fa-calendar-minus"></i></form>
                </a>
                 </span>';
            }
            elseif ($codename === 'completed')
            {
                if($myrow['distribution_status'] == 0){
                $action_dom = '
                <a data-meeting_id="'.$id.'" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill distribute_profits" title="Distribute" > <i class="flaticon-paper-plane"></i>
                </a>
                 ';
                }else{
                    $action_dom = '
                    <a class="m-portlet__nav-link" title="Distribute" >Distributed</a>'; 
                }
            }
            elseif ($codename === 'scheduled')
            {
                // if (date_create() > date_create($myrow['meeting_date'] . ' ' . $myrow['start_time'] . ' +30min'))
                // {
                    $action_dom = '
                    <a data-meeting_id="'.$id.'" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill complete_appointment" title="Complete" > <i class="fa fa-calendar-check"></i>
                    </a>
                    <form class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="Cancel Meeting" id="cancel_appointment" ><input type="hidden" name="meeting_id" id="result" value="'.$id.'" /><i class="fa fa-calendar-minus"></i></form>
                </a>
                 </span>';
                // }
                // else
                // {
                    $action_dom .= '<a data-meeting_id="'.$id.'" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill insert-hangout-link" title="'. (($myrow['hangout_link'])? $myrow['hangout_link'] : "") .'" > <i class="fa fa-video"></i>
                    </a>';
                // }
            }
         elseif ($codename === 'rescheduled')
            {

                    $sql = "SELECT meeting_date, start_time FROM meeting_selected_slots WHERE meetings_id = $id ORDER BY id DESC LIMIT 1";
                    // dd($sql);
                    $data = mysqli_query($con, $sql);
                    if ($data && mysqli_num_rows($data) === 1) {
                        $data = mysqli_fetch_assoc($data);
                        $menteeSlots = $data['meeting_date'] ." ". $data['start_time'];
                    }
            
                    $action_dom = '
                    <a href="reschedule_meeting.php?meeting_id='.$id.'&type=reschedule" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="Reschedule Meeting" > <i class="fa fa-calendar-plus"></i>
                    </a>
                    <a data-meeting_id="'.$id.'" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill complete_appointment" title="Complete" > <i class="fa fa-calendar-check"></i>
                    </a>
                    <form class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="Cancel Meeting" id="cancel_appointment" ><input type="hidden" name="meeting_id" id="result" value="'.$id.'" /><i class="fa fa-calendar-minus"></i></form>
                </a>
                 </span>';
               
                    // $action_dom .= '<a data-meeting_id="'.$id.'" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill insert-hangout-link" title="'. (($myrow['hangout_link'])? $myrow['hangout_link'] : "") .'" > <i class="fa fa-video"></i>
                    // </a>';
               
			}
			if($action_dom) $actionsArray  = ["Actions" => $action_dom,];
			$records["aaData"][] = array_merge($actionsArray, array(
				"id" => $id,
				"mentor"=>$mentor,
				"mentee"=>$mentee,
				"topic" => $topic,
				'mentee_slots' => $menteeSlots,
				'mentor_slots' => $mentorSlot,
				"status" => $status,
			));
          
            
            $sr_no++; 
        }while($myrow = mysqli_fetch_array($result));
    }
    else
    {
        $records["aaData"] = [];
    }

    $secho = 0;
    if ( isset( $_REQUEST['sEcho'] ) ) {
        $secho = intval( $_REQUEST['sEcho'] );
    }

    $result = [
        'iTotalRecords'        => $total_records,
        'iTotalDisplayRecords' => $filteredtotal,
        'sEcho'                => $secho,
        'sColumns'             => '',
        'aaData'               => $records["aaData"],
    ];
    

echo json_encode($result, JSON_PRETTY_PRINT );