<?php
    require_once("../../../includes/initialize.php");
    require("../logincheck.php"); 
                   
    $cat_id = mysqli_real_escape_string($con, $_POST["cat_id"]);
    $client_id = sanitize_input($_SESSION['USER']['USER_ID']);
    $all_sub_cat = "";
   
    $sql_sub_cat = "SELECT id, name FROM category WHERE parent_id = '".$cat_id."' ORDER BY name ASC";
    $result_sub_cat = mysqli_query($con, $sql_sub_cat);
    if($result_sub_cat && $myrow_sub_cat = mysqli_fetch_array($result_sub_cat))
    {
        $all_sub_cat = '<div class="form-group col-lg-4 m-form__group-sub col-lg-cust sub_cat_div">
                            <label for="category">Sub Category <span class="m--font-danger">*</span></label>
                                    <select class="form-control category sub_cat_div" required name="category">
                                        <option value="">Select Sub Category</option>
                    ';
        do
        {
            $sub_cat_id = $myrow_sub_cat["id"];
            $sub_cat_name = $myrow_sub_cat["name"];
            
            $all_sub_cat .= '<option value="'.$sub_cat_id.'">'.$sub_cat_name.'</option>';
        }
        while($myrow_sub_cat = mysqli_fetch_array($result_sub_cat));
        $all_sub_cat .= '<select>
                        </div>';
    }else {
        $sql_sub_topic = "SELECT id, name FROM topic WHERE category_id = '".$cat_id."' AND active = 1 ORDER BY name ASC";
        $result_sub_topic = mysqli_query($con, $sql_sub_topic);
        if($result_sub_topic && $myrow_sub_topic = mysqli_fetch_array($result_sub_topic)){

            $all_sub_cat = '<div class="form-group col-lg-4 m-form__group-sub col-lg-cust sub_cat_div">
                                <label for="category">Topic <span class="m--font-danger">*</span></label>
                                        <select class="form-control sub_cat_div" required name="topic_id" id="topic_id">
                                            <option value="">Select Topic</option>
                        ';
            do
            {
                $sub_cat_id = $myrow_sub_topic["id"];
                $sub_cat_name = $myrow_sub_topic["name"];
                
                $all_sub_cat .= '<option value="'.$sub_cat_id.'">'.$sub_cat_name.'</option>';
            }
            while($myrow_sub_topic = mysqli_fetch_array($result_sub_topic));
            $all_sub_cat .= '<select>
                            </div>';
        }
    }

    echo $all_sub_cat;
?>