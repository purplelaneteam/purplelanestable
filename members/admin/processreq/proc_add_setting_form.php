<?php
    require_once("../../../includes/initialize.php");
    require("../logincheck.php");
    $jsonarray = array();
    if(count($_POST)>0)
    {
        $attribute = $_POST["attribute"];
        $count_total = count($attribute);
        $implode_attribute = implode(",",$attribute);
        $value = $_POST["value"];
        $i=0;
        $value_sql = '';
        $coma = ',';
        while($i<$count_total){
            $temp_attribute = $attribute[$i];
            $code_attribute = getCodename($temp_attribute);
            $temp_value = $value[$i];
            if($i==($count_total-1)){
                $coma = '';
            }
            $value_sql .= "('".$temp_attribute."','".$code_attribute."','".$temp_value."',now())$coma";
            $i++;
        }
        $result_st = mysqli_query($con, "START TRANSACTION");
        if(!$result_st){
            ajax_error("Something went wrong. ");
        }
        $sql_delete = "TRUNCATE settings";
        $result_delete = mysqli_query($con, $sql_delete);
        if($result_delete===true){
            $sql_check_attribute = 'SELECT id FROM `settings` WHERE name IN ("' . implode('", "', $attribute) . '")';
            $result_check_attribute = mysqli_query($con, $sql_check_attribute);
            if(mysqli_num_rows($result_check_attribute) > 0){
                ajax_error("Attribute Name  Already Exists.");
            }
            $sql_insert = "INSERT INTO `settings`(`name`,`codename`,`price`,`addedon`) VALUES $value_sql";
            $result_insert = mysqli_query($con, $sql_insert);
            if($result_insert === true){
                $result_c = mysqli_query($con, "COMMIT");
                if(!$result_c){
                    ajax_error("Something went wrong. ");
                } else {
                    ajax_send(0,"Content added successfully");
                }
            } else {
                ajax_error("Something went wrong, Please Try again.");
            }
        } else {
            ajax_error("Something went wrong.");
        }
    }
?>