<?php
    require_once("../../../includes/initialize.php");
    require("../logincheck.php");
    $jsonarray = array();

    $sql = "START TRANSACTION";
    $result = mysqli_query($con, $sql);
    if(!$result)
    {
        $error = "Something went wrong. Please try again later.";
        ajax_error($error);
    }
    $slot1 = $_POST["slot1"].':00';
    $comment = sanitize_input($_POST["comment"]);
    $meeting_id = sanitize_input($_POST["meeting_id"]);

    if($slot1 =="" || $comment=="" || $meeting_id=="")
    {
        $jsonarray['code']="0";
        $jsonarray['msg'] = "All field is mandatory";
        echo json_encode($jsonarray);exit;
    }
    $slot=explode(' ',$slot1);
    // Change Meeting Slot in table
    $selected_date = strtotime("$slot1");
    $meeting_date = date("Y-m-d", $selected_date);
    $start_time = date("H:i:s", $selected_date);
    $end_time = date("H:i:s", strtotime("$slot1 + 30min"));

    $sql = "SELECT id FROM meeting_status WHERE codename = 'rescheduled'";

    $result = mysqli_query($con, $sql);
    $status_id = mysqli_fetch_assoc($result)['id'];
    if (!$status_id)
    {
        $error = "Something went wrong while adding meeting. Please try again later.";
        ajax_error($error);
    }

    $sql = "SELECT * FROM  meetings WHERE id = $meeting_id";
    $result = mysqli_query($con, $sql);
    $meeting = mysqli_fetch_assoc($result);

    $sql_insert = "INSERT INTO `meetings_status_history` SET 
        meetings_id = '".$meeting['id']."',
        meetings_status_id = '".$meeting['meeting_status_id']."',
        meeting_selected_slots_id = NULL,
        meeting_date = '".$meeting['meeting_date']."',
        cost_card_price = '".$meeting['cost_card_price']."',
        service_tax_price = '".$meeting['service_tax_price']."',
        platform_charges = '".$meeting['platform_charges']."',
        selected_time = '".$meeting['selected_time']."',
        comments = '".$meeting['comments']."'
    ";

    $result_insert = mysqli_query($con, $sql_insert);
    if(!$result_insert) 
    {
        $error = "Something went wrong while adding meeting. Please try again later.";
        ajax_error($error);
    }


    $sql_insert = "INSERT INTO meeting_selected_slots (meetings_id, meeting_date, start_time, end_time) VALUES ($meeting_id, '$meeting_date', '$start_time', '$end_time')";
    $result_insert = mysqli_query($con, $sql_insert);
    if(!$result_insert) 
    {
        $error = "Something went wrong while adding meeting. Please try again later.";
        ajax_error($error);
    }


    $sql_update = "UPDATE meetings set selected_time='$start_time', meeting_date='$meeting_date', meeting_status_id=$status_id, comments = '$comment' WHERE id = $meeting_id";

    $result_update = mysqli_query($con, $sql_update);
    if(!$result_update) 
    {
        $error = "Something went wrong while adding meeting. Please try again later.";
        ajax_error($error);
    }

    /* EMAIL SENDING */
    
    if (SEND_EMAIL_RESCHEDULING_MENTOR)
    {
        $sql = "SELECT email, fname, lname FROM mentor WHERE id = ".$meeting['mentor_id'];
        $result = mysqli_query($con, $sql);
        $mentor = mysqli_fetch_assoc($result);
        if (!$mentor)
        {
            $error = "Unable To send email";
            ajax_error($error);
        }
        else
        {
            $email_subject = "Your ".SITE_NAME." Meeting Rescheduled";
            $mailbody = "Hi ".ucwords($mentor['fname']. " " . $mentor['lname']).",<br/><br/>
                        <p>Your meeting has been rescheduled</p>
                        <a href='".SITE_URL."/mentor/my_appointments.php'>See your rescheduled meeting.</a><br/><br/>
                        Thanks<br/>
                        Regards<br/>
                        '".CONTACTUS_FROM_NAME."'";

            $send_mail = send_mail($email_subject, $mailbody, $mentor['email']);
        }
    }

    if (SEND_EMAIL_RESCHEDULING_MENTEE)
    {
        $sql = "SELECT email, fname, lname FROM mentee WHERE id = ".$meeting['mentee_id'];
        $result = mysqli_query($con, $sql);
        $mentee = mysqli_fetch_assoc($result);
        if (!$mentee)
        {
            $error = "Unable To send email";
            ajax_error($error);
        }
        else
        {
            $email_subject = "Your ".SITE_NAME." Meeting Rescheduled";
            $mailbody = "Hi ".ucwords($mentee['fname']. " " . $mentee['lname']).",<br/><br/>
                        <p>Your meeting has been rescheduled</p>
                        <a href='".SITE_URL."'>Check your meetings here</a><br/><br/>
                        Thanks<br/>
                        Regards<br/>
                        '".CONTACTUS_FROM_NAME."'";

            $send_mail = send_mail($email_subject, $mailbody, $mentee['email']);
        }
    }
    
    
    

    
    $sql = "COMMIT";
    $result = mysqli_query($con, $sql);
    if(!$result)
    {
        $error = "commit: Something went wrong. Please try again later.";
        ajax_error($error);
    }
    else
    {
        $jsonarray["code"] = 0;
        $jsonarray["msg"] = "Rescheduled Successfully.";
        echo json_encode($jsonarray);
    }