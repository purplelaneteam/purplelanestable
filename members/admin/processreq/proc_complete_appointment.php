<?php
    require_once("../../../includes/initialize.php");
    require("../logincheck.php");

    $jsonarray = array();

    $sql = "START TRANSACTION";
    $result = mysqli_query($con, $sql);
    if(!$result)
    {
        $error = "Something went wrong. Please try again later.";
        ajax_error($error);
    }
    $meeting_id = sanitize_input($_POST["meeting_id"]);

    $sql = "SELECT id FROM meeting_status WHERE codename = 'completed'";

    $result = mysqli_query($con, $sql);
    $status_id = mysqli_fetch_assoc($result)['id'];
    if (!$status_id)
    {
        $error = "Something went wrong while adding meeting. Please try again later.";
        ajax_error($error);
    }

    $sql_insert = "update meetings set meeting_status_id=$status_id WHERE id=$meeting_id";

    $result_update = mysqli_query($con, $sql_insert);
    if(!$result_update) 
    {
        $error = "Something went wrong while completing meeting. Please try again later.";
        ajax_error($error);
    }

    $sql = "SELECT * FROM  meetings WHERE id = $meeting_id";
    $result = mysqli_query($con, $sql);
    $meeting = mysqli_fetch_assoc($result);
    
    // insert feedback participant records : Added by Manoj

    $mentor_id = $meeting['mentor_id'];
    $mentee_id = $meeting['mentee_id'];
    $mentee_token =md5(time());
    $sql_mentee_insert = "INSERT INTO feedback_participants SET meetings_id = $meeting_id, participant_id = $mentee_id, participant_type = 'mentee',status = 'inprog' , feedback_token ='$mentee_token' ";

    $result_insert = mysqli_query($con, $sql_mentee_insert);
    if(!$result_insert) 
    {   
        $error = "Something went wrong while adding mentee feedback row. Please try again later.";
        ajax_error($error);
    }
    $mentee_feedback_id = mysqli_insert_id($con);

    $mentor_token =md5(time()+1);
    $sql_mentee_insert = "INSERT INTO feedback_participants SET meetings_id = $meeting_id, participant_id = $mentor_id, participant_type = 'mentor',status = 'inprog' , feedback_token ='$mentor_token' ";

    $result_insert = mysqli_query($con, $sql_mentee_insert);
    if(!$result_insert) 
    {   
        $error = "Something went wrong while adding mentor feedback row. Please try again later.";
        ajax_error($error);
    }
    $mentor_feedback_id = mysqli_insert_id($con);

    // insert feedback participant records : end
    

    // send email to mentee    
    $sql = "SELECT email, fname, lname FROM mentee WHERE id = ".$meeting['mentee_id'];
    $result = mysqli_query($con, $sql);
    $mentee = mysqli_fetch_assoc($result);
    if (!$mentee)
    {
        $error = "Unable To send email";
        ajax_error($error);
    }
    else
    {
        $email_subject = "Your Meeting Completed";
        $mailbody = "Hi ".ucwords($mentee['fname']. " " . $mentee['lname']).",<br/> 
                    <p>Your meeting with mentor has been completed. Hope you had a good talk with mentor and clarified your questions. </p>
                    <a href='".SITE_URL."/mentee_feedback.php?feedbackid=".$mentee_feedback_id."&feedbacktoken=".$mentee_token."'>Please provide your feedback here.</a><br/><br/>
                    Thanks &
                    Regards<br/>
                    '".CONTACTUS_FROM_NAME."'";

        $send_mail = send_mail($email_subject, $mailbody, $mentee['email']);
    }

  // send email to mentor
    $sql = "SELECT email, fname, lname FROM mentor WHERE id = ".$meeting['mentor_id'];
    $result = mysqli_query($con, $sql);
    $mentor = mysqli_fetch_assoc($result);
    if (!$mentor)
    {
        $error = "Unable To send email to mentor";
        ajax_error($error);
    }
    else
    {
        $email_subject = "Your Meeting Was Completed";
        $mailbody = "Hi ".ucwords($mentor['fname']. " " . $mentor['lname']).",<br/> 
                    <p>Your meeting has been completed</p>
                    <a href='".SITE_URL."/mentor_feedback.php?feedbackid=".$mentor_feedback_id."&feedbacktoken=".$mentor_token."'>Please provide your feedback here.</a><br/><br/>
                    Thanks &
                    Regards<br/>
                    '".CONTACTUS_FROM_NAME."'";

        $send_mail = send_mail($email_subject, $mailbody, $mentor['email']);
    }
    
    $sql = "COMMIT";
    $result = mysqli_query($con, $sql);
    if(!$result)
    {
        $error = "commit: Something went wrong. Please try again later.";
        ajax_error($error);
    }
    else
    {
        $jsonarray["code"] = 0;
        $jsonarray["msg"] = "Meeting Completed Successfully.";
        echo json_encode($jsonarray);
    }