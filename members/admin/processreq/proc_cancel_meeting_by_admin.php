<?php
    require_once("../../../includes/initialize.php");
    require("../logincheck.php");

    $jsonarray = array();

    $sql = "START TRANSACTION";
    $result = mysqli_query($con, $sql);
    if(!$result)
    {
        $error = "Something went wrong. Please try again later.";
        ajax_error($error);
    }
    $meeting_id = sanitize_input($_POST["meeting_id"]);

    $sql = "SELECT id FROM meeting_status WHERE codename = 'admin cancelled'";

    $result = mysqli_query($con, $sql);
    $status_id = mysqli_fetch_assoc($result)['id'];
    if (!$status_id)
    {
        $error = "Something went wrong while adding meeting. Please try again later.";
        ajax_error($error);
    }

    $sql_insert = "update meetings set meeting_status_id=$status_id WHERE id=$meeting_id";

    $result_update = mysqli_query($con, $sql_insert);
    if(!$result_update) 
    {
        $error = "Something went wrong while cancelling meeting. Please try again later.";
        ajax_error($error);
    }

    $sql = "SELECT * FROM  meetings WHERE id = $meeting_id";
    $result = mysqli_query($con, $sql);
    $meeting = mysqli_fetch_assoc($result);

    if (SEND_EMAIL_CANCEL_MENTOR)
    {
        $sql = "SELECT email, fname, lname FROM mentor WHERE id = ".$meeting['mentor_id'];
        $result = mysqli_query($con, $sql);
        $mentor = mysqli_fetch_assoc($result);
        if (!$mentor)
        {
            $error = "Unable To send email";
            ajax_error($error);
        }
        else
        {
            $email_subject = "PurpleLane.in: Meeting Cancelled - $meeting_id";
            $mailbody = "Hi ".ucwords($mentor['fname']. " " . $mentor['lname']).",<br/> 
                        <p>Your meeting has been cancelled</p>
                        <a href='".SITE_URL."'>Check your meetings here</a><br/><br/>
                        Thanks & 
                        Regards<br/>
                        PurpleLane Team";

            $send_mail = send_mail($email_subject, $mailbody, $mentor['email']);
        }
    }

    if (SEND_EMAIL_CANCEL_MENTEE)
    {
        $sql = "SELECT email, fname, lname FROM mentee WHERE id = ".$meeting['mentee_id'];
        $result = mysqli_query($con, $sql);
        $mentee = mysqli_fetch_assoc($result);
        if (!$mentee)
        {
            $error = "Unable To send email";
            ajax_error($error);
        }
        else
        {
            $email_subject = "PurpleLane.in: Meeting Cancelled - $meeting_id";
            $mailbody = "Hi ".ucwords($mentee['fname']. " " . $mentee['lname']).",<br/> 
                        <p>Your meeting has been cancelled. We will initiate the refund process.</p>
                        <a href='".SITE_URL."/mentee/my_appointments.php'>See your canceled meeting.</a><br/><br/>
                         Thanks & 
                        Regards<br/>
                        PurpleLane Team";

            $send_mail = send_mail($email_subject, $mailbody, $mentee['email']);
        }
    }
    
    $sql = "COMMIT";
    $result = mysqli_query($con, $sql);
    if(!$result)
    {
        $error = "commit: Something went wrong. Please try again later.";
        ajax_error($error);
    }
    else
    {
        $jsonarray["code"] = 0;
        $jsonarray["msg"] = "Deleted Successfully.";
        echo json_encode($jsonarray);
    }