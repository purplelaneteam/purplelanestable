<?php
	require_once("../../../includes/initialize.php");
    require("../logincheck.php");
	 
	// initilize all variable
    $params = array(); 
    $columns = array();
    $totalRecords = array();
    $data = array();
    $table = 'orders ';
	$params = $_REQUEST;

	//define index of column
	$columns = array(
		0 => 'id', 
		1 => 'transaction_id', 
        2 => 'amount',
        3 => 'coupon',
        4 => 'coupon',
        5 => 'coupon',
        6 => 'status',
        7 => 'addedon',
	);

    $sqlTot = "";
    $sqlRec = "";
    
	// check search value exist
	if(!empty($params['search']['value']) ) {   
        $where = "WHERE ";
        // $where .=" AND ";
		$where .=" ( transaction_id LIKE '%".$params['search']['value']."%' ";    
        $where .=" OR amount LIKE '%".$params['search']['value']."%' ";    
        $where .=" OR order_id LIKE '%".$params['search']['value']."%' "; 
        $where .=" OR coupon LIKE '%".$params['search']['value']."%' "; 
		$where .=" OR status LIKE '%".$params['search']['value']."%' )";
	}

	// getting total number records without any search
	$sql = "SELECT  order_id, coupon, transaction_id, amount, total, discount, status, addedon
    FROM $table ";
 //echo $sql;
	$sqlTot .= $sql;
    $sqlRec .= $sql;
     
	//concatenate search sql if value exist
	if(isset($where) && $where != '') {

		$sqlTot .= $where;
		$sqlRec .= $where;
	}
    $column = "";
    //print_r($columns[$params['order'][0]['column']]);
    
    $column .= $columns[$params['order'][0]['column']]; 
    

 	$sqlRec .=  " ORDER BY ". $column." ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
//   echo $sqlRec;exit;
$queryTot = mysqli_query($con, $sqlTot);
//   print_r($queryTot);exit;

    $totalRecords = mysqli_num_rows($queryTot);

	$queryRecords = mysqli_query($con, $sqlRec) /* or die("error to fetch employees data") */;

    //iterate on results row and create new index array of data
    $i = 1;
	while( $row = mysqli_fetch_array($queryRecords) ) { 
       // print_r($row);exit;
        $mentor_id = $row['mentor_id'] ;
        $actionoption = '';
        $active_status = '';
        $profile_pic = '';
        $addedon = date('d M Y', (strtotime($row['addedon'])));
       
        $coupon = $row['coupon'];
        // $coupon = $coupon['coupon'];

        // $discountAmt = getDiscountOnly($coupon, $row['amount']);
        $discountAmt = $row['discount'];
        $totalAmountBeforeDiscount = $row['amount'] + $discountAmt;
        $menterAmount= $row['amount'];
        $data[] = array(
              $row['order_id'],
              $row['transaction_id'],
              $row['amount'],
              $row['coupon'],
              $discountAmt,
              $totalAmountBeforeDiscount,
              $row['status'],
              $addedon,
            
        );
        $i ++;
	}	

	$json_data = array(
			"draw"            => intval( $params['draw'] ),   
			"recordsTotal"    => intval( $totalRecords ),  
			"recordsFiltered" => intval($totalRecords),
			"data"            => $data   // total data array
			);

	echo json_encode($json_data);  // send data as json format
?>
	