<?php
    require_once("../../../includes/initialize.php");
    require("../logincheck.php");
   
    if(count($_POST)>0)
    {
       
       if(isset($_POST['action']) && $_POST['action']=="add")
       {
            $cost_card_id = $_POST["cost_card_id"];
            $price = $_POST["price"];
            $addedon=date("Y-m-d H:i:s");
            if(!empty($_POST['cost_card_id']) && !empty($_POST['price']))
            {
                $sql_topic_insert = "INSERT INTO 
                    cost_card_type_prices(cost_card_type_id, cost_card_type_price, addedon)
                    VALUES('".$cost_card_id."','".$price."','".$addedon."')";
                $result_topic_insert = mysqli_query($con, $sql_topic_insert);
                $lastid_topic_insert = mysqli_insert_id($con); 
                if(mysqli_affected_rows($con)<=0) 
                {
                    $error = "Something went wrong while adding Card Price. Plese make sure price is not added to same card.";
                    ajax_error($error);
                }
                else 
                {
                    $jsonarray["code"] = 0;
                    $jsonarray["msg"] = "Card price Successfully added.";
                    echo json_encode($jsonarray);
                }
            }
            else 
            {
                $error = "All Field Mandatory";
                ajax_error($error);
            }
          
       }
       elseif(isset($_POST['action']) && $_POST['action']=="edit")
       {
            $id = sanitize_input($_POST["id"]);
            $cost_card_id = $_POST["cost_card_id"];
            $price = $_POST["price"];
            $modifiedon=date("Y-m-d H:i:s");
            if(!empty($_POST['cost_card_id']) && !empty($_POST['price']))
            {
                $sql_topic_update = "UPDATE cost_card_type_prices SET
                    cost_card_type_id ='".$cost_card_id."',
                    cost_card_type_price='".$price."', 
                    modifiedon='".$modifiedon."' 
                    WHERE id='$id'";
                $result_topic_update = mysqli_query($con, $sql_topic_update);
              
                if(!$result_topic_update) 
                {
                    $error = "Something went wrong while adding Card Price. Plese make sure price is not added to same card.";
                    ajax_error($error);
                }
                else 
                {
                    $jsonarray["code"] = 0;
                    $jsonarray["msg"] = "Card Price Successfully Update.";
                    echo json_encode($jsonarray);
                }
            }
            else 
            {
                $error = "All Field Mandatory";
                ajax_error($error);
            }
       }
       elseif(isset($_POST['action']) && $_POST['action']=="delete")
       {
            $id=$_POST['id'];
            $status=$_POST['status'];
            if(!empty($_POST['id']))
            {
                $sql_topic_update = "UPDATE topic SET
                active='$status' WHERE id='$id'";
                $result_topic_update = mysqli_query($con, $sql_topic_update);
                if(!$result_topic_update) 
                {
                    $error = "Something went wrong while delete Topic. Please try again later.";
                    ajax_error($error);
                }
                else 
                {
                    $jsonarray["code"] = 0;
                    $jsonarray["msg"] = "Topic Successfully Delete.";
                    echo json_encode($jsonarray);
                }
            }
            else 
            {
                $error = "All Field Mandatory";
                ajax_error($error);
            }
       }
       else 
        {
            $error = "Something went wrong . Please try again later.";
            ajax_error($error);
        }
    }
    else 
    {
        $error = "Something went wrong . Please try again later.";
        ajax_error($error);
    }
   