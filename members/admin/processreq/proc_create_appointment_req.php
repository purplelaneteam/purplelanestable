<?php
    require_once("../../../includes/initialize.php");
    require_once("../logincheck.php");
    $cost_card_price = getMentorRate($con,'', $_POST['mentor_id']);

    $service_tax_price = mysqli_fetch_assoc(getSettingsPrice($con, 'service_tax_price'));
    $platform_charges = mysqli_fetch_assoc(getSettingsPrice($con, 'platform_charges'));

    $data = $_POST;

    $email = sanitize_input($data['email']);
    
    $sql = "SELECT email, fname, lname, id FROM mentee WHERE email = '$email'";
    $result = mysqli_query($con, $sql);
    $mentee = mysqli_fetch_assoc($result);
    if (!$mentee)
    {
        $jsonarray["code"] = 1;
        $jsonarray["msg"] = "Mentee is not registered";
        echo json_encode($jsonarray);exit;
    }

    
    
    $data['cost_card_price'] = $cost_card_price;
    
    $data['service_tax_price'] = $service_tax_price['price'];
    $data['platform_charges'] = $platform_charges['price'];

    $data['mentee_id'] = $mentee['id'];
    
    $data = serialize($data);
    /* Check input before order */
    $slot1 = $_POST["slot1"].':00';
    $slot2 = $_POST["slot2"].':00';

    $is_date_valid = check_date_time($slot1,$slot2);

    $selected_date1 = strtotime("$slot1");
    $selected_date2 = strtotime("$slot2");
    
    $meeting_date1 = date("Y-m-d", $selected_date1);
    $start_time1 = date("H:i:s", $selected_date1);
    $end_time1 = date("H:i:s", strtotime("$slot1 + 30min"));

    $meeting_date2 = date("Y-m-d", $selected_date2);
    $start_time2 = date("H:i:s", $selected_date2);
    $end_time2 = date("H:i:s", strtotime("$slot2 + 30min"));

    $comment = sanitize_input($_POST["comment"]);
    $title = sanitize_input($_POST["title"]);
    $mentor_id = sanitize_input($_POST["mentor_id"]);
    $topic_id = sanitize_input($_POST["topic_id"]);
    
    $date = date('Y-m-d H:i:s');

    

    if(!($slot1 || $slot2 ) || !$comment || !$mentor_id || !$topic_id || !$mentee['id'])
    {
        $error = "All field is mandatory";exit;
        ajax_error($error);
    }
    /* Generate order when inputs are present */
    $order_id = "ORDS" . rand(10000,99999999);
    $temp_key = generateRandomString(20);
    $customer_id = $_SESSION['id'];
    $sql_insert = "INSERT INTO orders SET order_id = '$order_id', customer_id = '$customer_id', data = '$data', temp_key = '$temp_key'";

    $result = mysqli_query($con, $sql_insert);
    if(!$result) 
    {
        $error = "Something went wrong while adding meeting. Please try again later.";
        ajax_error($error);exit;
    }
    else
    {
        $jsonarray["code"] = 0;
        $jsonarray["msg"] = "Created successfully";
        echo json_encode($jsonarray);
    }

    if (SEND_EMAIL_APPOINTMENT_MENTEE)
    {
        $email_subject = "Your ".SITE_NAME." Meeting has been created";
        $mailbody = "Hi ".ucwords($mentee['fname']. " " . $mentee['lname']).",<br/><br/>
                    <p>Your meeting has been created.</p>
                    <p>Link will expire in 10min</p>
                    <a href='".SITE_URL."/mentee/appointment_created_proceed_to_pay.php?order=$temp_key'>You can proceed to pay.</a><br/><br/>
                    Thanks<br/>
                    Regards<br/>
                    '".CONTACTUS_FROM_NAME."'";

        $send_mail = send_mail($email_subject, $mailbody, $mentee['email']);
    }
    
    ?>
