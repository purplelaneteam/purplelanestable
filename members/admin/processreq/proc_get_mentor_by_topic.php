<?php
   require_once("../../../includes/initialize.php");
   require("../logincheck.php");

   $mentors = '';
   $topic_id = mysqli_real_escape_string($con, $_GET["id"]);
   $result_sub_cat = getMentorByTopics($con, $topic_id);
   if($result_sub_cat)
   {
       $admin_mentor_info = true;
       foreach ($result_sub_cat as $key => $myrow_mentor) {
            $mentor = selectedMentorInfoArray($con, $myrow_mentor);
            ob_start();
            include '../../../mentee/include/mentor_info.php';
            $string = ob_get_clean();
            $mentors .= $string;
        //    $mentors[] = getMentorBasicInfo($con, $myrow_mentor);
       }
   }
   $jsonarray["mentors"] = $mentors;
   
   echo json_encode($jsonarray);
?>