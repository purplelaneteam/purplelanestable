<?php
	require_once("../../../includes/initialize.php");
    require("../logincheck.php");
	 
	// initilize all variable
    $params = array(); 
    $columns = array();
    $totalRecords = array();
    $data = array();
    $table = 'mentor m, cost_card_type cct';
	$params = $_REQUEST;

	//define index of column
	$columns = array(
		0 => 'm.fname', 
		1 => 'm.email',
		2 => 'm.mobile',
        3 => 'm.paytm_mobile',
		4 => 'm.total_experience',
        5 => 'cct.name',
        6 => 'm.overided_price',
        7 => 'm.is_active',
	);

    $where = "WHERE cct.id = m.cost_card_type_id";
    $sqlTot = "";
    $sqlRec = "";

	// check search value exist
	if(!empty($params['search']['value']) ) {   
		$where .=" AND ";
		$where .=" ( m.fname LIKE '".$params['search']['value']."%' OR m.lname LIKE '".$params['search']['value']."%'";    
		$where .=" OR m.email LIKE '".$params['search']['value']."%' ";    
		$where .=" OR m.mobile LIKE '".$params['search']['value']."%' ";
		$where .=" OR m.paytm_mobile LIKE '".$params['search']['value']."%' ";
        $where .=" OR m.total_experience LIKE '".$params['search']['value']."%' ";
        $where .=" OR cct.name LIKE '".$params['search']['value']."%' ";
        $where .=" OR cct.price LIKE '".$params['search']['value']."%' ";
		$where .=" OR m.is_active LIKE '".$params['search']['value']."%' )";
	}

	// getting total number records without any search
	$sql = "SELECT m.id as mentor_id, CONCAT(m.fname, ' ' , m.lname) as name , cct.name as cctname, cct.price as cctprice, m.email, m.mobile, m.paytm_mobile, m.total_experience, m.profile_pic , m.is_active, m.addedon, m.overided_price
    FROM $table ";
 
	$sqlTot .= $sql;
    $sqlRec .= $sql;
     
	//concatenate search sql if value exist
	if(isset($where) && $where != '') {

		$sqlTot .= $where;
		$sqlRec .= $where;
	}
    $column = "";
    //print_r($columns[$params['order'][0]['column']]);
    
    $column .= $columns[$params['order'][0]['column']]; 
    

 	$sqlRec .=  " ORDER BY ". $column." ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
    // dd($sqlRec);
	$queryTot = mysqli_query($con, $sqlTot);


	$totalRecords = mysqli_num_rows($queryTot);

	$queryRecords = mysqli_query($con, $sqlRec) or die("error to fetch employees data");

    //iterate on results row and create new index array of data
    $i = 1;
	while( $row = mysqli_fetch_array($queryRecords) ) { 
       // print_r($row);exit;
        $mentor_id = $row['mentor_id'] ;
        $actionoption = '';
        $active_status = '';
        $profile_pic = '';
        $addedon = date('d M Y', (strtotime($row['addedon'])));
        
        $img = MENTOR_IMAGE_UPLOAD_FOLDER_LINK.$row["profile_pic"];
        $profile_pic .= '<img src="'.$img.'" width="50px" height="50px">';
        if($row['is_active'] == 1)
        {
            $active_status = '<span style="width: 100px;"><span class="m-badge  m-badge--success m-badge--wide">Active</span></span>';
        }   
        else
        {
            $active_status = '<span style="width: 100px;"><span class="m-badge  m-badge--danger m-badge--wide">Inactive</span></span>';
        }   
        
       $actionoption = '';
        if($row['is_active'] == 1)
        {
            $status = "'Deactivate'";
            $actionoption .= '<input type="hidden" name="active_deactive" value="'.$row['is_active'].'"/>
            <div class="links"><a href="javascript:void(0)" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="Deactivate" onclick="activate_deactivate_mentor('.$status.', '.$mentor_id.')">
            <i class="fas fa-thumbs-down" ></i>
            </a>';
        }
        else
        {
            $status = "'Activate'";
            $actionoption .='<input type="hidden" name="active_deactive" value="'.$row['active'].'"/>
            <div class="links"><a href="javascript:void(0)" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="Activate" onclick="activate_deactivate_mentor('.$status.','.$mentor_id.')">
            <i class="fas fa-thumbs-up"></i> 
            </a>';
        }
        $actionoption .='<a href="edit_mentor.php?mentor_id='.$mentor_id.'" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="Edit Mentor" > <i class="fas fa-edit"></i>
        </a>';
        $actionoption .='<a href="view_mentor.php?mentor_id='.$mentor_id.'" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="View Mentor" > <i class="far fa-eye"></i>
        </a></div>';

        $data[] = array(
              $row['name'],
              $row['email'],
              $row['mobile'],
              $row['paytm_mobile'],
              $row['total_experience'],
              $row['cctprice'],
              $row['overided_price'],
              $active_status,
              $actionoption,
        );
        $i ++;
	}	

	$json_data = array(
			"draw"            => intval( $params['draw'] ),   
			"recordsTotal"    => intval( $totalRecords ),  
			"recordsFiltered" => intval($totalRecords),
			"data"            => $data   // total data array
			);

	echo json_encode($json_data);  // send data as json format
?>
	