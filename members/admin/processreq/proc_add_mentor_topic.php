<?php
    require_once("../../../includes/initialize.php");
    require("../logincheck.php");
   
    if(count($_POST)>0)
    {
       
       if(isset($_POST['action']) && isset($_POST['mentor_id']) && $_POST['action']=="add")
       {
            $topic_id = $_POST["topic_id"];
            $mentor_id = $_POST["mentor_id"];
            $addedon=date("Y-m-d H:i:s");
            if(!empty($_POST['topic_id']))
            {
                $sql_topic_insert = "INSERT INTO 
                    mentor_categories(mentor_id, topic_id, addedon)
                    VALUES('".$mentor_id."','".$topic_id."','".$addedon."')";
                $result_topic_insert = mysqli_query($con, $sql_topic_insert);
                $lastid_topic_insert = mysqli_insert_id($con); 
                if(mysqli_affected_rows($con)<=0) 
                {
                    $error = "Something went wrong while delete Topic. Make sure topic is not added to this mentor. Please try again later.";
                    ajax_error($error);
                }
                else 
                {
                    $jsonarray["code"] = 0;
                    $jsonarray["msg"] = "Topic Successfully added.";
                    echo json_encode($jsonarray);
                }
            }
            else 
            {
                $error = "Please select topic.";
                ajax_error($error);
            }
          
       }
       elseif(isset($_POST['action']) && $_POST['action']=="delete")
       {
            $id=$_POST['delid'];
            if(!empty($_POST['delid']))
            {
                $sql_topic_update = "DELETE FROM mentor_categories
                WHERE id='$id'";
                $result_topic_update = mysqli_query($con, $sql_topic_update);
                if(!$result_topic_update) 
                {
                    $error = "Something went wrong while delete Topic. Please try again later.";
                    ajax_error($error);
                }
                else 
                {
                    $jsonarray["code"] = 0;
                    $jsonarray["msg"] = "Topic Successfully Delete.";
                    echo json_encode($jsonarray);
                }
            }
            else 
            {
                $error = "All Field Mandatory";
                ajax_error($error);
            }
       }
       else 
        {
            $error = "Something went wrong . Please try again later.";
            ajax_error($error);
        }
    }
    else 
    {
        $error = "Something went wrong . Please try again later.";
        ajax_error($error);
    }
   