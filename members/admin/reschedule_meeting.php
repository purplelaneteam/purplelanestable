<?php 
    require_once("../../includes/initialize.php");
    require_once("logincheck.php");
    $category = sanitize_input($_POST['category']);
    $date =  date('Y-m-d H:i:s');
    $success = "";
    $error = "";
    $status="activate";
    $status="deactivate";
    require_once("header.php");
    require_once("sidebar.php");
?>

<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">Reschedule Meeting</h3>
                <div class="form-group">
                            
                    <div style="display:none;" id="alert_div_basic" class="alert alert-danger alert-dismissible fade show" role="alert">
                        <p>Danger</p>
                        <button style="margin-top: -30px;" type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true"style="font-size: 31px;line-height: 0;">&times;</span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="m-content">
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__body">
                <?php 
                if($success!="")
                {
                ?>
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    </button>
                    <strong>Success!</strong> <?php echo $success; ?>
                </div>
                <?php 
                }
                ?>
                <?php 
                if($error!="")
                {
                ?>
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    </button>
                    <strong>Error!</strong> <?php echo $error; ?>
                </div>
                <?php 
                }
                ?>


                <div class="row">
                    <form id='reschedule_appointment_form' method='post' style='width:100%'>
                        <input type="hidden" value="<?php echo isset($_GET['type'])? $_GET['type']: '' ?>" id="reschedule_type">
                        <div class="col-md-12 col-sm-12 mb-5 mb-btm mb-md-0 order-md-0">
                            <div class="app-nfo app-info mb-4">
                                <h6 class="" style="padding-left: 0px">Choose Timings (Select start time to reschedule 30
                                    minute meeting) </h6>
                                <p class="spcev">
                                    <div class="row">
                                        <div class="col-md-3 col-sm-12">
                                            <div>
                                                <div id="picker"> </div>
                                                <input type="hidden" name='slot1' id="result" value="<?php echo date_format(date_create(), 'Y-m-d H:i') ?>" />
                                                <input type="hidden" name='meeting_id' id="meeting_id"
                                                    value="<?php echo $_GET['meeting_id']?>" />
                                            </div>
                                        </div>

                                        <!---/col-->
                                    </div>
                                </p>
                                <hr />
                                <h6 class="" style="padding-left: 0px">A brief overview of meeting </h6>

                                <p class="spcev">
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12">
                                            <textarea class="form-control" rows="5" name="comment" id="comment"
                                                placeholder="enter your message here"
                                                style="border:1px solid #ccc"></textarea>
                                        </div>
                                    </div>
                                    <input type='submit' class="btn btn-alpha mr-lg-3 mr-2 cnect" value='Reschedule'>
                                </p>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<?php require('footer.php');?>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.0/moment-with-locales.min.js">
</script>

<script type="text/javascript" src="/js/datetimepicker.js"></script>


<script type="text/javascript">
    $(document).ready(function () {
        minute = moment().minute()
		if (minute >= 30) minute = 30
		else minute = 0;
		const tomorrow = moment().add(1, 'days').minute(minute)

        $('#picker').dateTimePicker({selectDate: tomorrow, startDate: tomorrow, minuteIncrement: 30});
        $('#picker-no-time').dateTimePicker({
            showTime: false,
            dateFormat: 'DD/MM/YYYY'
        });
        $("#book_mentor").hide();

    })
</script>