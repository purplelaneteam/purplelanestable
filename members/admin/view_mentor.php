<?php 
    require_once("../../includes/initialize.php");
    require_once("logincheck.php");
    $erroroccured = 0;
    $errormsg = "";
    $action="add";  
    $tab = $_GET['tab'];

    $categories_sql = "SELECT id,name FROM category WHERE parent_id IS NULL";
    $categories_result = mysqli_query($con, $categories_sql);
    $categories = [];
    foreach($categories_result as $category_result_value){
        $categories[] = $category_result_value;
    }

    $companies = [];
    $sql_get_companies = "SELECT id, name, codename FROM company";
    $result_get_companies = mysqli_query($con, $sql_get_companies);
    while($myrow_get_company = mysqli_fetch_array($result_get_companies))
    {
        $companies[] = array("id"=>$myrow_get_company["id"],
                            "name"=>$myrow_get_company["name"],
                            "codename"=>$myrow_get_company["codename"]
        );
    }

    $skills = [];
    $sql_get_skills = "SELECT id, name, codename FROM skill";
    $result_get_skills = mysqli_query($con, $sql_get_skills);
    while($myrow_get_skill = mysqli_fetch_array($result_get_skills))
    {
        $skills[] = array("id"=>$myrow_get_skill["id"],
                            "name"=>$myrow_get_skill["name"],
                            "codename"=>$myrow_get_skill["codename"]
        );
    }

    if(isset($_GET['mentor_id']))
    {
        $mentor_id = sanitize_input($_GET['mentor_id']);
        $edit_query="SELECT fname, lname ,email,mobile,paytm_mobile,total_experience,profile_pic,cost_card_type_id,overided_price FROM mentor WHERE id='$mentor_id'";
        //   echo $edit_query;exit;
        if(!$edit_mentor=mysqli_query($con,$edit_query))
        {
            $erroroccured = 1;
            $errormsg = "ERROR_CUST_00:Something went wrong";
        }
        else
        {
            if($result_edit_mentor=mysqli_fetch_array($edit_mentor))
            {
                
                $fname=$result_edit_mentor['fname'];
                $lname=$result_edit_mentor['lname'];
                $email=$result_edit_mentor['email'];
                $mobile=$result_edit_mentor['mobile'];
                $paytm_mobile=$result_edit_mentor['paytm_mobile'];
                $total_experience=$result_edit_mentor['total_experience'];
                $profile_pic= MENTOR_IMAGE_UPLOAD_FOLDER_LINK.$result_edit_mentor['profile_pic'];
                $cost_card_type_id=$result_edit_mentor['cost_card_type_id'];
                $overrided_price=$result_edit_mentor['overided_price'];
                $skills = getSkillsOfMentor($con, $mentor_id);
                $company = mysqli_fetch_assoc(getCompanyOfMentor($con, $mentor_id));
                $cost_card_type = mysqli_fetch_assoc(getCostCardTypeOfMentor($con, $mentor_id));
                $bank = getMentorBankDeatils($con, $mentor_id);
                $education = getMentorEducation($con, $mentor_id);
                $experience = getMentorExperience($con, $mentor_id);
                
            }
        }
        $action="edit";

        $query_company = "SELECT company_id FROM mentor_company WHERE mentor_id='".$mentor_id."' ORDER BY id";
        $result_company = mysqli_query($con, $query_company);
        $row_company = mysqli_fetch_assoc($result_company);

        if($row_company == ''){
            $company_id = '';

        }else{
            $company_id = $row_company['company_id'];
        }

        $skills_selected = [];
        $query_skills_selected = "SELECT skill_id FROM mentor_skill WHERE mentor_id='".$mentor_id."'";
        $result_get_skills_selected = mysqli_query($con, $query_skills_selected);
        while($myrow_get_skills_selected = mysqli_fetch_array($result_get_skills_selected))
        {
            $skills_selected[] = $myrow_get_skills_selected["skill_id"];
        }

        $topics_sql = "SELECT mc.id, t.name as topic_name, c.id as cat_id, c.name as cat_name, mc.addedon as addedon FROM mentor_categories mc INNER JOIN topic t ON mc.topic_id = t.id INNER JOIN category c ON t.category_id = c.id WHERE mentor_id = $mentor_id";
        $topics_result = mysqli_query($con, $topics_sql);
        $topics = [];
        foreach($topics_result as $topic_result_value){
            $topics[] = $topic_result_value;
        }
    }
    $levels=getLevel($con);
    require_once("header.php"); 
    require_once("sidebar.php");
?>
<?php
if($erroroccured==1)
{
?>
<div class="alert alert-danger" role="alert">
    <?php echo $errormsg;?>
</div>
<?php
exit;
}
?>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-subheader">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">Add Mentor</h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__item">
                        <a href="#" class="m-nav__link">
                            <span class="m-nav__link-text">Manage Mentor</span>
                        </a>
                    </li>
                    <li class="m-nav__separator">-</li>
                    <li class="m-nav__item">
                        <a href="javascript:void(0)" class="m-nav__link">
                            <span class="m-nav__link-text">Edit Mentor</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="m-content" style="padding-top:10px;">
        <div class="m-portlet m-portlet--tabs">
            <div class="m-portlet__head">
                <div class="m-portlet__head-tools">
                    <ul class="nav nav-tabs m-tabs m-tabs-line m-tabs-line--brand m-tabs-line-danger" role="tablist">
                        <li class="nav-item m-tabs__item">
                            <a class="nav-link m-tabs__link <?php if($tab == '') echo 'active'; ?>" data-toggle="tab"
                                href="#basic_info" role="tab">
                                Basic Info
                            </a>
                        </li>
                        <li class="nav-item m-tabs__item">
                            <a class="nav-link m-tabs__link <?php if($tab == 'edit_tab') echo 'active'; ?>"
                                data-toggle="tab" href="#bank_account" role="tab">
                                Bank account details
                            </a>
                        </li>
                        <li class="nav-item m-tabs__item">
                            <a class="nav-link m-tabs__link <?php if($tab == 'edit_tab') echo 'active'; ?>"
                                data-toggle="tab" href="#education" role="tab">
                                Education
                            </a>
                        </li>
                        <li class="nav-item m-tabs__item">
                            <a class="nav-link m-tabs__link <?php if($tab == 'edit_tab') echo 'active'; ?>"
                                data-toggle="tab" href="#experience" role="tab">
                                Experience
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="m-portlet__body">
                <div class="tab-content">
                    <div class="tab-pane <?php if($tab == '') echo 'active'; ?>" id="basic_info">
                        <div class="jumbotron mentor-info">
                            <div class="info-container">
                                <img class="profile" src="<?php echo $profile_pic ?>" alt="" srcset="">
                                <div class="info-block">
                                    <div class="lead">
                                        <p class="title">First Name</p>
                                        <p class="info-detail"><?php echo $fname ?></p>
                                    </div>
                                    <div class="lead">
                                        <p class="title">Last Name</p>
                                        <p class="info-detail"><?php echo $lname ?></p>
                                    </div>
                                    <div class="lead">
                                        <p class="title">Email</p>
                                        <p class="info-detail"><?php echo $email ?></p>
                                    </div>
                                    <div class="lead">
                                        <p class="title">Mobile</p>
                                        <p class="info-detail"><?php echo $mobile ?></p>
                                    </div>
                                    <div class="lead">
                                        <p class="title">Paytm</p>
                                        <p class="info-detail"><?php echo $paytm_mobile ?></p>
                                    </div>
                                    <div class="lead">
                                        <p class="title">Total Experience</p>
                                        <p class="info-detail"><?php echo $total_experience ?> yrs.</p>
                                    </div>
                                </div>
                                <hr class="my-4">
                                <div class="info-block">
                                    <div class="lead">
                                        <p class="title">Cost Card Type</p>
                                        <p class="info-detail"><?php echo $cost_card_type['name']." (Rs. ".$cost_card_type['price'].")" ?></p>
                                    </div>
                                    <div class="lead">
                                        <p class="title">Over Rided Price</p>
                                        <p class="info-detail">Rs. <?php echo $overrided_price ?></p>
                                    </div>
                                    <div class="lead">
                                        <p class="title">Company</p>
                                        <p class="info-detail"><?php echo $company['name'] ?></p>
                                    </div>
                                    <div class="lead">
                                        <p class="title">Skills</p>
                                        <p class="info-detail">
                                            <?php 
                                                foreach($skills as $key => $skill)
                                                {
                                                    if ($key == 0)
                                                    {
                                                        echo $skill['name'];
                                                    }
                                                    else
                                                    {
                                                        echo ", ".$skill['name'];
                                                    }
                                                }
                                            ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="bank_account">
                        <div class="jumbotron mentor-info">
                            <div class="info-container">
                                <div class="info-block bank">
                                    <div class="lead">
                                        <p class="title">Bank Name</p>
                                        <p class="info-detail"><?php echo $bank['bank_name'] ?></p>
                                    </div>
                                    <div class="lead">
                                        <p class="title">Account Number</p>
                                        <p class="info-detail"><?php echo $bank['account_number'] ?></p>
                                    </div>
                                    <div class="lead">
                                        <p class="title">IFSC</p>
                                        <p class="info-detail"><?php echo $bank['ifsc'] ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="education">
                        <div class="jumbotron mentor-info">
                            <div class="info-container">
                                <div class="info-block">
                                    <div class="education">
                                        <?php foreach($education as $data): ?>
                                            <p class="title"><?php echo $data['name'] ?></p>
                                            <div class="description"><?php echo $data['description'] ?></div>
                                            <hr class="my-4">
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="experience">
                        <div class="jumbotron mentor-info">
                            <div class="info-container">
                                <div class="info-block">
                                    <div class="experience">
                                        <?php foreach($experience as $data): ?>
                                            <p class="title"><?php echo $data['name'] ?></p>
                                            <div class="description"><?php echo $data['description'] ?></div>
                                            <hr class="my-4">
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php require('footer.php');?>
