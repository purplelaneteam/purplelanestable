<?php 
    require_once("../../includes/initialize.php");
    require_once("logincheck.php");
    $category = sanitize_input($_POST['category']);
    $date =  date('Y-m-d H:i:s');
    $success = "";
    $error = "";
    $status="activate";
    $status="deactivate";
    require_once("header.php");
    require_once("sidebar.php");
?>

<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">List Transaction</h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__item m-nav__item--home">
                        <a href="" class="m-nav__link">
                            <span class="m-nav__link-text">Manage Transaction</span>
                        </a>
                    </li>
                   <li class="m-nav__separator">-</li>
                     <li class="m-nav__item">
                        <a href="" class="m-nav__link">
                            <span class="m-nav__link-text">List Mentee Transaction</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="m-content">
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__body">
                <?php 
                if($success!="")
                {
                ?>
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    </button>
                    <strong>Success!</strong> <?php echo $success; ?>
                </div>
                <?php 
                }
                ?>
                 <?php 
                if($error!="")
                {
                ?>
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    </button>
                    <strong>Error!</strong> <?php echo $error; ?>
                </div>
                <?php 
                }
                ?>
                <table class="table table-striped- table-bordered table-hover table-checkable" id="mentee_transc_list"> 
                    <thead>
                        <tr>
                            <th>Meeting Id</th>
                            <th>Order Id</th>
                            <th>Type</td>
                            <th>Coupon</td>
                            <th>Amount</td>
                            <th>Discount</th>
                            <th>Total Amount</th>
                            <th>Mentor Amount</th>
                            <th>Status</td>
                            <th>Transaction Status</td>
                            <th>Transaction Status Remark</td>
                            <th>Added on</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                  
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="modal" id="package-details-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Reason</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <form class="m-form m-form--fit m-form--label-align-right"  id="package_update_form" method="POST" action="javascript:void(0)">
                <div class="modal-body">
                    <div class="form-row">
                        <input type="hidden" name="package_id" id="package_id">
                        <div class="form-group col-lg-12 m-form__group-sub col-lg-cust">
                            <input type="file" class="form-control" name="package_featured_image" id="package_featured_image" placeholder="icon" required>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-success cancel" id="">Submit</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal" id="reason-detials-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Closing Details</h4>
                <button type="button" class="close" data-dismiss="modal" >&times;</button>
            </div>
            <form class="m-form m-form--fit m-form--label-align-right"  id="trans_status_update" method="POST" action="javascript:void(0)">
                <div class="modal-body">
                    <div class="form-row">
                        <input type="hidden" name="trans_id" id="trans_id">
                        <div class="form-group col-lg-6 m-form__group-sub col-lg-cust ">
                            <label for="transaction_status">Status<span class="m--font-danger">*</span></label>
                            <select class="form-control category_id" required name="transaction_status" id="transaction_status" >
                                <option value="">Select Status</option>
                                <option value="refund">Refund</option>
                                <option value="close">Closed</option>
                            </select>
                        </div>
                        <div class="col-lg-5 amount-refund-close-div">
                            <span id="amount-refund-close"></span>
                        </div>
                        <div class="form-group col-lg-12 m-form__group-sub col-lg-cust">
                            <label for="closing_remark">Remark<span class="m--font-danger">*</span></label>
                            <textarea class="form-control" name="closing_remark" id="closing_remark" required rows="3"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-success cancel" id="trans_submit">Submit & Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal" id="view-detials-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Closing Details</h4>
                <button type="button" class="close" data-dismiss="modal" >&times;</button>
            </div>
            
                <div class="modal-body">
                    <div class="form-row">
                        <div class="form-group col-lg-12 m-form__group-sub col-lg-cust">
                        <label for="name">Closing Remark<span class="m--font-danger">*</span></label>
                            <textarea class="form-control" disabled id="closing_remark_view" required rows="3"></textarea>
                        </div>
                    </div>
                </div>
        </div>
    </div>
</div>
<?php require('footer.php');?>
<script>
//  function activate_deactivate_package(empid,delid){  
//     var confirmation = confirm("are you sure you want to Activate this?");
//     if (confirmation) {
//     var action="Activate";
//      $.ajax({
//           url: "processreq/proc_add_mentor.php",
//           type: "POST",
//           data: {action:action,delid:delid},
//           success: function(){
//             setTimeout(location.reload.bind(location), 5000);
//           }
//       });
//        alert('Mentor Activated');
//     }
// }

// function activate_deactivate_package(empid,delid){  
//     var confirmation = confirm("are you sure you want to Deactivate this?");
//     if (confirmation) {
//     var action="Deactivate";
//      $.ajax({
//           url: "processreq/proc_add_mentor.php",
//           type: "POST",
//           data: {action:action,delid:delid},
//           success: function(){
//             // setTimeout(location.reload.bind(location), 5000);
//           }
//       });
//        alert('Mentor Deactivate');
//     }
// }

$(document).on('submit', '#trans_status_update', function (e) {
    e.preventDefault();
    const trans_id=$('#trans_id').val();
    const closing_remark=$('#closing_remark').val();
    const transaction_status=$('#transaction_status').val();
    $('#trans_submit').attr('disabled', true)
    console.log("SDFSDFSDf");
    
    $.ajax({
        type: 'POST',
        url: member_url_path + '/processreq/proc_change_trans_status.php',
        data: {trans_id, closing_remark, transaction_status},
        dataType: "json",
        success: function (response) {
            if (response.code) {
                swal('Error!', response.msg, 'error')
                $('#trans_submit').attr('disabled', false)
            } else {
                swal({
                    title: "Success!",
                    text: response.msg,
                    type: "success",
                }).then(function () {
                    window.location.href = "list_mentee_transc.php";
                });
            }
        },
        error: function (response) {
            $('#trans_submit').attr('disabled', false)
            swal('Error!', response.msg, 'error');
        }
    });
})

$(document).on('click', '.update_status', function () {
    $('#closing_remark').val('')
    $('#trans_id').val('');
    $('#transaction_status').val('');

    var trans_remark = $(this).attr('data-trans_remark')
    var trans_id = $(this).attr('data-trans_id')

    let refund = $(this).attr('data-refund');
    let close = $(this).attr('data-close');
    $('#amount-refund-close').html("")

    $('#transaction_status').change(function() {
        let value = $(this).val()
        if(value == 'refund') $('#amount-refund-close').html("<?php echo CURRENCY_SYMBOL?> "+ refund)
        if(value == 'close') $('#amount-refund-close').html("<?php echo CURRENCY_SYMBOL?> "+ close)
        
    })

    console.log(`refund: ${refund}`);
    console.log(`close: ${close}`);

    $()
    

    $('#closing_remark').text(trans_remark);
    $('#trans_id').val(trans_id);
    $('#reason-detials-modal').modal('toggle');
})

$(document).on('click', '.view_status', function () {
    var trans_remark = $(this).attr('data-trans_remark')
    var trans_id = $(this).attr('data-trans_id')
    $('#closing_remark_view').text(trans_remark);
    $('#view-detials-modal').modal('toggle');
})
</script>
