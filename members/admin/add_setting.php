<?php 
    require_once("../../includes/initialize.php");
    require_once("logincheck.php");
    require_once("header.php"); 
    require_once("sidebar.php");
    $sql = "SELECT name, price FROM settings";
    $result = mysqli_query($con, $sql);
?>
<div class="m-grid__item m-grid__item--fluid m-wrapper">    			    
<div class="m-subheader">
    <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">Add Setting</h3>
            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                <li class="m-nav__item m-nav__item--home">
                    <a href="#" class="m-nav__link m-nav__link--icon">
                        <i class="m-nav__link-icon la la-home"></i>
                    </a>
                </li>
                <li class="m-nav__item">
                    <a href="#" class="m-nav__link">
                        <span class="m-nav__link-text">Manage Setting</span>
                    </a>
                </li>
                <li class="m-nav__separator">-</li>
                <li class="m-nav__item">
                    <a href="javascript:void(0)" class="m-nav__link">
                        <span class="m-nav__link-text">Add Setting</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>
<div class="m-content" style="padding-top:10px;">
<form class="m-form m-form--fit m-form--label-align-right" id="add_setting_form" name="add_setting_form" method="POST" enctype=multipart/form-data>
        <div class="m-portlet m-portlet--tabs">
            <div class="m-portlet__body">
                <?php
                if(mysqli_num_rows($result) > 0){
                    $i=0;
                    while($row = mysqli_fetch_assoc($result)){
                        $attribute = $row['name'];
                        $value = $row['price'];
                        echo '
                        <div class="form-row more_views">
                            <div class="form-group col-lg-2 m-form__group-sub col-lg-cust">
                                <input class="form-control" name="attribute[]" id="attribute[]" placeholder="Enter Attribute" value="'.$attribute.'" required>
                            </div>
                            <div class="form-group col-lg-3 m-form__group-sub col-lg-cust">
                                <input class="form-control" name="value[]" id="value[]" placeholder="Enter Value" value="'.$value.'" required>
                            </div>';
                        if($i!=0){
                        echo '<div class="col-md-1">
                                <button type="button" class="btn m-btn--pill m-btn--air btn-danger" onClick="remove(this)"><i class="flaticon-delete-1"></i></button>
                            </div>';
                        }
                        echo '</div>';
                        $i++;
                    }
                } else { ?>
                    <div class="form-row" id="">
                        <div class="form-group col-lg-2 m-form__group-sub col-lg-cust">
                            <input class="form-control" name="attribute[]" id="attribute[]" placeholder="Enter Attribute" required>
                        </div>
                        <div class="form-group col-lg-3 m-form__group-sub col-lg-cust">
                            <input class="form-control" name="value[]" id="value[]" placeholder="Enter Value" required>
                        </div>
                    </div>
                    <?php
                }
                ?>
                <div id="add_more_div"></div>
                <div class="form-row">
                    <div class="form-group col-lg-cust">
                        <input class="btn btn-primary" type="button" name="add_more" id="add_more" value="Add More">
                    </div>
                    <div class="form-group col-lg-cust">
                        <button class="btn btn-success" type="submit">Submit</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
</div>

<?php require('footer.php');?>
<script>
$("#add_more").click(function(){
    $("#add_more_div").append(`
    <div class="form-row more_views">
        <div class="form-group col-lg-2 m-form__group-sub col-lg-cust">
            <input class="form-control" name="attribute[]" id="attribute[]" placeholder="Enter Attribute" required>
        </div>
        <div class="form-group col-lg-3 m-form__group-sub col-lg-cust">
            <input class="form-control" name="value[]" id="value[]" placeholder="Enter Value" required>
        </div>
        <div class="col-md-1">
            <button type="button" class="btn m-btn--pill m-btn--air btn-danger" onClick="remove(this)"><i class="flaticon-delete-1"></i></button>
        </div>
    </div>
    `);
});
function remove(element)
{
$(element).closest('.more_views').remove()
}
</script>
