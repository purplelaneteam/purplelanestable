<?php 
  require 'includes/initialize.php';
  $error = $_SESSION['error'];
$success = $_SESSION['success'];

if ($success) {
    unset($_SESSION['success']);
}
if ($error) {
    unset($_SESSION['error']);
}
?>
<!DOCTYPE html>
<html lang="en">


<head>
    <title>Guidelines For Mentors | PurpleLane</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <?php include 'head.php' ?>
	
	<style>
    nav.navbar.navbar-expand-lg.navbar-togglable.navbar-dark.navbarall{
      background: inherit !important;
      background-color: #2a0653 !important;
    }

.accordion a {
  position: relative;
  display: -webkit-box;
  display: -webkit-flex;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-orient: vertical;
  -webkit-box-direction: normal;
  -webkit-flex-direction: column;
  -ms-flex-direction: column;
  flex-direction: column;
  width: 100%;
  padding: 1rem 3rem 1rem 1rem;
  color: #7288a2;
  font-size: 1.15rem;
  font-weight: 400;
  border-bottom: 1px solid #e5e5e5;
}

.accordion a:hover,
.accordion a:hover::after {
  cursor: pointer;
  color: #2c0754;
}

.accordion a:hover::after {
  border: 1px solid #2c0754;
}

.accordion a.active {
  color: #2c0754;
  border-bottom: 1px solid #2c0754;
}

.accordion a::after {
  font-family: 'FontAwesome';
  content: '\f067';
  position: absolute;
  float: right;
  right: 1rem;
  font-size: 1rem;
  color: #7288a2;
  padding: 4px;
  width: 30px;
  height: 30px;
  -webkit-border-radius: 50%;
  -moz-border-radius: 50%;
  border-radius: 50%;
  border: 1px solid #7288a2;
  text-align: center;
}

.accordion a.active::after {
  font-family: 'FontAwesome';
  content: '\f068';
  color: #2c0754;
  border: 1px solid #2c0754;
}

.accordion .content {
  opacity: 0;
  padding: 0 1rem;
  max-height: 0;
  border-bottom: 1px solid #e5e5e5;
  overflow: hidden;
  clear: both;
  -webkit-transition: all 0.2s ease 0.15s;
  -o-transition: all 0.2s ease 0.15s;
  transition: all 0.2s ease 0.15s;
}

.accordion .content p {
  font-size: 1rem;
  font-weight: 300;
}

.accordion .content.active {
  opacity: 1;
  padding: 1rem;
  max-height: 100%;
  -webkit-transition: all 0.35s ease 0.15s;
  -o-transition: all 0.35s ease 0.15s;
  transition: all 0.35s ease 0.15s;
}

.alt-color {
    color: #491066;
}
	</style>
	

	
</head>
<body data-spy="scroll" data-target=".navbar" data-offset="82">
    <div class="site-loader">
        <!---======Preloader===========-->
        <div class="loader-dots">
            <div class="circle circle-1"></div>
            <div class="circle circle-2"></div>
        </div>
    </div>
    <div class="site__layer"></div>
    <?php $link = '/'; ?>
    <?php include 'header.php' ?>
	
	
   
	<section class="burger2 app" id="app">
        <div class="container">
            <!-- container-start -->
			<div class="row">
									<div class="col-lg-12 col-sm-12">
										
										<h5 class="section-heading">Guidelines For Mentors</h5>
									
									 
                                        <OL>
	<LI><P ALIGN=JUSTIFY STYLE="margin-top: 0.17in; margin-bottom: 0in; line-height: 100%">
	<FONT COLOR="#000000"><FONT FACE="Arial, serif"><B>Do not assume
	anything about your mentee – ask questions instead.</B></FONT></FONT></P>
    <P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><BR>
	<UL>
		<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><FONT COLOR="#000000"><FONT FACE="Arial, serif"><FONT SIZE=2>Do
		not assume. Ask them about their aspirations and what they want and
		expect from you, be it support, guidance, insight, etc.</FONT></FONT></FONT></P>
	</UL>
</OL>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><BR>
</P>
<OL>
	<UL>
		<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><FONT COLOR="#000000"><FONT FACE="Arial, serif"><FONT SIZE=2>It
		is important to understand a mentee's challenges, goals, desires,
		and feelings so that you can best support them, engage with them
		and encourage them</FONT></FONT></FONT></P>
	</UL>
</OL>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><BR>
</P>
<OL START=2>
	<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><FONT COLOR="#000000"><FONT FACE="Arial, serif"><B>Listen</B></FONT></FONT></P>
</OL>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><BR>
</P>
<OL>
	<UL>
		<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><FONT COLOR="#000000"><FONT FACE="Arial, serif"><FONT SIZE=2>Listening
		is much harder than speaking. You cannot provide your best advice
		to someone unless you understand exactly where they’re coming
		from. So listen first.</FONT></FONT></FONT></P>
	</UL>
</OL>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><BR>
</P>
<OL>
	<UL>
		<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><FONT COLOR="#000000"><FONT FACE="Arial, serif"><FONT SIZE=2>A
		good listening tip is to take notes during your mentoring sessions
		to stay actively engaged. If you can give your mentee some
		direction, make sure to follow up on that direction the next time
		you meet</FONT></FONT></FONT><FONT COLOR="#000000"><FONT FACE="Arial, serif">.</FONT></FONT></P>
	</UL>
</OL>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><BR>
</P>
<OL START=3>
	<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><FONT COLOR="#000000"><FONT FACE="Arial, serif"><B>Set
	Expectations</B></FONT></FONT></P>
</OL>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><BR>
</P>
<OL>
	<UL>
		<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><FONT COLOR="#000000"><FONT FACE="Arial, serif"><FONT SIZE=2>Understand
		your mentee’s goals and set realistic expectations for their
		achievement. If required, help set up a system to measure
		achievement.</FONT></FONT></FONT></P>
	</UL>
</OL>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><BR>
</P>
<OL>
	<UL>
		<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><FONT COLOR="#000000"><FONT FACE="Arial, serif"><FONT SIZE=2>Provide
		suggestions and advice on goals and activities that lead to
		effective and rewarding work. Tell stories about how others made
		their way in the career that might be relevant to the mentee</FONT></FONT></FONT><FONT COLOR="#000000"><FONT FACE="Arial, serif">.</FONT></FONT></P>
	</UL>
</OL>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><BR>
</P>
<OL START=4>
	<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><FONT COLOR="#000000"><FONT FACE="Arial, serif"><B>Motivate
	and inspire</B></FONT></FONT></P>
</OL>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><BR>
</P>
<OL>
	<UL>
		<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><FONT COLOR="#000000"><FONT FACE="Arial, serif"><FONT SIZE=2>Mentor
		is a role model. As a mentor, your life experience is in the
		spotlight. Be your best and consistent.</FONT></FONT></FONT></P>
	</UL>
</OL>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><BR>
</P>
<OL START=5>
	<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><FONT COLOR="#000000"><FONT FACE="Arial, serif"><B>Show
	empathy</B></FONT></FONT></P>
</OL>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><BR>
</P>
<OL>
	<UL>
		<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><FONT COLOR="#000000"><FONT FACE="Arial, serif"><FONT SIZE=2>Empathy
		is a vital character trait of a good mentor. You should be able to
		understand how your mentee is feeling and how to best approach
		guiding them</FONT></FONT></FONT><FONT COLOR="#000000"><FONT FACE="Arial, serif">.</FONT></FONT></P>
	</UL>
</OL>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><BR>
</P>
<OL START=6>
	<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><FONT COLOR="#000000"><FONT FACE="Arial, serif"><B>Establish
	Mutual Respect</B></FONT></FONT></P>
</OL>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><BR>
</P>
<OL>
	<UL>
		<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><FONT COLOR="#000000"><FONT FACE="Arial, serif"><FONT SIZE=2>The
		mentoring relationship should be based on mutual respect, trust and
		support.</FONT></FONT></FONT></P>
		<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><FONT COLOR="#000000"><FONT FACE="Arial, serif"><FONT SIZE=2>Always
		maintain confidentiality between one another.&nbsp;</FONT></FONT></FONT></P>
		<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><FONT COLOR="#000000"><FONT FACE="Arial, serif"><FONT SIZE=2>Keep
		any commitments that are made.&nbsp;</FONT></FONT></FONT></P>
		<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><FONT COLOR="#000000"><FONT FACE="Arial, serif"><FONT SIZE=2>Evaluate
		the relationship at various points within an agreed-upon time
		frame.</FONT></FONT></FONT></P>
		<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><FONT COLOR="#000000"><FONT FACE="Arial, serif"><FONT SIZE=2>Build
		trust with your mentee by being open and honest.</FONT></FONT></FONT></P>
	</UL>
</OL>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><BR>
</P>
<OL START=7>
	<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><FONT COLOR="#000000"><FONT FACE="Arial, serif"><B>Help
	them see the bigger picture</B></FONT></FONT></P>
</OL>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><BR>
</P>
<OL>
	<UL>
		<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><FONT COLOR="#000000"><FONT FACE="Arial, serif"><FONT SIZE=2>A
		good mentor keeps the big picture in mind. Work with them as if you
		will be their mentor forever – this will make sure you are
		keeping their long-term best interests at heart.</FONT></FONT></FONT></P>
	</UL>
</OL>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><BR>
</P>
<OL START=8>
	<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><FONT COLOR="#000000"><FONT FACE="Arial, serif"><B>Share
	Skills, Knowledge, and Expertise</B></FONT></FONT></P>
</OL>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><BR>
</P>
<OL>
	<UL>
		<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><FONT COLOR="#000000"><FONT FACE="Arial, serif"><FONT SIZE=2>A
		good mentor is willing to teach what he knows and accepts the
		mentee where they currently are in their professional development.</FONT></FONT></FONT></P>
	</UL>
</OL>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><BR>
</P>
<OL>
	<UL>
		<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><FONT COLOR="#000000"><FONT FACE="Arial, serif"><FONT SIZE=2>Be
		willing to share your experiences – the good, the bad and the
		ugl</FONT></FONT></FONT><FONT COLOR="#000000"><FONT FACE="Arial, serif">y.&nbsp;</FONT></FONT></P>
	</UL>
</OL>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><BR>
</P>
<OL START=9>
	<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><FONT COLOR="#000000"><FONT FACE="Arial, serif"><B>Deliver
	constructive feedback</B></FONT></FONT></P>
</OL>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><BR>
</P>
<OL>
	<UL>
		<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><FONT COLOR="#000000"><FONT FACE="Arial, serif"><FONT SIZE=2>One
		of the key responsibilities of a good mentor is to provide guidance
		and constructive feedback to their mentee.&nbsp;</FONT></FONT></FONT></P>
	</UL>
</OL>
<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><BR>
</P>
<OL>
	<UL>
		<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0.17in; line-height: 100%">
		<FONT COLOR="#000000"><FONT FACE="Arial, serif"><FONT SIZE=2>This
		is where the mentee most likely will grow the most by identifying
		their current strengths and weaknesses and learning how to use
		these to make themselves successful in the field. </FONT></FONT></FONT>
		</P>
	</UL>
</OL>
</div>
				
				</div>
            </div>
           
          
			
			
        </div>
        <!-- container end -->
    </section>

<!-- Forgot Password End-->
<!-- foooter started -->
<?php
    include("footer.php");
    include("footer_tags.php");
?>
    
</body>

</html>