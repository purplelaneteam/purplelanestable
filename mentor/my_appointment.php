<?php
require_once "../includes/initialize.php";
require_once "logincheck.php";

$id = $_SESSION['id'];
$userMentor = getMentorBasicInfo($con, $id);
$requested=getMeetingOfMentor($con,'pending');
$rescheduled=getMeetingOfMentor($con,'rescheduled');
$scheduled=getMeetingOfMentor($con,'scheduled');
$completed=getMeetingOfMentor($con,'completed');
$mentor_cancelled=getMeetingOfMentor($con,'mentor cancelled');
$admin_cancelled=getMeetingOfMentor($con,'admin cancelled');
$picker_ids = [];
$error = $_SESSION['error'];
$success = $_SESSION['success'];
if($success) {
	unset($_SESSION['success']);
}
if($error) {
	unset($_SESSION['error']);
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<title><?php echo SITE_NAME; ?></title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="keywords" content="" />
	<meta name="description" content="" />
	<meta name="author" content="" />
	<?php
	include("../head.php");
	?>
</head>

<body data-spy="scroll" data-target=".navbar" data-offset="82">


	<div class="site__layer"></div>
	<?php include('navbar.php');?>
	<section class="burger2 app" id="app">
		<div class="container">
			<!-- container-start -->
			<div class="row">
				<div class="col-lg-9 col-sm-12">
					<h5 class="section-heading">My Appointments</h5>
					<p class="spcev">
					</p>
				</div>
			</div>
			<?php if($error || $success): ?>
			<div class="message">
				<div style="" id="alert_div_basic"
					class="alert alert-<?php echo ($error)? 'danger': 'success' ?> alert-dismissible fade show"
					role="alert">
					<p><?php echo ($error)? $error: $success ?></p>
					<button style="margin-top: -30px;" type="button" class="close" data-dismiss="alert"
						aria-label="Close">
						<span aria-hidden="true" style="font-size: 31px;line-height: 0;">&times;</span>
					</button>
				</div>
			</div>
			<?php endif; ?>
			<div class="container">
				<div class="row">
					<div class="col-md-12 text-center" style="margin-top:40px">
						<nav class="nav-justified ">
							<div class="nav nav-tabs " id="nav-tab" role="tablist">

								<a class="nav-item nav-link active" id="pop1-tab" data-toggle="tab" href="#pop1" role="tab" aria-controls="pop1" aria-selected="true"> <b><i class="fa fa-handshake" aria-hidden="true"></i> Requested</b></a>
								<a class="nav-item nav-link " id="pop2-tab" data-toggle="tab" href="#pop2" role="tab" aria-controls="pop2" aria-selected="false"> <b><i class="fa fa-handshake" aria-hidden="true"></i> Re-scheduled</b></a>
								<a class="nav-item nav-link " id="pop2-tab" data-toggle="tab" href="#pop3" role="tab" aria-controls="pop3" aria-selected="false"> <b><i class="fa fa-handshake" aria-hidden="true"></i> Scheduled</b></a>
								<a class="nav-item nav-link " id="pop3-tab" data-toggle="tab" href="#pop4" role="tab" aria-controls="pop4" aria-selected="false"> <b><i class="fa fa-handshake" aria-hidden="true"></i> Completed</b></a>
								<a class="nav-item nav-link " id="pop4-tab" data-toggle="tab" href="#pop5" role="tab" aria-controls="pop5" aria-selected="false"> <b><i class="fa fa-handshake" aria-hidden="true"></i> Cancelled</b></a>

							</div>
						</nav>
						<div class="tab-content" id="nav-tabContent">


							<div class="tab-pane fade show active" id="pop1" role="tabpanel" aria-labelledby="pop1-tab">
								<div class="pt-3"></div>

								<!-- Scheduled start -->

								<div class="row">

									<!---/col-->
									<div class="col-md-12 col-sm-12  ">

										<div class="store-btns">
											<h2 class="section-heading review-head"></h2>

											<?php
													if(isset($requested) && !empty($requested))
													{
														$i=1;
														foreach($requested as $key => $meeting)
														{
															include './include/meeting_info.php';
														}
													}
													else
													{
														echo "No meetings Found";
													}
												?>

										</div>
									</div>
									<!---/col-->

								</div>

								<!-- Scheduled END -->

							</div>

							<div class="tab-pane fade " id="pop2" role="tabpanel" aria-labelledby="pop2-tab">
								<div class="pt-3"></div>

								<!-- Requests start -->

								<div class="row">

									<!---/col-->
									<div class="col-md-12 col-sm-12  ">

										<div class="store-btns">
											<h2 class="section-heading review-head"></h2>
											<?php
													if(isset($rescheduled) && !empty($rescheduled))
													{
														$i=1;
														foreach($rescheduled as $key => $meeting)
														{
															include './include/meeting_info.php';
															
														}
													}
													else
													{
														echo "No meetings Found";
													}
												?>
										</div>
									</div>
									<!---/col-->

								</div>

								<!-- Requests END -->

							</div>


							<div class="tab-pane fade" id="pop3" role="tabpanel" aria-labelledby="pop3-tab">
								<div class="pt-3"></div>

								<!-- completed start -->

								<div class="row">

									<!---/col-->
									<div class="col-md-12 col-sm-12  ">

										<div class="store-btns">
											<h2 class="section-heading review-head"></h2>


											<?php
													if(isset($scheduled) && !empty($scheduled))
													{
														$i=1;
														foreach($scheduled as $key => $meeting)
														{
															include './include/meeting_info.php';
														}
													}
													else
													{
														echo "No meetings Found";
													}
												?>
										</div>
									</div>
									<!---/col-->

								</div>

								<!-- completed END -->


							</div>

							<div class="tab-pane fade" id="pop4" role="tabpanel" aria-labelledby="pop4-tab">
								<div class="pt-4"></div>

								<!-- cancelled start -->

								<div class="row">

									<!---/col-->
									<div class="col-md-12 col-sm-12  ">

										<div class="store-btns">
											<h2 class="section-heading review-head"></h2>

											<?php
													if(isset($completed) && !empty($completed))
													{
														$i=1;
														foreach($completed as $key => $meeting)
														{
															include './include/meeting_info.php';
														}
													}
													else
													{
														echo "No meetings Found";
													}
												?>
										</div>
									</div>
									<!---/col-->

								</div>

								<!-- cancelled END -->

							</div>

							<div class="tab-pane fade" id="pop5" role="tabpanel" aria-labelledby="pop5-tab">
								<div class="pt-5"></div>

								<!-- cancelled start -->

								<div class="row">

									<!---/col-->
									<div class="col-md-12 col-sm-12  ">

										<div class="store-btns">
											<h2 class="section-heading review-head"></h2>
											<?php
													if(isset($mentor_cancelled) && !empty($mentor_cancelled))
													{
														$i=1;
														foreach($mentor_cancelled as $key => $meeting)
														{
															include './include/meeting_info.php';
														}
													}
													if(isset($admin_cancelled) && !empty($admin_cancelled))
													{
														$i=1;
														foreach($admin_cancelled as $key => $meeting)
														{
															include './include/meeting_info.php';
														}
													}
													if(empty($mentor_cancelled) && empty($admin_cancelled))
													{
														echo "No meetings Found";
													}
												?>
										</div>
									</div>
									<!---/col-->

								</div>

								<!-- cancelled END -->

							</div>
						</div>
					</div>
				</div>
			</div>




		</div>
		<!-- container end -->
	</section>



    <!-- Modal :set-->
	<div id="cancel-modal" class="modal cencel_metting" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <form action="/mentor/processreq/proc_cancel_meeting_by_mentor.php" method="POST" >
                <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Are you sure you want to cancel the meeting ?</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="cancel-meeting" name="meeting_id">
                    <input type="hidden" id="cancel-mentee" name="mentee_id">
                    <textarea  placeholder="Please tell us why you want to cancel the meeting schedule."></textarea>
                </div>
                <div class="modal-footer text-center">
                    <button type="submit" class="btn btn-default">Confirm</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal" onclick="$('#cancel-modal').hide()">Cancel</button>
                </div>
                </div>
            </form>

        </div>
        </div>
 <!-- Modal :end -->

 <!-- Modal :set-->
	<div id="reschedule-modal" class="modal cencel_metting" role="dialog">
        <div class="modal-dialog">

			<!-- Modal content-->
			<form action="/mentor/processreq/proc_reschedule_by_mentor.php" method="POST">
				<div class="modal-content">
				<div class="modal-header">
					<h4>Propose a new 30 mins slot:</h4>
					<input type="hidden" name="meeting_id" id="reschedule-meeting">
					<input type="hidden" id="res_mentee_id" name="mentee_id">
				</div>
				<div id="picker"> </div>
				<input type="hidden" name='slot1' id="result" value="" />
					
					<div class="modal-footer text-center">
						<button type="submit" class="btn btn-default" >Confirm</button>
						<button type="button" class="btn btn-default" data-dismiss="modal" onclick="$('#reschedule-modal').hide()">Cancel</button>
					</div>
				</div>
				</div>
			</form>

        </div>
        </div>
 <!-- Modal :end -->
	<?php 
require_once('../footer.php');
require_once('../footer_tags.php');

?>

	<script>
		$(function () {
			minute = moment().minute()
			if (minute >= 30) minute = 30
			else minute = 0;
			const tomorrow = moment().add(1, 'days').minute(minute)
			$("#book_mentor").hide();

			$("#picker").dateTimePicker({
				selectDate: tomorrow,
				startDate: tomorrow,
				minuteIncrement: 30
			});
		})
	</script>