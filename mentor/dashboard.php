<?php
require_once "../includes/initialize.php";
require_once "logincheck.php";

$id = $_SESSION['id'];
$user = getMentorBasicInfo($con, $id);

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?php echo SITE_NAME;?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <?php require_once '../head.php' ?>
    <style>
    .nav{
    
    display: block;
        flex-wrap: wrap;
        padding-left: 0;
        margin-bottom: 0;
        list-style: none;
    }
    </style>

  <link rel="stylesheet" type="text/css" href="../css/jquery.datetimepicker.css"/>
   
</head>
<body data-spy="scroll" data-target=".navbar" data-offset="82">
    <div class="site-loader">
        <!---======Preloader===========-->
        <div class="loader-dots">
            <div class="circle circle-1"></div>
            <div class="circle circle-2"></div>
        </div>
    </div>
    <div class="site__layer"></div>
    <?php require_once '../header.php' ?>
    <?php require_once 'navbar.php' ?>

	<section class="burger app" id="app">
	<div class="container">
		<!-- container-start -->
		<div class="container">
			<div class="row">
				<div class="col-md-12 text-center" style="margin-top:40px">
					<div class="tab-content" id="nav-tabContent">
						<div class="tab-pane fade show active" id="pop2" role="tabpanel" aria-labelledby="pop2-tab">
							<!-- Choose a Mentor start -->
							<div class="row">
								<div class="col-md-12 col-sm-12  ">
									<div class="store-btns">
										<div class=" review-box">
											<div class="tabs">
												<div class="container">
													<div class="row">
														<div class="col-md-3">
															<nav class="nav-justified ">
																<div class="nav nav-tabs " id="nav-tab" role="tablist">
																	<a class="nav-item nav-link active" id="pop1-tab" data-toggle="tab" href="#pop1" role="tab" aria-controls="pop1" aria-selected="true"> <b>Dashboard</b>
																	</a>
																</div>
															</nav>
														</div>
														<div class="col-md-9">
															<div class="tab-content">
																<div class="tab-pane fade show active" id="pop1" role="tabpanel" aria-labelledby="pop1-tab">
																	<p>
																		
																	</p>
																</div>
																<div class="tab-pane fade" id="pop2a" role="tabpanel" aria-labelledby="pop2a-tab">
																	<div class="row">
																		<div class="col-md-5">
																			<p>
																				<div class="card border-primary rounded-0">
																					<div class="card-header p-0">
																						<div class="bg-info text-white text-center py-2">
																							<h6> Availability
                                                                                            <span class="edt"><i class="fa fa-pencil-square"></i></span>
                                                                                            </h6>
																						</div>
																					</div>
																					<div class="card-body p-3">
																						<div id="demo2">

                                                                                        </div>
																					</div>
																				</div>
																			</p>
																		</div>
																		<div class="col-md-7">
																			<p>
																				<div class="card border-primary rounded-0">
																					<div class="card-header p-0">
																						<div class="bg-info text-white text-center py-2">
																							<h6> Price Range
                                                                     <span class="edt"><i class="fa fa-pencil-square"></i></span>
                                                                  </h6>
																						</div>
																					</div>
																					<div class="card-body p-3">
																						<!--Body-->
																						<div class="form-group">
																							<lable>Set call rate( Hourly)</lable>
																							<div class="input-group mb-2">
																								<div class="input-group-prepend">
																									<div class="input-group-text"><i class="fa fa-inr text-info"></i>
																									</div>
																								</div>
																								<input type="text" class="form-control" id="nombre" name="nombre" placeholder="Hourly" required>
																							</div>
																						</div> <span>Once the password is changed, you'll be redirected to the login page.</span>
																						<a href="#" class="btn btn-alpha mr-lg-3 mr-2 cnect"> Save Changes <i class="fa fa-angle-right ml-3"></i></a>
																					</div>
																				</div>
																			</p>
																		</div>
																	</div>
																</div>
																<div class="tab-pane fade" id="pop3" role="tabpanel" aria-labelledby="pop3-tab">
																	<p>
																		<div class="card border-primary rounded-0">
																			<div class="card-header p-0">
																				<div class="bg-info text-white text-center py-2">
																					<h6> Bank Account Details 
                                                               <span class="edt"><i class="fa fa-pencil-square"></i></span>
                                                            </h6>
																				</div>
																			</div>
																			<div class="card-body p-3">
																				<!--Body-->
																				<div class="form-group">
																					<div class="input-group mb-2">
																						<div class="input-group-prepend">
																							<div class="input-group-text"><i class="fa fa-user text-info"></i>
																							</div>
																						</div>
																						<input type="text" class="form-control" id="nombre" name="nombre" placeholder="Bank Name" required>
																					</div>
																				</div>
																				<div class="form-group">
																					<div class="input-group mb-2">
																						<div class="input-group-prepend">
																							<div class="input-group-text"><i class="fa fa-check-square text-info"></i>
																							</div>
																						</div>
																						<input type="text" class="form-control" id="nombre" name="email" placeholder="Account Number" required>
																					</div>
																				</div>
																				<div class="form-group">
																					<div class="input-group mb-2">
																						<div class="input-group-prepend">
																							<div class="input-group-text"><i class="fa fa-check-square text-info"></i>
																							</div>
																						</div>
																						<input type="text" class="form-control" id="nombre" name="text" placeholder="IFS Code" required>
																					</div>
																				</div> <a href="#" class="btn btn-alpha mr-lg-3 mr-2 cnect"> Save Changes <i class="fa fa-angle-right ml-3"></i></a>
																			</div>
																		</div>
																	</p>
																</div>
																<div class="tab-pane fade" id="pop4" role="tabpanel" aria-labelledby="pop4-tab">
																	<p>
																		<div class="card border-primary rounded-0">
																			<div class="card-header p-0">
																				<div class="bg-info text-white text-center py-2">
																					<h6> Change Password
                                                               <span class="edt"><i class="fa fa-pencil-square"></i></span>
                                                            </h6>
																				</div>
																			</div>
																			<div class="card-body p-3">
																				<!--Body-->
																				<div class="form-group">
																					<div class="input-group mb-2">
																						<div class="input-group-prepend">
																							<div class="input-group-text"><i class="fa fa-lock text-info"></i>
																							</div>
																						</div>
																						<input type="text" class="form-control" id="nombre" name="nombre" placeholder="Current Password" required>
																						<div class="input-group-addon"> <a href=""><i class="fa fa-eye-slash" aria-hidden="true"></i></a>
																						</div>
																					</div>
																				</div>
																				<div class="form-group">
																					<div class="input-group mb-2">
																						<div class="input-group-prepend">
																							<div class="input-group-text"><i class="fa fa-key text-info"></i>
																							</div>
																						</div>
																						<input type="text" class="form-control" id="nombre" name="email" placeholder="New Password" required>
																						<div class="input-group-addon"> <a href=""><i class="fa fa-eye-slash" aria-hidden="true"></i></a>
																						</div>
																					</div>
																				</div>
																				<div class="form-group">
																					<div class="input-group mb-2">
																						<div class="input-group-prepend">
																							<div class="input-group-text"><i class="fa fa-key text-info"></i>
																							</div>
																						</div>
																						<input type="text" class="form-control" id="nombre" name="text" placeholder="Retype New Password" required>
																						<div class="input-group-addon"> <a href=""><i class="fa fa-eye-slash" aria-hidden="true"></i></a>
																						</div>
																					</div>
																				</div> <a href="#" class="btn btn-alpha mr-lg-3 mr-2 cnect"> Save Changes <i class="fa fa-angle-right ml-3"></i></a>
																			</div>
																		</div>
																	</p>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<!---/col-->
								</div>
								<!-- /row  -->
							</div>
							<div class="tab-pane fade" id="pop3" role="tabpanel" aria-labelledby="pop3-tab">
								<div class="pt-3"></div>
								<p>3. There's another way to do this for layouts that doesn't have to put the navbar inside the container, and which doesn't require any CSS or Bootstrap overrides. Simply place a div with the Bootstrap container class around the navbar. This will center the links inside the navbar:</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- container end -->
</section>
<?php 
require_once('../footer.php');
require_once('../footer_tags.php');
?>
	