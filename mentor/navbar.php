<nav class="nav-fixed navbar navbar-expand-lg navbar-togglable navbar-dark" style="">
    <div class="container">
        <!-- Brand/ logo -->
        <?php if($_SESSION['type'] == 'mentee' ) { 
                $link = "/mentee/my_appointments.php";
            }
            elseif($_SESSION['type'] == 'mentor' ) {
                $link = "/mentor/my_appointment.php";
            }
        ?>
        <a class="navbar-brand" style="max-width:20%" href="<?php echo $link ?>"><img src="/images/logo.png" alt="logo image" class="img-fluid-logo"></a>
        <!-- Toggler -->
        <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span></span><span></span><span></span>
        </button>
        <!-- Collapse -->
        <div class="collapse navbar-collapse" id="navbarCollapse">
            <!-- Links -->
            <ul class="navbar-nav ml-auto">
                
                
                
            </ul>
            
                <div class="">
                    <div class="card-header msg_head">
                        <div class="d-flex bd-highlight">
                            <div class="img_cont">
                            <?php
                                // $profile_pic_path="/images/mentor.jpg";
                                if($userMentor['profile_pic']!='')
                                {
                                    $profile_pic_path=MENTOR_IMAGE_UPLOAD_FOLDER_LINK.$userMentor['profile_pic'];
                                }
                                // dd($profile_pic_path);
                                ?>
                                <img src="<?php echo $profile_pic_path;?>" alt="MI" class="rounded-circle user_img">
                                
                            </div>
                            <div class="user_info">
                                <span><?php echo $userMentor['fname']?></span>
                                
                            </div>
                        
                        </div>
                        <div class="d-flex bd-highlight">
                            <i class="fa fa-ellipsis-v dropbtn" id="action_menu_btn" onclick="myFunction()"></i>

                            <div id="myDropdown" class="dropdown-content" style="left:-5%;">
                                <a href="edit_profile.php"><i class="fa fa-user-o"></i> Edit profile</a>
                                <a href="my_appointment.php"><i class="fa fa-file-text"></i> My Appointments</a>
                                <a href="my_transaction.php"><i class="fa fa-file-text"></i> My Transaction</a>
                                <a href="../contact_us.php"><i class="fa fa-info-circle"></i> Help Center</a>
                                <a href="<?php echo SITE_URL; ?>/logout.php"><i class="fa fa-sign-out"></i> Logout</a>
                            </div>
                        </div>
                    </div>
                
                </div>
        </div>
        <!-- / .navbar-collapse -->
    </div>
    <!-- / .container -->
</nav>