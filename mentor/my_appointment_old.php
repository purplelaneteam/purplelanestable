<?php
require_once "../includes/initialize.php";
require_once "logincheck.php";

$id = $_SESSION['id'];
$userMentor = getMentorBasicInfo($con, $id);
$requested=getMeetingOfMentor($con,'pending');
$rescheduled=getMeetingOfMentor($con,'rescheduled');
$scheduled=getMeetingOfMentor($con,'scheduled');
$completed=getMeetingOfMentor($con,'completed');
$mentor_cancelled=getMeetingOfMentor($con,'mentor cancelled');
$admin_cancelled=getMeetingOfMentor($con,'admin cancelled');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Purple Lane</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="" />
    <meta name="description" content="" />
	<meta name="author" content="" />
	<?php
	include("../head.php");
	?>
</head>
<body data-spy="scroll" data-target=".navbar" data-offset="82">
	

    <div class="site__layer"></div>
    <?php include('navbar.php');?>
	<section class="burger2 app" id="app">
        <div class="container">
            <!-- container-start -->
			<div class="row">
						<div class="col-lg-9 col-sm-12">
							<h5 class="section-heading">My Appointments</h5>
						<p class="spcev">
						</p></div>
						<!-- <div class="col-lg-3 col-sm-12 hidden-xs hidden-sm"> -->
							<!-- <label class="pull-right"> -->
							    <!-- <div class="d-flex justify-content-center h-100"> -->
									<!-- <div class="searchbar"> -->
									    <!-- <input class="search_input" type="text" name="" placeholder="Search..."> -->
									    <!-- <a href="#" class="search_icon"><i class="fa fa-search"></i></a> -->
									<!-- </div> -->
								<!-- </div> -->
							<!-- </label> -->
						<!-- </div> -->
									
								</div>
			<div class="container">
				<div class="row">
					<div class="col-md-12 text-center" style="margin-top:40px">
						<nav class="nav-justified ">
						  <div class="nav nav-tabs " id="nav-tab" role="tablist">
							
							<a class="nav-item nav-link active" id="pop1-tab" data-toggle="tab" href="#pop1" role="tab" aria-controls="pop1" aria-selected="true"> <b><i class="fa fa-handshake" aria-hidden="true"></i> Requested</b></a>
							<a class="nav-item nav-link " id="pop2-tab" data-toggle="tab" href="#pop2" role="tab" aria-controls="pop2" aria-selected="false"> <b><i class="fa fa-handshake" aria-hidden="true"></i> Re-scheduled</b></a>
							<a class="nav-item nav-link " id="pop2-tab" data-toggle="tab" href="#pop3" role="tab" aria-controls="pop3" aria-selected="false"> <b><i class="fa fa-handshake" aria-hidden="true"></i> Scheduled</b></a>
							<a class="nav-item nav-link " id="pop3-tab" data-toggle="tab" href="#pop4" role="tab" aria-controls="pop4" aria-selected="false"> <b><i class="fa fa-handshake" aria-hidden="true"></i> Completed</b></a>
							<a class="nav-item nav-link " id="pop4-tab" data-toggle="tab" href="#pop5" role="tab" aria-controls="pop5" aria-selected="false"> <b><i class="fa fa-handshake" aria-hidden="true"></i> Cancelled</b></a>
							
						  </div>
						</nav>
						<div class="tab-content" id="nav-tabContent">
						    
							
						    <div class="tab-pane fade show active" id="pop1" role="tabpanel" aria-labelledby="pop1-tab">
							   <div class="pt-3"></div>
							   
								<!-- Scheduled start -->
								
                                <div class="row">
								   
									<!---/col-->
									<div class="col-md-12 col-sm-12  ">
									   
										<div class="store-btns">
											 <h2 class="section-heading review-head"></h2>
											 <table class='table table-striped' id='requested_meeting_tbl'  width='100%'>
                                            	<thead>
													<tr>
													<td>Action</td>
													<td>Sr No</td>
													<td>Meeting ID</td>
													<td>Mentee Name</td>
													<td>Subject</td>
													<td>Cost Card Price</td>
													
													</tr>
												</thead>
												<tbody>
												<?php
													if(isset($requested))
													{
														$i=1;
														foreach($requested as $req)
														{
															echo '<tr><td><a href="confirm_appointment.php?meeting_id='.$req['id'].'&mentee_id='.$req['mentee_id'].'" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="Confirm Meeting" ><i class="fa fa-calendar-check"></i>
															</a>
															<a href="reschedule_meeting.php?meeting_id='.$req['id'].'&mentee_id='.$req['mentee_id'].'" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="Reschedule Meeting" > <i class="fa fa-calendar-plus"></i>
															</a>
															<a class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="Cancel Meeting" id="cancelled_appointment_by_mentor" data-id="'.$req['id'].'_'.$req['mentee_id'].'"> <i class="fa fa-calendar-minus"></i>
															</a>
															</a>
															</td><td>'.$i++.'</td><td>'.$req['id'].'</td><td>'.$req['mentee'].'</td><td>'.$req['topic'].'</td><td>'.$req['cost_card_price'].'</td>';
															echo '</tr>';
														}
													}
													else
													{

													}
												?> 
												</tbody>
                                            </table>
										</div>
									</div>
									<!---/col-->
								   
								</div>
                                 
								<!-- Scheduled END -->
							
						    </div>
						
    						<div class="tab-pane fade " id="pop2" role="tabpanel" aria-labelledby="pop2-tab">
								<div class="pt-3"></div>
								
								<!-- Requests start -->
								
                                <div class="row">
								   
									<!---/col-->
									<div class="col-md-12 col-sm-12  ">
									   
										<div class="store-btns">
											 <h2 class="section-heading review-head"></h2>
											 <table class='table table-striped' id='rescheduled_meeting_tbl'  width='100%'>
                                            	<thead>
													<tr>
													<td>Sr No</td>
													<td>Meeting ID</td>
													<td>Mentee Name</td>
													<td>Subject</td>
													<td>Cost Card Price</td>
													
													</tr>
												</thead>
												<tbody>
												<?php
													if(isset($rescheduled))
													{
														$i=1;
														foreach($rescheduled as $req)
														{
															echo '<tr><td>'.$i++.'</td><td>'.$req['id'].'</td><td>'.$req['mentee'].'</td><td>'.$req['topic'].'</td><td>'.$req['cost_card_price'].'</td></tr>';
															
														}
													}
													else
													{

													}
												?> 
												</tbody>
                                            </table>
										</div>
									</div>
									<!---/col-->
								   
								</div>
                                 
								<!-- Requests END -->
							
							</div>
							
							
							<div class="tab-pane fade" id="pop3" role="tabpanel" aria-labelledby="pop3-tab">
							   <div class="pt-3"></div>
								
								<!-- completed start -->
								
                                <div class="row">
								   
									<!---/col-->
									<div class="col-md-12 col-sm-12  ">
									   
										<div class="store-btns">
											 <h2 class="section-heading review-head"></h2>
										
											 <table class='table table-striped' id='scheduled_meeting_tbl'  width='100%'>
                                            	<thead>
													<tr>
													<td>Sr No</td>
													<td>Meeting ID</td>
													<td>Mentee Name</td>
													<td>Subject</td>
													<td>Cost Card Price</td>
													<td>Meeting Date</td>
													<td>Selected time</td>
													</tr>
												</thead>
												<tbody>
												<?php
													if(isset($scheduled))
													{
														$i=1;
														foreach($scheduled as $req)
														{
															echo '<tr><td>'.$i++.'</td><td>'.$req['id'].'</td><td>'.$req['mentee'].'</td><td>'.$req['topic'].'</td><td>'.$req['cost_card_price'].'</td><td>'.$req['meeting_date'].'</td><td>'.$req['start_time'].'</td></tr>';
														}
													}
													else
													{

													}
												?> 
												</tbody>
                                            </table>
										</div>
									</div>
									<!---/col-->
								   
								</div>
                                 
								<!-- completed END -->
							
							  
						    </div>
							
							<div class="tab-pane fade" id="pop4" role="tabpanel" aria-labelledby="pop4-tab">
								<div class="pt-4"></div>
								
								<!-- cancelled start -->
								
                                <div class="row">
								   
									<!---/col-->
									<div class="col-md-12 col-sm-12  ">
									   
										<div class="store-btns">
											 <h2 class="section-heading review-head"></h2>
											 <table class='table table-striped' id='completed_meeting_tbl'  width='100%'>
                                            	<thead>
													<tr>
													<td>Sr No</td>
													<td>Meeting ID</td>
													<td>Mentee Name</td>
													<td>Subject</td>
													<td>Cost Card Price</td>
													<td>Meeting Date</td>
													<td>Selected time</td>
													</tr>
												</thead>
												<tbody>
												<?php
													if(isset($completed))
													{
														$i=1;
														foreach($completed as $req)
														{
															echo '<tr><td>'.$i++.'</td><td>'.$req['id'].'</td><td>'.$req['mentee'].'</td><td>'.$req['topic'].'</td><td>'.$req['cost_card_price'].'</td><td>'.$req['meeting_date'].'</td><td>'.$req['start_time'].'</td></tr>';
														}
													}
													else
													{

													}
												?> 
												</tbody>
                                            </table>
											
										</div>
									</div>
									<!---/col-->
								   
								</div>
                                 
								<!-- cancelled END -->
							
							</div>
						  
							<div class="tab-pane fade" id="pop5" role="tabpanel" aria-labelledby="pop5-tab">
								<div class="pt-5"></div>
								
								<!-- cancelled start -->
								
                                <div class="row">
								   
									<!---/col-->
									<div class="col-md-12 col-sm-12  ">
									   
										<div class="store-btns">
											 <h2 class="section-heading review-head"></h2>
										
											 <table class='table table-striped' id='cancelled_meeting_tbl'  width='100%'>
                                            	<thead>
													<tr>
													<td>Sr No</td>
													<td>Meeting ID</td>
													<td>Mentee Name</td>
													<td>Subject</td>
													<td>Cost Card Price</td>
													</tr>
												</thead>
												<tbody>
												<?php
													if(isset($mentor_cancelled))
													{
														$i=1;
														foreach($mentor_cancelled as $req)
														{
															echo '<tr><td>'.$i++.'</td><td>'.$req['id'].'</td><td>'.$req['mentee'].'</td><td>'.$req['topic'].'</td><td>'.$req['cost_card_price'].'</td></tr>';
														}
													}
													if(isset($admin_cancelled))
													{
														$i=1;
														foreach($admin_cancelled as $req)
														{
															echo '<tr><td>'.$i++.'</td><td>'.$req['id'].'</td><td>'.$req['mentee'].'</td><td>'.$req['topic'].'</td><td>'.$req['cost_card_price'].'</td></tr>';
														}
													}
												?> 
												</tbody>
                                            </table>
											
										</div>
									</div>
									<!---/col-->
								   
								</div>
                                 
								<!-- cancelled END -->
							
							</div>
						</div>
					</div>
				</div>
            </div>
           
          
			
			
        </div>
        <!-- container end -->
    </section>
	<?php 
require_once('../footer.php');
require_once('../footer_tags.php');

?>

