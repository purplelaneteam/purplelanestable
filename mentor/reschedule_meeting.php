<?php
require_once "../includes/initialize.php";
require_once "logincheck.php";
$id = $_SESSION['id'];
checkMeeting($_GET['meeting_id']);
require_once "header.php";
$meetings = getMeetingSlots($con, $_GET['meeting_id']);

foreach ($meetings as $key => $m) {
    if ($key == 0)
    {
        $meeting1 = $m;
    }
    else if($key == 1)
    {
        $meeting2 = $m;
    }
    else
    {
        break;
    }
}
?>
<style>
.select2-container .select2-selection--single {
    box-sizing: border-box;
    cursor: pointer;
    display: block;
    height: 42px;
    user-select: none;
    -webkit-user-select: none;
}
.select2-container--default .select2-selection--single .select2-selection__rendered {
    color: #444;
    line-height: 41px;
}
.select2-container--default .select2-selection--single .select2-selection__arrow b {
    border-color: #888 transparent transparent transparent;
    border-style: solid;
    border-width: 5px 4px 0 4px;
    height: 0;
    left: 50%;
    margin-left: -4px;
    margin-top: 4px;
    position: absolute;
    top: 50%;
    width: 0;
}
</style>
<?php require_once '../header.php' ?>
<?php require_once 'navbar.php' ?>
	
	
	
	<section class="app" id="app">

        <div class="container">
            <!-- container-start -->
			<div class="row">
				<div class="col-lg-9 col-sm-12">
					<h5 class="section-heading">ReSchedule Details</h5>
				</div>
			</div>
			
			<div class="container">
				
				    <!-- completed start -->
					<div class="row">
						<div class="col-md-12 col-sm-12  ">
							<div class="store-btns"><br><br><br><br>
								<h4 class="section-heading review-head">Reschedule Meeting</h4>
                                <div class="form-group">
                            
                                    <div style="display:none;" id="alert_div_basic" class="alert alert-danger alert-dismissible fade show" role="alert">
                                        <p>Danger</p>
                                        <button style="margin-top: -30px;" type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true"style="font-size: 31px;line-height: 0;">&times;</span>
                                        </button>
                                    </div>
                                </div>
								<div class="row review-box">
								<form id='reschedule_appointment_form' method='post' style='width:100%'>
									<!---/col-->
									<div class="col-md-12 col-sm-12 mb-5 mb-btm mb-md-0 order-md-0">
										<div class="app-nfo app-info mb-4">
                                        <h6 class=""  style="padding-left: 0px">Proposed Slots: </h6>
                                            <h6 style="font-weight:normal"><b>First Slot:</b>
                                            <span class="detail"><?php echo "On ".$meeting1['meeting_date']." from ".$meeting1['start_time']." to ".$meeting1['end_time'] ?></span></h6>
                                            <h6 class="" style="font-weight:normal"><b>Second Slot:</b>
                                            <span class="detail"><?php echo "On ".$meeting2['meeting_date']." from ".$meeting2    ['start_time']." to ".$meeting2['end_time'] ?></span></h6><hr/>
											<h6 class="" style="padding-left: 0px">Choose Timings (Select start time to reschedule 30 minute meeting) </h6>
											<p class="spcev">
											<div class="row">
												<div class="col-md-3 col-sm-12">
                                                    <div>
                                                        <div id="picker"> </div>
                                                        <input type="hidden" name='slot1' id="result" value="" />
                                                        <input type="hidden" name='meeting_id' id="meeting_id" value="<?php echo $_GET['meeting_id']?>"/>
                                                        <input type="hidden" name='mentee_id' id="mentee_id" value="<?php echo $_GET['mentee_id']?>"/>
                                                    </div>
												</div>
												
											<!---/col-->
                                            </div>
											</p>
											<hr/>
											<h6 class="" style="padding-left: 0px">A brief overview of meeting </h6>
											
											<p class="spcev">
												<div class="row">
												<div class="col-md-12 col-sm-12">
												    <textarea class="form-control" rows="5" name="comment" id="comment" placeholder="enter your message here" style="border:1px solid #ccc"></textarea>
                                                </div>
                                                </div>
												<input type='submit' class="btn btn-alpha mr-lg-3 mr-2 cnect" value='Reschedule'>
											</p>
										</div>
									</div>
								</form>
								</div>
									<br>		
							</div>
						</div>
						<!---/col-->
					</div>
                    <!-- completed END -->
				
            </div>
        </div>
        <!-- container end -->
    </section>
	<?php 
require_once('../footer.php');
require_once('../footer_tags.php');

?>


<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
   
   <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.0/moment-with-locales.min.js"></script>
   
   <script type="text/javascript" src="/js/datetimepicker.js"></script>
    

	<script type="text/javascript">
	
    $(document).ready( function () {
        minute = moment().minute()
		if (minute >= 30) minute = 30
		else minute = 0;
        const tomorrow = moment().add(1, 'days').minute(minute)
        
        
        $('#picker').dateTimePicker({ selectDate: tomorrow, startDate: tomorrow,minuteIncrement: 30});
		$('#picker-no-time').dateTimePicker({ showTime: false, dateFormat: 'DD/MM/YYYY'});
		$("#book_mentor").hide();

    })
	
	$(document).ready( function () {
        minute = moment().minute()
		if (minute >= 30) minute = 30
		else minute = 0;
        const tomorrow = moment().add(1, 'days').minute(minute)

        $('#picker2').dateTimePicker({selectDate: tomorrow, startDate: tomorrow,minuteIncrement: 30});
        $('#picker-no-time').dateTimePicker({ showTime: false, dateFormat: 'DD/MM/YYYY'});
    })
	
	$(document).ready( function () {
        minute = moment().minute()
		if (minute >= 30) minute = 30
		else minute = 0;
        const tomorrow = moment().add(1, 'days').minute(minute)

        $('#picker3').dateTimePicker({selectDate: tomorrow, startDate: tomorrow,minuteIncrement: 30});
        $('#picker-no-time').dateTimePicker({ showTime: false, dateFormat: 'DD/MM/YYYY'});
    })
	
	(function($){
  
   $( '.toggle' ).click(function(){
     var target = $( this ).data( 'menu' );
     
     $( '#' + target ).toggleClass( 'menu--open' );
   });
  
})(jQuery);
    </script>

    
</body>

</html>