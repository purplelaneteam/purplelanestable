<?php
    $meeting_id = $meeting['id'];
    $get_rating_query = 'SELECT AVG(ratings) as rating FROM mentor_ratings WHERE meetings_id IN (SELECT id FROM meetings WHERE mentor_id = '.$meeting['mentor_id'].')';
    $get_rating_result = mysqli_query($con, $get_rating_query);
    if($get_rating_result) {
        $get_rating_row = mysqli_fetch_array($get_rating_result);
        $rating_val = $get_rating_row['rating'];
        $rating_val = $rating_val * 20;
    }
    else {
        $rating_val = 0; 
    }

    if(in_array($meeting['codename'], ['rescheduled', 'mentor cancelled', 'admin cancelled', 'mentee cancelled', 'completed'])){
        $get_rtime_sql = "SELECT id,`meeting_date`, `start_time` FROM `meeting_selected_slots` WHERE meetings_id='".$meeting_id."' ORDER BY addedon DESC LIMIT 1";
        $get_rtime_result = mysqli_query($con, $get_rtime_sql);
        $get_rtime_row = mysqli_fetch_array($get_rtime_result);
        $db_date = $get_rtime_row['meeting_date'];
        $db_start_time = $get_rtime_row['start_time'];
        $temp_end_time = strtotime("$db_start_time + 30min" );
        $cal_end_time = date('H:i:s', $temp_end_time);
    } elseif ($meeting['start_time']) {
        $date = date('Y-m-d');
        $db_date = $meeting['meeting_date'];
        $time = date('H:i:s');
        $db_start_time = $meeting['start_time'];
        $temp_end_time = strtotime("$db_start_time + 30min" );
        $cal_end_time = date('H:i:s', $temp_end_time);
        $email = $meeting['email'];
    }
    if(in_array($meeting['codename'], ['pending'])) {
        $sql = "SELECT id,`meeting_date`, `start_time` FROM `meeting_selected_slots` WHERE meetings_id='".$meeting_id."' ORDER BY addedon DESC";
        $slots = mysqli_query($con, $sql);
    }
?>

    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center" style="margin-top:5px">

                <div class="tab-content" id="nav-tabContent">


                    <div class="tab-pane fade show active" id="pop1" role="tabpanel" aria-labelledby="pop1-tab">

                        <!-- Scheduled start -->

                        <div class="row">

                            <!---/col-->
                            <div class="col-md-12 col-sm-12  ">

                                <div class="store-btns">

                                
                                        
                                    <div class="row review-box">
                                             <!-- Canceled :: Section :: Set -->
                                             <?php if (in_array($meeting['codename'], ['mentor cancelled', 'admin cancelled', 'mentee cancelled'])): ?>
                                                <div class="col-md-12 canceled_section text-left">
                                                    <h4>Your Schedule has been Canceled</h4>
                                                    <h5><?php echo date_format(date_create($db_date . ' '. $db_start_time)," l, j F Y H:ia"); ?></h5>
                                                </div>
                                            <?php endif; ?>
                                       
                                        <!-- Canceled :: Section :: End -->
                                       
                                        <div class="custom_mmt col-md-3 col-sm-12">
                                            <div class="app-image">
                                                <img src="../images/mentee/<?php echo $meeting['profile_pic'] ?>" class="img-fluid rev-mentor-pic">
                                            </div>
                                            <h5 class="text-center"><?php echo $meeting['mentee'] ?> </h5>
                                            <!-- <h6 class="text-center">College of science Engineer</h6> -->
                                        </div>
                                        <!---/col-->
                                        <div class="col-md-9 col-sm-12 mb-5 mb-btm mb-md-0 order-md-0">
                                            <div class="title_discription app-nfo app-info mb-4">

                                                <h5 class="subject-title">Title</h5>
                                                <p class="topic"><?php echo $meeting['topic'] ?>: <?php echo $meeting['title'] ?></p>
                                                <h5 class="">Discription </h5>
                                                <p><?php echo $meeting['comments'] ?></p>
                                                
                                            </div>
                                            

                                        </div>

                                        
                                        <div class="schedulde_solt col-md-4 text-center"> 
                                            <?php if (in_array($meeting['codename'], ['scheduled', 'completed', 'rescheduled'])): ?>
                                                <h5>Schedulde Solt:</h5>
                                                <h6><?php echo date_format(date_create($db_date . ' '. $db_start_time)," l, j F Y H:ia"); ?></h6>
                                            <?php endif; ?>
                                        </div>

                                        <div class="schedulde_solt col-md-8 text-right">
                                        </div>
                                        
                                        <!--  Requested  :: Set -->
                                        
                                        <div class="schedulde_solt col-md-4 text-center">
                                        <?php if (in_array($meeting['codename'], ['pending'])):  ?>
                                            <h5>Requested Solt:</h5>
                                            <?php foreach($slots as $slot):?>
                                                <h6><?php echo date_format(date_create($slot['meeting_date'] . ' '. $slot['start_time']),"l, j F Y h:ia"); ?></h6>
                                            <?php endforeach;?>
                                        <?php endif;?>
                                        </div>
                                        <form action="/mentor/processreq/proc_confirm_by_mentor.php" method="POST" class="col-md-6" id="confirm-form-by-mentor-<?php echo $meeting_id ?>">
                                            <div class="schedulde_solt text-right">
                                                <?php if (in_array($meeting['codename'], ['pending'])): ?>
                                                    <h5>Confirm one 30 minutes slot:</h5>
                                                    <div class="text-left input_cust slots-select-meeting">
                                                        <input type="hidden" name="selected_slot" id="selected-slot-<?php echo $meeting_id ?>">
                                                        <input type="hidden" name="meeting_id" value="<?php echo $meeting_id ?>">
                                                        <?php foreach($slots as $s => $slot): ?>
                                                            <div class="form-group">
                                                                <select name="slot<?php echo $s+1 ?>" id="slot<?php echo $s+1 ?>" data-id="<?php echo $meeting_id ?>" data-not-slot="<?php echo ($s == 0) ? 2: 1; ?>" class="form-control select-the-slot-on-click">
                                                                    <option selected disabled> Select from available slots</option>
                                                                    <?php foreach(getSlots(date('l, j F Y h:ia', strtotime($slot['meeting_date']. " ". $slot['start_time']))) as $meetingSlot): ?>
                                                                        <option value="<?php echo date('l, j F Y h:ia', $meetingSlot['start']) ?>">
                                                                            <?php echo date('h:ia', $meetingSlot['start']) ?> 
                                                                        </option>
                                                                    <?php endforeach; ?>
                                                                </select>
                                                            </div>
                                                        <?php endforeach; ?>
                                                        <!-- <input type="text" placeholder="9.00 pm"> -->
                                                        <!-- <input type="text" placeholder="9.00 pm"> -->
                                                    </div>
                                                <?php endif;?>
                                            </div>
                                        </form>
                                        <?php if (in_array($meeting['codename'], ['pending'])): ?>
                                        <div class="schedulde_solt col-md-2 text-right">
                                            <button type="bitton" class="confirm confirm-form-by-mentor-button" data-id="<?php echo $meeting_id ?>">Confirm</button>
                                        </div>
                                        <?php endif; ?>
                                        <?php if (in_array($meeting['codename'], ['scheduled']) && $meeting['hangout_link'] /* && strtotime('now') > strtotime($db_date . " ". $db_start_time) && strtotime('now') < strtotime($db_date . " " . $cal_end_time) */): ?>
                                        <div class="schedulde_solt col-md-2 text-right">
                                            <a target="_blank" class="confirm connect_with_hangout" href="<?php echo $meeting['hangout_link'] ?>" data-start_time="<?php echo $db_start_time ?>" data-end_time="<?php echo $cal_end_time ?>" data-email="<?php echo $email ?>">Connect</a>
                                        </div>
                                        <?php endif; ?>

                                        <!--  Requested  :: End -->
                                        <?php if (in_array($meeting['codename'], ['pending'])): ?>
                                        <div class="col-md-12 propose_cancel_sche">
                                            <span><i>or</i></span>
                                            <button
                                            class="new-time-slot" data-id="<?php echo $meeting_id ?>" data-mentee="<?php echo $meeting_id?>"
                                            data-toggle="modal" data-target="#reschedule-modal" type="button">
                                                <h4 >What to propose new time slot ?</h4>
                                            </button>
                                            <button
                                            class="cancel-meeting" data-id="<?php echo $meeting_id ?>" data-mentee="<?php echo $meeting['mentee_id']?>"
                                            data-toggle="modal" data-target="#cancel-modal" type="button">
                                                <h4 >You want to cancel the schedule ?</h4>
                                            </button>
                                        </div>
                                        <?php endif; ?>


                                    </div>
                                </div>
                                <!---/col-->

                            </div>

                            <!-- Scheduled END -->

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


<style>
.dtp_modal-content {
	z-index: 99999 !important ;
}
</style>

