<?php
require_once "../includes/initialize.php";
require_once "logincheck.php";

$id = $_SESSION['id'];
$user = getMentorBasicInfo($con, $id);
$userMentor = $user;
$bank = getMentorBankDeatils($con, $id);
$education = getMentorEducation($con, $id);
$experience = getMentorExperience($con, $id);

$companies = [];
$sql_get_companies = "SELECT id, name, codename FROM company";
$result_get_companies = mysqli_query($con, $sql_get_companies);
while($myrow_get_company = mysqli_fetch_array($result_get_companies))
{
	$companies[] = array("id"=>$myrow_get_company["id"],
						"name"=>$myrow_get_company["name"],
						"codename"=>$myrow_get_company["codename"]
	);
}

$skills = [];
$sql_get_skills = "SELECT id, name, codename FROM skill";
$result_get_skills = mysqli_query($con, $sql_get_skills);
while($myrow_get_skill = mysqli_fetch_array($result_get_skills))
{
	$skills[] = array("id"=>$myrow_get_skill["id"],
						"name"=>$myrow_get_skill["name"],
						"codename"=>$myrow_get_skill["codename"]
	);
}

	$query_company = "SELECT company_id FROM mentor_company WHERE mentor_id='".$id."' ORDER BY id";
	$result_company = mysqli_query($con, $query_company);
	$row_company = mysqli_fetch_assoc($result_company);

	if($row_company == ''){
		$company_id = '';

	}else{
		$company_id = $row_company['company_id'];
	}

	$skills_selected = [];
	$query_skills_selected = "SELECT skill_id FROM mentor_skill WHERE mentor_id='".$id."'";
	$result_get_skills_selected = mysqli_query($con, $query_skills_selected);
	while($myrow_get_skills_selected = mysqli_fetch_array($result_get_skills_selected))
	{
		$skills_selected[] = $myrow_get_skills_selected["skill_id"];
	}




?>
<!DOCTYPE html>
<html lang="en">

<head>
	<title><?php echo SITE_NAME;?></title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="keywords" content="" />
	<meta name="description" content="" />
	<meta name="author" content="" />
	<?php require_once '../head.php' ?>
	<style>
		.nav {

			display: block;
			flex-wrap: wrap;
			padding-left: 0;
			margin-bottom: 0;
			list-style: none;
		}

		.basic_user_image {
			height: 100px;
			width: 100px;
		}

		.company_div .select2-container .select2-selection--single {
			min-height: 32px;
		}
	</style>

	<link rel="stylesheet" type="text/css" href="../css/jquery.datetimepicker.css" />

</head>

<body data-spy="scroll" data-target=".navbar" data-offset="82">
	<div class="site-loader">
		<!---======Preloader===========-->
		<div class="loader-dots">
			<div class="circle circle-1"></div>
			<div class="circle circle-2"></div>
		</div>
	</div>
	<div class="site__layer"></div>
	<?php require_once '../header.php' ?>
	<?php require_once 'navbar.php' ?>

	<section class="burger app" id="app">
		<div class="container">
			<!-- container-start -->
			<div class="container">
				<div class="row">
					<div class="col-md-12 text-center" style="margin-top:40px">
						<div class="tab-content" id="nav-tabContent">
							<div class="tab-pane fade show active" id="pop2" role="tabpanel" aria-labelledby="pop2-tab">
								<!-- Choose a Mentor start -->
								<div class="row">
									<div class="col-md-12 col-sm-12  ">
										<div class="store-btns">
											<div class=" review-box">
												<div class="tabs">
													<div class="container">
														<div class="row">
															<div class="col-md-3">
																<nav class="nav-justified ">
																	<div class="nav nav-tabs " id="nav-tab"
																		role="tablist">
																		<a class="nav-item nav-link active"
																			id="pop1-tab" data-toggle="tab" href="#pop1"
																			role="tab" aria-controls="pop1"
																			aria-selected="true"> <b>Basic
																				Information</b>
																		</a>
																		<a style="display:none;" class="nav-item nav-link" id="pop2a-tab"
																			data-toggle="tab" href="#pop2a" role="tab"
																			aria-controls="pop2a" aria-selected="false">
																			<b>Engagement Preferences</b>
																		</a>
																		<a class="nav-item nav-link" id="pop3-tab"
																			data-toggle="tab" href="#pop3" role="tab"
																			aria-controls="pop3" aria-selected="false">
																			<b>Bank Account Details</b>
																		</a>
																		<a class="nav-item nav-link" id="pop4-tab"
																			data-toggle="tab" href="#pop4" role="tab"
																			aria-controls="pop4" aria-selected="false">
																			<b>Change password</b>
																		</a>
																		<a class="nav-item nav-link" id="pop5-tab"
																			data-toggle="tab" href="#pop5" role="tab"
																			aria-controls="pop5" aria-selected="false">
																			<b>Education</b>
																		</a>
																		<a class="nav-item nav-link" id="pop6-tab"
																			data-toggle="tab" href="#pop6" role="tab"
																			aria-controls="pop6" aria-selected="false">
																			<b>Experience</b>
																		</a>
																	</div>
																</nav>
															</div>
															<div class="col-md-9">
																<div style="display:none;" id="alert_div_basic"
																	class="alert alert-danger alert-dismissible fade show"
																	role="alert">
																	<p>Danger</p>
																	<button style="margin-top: -30px;" type="button"
																		class="close" data-dismiss="alert"
																		aria-label="Close">
																		<span aria-hidden="true"
																			style="font-size: 31px;line-height: 0;">&times;</span>
																	</button>
																</div>
																<div class="tab-content">
																	<div class="tab-pane fade show active" id="pop1"
																		role="tabpanel" aria-labelledby="pop1-tab">
																		<p>
																			<div class="card border-primary rounded-0">
																				<div class="card-header p-0">
																					<div
																						class="bg-info text-white text-center py-2">
																						<h6> Basic Information
																							<span class="edt"><i
																									class="fa fa-pencil-square"></i></span>
																						</h6>
																					</div>
																				</div>
																				<form id="mentor_update_form"
																					method="POST"
																					enctype="multipart/form-data">
																					<input type="hidden"
																						name="mentor_id"
																						value="<?php echo $user['id'];?>">
																					<div class="card-body p-3">
																						<!--Body-->
																						<div class="form-group">
																							<div style="display:none;"
																								id="alert_div_basic"
																								class="alert alert-danger alert-dismissible fade show"
																								role="alert">
																								<p>Danger</p>
																								<button
																									style="margin-top: -30px;"
																									type="button"
																									class="close"
																									data-dismiss="alert"
																									aria-label="Close">
																									<span
																										aria-hidden="true"
																										style="font-size: 31px;line-height: 0;">&times;</span>
																								</button>
																							</div>
																						</div>
																						<div class="form-group">
																							<div class="img_cont">
																								<input
																									class="form-control"
																									accept="image/*"
																									style="display:none;"
																									type="file"
																									name="profile_pic"
																									id="profile_pic"
																									placeholder="Profile Pic">
																								<img id="image_preview"
																									src="<?php echo MENTOR_IMAGE_UPLOAD_FOLDER_LINK.$user['profile_pic'];?>"
																									class="rounded-circle basic_user_image">

																							</div>
																							<button type=Button
																								id="change_profile"
																								class="btn btn-alpha cnect">Change
																								Profile Picture</button>
																						</div>
																						<div class="form-group">
																							<div
																								class="input-group mb-2">
																								<div
																									class="input-group-prepend">
																									<div
																										class="input-group-text">
																										<i
																											class="fa fa-user text-info"></i>
																									</div>
																								</div>
																								<input type="text"
																									class="form-control"
																									id="fname"
																									name="fname"
																									placeholder="First Name"
																									value="<?php echo $user['fname'];?>"
																									required>
																							</div>
																						</div>
																						<div class="form-group">
																							<div
																								class="input-group mb-2">
																								<div
																									class="input-group-prepend">
																									<div
																										class="input-group-text">
																										<i
																											class="fa fa-user text-info"></i>
																									</div>
																								</div>
																								<input type="text"
																									class="form-control"
																									id="lname"
																									name="lname"
																									placeholder="Last Name"
																									value="<?php echo $user['lname'];?>"
																									required>
																							</div>
																						</div>
																						<div class="form-group">
																							<div
																								class="input-group mb-2">
																								<div
																									class="input-group-prepend">
																									<div
																										class="input-group-text">
																										<i
																											class="fa fa-envelope text-info"></i>
																									</div>
																								</div>
																								<input type="email"
																									class="form-control"
																									id="email"
																									name="email"
																									placeholder="Email"
																									required
																									value="<?php echo $user['email'];?>"
																									readonly>
																							</div>
																						</div>
																						<div class="form-group">
																							<div
																								class="input-group mb-2">
																								<div
																									class="input-group-prepend">
																									<div
																										class="input-group-text">
																										<i
																											class="fa fa-briefcase text-info"></i>
																									</div>
																								</div>
																								<input type="text"
																									class="form-control"
																									id="total_experience"
																									name="total_experience"
																									placeholder="Total Experience"
																									required
																									value="<?php echo $user['total_experience'];?>">
																							</div>
																						</div>
																						<div class="form-group">
																							<div
																								class="input-group mb-2">
																								<div
																									class="input-group-prepend">
																									<div
																										class="input-group-text">
																										<i
																											class="fa fa-phone text-info"></i>
																									</div>
																								</div>
																								<input type="text"
																									class="form-control"
																									id="prsn_phone"
																									name="mobile"
																									placeholder="Phone"
																									value="<?php echo $user['mobile'];?>"
																									required>

																							</div>
																							<small id="nombre"
																								class="form-text text-muted"
																								style="text-align: start;">Please
																								enter <span
																									style="color:#54136d !important;">Personal
																									mobile</span>
																								number.</small>
																						</div>
																						<div class="form-group">
																							<div
																								class="input-group mb-2">
																								<div
																									class="input-group-prepend">
																									<div
																										class="input-group-text">
																										<i
																											class="fa fa-phone text-info"></i>
																									</div>
																								</div>
																								<input type="text"
																									class="form-control"
																									id="paytm_phone"
																									name="paytm_mobile"
																									placeholder="Paytm Phone"
																									value="<?php echo $user['paytm_mobile'];?>"
																									required>

																							</div>
																							<small id="nombre"
																								class="form-text text-muted"
																								style="text-align: start;">Please
																								enter <span
																									style="color:#54136d !important;">PayTM
																									mobile</span>
																								number.</small>
																						</div>
																						<div class="form-group">
																							<div
																								class="input-group mb-2">
																								<div
																									class="input-group-prepend">
																									<div
																										class="input-group-text">
																										<i
																											class="fa fa-briefcase text-info"></i>
																									</div>
																								</div>
																								<input type="textarea"
																									class="form-control"
																									id="description"
																									name="description"
																									placeholder="Description"
																									value="<?php echo $user['description'];?>"
																									required>

																							</div>
																						</div>
																						<div class="form-group">
																							<div class="row">
																								<div
																									class="col-lg-6 company_div">
																									<label class="full"
																										for="company">Company</label>
																									<select
																										class="form-control"
																										id="company"
																										name="company">
																										<option>
																										</option>
																										<?php
																								foreach($companies as $company)
																								{
																								?>
																										<option
																											value="<?php echo $company['name'];?>"
																											<?php echo checkSelected($company['id'], $company_id); ?>>
																											<?php echo $company['name'];?>
																										</option>
																										<?php
																								}
																								?>
																									</select>
																								</div>
																								<div class="col-lg-6">
																									<label class="full"
																										for="skill">Skills</label>
																									<select
																										class="form-control"
																										id="skill"
																										name="skill[]"
																										multiple="multiple">
																										<?php
																								foreach($skills as $skill)
																								{
																								?>
																										<option
																											value="<?php echo $skill['name'];?>"
																											<?php echo checkSelected($skill['id'], $skills_selected); ?>>
																											<?php echo $skill['name'];?>
																										</option>
																										<?php
																								}
																								?>
																									</select>
																								</div>

																							</div>
																						</div> <button type="submit"
																							class="btn btn-alpha mr-lg-3 mr-2 cnect">
																							Save Changes <i
																								class="fa fa-angle-right ml-3"></i></button>
																				</form>
																			</div>
																	</div>
																	</p>
																</div>
																<div class="tab-pane fade" id="pop2a" role="tabpanel"
																	aria-labelledby="pop2a-tab">
																	<div class="row">
																		<div class="col-md-5">
																			<p>
																				<div
																					class="card border-primary rounded-0">
																					<div class="card-header p-0">
																						<div
																							class="bg-info text-white text-center py-2">
																							<h6> Availability
																								<span class="edt"><i
																										class="fa fa-pencil-square"></i></span>
																							</h6>
																						</div>
																					</div>
																					<div class="card-body p-3">
																						<div id="demo2">

																						</div>
																					</div>
																				</div>
																			</p>
																		</div>
																		<div class="col-md-7">
																			<p>
																				<div
																					class="card border-primary rounded-0">
																					<div class="card-header p-0">
																						<div
																							class="bg-info text-white text-center py-2">
																							<h6> Price Range
																								<span class="edt"><i
																										class="fa fa-pencil-square"></i></span>
																							</h6>
																						</div>
																					</div>
																					<div class="card-body p-3">
																						<!--Body-->
																						<div class="form-group">
																							<label>Set call rate(
																								Hourly)</label>
																							<div
																								class="input-group mb-2">
																								<div
																									class="input-group-prepend">
																									<div
																										class="input-group-text">
																										<i
																											class="fa fa-inr text-info"></i>
																									</div>
																								</div>
																								<input type="text"
																									class="form-control"
																									id="nombre"
																									name="nombre"
																									placeholder="Hourly"
																									required>
																							</div>
																						</div> <span>Once the password
																							is changed, you'll be
																							redirected to the login
																							page.</span>
																						<a href="#"
																							class="btn btn-alpha mr-lg-3 mr-2 cnect">
																							Save Changes <i
																								class="fa fa-angle-right ml-3"></i></a>
																					</div>
																				</div>
																			</p>
																		</div>
																	</div>
																</div>
																<div class="tab-pane fade" id="pop3" role="tabpanel"
																	aria-labelledby="pop3-tab">
																	<form id="mentor_bank_account">
																		<input type="hidden" name="mentor_id"
																			value="<?php echo $_SESSION['id']?>">
																		<div class="card border-primary rounded-0">
																			<div class="card-header p-0">
																				<div
																					class="bg-info text-white text-center py-2">
																					<h6> Bank Account Details
																						<span class="edt"><i
																								class="fa fa-pencil-square"></i></span>
																					</h6>
																				</div>
																			</div>
																			<div class="card-body p-3">
																				<!--Body-->
																				<div class="form-group">
																					<div class="input-group mb-2">
																						<div
																							class="input-group-prepend">
																							<div
																								class="input-group-text">
																								<i
																									class="fa fa-user text-info"></i>
																							</div>
																						</div>
																						<input type="text"
																							class="form-control"
																							id="name" name="name"
																							placeholder="Bank Name"
																							required value="<?php echo ($bank) ? $bank['bank_name']: '' ?>">
																					</div>
																				</div>
																				<div class="form-group">
																					<div class="input-group mb-2">
																						<div
																							class="input-group-prepend">
																							<div
																								class="input-group-text">
																								<i
																									class="fa fa-check-square text-info"></i>
																							</div>
																						</div>
																						<input type="text"
																							class="form-control"
																							id="number" name="number"
																							placeholder="Account Number"
																							required value="<?php echo ($bank) ? $bank['account_number']: '' ?>">
																					</div>
																				</div>
																				<div class="form-group">
																					<div class="input-group mb-2">
																						<div
																							class="input-group-prepend">
																							<div
																								class="input-group-text">
																								<i
																									class="fa fa-check-square text-info"></i>
																							</div>
																						</div>
																						<input type="text"
																							class="form-control"
																							id="ifsc" name="ifsc"
																							placeholder="IFS Code"
																							required value="<?php echo ($bank) ? $bank['ifsc']: '' ?>">
																					</div>
																				</div>
																				<button type="submit"
																					class="btn btn-alpha mr-lg-3 mr-2 cnect">
																					Save Changes <i
																						class="fa fa-angle-right ml-3"></i></button>
																			</div>
																		</div>
																	</form>
																</div>
																<div class="tab-pane fade" id="pop4" role="tabpanel"
																	aria-labelledby="pop4-tab">
																	<div class="card border-primary rounded-0">
																		<div class="card-header p-0">
																			<div
																				class="bg-info text-white text-center py-2">
																				<h6> Change Password
																					<span class="edt"><i
																							class="fa fa-pencil-square"></i></span>
																				</h6>
																			</div>
																		</div>
																		<form id="mentor_password_form" method="POST"
																			enctype="multipart/form-data">
																			<input type="hidden" name="mentor_id"
																				value="<?php echo $user['id'];?>">
																			<div class="card-body p-3">
																				<!--Body-->
																				<div class="form-group">
																					<div style="display:none;"
																						id="alert_div_password"
																						class="alert alert-danger alert-dismissible fade show"
																						role="alert">
																						<p>Danger</p>
																						<button
																							style="margin-top: -30px;"
																							type="button" class="close"
																							data-dismiss="alert"
																							aria-label="Close">
																							<span aria-hidden="true"
																								style="font-size: 31px;line-height: 0;">&times;</span>
																						</button>
																					</div>
																				</div>
																				<div class="form-group">
																					<div class="input-group mb-2">
																						<div
																							class="input-group-prepend">
																							<div
																								class="input-group-text">
																								<i
																									class="fa fa-lock text-info"></i>
																							</div>
																						</div>
																						<input type="password"
																							class="form-control"
																							id="current_password"
																							name="current_password"
																							placeholder="Current Password"
																							required>
																						<div class="input-group-addon">
																						</div>
																					</div>
																				</div>
																				<div class="form-group">
																					<div class="input-group mb-2">
																						<div
																							class="input-group-prepend">
																							<div
																								class="input-group-text">
																								<i
																									class="fa fa-key text-info"></i>
																							</div>
																						</div>
																						<input type="password"
																							class="form-control"
																							id="new_password"
																							name="new_password"
																							placeholder="New Password"
																							required>
																						<div class="input-group-addon">
																						</div>
																					</div>
																				</div>
																				<div class="form-group">
																					<div class="input-group mb-2">
																						<div
																							class="input-group-prepend">
																							<div
																								class="input-group-text">
																								<i
																									class="fa fa-key text-info"></i>
																							</div>
																						</div>
																						<input type="password"
																							class="form-control"
																							id="confirm_password"
																							name="confirm_password"
																							placeholder="Retype New Password"
																							required>
																						<div class="input-group-addon">
																						</div>
																					</div>
																				</div>
																				<button type="submit"
																					class="btn btn-alpha mr-lg-3 mr-2 cnect">
																					Save Changes <i
																						class="fa fa-angle-right ml-3"></i>
																				</button>
																			</div>
																		</form>
																	</div>
																</div>
																<div class="tab-pane fade" id="pop5" role="tabpanel"
																	aria-labelledby="pop5-tab">
																	<div class="education-tab card border-primary rounded-0">
																		<div class="card-header p-0">
																			<div
																				class="bg-info text-white text-center py-2">
																				<h6> Education
																					<span class="edit-education edt"><i
																							class="fa fa-pencil-square"></i></span>
																				</h6>
																			</div>
																		</div>
																		<div class="education">
																			<?php foreach($education as $data): ?>
																				<div>
																					<p class="title"><?php echo $data['name'] ?></p>
																					<div class="description"><?php echo $data['description'] ?><span class="pull-right education-experience-delete"  data-type="education"data-id="<?php echo $data['id'] ?>" data-name="<?php echo $data['name'] ?>">Delete</span></div>
																				</div>
																			<?php endforeach; ?>
																		</div>
																		<form id="mentor_education_form" method="POST"
																			enctype="multipart/form-data">
																			<input type="hidden" name="mentor_id"
																				value="<?php echo $_SESSION['id'];?>">
																			<div class="card-body p-3">
																				<!--Body-->
																				<div class="form-group">
																					<div style="display:none;"
																						id="alert_div_password"
																						class="alert alert-danger alert-dismissible fade show"
																						role="alert">
																						<p>Danger</p>
																						<button
																							style="margin-top: -30px;"
																							type="button" class="close"
																							data-dismiss="alert"
																							aria-label="Close">
																							<span aria-hidden="true"
																								style="font-size: 31px;line-height: 0;">&times;</span>
																						</button>
																					</div>
																				</div>
																				<div class="form-group">
																					<div class="input-group mb-2">
																						<div
																							class="input-group-prepend">
																							<div
																								class="input-group-text">
																								<i
																									class="fas fa-graduation-cap text-info"></i>
																							</div>
																						</div>
																						<input type="text"
																							class="form-control"
																							id="education_name"
																							name="name"
																							placeholder="Name" required>
																					</div>
																				</div>
																				<div class="row">
																					<div class="col-md-12 col-sm-12">
																						<textarea class="form-control"
																							rows="5" name="description"
																							id="details"
																							placeholder="Detaild about the qualification"
																							style="border:1px solid #ccc"></textarea>
																					</div>
																				</div>
																			</div>
																			<button type="submit"
																				class="btn btn-alpha mr-lg-3 mr-2 cnect">Save
																				Education <i
																					class="fa fa-angle-right ml-3"></i>
																			</button>
																	</div>
																	</form>
																</div>
																<div class="tab-pane fade" id="pop6" role="tabpanel"
																	aria-labelledby="pop6-tab">
																	<div class="experience-tab card border-primary rounded-0">
																		<div class="card-header p-0">
																			<div
																				class="bg-info text-white text-center py-2">
																				<h6> Experience
																					<span class="edt edit-experience"><i
																							class="fa fa-pencil-square"></i></span>
																				</h6>
																			</div>
																		</div>
																		<div class="experience">
																			<?php foreach($experience as $data): ?>
																				<div>
																					<p class="title"><?php echo $data['name'] ?></p>
																					<div class="description"><?php echo $data['description'] ?><span class="pull-right education-experience-delete" data-type="experience" data-id="<?php echo $data['id'] ?>" data-name="<?php echo $data['name'] ?>">Delete</span></div>
																				</div>
																			<?php endforeach; ?>
																		</div>
																		<form id="mentor_experience_form" method="POST"
																			enctype="multipart/form-data">
																			<input type="hidden" name="mentor_id"
																				value="<?php echo $_SESSION['id'];?>">
																			<div class="card-body p-3">
																				<!--Body-->
																				<div class="form-group">
																					<div style="display:none;"
																						id="alert_div_password"
																						class="alert alert-danger alert-dismissible fade show"
																						role="alert">
																						<p>Danger</p>
																						<button
																							style="margin-top: -30px;"
																							type="button" class="close"
																							data-dismiss="alert"
																							aria-label="Close">
																							<span aria-hidden="true"
																								style="font-size: 31px;line-height: 0;">&times;</span>
																						</button>
																					</div>
																				</div>
																				<div class="form-group">
																					<div class="input-group mb-2">
																						<div
																							class="input-group-prepend">
																							<div
																								class="input-group-text">
																								<i
																									class="fas fa-user-cog text-info"></i>
																							</div>
																						</div>
																						<input type="text"
																							class="form-control"
																							id="experience_name"
																							name="name"
																							placeholder="Name" required>
																					</div>
																				</div>
																				<div class="row">
																					<div class="col-md-12 col-sm-12">
																						<textarea class="form-control"
																							rows="5" name="description"
																							id="details"
																							placeholder="Detaild about your experience"
																							style="border:1px solid #ccc"></textarea>
																					</div>
																				</div>
																			</div>
																			<button type="submit"
																				class="btn btn-alpha mr-lg-3 mr-2 cnect">Save
																				Experience <i
																					class="fa fa-angle-right ml-3"></i>
																			</button>
																	</div>
																	</form>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!---/col-->
						</div>
						<!-- /row  -->
					</div>
					<div class="tab-pane fade" id="pop3" role="tabpanel" aria-labelledby="pop3-tab">
						<div class="pt-3"></div>
						<p>3. There's another way to do this for layouts that doesn't have to put the navbar inside
							the container, and which doesn't require any CSS or Bootstrap overrides. Simply place a
							div with the Bootstrap container class around the navbar. This will center the links
							inside the navbar:
					</div>
				</div>
			</div>
		</div>
		</div>
		</div>
		</div>
		<!-- container end -->
	</section>
	<?php 
require_once('../footer.php');
require_once('../footer_tags.php');
?>

<script>
	$(function() {
		$('.education-experience-delete').click(function(e) {
			const name = $(this).attr('data-name')
			const id = $(this).attr('data-id')
			const type = $(this).attr('data-type')
			const shouldDelete = $(this).parent().parent()
			
			swal({
            title: "Are you sure?",
            text: `You want to delete ${name} experience!`,
            icon: "warning",
			buttons: {
				cancel: {
					text: "Cancel",
					value: false,
					visible: true,
					className: "",
					closeModal: true,
				},
				confirm: {
					text: "OK",
					value: true,
					visible: true,
					className: "",
					closeModal: true
				}
			},
            reverseButtons: !0
        }).then(function(value) {
            if(value)
            {
                $.ajax({
                    url: "/mentor/processreq/proc_delete_experience_education.php",
                    type: "POST",
                    data: {id, type},
                    success: function(response){
                        if (response.code) {
                            swal('Error!', response.msg, 'error')
                        } else {
                            swal({
                                title: "Success!",
                                text: response.msg,
                                type: "success",
                            }).then(function () {
								shouldDelete.remove()
                            });
                        }
                    },
                    error: function (response) {
                        swal('Error!', response.msg, 'error');
                    }
                });
            }
        })
		})
		$('.education-delete')
	})
</script>