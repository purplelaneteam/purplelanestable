<?php
    require_once("../../includes/initialize.php");
    require("../logincheck.php");
    $jsonarray = array();
    $error = '';

    $sql = "START TRANSACTION";
    $result = mysqli_query($con, $sql);
    if(!$result)
    {
        $error = "Something went wrong. Please try again later.";
        $message = $error;
    }

    $id = sanitize_input($_POST["id"]);
    $type = sanitize_input($_POST["type"]);
    if($type == 'education') {
        $type = 'mentor_education';
    }
    elseif($type == 'experience') {
        $type = 'mentor_experience';
    }

    $sql = "DELETE from $type WHERE id = $id";
    $result = mysqli_query($con, $sql);
    if(!$result)
    {
        $error = "Something went wrong. Please try again later.";
        $message = $error;
    }
    else {
        $message = 'Deleted Successfully.';
    }

    $sql = "COMMIT";
    $result = mysqli_query($con, $sql);
    if(!$result)
    {
        $error = "commit: Something went wrong. Please try again later.";
        $message = $error;
    }
    
    if($error) {
        echo json_encode([
            'code' => true,
            'msg' => $message,
        ]);exit;
    }

    echo json_encode([
        'code' => false,
        'msg' => $message,
    ]);