<?php
    require_once("../../includes/initialize.php");
    require("../logincheck.php");
    $jsonarray = array();

    $sql = "START TRANSACTION";
    $result = mysqli_query($con, $sql);
    if(!$result)
    {
        $error = "Something went wrong. Please try again later.";
        ajax_error($error);
    }
    $current_password = sanitize_input($_POST["current_password"]);
    $new_password = sanitize_input($_POST["new_password"]);
    $confirm_password = sanitize_input($_POST["confirm_password"]);
    $date=date('Y-m-d H:i:s');

    $mentor_id = sanitize_input($_POST["mentor_id"]);
        if(($current_password =="" || $new_password == "" || $confirm_password=="")
        )
        {
            $jsonarray['code']="0";
            $jsonarray['msg'] = "All field is mandatory";
            echo json_encode($jsonarray);exit;
        }

        $check_email = "SELECT email
                        FROM mentor
                        WHERE password ='".MD5($current_password)."' AND id = $mentor_id";
        
        $result_check = mysqli_query($con, $check_email);
        $row_cnt = mysqli_num_rows($result_check);
        if(!$row_cnt > 0)
        {
            $error = "Old Password did not match.";
            ajax_error($error);
        }

        if($new_password != $confirm_password)
        {
            $jsonarray['code']="0";
            $jsonarray['msg'] = "New password did not match with confirm password.";
            echo json_encode($jsonarray);exit;
        }

        $sql_update = "UPDATE mentor SET password = '".MD5($new_password)."' WHERE id = '$mentor_id'";
        //echo $sql_insert;exit;
        $result_update = mysqli_query($con, $sql_update);
        if(!$result_update) 
        {
            $error = "Something went wrong while updating mentor. Please try again later.";
            ajax_error($error);
        }

    $sql = "COMMIT";
    $result = mysqli_query($con, $sql);
    if(!$result)
    {
        $error = "commit: Something went wrong. Please try again later.";
        ajax_error($error);
    }
    else
    {
        $jsonarray["code"] = 0;
        $jsonarray["msg"] = "Mentor password updated succesfully.";
        echo json_encode($jsonarray);
    }