<?php
    require_once("../../includes/initialize.php");
    require("../logincheck.php");
    $jsonarray = array();

    $sql = "START TRANSACTION";
    $result = mysqli_query($con, $sql);
    if(!$result)
    {
        $error = "Something went wrong. Please try again later.";
        ajax_error($error);
    }
    $fname = sanitize_input($_POST["fname"]);
    $lname = sanitize_input($_POST["lname"]);
    $email = sanitize_input($_POST["email"]);
    $mobile = sanitize_input($_POST["mobile"]);
    $description = sanitize_input($_POST["description"]);
    $paytm_mobile = sanitize_input($_POST["paytm_mobile"]);
    $total_experience = sanitize_input($_POST["total_experience"]);
    $date=date('Y-m-d H:i:s');
    $company = sanitize_input($_POST["company"]);
    $skills = $_POST["skill"];
    $company_codename = str_replace(" ","_",strtolower($company));

    $mentor_id = sanitize_input($_POST["mentor_id"]);
        if($fname =="" || $lname == ""|| $email=="" || $mobile=="" || $paytm_mobile=="" || $total_experience=="" || $skills == "" || $company == ""  || $description=="")
        {
            $jsonarray['code']="0";
            $jsonarray['msg'] = "All field is mandatory";
            echo json_encode($jsonarray);exit;
        }
        if(!is_numeric($total_experience))
        {
            $jsonarray['code']="0";
            $jsonarray['msg'] = "Experience should be numeric";
            echo json_encode($jsonarray);exit;
        }
      
        $check_email = "SELECT email
                        FROM mentor
                        WHERE email='".$email."' AND id!= $mentor_id";

        $result_check = mysqli_query($con, $check_email);
        $row_cnt = mysqli_num_rows($result_check);
        if($row_cnt > 0)
        {
            $error = "Email already exists.";
            ajax_error($error);
        }

        $sql_mentor_detail = "SELECT profile_pic
                        FROM mentor
                        WHERE id = $mentor_id";

        $result_mentor_detail = mysqli_query($con, $sql_mentor_detail);
        $cnt_mentor_detail = mysqli_num_rows($result_mentor_detail);
        if($cnt_mentor_detail > 0)
        {
            if($myrow_mentor_details =mysqli_fetch_array($result_mentor_detail))
            {
                $profile_pic = $myrow_mentor_details['profile_pic'];
            }
        }

        if(count($_FILES) > 0)
        {
            if($_FILES["profile_pic"]["name"] != "")
            {
                $temp = explode(".", $_FILES["profile_pic"]["name"]);
                $type = ($_FILES["profile_pic"]["type"]);
                $extension = end($temp);
                if (in_array($type, $mentor_profile_pic_allowed_type)
                && ($_FILES["profile_pic"]["size"] < MENTOR_PROFILE_IMAGE_SIZE * 1000)
                && in_array($extension, $mentor_profile_pic_allowed_ext))
                {   
                    if ($_FILES["profile_pic"]["error"] > 0)
                    {
                        $error = "Error occured";
                        ajax_error($error);
                    }
                
                    else
                    {
                        $target_dir = MENTOR_IMAGE_UPLOAD_FOLDER;

                        $profile_pic = uniqid().".".$extension;

                        $file_path = $target_dir."/".$profile_pic;
                        $attachment_file_path = $_SERVER["DOCUMENT_ROOT"]."/".$file_path;
                        //move file to temp image folder
                        move_uploaded_file($_FILES["profile_pic"]["tmp_name"], $attachment_file_path);
                        

                    }
                }
                else
                {
                    $error = "Please check file dimension and size of icon";
                    ajax_error($error);
                }
            }
            else
            {
                $profile_pic = $profile_pic;
            }
        }
        else 
        {
            $profile_pic = $profile_pic;
        }
        $sql_update = "UPDATE mentor SET fname = '".$fname."', lname = '".$lname."', email = '".$email."' , mobile = '".$mobile."', paytm_mobile = '".$paytm_mobile."', description = '".$description."',profile_pic = '".$profile_pic."', total_experience = '".$total_experience."' WHERE id = '$mentor_id'";
        // echo $sql_insert;exit;
        $result_update = mysqli_query($con, $sql_update);
        if(!$result_update) 
        {
            $error = "Something went wrong while updating mentor. Please try again later.";
            ajax_error($error);
        }

        $sql_menc_delete = "DELETE FROM mentor_company WHERE mentor_id='$mentor_id'";
        $result_menc_delete = mysqli_query($con, $sql_menc_delete);
        if(!$result_menc_delete) 
        {
            $error = "Something went wrong while delete mentor company. Please try again later.";
            ajax_error($error);
        }

        $query_company = "SELECT id FROM company WHERE codename='".$company_codename."' ORDER BY id";
        $result_company = mysqli_query($con, $query_company);
        $row_company = mysqli_fetch_assoc($result_company);

        if($row_company == ''){
            $sql_insert_comp = "INSERT INTO company (name, codename, addedon) VALUES ('$company','$company_codename','$date')";
        
            $result_insert_skill = mysqli_query($con, $sql_insert_comp);
            $company_id = mysqli_insert_id($con);

            if(mysqli_affected_rows($con)<=0) 
            {
                $error = "Something went wrong while adding company. Please try again later.";
                ajax_error($error);
            }

        }else{
            $company_id = $row_company['id'];
        }

        $sql_insert_men_comp = "INSERT INTO mentor_company (mentor_id, company_id, addedon) VALUES ('$mentor_id','$company_id','$date')";
        $result_insert_men_comp = mysqli_query($con, $sql_insert_men_comp);
        $men_comp_id = mysqli_insert_id($con);

        if(mysqli_affected_rows($con)<=0) 
        {
            $error = "Something went wrong while adding mentor company. Please try again later.";
            ajax_error($error);
        }

        $sql_mens_delete = "DELETE FROM mentor_skill WHERE mentor_id='$mentor_id'";
        $result_mens_delete = mysqli_query($con, $sql_mens_delete);
        if(!$result_mens_delete) 
        {
            $error = "Something went wrong while delete mentor skill. Please try again later.";
            ajax_error($error);
        }

        foreach ($skills as $skill_value) {
            $skill = sanitize_input($skill_value);
            $skill_codename = str_replace(" ","_",strtolower($skill));

            $query_skill = "SELECT id FROM skill WHERE codename='".$skill_codename."' ORDER BY id";
            $result_skill = mysqli_query($con, $query_skill);
            $row_skill = mysqli_fetch_assoc($result_skill);

            if($row_skill == ''){
                $sql_insert_skill = "INSERT INTO skill (name, codename, addedon) VALUES ('$skill','$skill_codename','$date')";
            
                $result_insert_skill = mysqli_query($con, $sql_insert_skill);
                $skill_id = mysqli_insert_id($con);

                if(mysqli_affected_rows($con)<=0) 
                {
                    $error = "Something went wrong while adding skill. Please try again later.";
                    ajax_error($error);
                }

            }else{
                $skill_id = $row_skill['id'];
            }

            $sql_insert_men_comp = "INSERT INTO mentor_skill (mentor_id, skill_id, addedon) VALUES ('$mentor_id','$skill_id','$date')";
            $result_insert_men_comp = mysqli_query($con, $sql_insert_men_comp);
            $men_comp_id = mysqli_insert_id($con);

            if(mysqli_affected_rows($con)<=0) 
            {
                $error = "Something went wrong while adding mentor skill. Please try again later.";
                ajax_error($error);
            }
        }

    $sql = "COMMIT";
    $result = mysqli_query($con, $sql);
    if(!$result)
    {
        $error = "commit: Something went wrong. Please try again later.";
        ajax_error($error);
    }
    else
    {
        $jsonarray["code"] = 0;
        $jsonarray["msg"] = "Mentor updated succesfully.";
        echo json_encode($jsonarray);
    }