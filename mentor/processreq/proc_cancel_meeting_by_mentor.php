<?php
    require_once("../../includes/initialize.php");
    require("../logincheck.php");
    $jsonarray = array();

    $sql = "START TRANSACTION";
    $result = mysqli_query($con, $sql);
    if(!$result)
    {
        $error = "Something went wrong. Please try again later.";
        $_SESSION['error'] = $error;
    }
    $meeting_id = sanitize_input($_POST["meeting_id"]);
    
    $date=date('Y-m-d H:i:s');
    $mentee_id = sanitize_input($_POST["mentee_id"]);
    
    $sql_get_status_code="SELECT id FROM meeting_status WHERE codename='mentor cancelled'";
    $result_get_status_code=mysqli_query($con,$sql_get_status_code);
    while($row=mysqli_fetch_assoc($result_get_status_code))
    {
        $status_id=$row['id'];
    }
    $sql_get_old_data="SELECT id, meeting_status_id, meeting_selected_slots_id, meeting_date, cost_card_price, service_tax_price, platform_charges, selected_time FROM meetings WHERE id=$meeting_id";
    $result_get_old_data=mysqli_query($con,$sql_get_old_data);
    if($row=mysqli_fetch_assoc($result_get_old_data))
    {
        $id=$row['id'];
        $meeting_status_id=$row['meeting_status_id'];
        $meeting_selected_slots_id=$row['meeting_selected_slots_id'];
        $meeting_date=$row['meeting_date'];
        $cost_card_price=$row['cost_card_price'];
        $service_tax_price=$row['service_tax_price'];
        $platform_charges=$row['platform_charges'];
        $selected_time=$row['selected_time'];
    }
    else
    {
        $error = "ERROR 2: Something went wrong. Please try again later. ";
        $_SESSION['error'] = $error;
    }

    $sql_put_history="INSERT INTO meetings_status_history (meetings_id, meetings_status_id, meeting_selected_slots_id, meeting_date, cost_card_price, service_tax_price, platform_charges, selected_time,addedon) VALUES ('$id','$meeting_status_id', NULL,'$meeting_date','$cost_card_price','$service_tax_price','$platform_charges','$selected_time','$date')";
    
    $result_put_history = mysqli_query($con, $sql_put_history);
    if(!$result_put_history) 
    {
        $error = "ERROR 3: Something went wrong. Please try again later.";
        $_SESSION['error'] = $error;
    }
    
    $sql_insert = "update meetings set meeting_status_id=".$status_id."  WHERE id=$meeting_id";

    $result_update = mysqli_query($con, $sql_insert);
    if(!$result_update) 
    {
        $error = "ERROR 4: Something went wrong while adding meeting. Please try again later.";
        $_SESSION['error'] = $error;
    }
    if(CANCEL_MEETING_MAIL_MENTEE)
    {
        $sql_get_mentee_email="SELECT email,fname FROM mentee WHERE id=$mentee_id";
        $result_get_mentee_email=mysqli_query($con,$sql_get_mentee_email);
        if($row=mysqli_fetch_assoc($result_get_mentee_email))
        {
            $email=$row['email'];
            $fname=$row['fname'];
        }
        $email_subject = "PurpleLane.in: Your Meeting Cancelled - $meeting_id";
        $mailbody = "Hi $fname,<br/>
                    Unfortunately your meeting request cancelled by the mentor. We will initiate the refund process.<br>
                    <br/><a href='".SITE_URL."'>Check your meetings here</a><br/><br/><br/>
                    Thanks & 
                    Regards<br/>
                    '".CONTACTUS_FROM_NAME."'";

        $mail_file = "../../../../includes/class.phpmailer.php";
        $send_mail = send_mail($email_subject, $mailbody, $email, $mail_file);

        if(!$send_mail)
        {
            $error = "ERROR 3: Something went wrong. Please try again later.";
            $_SESSION['error'] = $error;
        }
    }
   

    $sql = "COMMIT";
    $result = mysqli_query($con, $sql);
    if(!$result)
    {
        $error = "commit: Something went wrong. Please try again later.";
        $_SESSION['error'] = $error;
    }
    if(!$success) {
        $_SESSION['success'] = "Meeting cancelled.";
    }
    
    header("Location: {$_SERVER['HTTP_REFERER']}");