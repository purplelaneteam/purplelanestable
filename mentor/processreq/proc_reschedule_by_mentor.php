<?php
    require_once("../../includes/initialize.php");
    require("../logincheck.php");
    $jsonarray = array();

    $sql = "START TRANSACTION";
    $result = mysqli_query($con, $sql);
    if(!$result)
    {
        $error = "Something went wrong. Please try again later.";
        $_SESSION['error'] = $error;
    }
    $slot1 = $_POST["slot1"].':00';
    // $comment = sanitize_input($_POST["comment"]);
    $comment = '';
    $meeting_id = sanitize_input($_POST["meeting_id"]);
    $date=date('Y-m-d H:i:s');

    $mentee_id = sanitize_input($_POST["mentee_id"]);
    if($slot1 =="" || $meeting_id=="")
    {
        $jsonarray['code']="0";
        $jsonarray['msg'] = "All field is mandatory";
        echo json_encode($jsonarray);exit;
    }

    $slot=explode(' ',$slot1);
    
    $sql_get_status_code="SELECT id FROM meeting_status WHERE codename='rescheduled'";
    $result_get_status_code=mysqli_query($con,$sql_get_status_code);
    while($row=mysqli_fetch_assoc($result_get_status_code))
    {
        $status_id=$row['id'];
    }

    $sql_put_new_slot="INSERT INTO meeting_selected_slots (meetings_id, meeting_date, start_time,addedon) VALUES ($meeting_id,'".$slot[0]."','".$slot[1]."','$date')";
    $result_put_new_slot=mysqli_query($con,$sql_put_new_slot);
    if($result_put_new_slot)
    {
        $selected_slot_id = $con->insert_id;
    }
    else
    {
        $error = "ERROR 1: Something went wrong. Please try again later. ";
        $_SESSION['error'] = $error;
    }

    $sql_get_old_data="SELECT id, meeting_status_id, meeting_selected_slots_id, meeting_date, cost_card_price, service_tax_price, platform_charges, selected_time FROM meetings WHERE id=$meeting_id";
    $result_get_old_data=mysqli_query($con,$sql_get_old_data);
    if($row=mysqli_fetch_assoc($result_get_old_data))
    {
        $id=$row['id'];
        $meeting_status_id=$row['meeting_status_id'];
        $meeting_selected_slots_id=($row['meeting_selected_slots_id']) ? $row['meeting_selected_slots_id']: 'NULL';
        $meeting_date=$row['meeting_date'];
        $cost_card_price=$row['cost_card_price'];
        $service_tax_price=$row['service_tax_price'];
        $platform_charges=$row['platform_charges'];
        $selected_time=$row['selected_time'];
    }
    else
    {
        $error = "ERROR 2: Something went wrong. Please try again later. ";
        $_SESSION['error'] = $error;
    }

    $sql_put_history="INSERT INTO meetings_status_history (meetings_id, meetings_status_id, meeting_selected_slots_id, meeting_date, cost_card_price, service_tax_price, platform_charges, selected_time,addedon) VALUES ('$id','$meeting_status_id',$meeting_selected_slots_id,'$meeting_date','$cost_card_price','$service_tax_price','$platform_charges','$selected_time','$date')";
    $result_put_history = mysqli_query($con, $sql_put_history);

    if(!$result_put_history) 
    {
        $error = "ERROR 3: Something went wrong. Please try again later.";
        $_SESSION['error'] = $error;
    }
    
    $sql_insert = "update meetings set selected_time='".$slot[1]."' , meeting_date='".$slot[0]."' , meeting_status_id=".$status_id." , meeting_selected_slots_id=".$selected_slot_id." WHERE id=$meeting_id";

    $result_update = mysqli_query($con, $sql_insert);
    if(!$result_update) 
    {
        $error = "ERROR 4: Something went wrong while adding meeting. Please try again later.";
        $_SESSION['error'] = $error;
    }
    if(RESCHEDULE_MEETING_MAIL_MENTEE)
    {
        $sql_get_mentee_email="SELECT email FROM mentee WHERE id=$mentee_id";
        $result_get_mentee_email=mysqli_query($con,$sql_get_mentee_email);
        if($row=mysqli_fetch_assoc($result_get_mentee_email))
        {
            $email=$row['email'];
        }

        $email_subject = "Your ".SITE_NAME." : Meeting Rescheduled ";
        $mailbody = "Hi,<br/><br/>
                    New slot for your rescheduled meeting is as follows<br>
                    Meeting ID : ".$meeting_id."<br>
                    Selected Meeting Date : ".$slot[1]."<br>
                    Selected Meeting Time : ".$slot[0]."
                    <br/>
                    <a href='".SITE_URL."'>Check your meetings here</a><br/><br/><br/>
                    
                    Thanks<br/>
                    Regards<br/>
                    '".CONTACTUS_FROM_NAME."'";

                    $mail_file = "../../../../includes/class.phpmailer.php";
        $send_mail = send_mail($email_subject, $mailbody, $email, $mail_file);

        if(!$send_mail)
        {
            $error = "ERROR 3: Something went wrong. Please try again later.";
            $_SESSION['error'] = $error;
        }
    }
    $sql = "COMMIT";
    $result = mysqli_query($con, $sql);
    if(!$result)
    {
        $error = "commit: Something went wrong. Please try again later.";
        $_SESSION['error'] = $error;
    }
    
    if(!$error) {
        $_SESSION['success']= "Rescheduled Successfully.";
    }

    header("Location: {$_SERVER['HTTP_REFERER']}");