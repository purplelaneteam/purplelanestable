<?php
    require_once("../../includes/initialize.php");
    require("../logincheck.php");
    $jsonarray = array();

    $sql = "START TRANSACTION";
    $result = mysqli_query($con, $sql);
    if(!$result)
    {
        $error = "Something went wrong. Please try again later.";
        $_SESSION['error'] = $error;
    }

    $meeting_id = sanitize_input($_POST["meeting_id"]);
    // $comment = sanitize_input($_POST["comment"]);
    $comment = '-';

    $slot1 = sanitize_input($_POST["slot1"]);
    $slot2 = sanitize_input($_POST["slot2"]);

    $date = date("Y-m-d H:i:s");

    $selected_date1 = strtotime("$slot1");
    $selected_date2 = strtotime("$slot2");
    $selected_slot = sanitize_input($_POST['selected_slot']);

    if ($selected_slot === 'slot1')
    {
        $meeting_date = date("Y-m-d", $selected_date1);
        $start_time = date("H:i:s", $selected_date1);
        $end_time = date("H:i:s", strtotime("$slot1 + 30 min"));
    }
    if ($selected_slot === 'slot2')
    {
        $meeting_date = date("Y-m-d", $selected_date2);
        $start_time = date("H:i:s", $selected_date2);
        $end_time = date("H:i:s", strtotime("$slot2 + 30 min"));
    }

    if(!($slot1 || $slot2) || !$comment || !$meeting_id)
    {
        $error = "Please select time slot to confirm";
        $_SESSION['error'] = $error;
        header("location: ../my_appointment.php");
        return;
    }

    $sql_get_status_code="SELECT id FROM meeting_status WHERE codename='scheduled'";
    $result_get_status_code=mysqli_query($con,$sql_get_status_code);
    while($row=mysqli_fetch_assoc($result_get_status_code))
    {
        $status_id=$row['id'];
    }

    $sql_put_new_slot="INSERT INTO meeting_selected_slots (meetings_id, meeting_date, start_time, end_time) VALUES ($meeting_id, '$meeting_date', '$start_time', '$end_time')";
    $result_put_new_slot=mysqli_query($con,$sql_put_new_slot);
    if($result_put_new_slot)
    {
        $selected_slot_id = $con->insert_id;
    }
    else
    {
        $error = "ERROR 1: Something went wrong. Please try again later. ";
        $_SESSION['error'] = $error;
    }

    $sql_get_old_data="SELECT id, meeting_status_id, meeting_selected_slots_id, meeting_date, cost_card_price, service_tax_price, platform_charges, selected_time, mentee_id,mentor_id FROM meetings WHERE id=$meeting_id";
    $result_get_old_data=mysqli_query($con,$sql_get_old_data);
    if($row=mysqli_fetch_assoc($result_get_old_data))
    {
        $mentee_id = $row['mentee_id'];
        $mentor_id = $row['mentor_id'];
        $id=$row['id'];
        $meeting_status_id=$row['meeting_status_id'];
        $meeting_selected_slots_id=$row['meeting_selected_slots_id'];
        // $meeting_date=$row['meeting_date']; comming as null
        $cost_card_price=$row['cost_card_price'];
        $service_tax_price=$row['service_tax_price'];
        $platform_charges=$row['platform_charges'];
        $selected_time=$row['selected_time'];
    }
    else
    {
        $error = "ERROR 2: Something went wrong. Please try again later. ";
        $_SESSION['error'] = $error;
    }

    $sql_put_history="INSERT INTO meetings_status_history (meetings_id, meetings_status_id, meeting_selected_slots_id, meeting_date, cost_card_price, service_tax_price, platform_charges, selected_time,addedon) VALUES ($id,$meeting_status_id, NULL,'$meeting_date','$cost_card_price','$service_tax_price','$platform_charges','$selected_time','$date')";
    $result_put_history = mysqli_query($con, $sql_put_history);
    if(!$result_put_history) 
    {
        $error = "ERROR 3: Something went wrong. Please try again later.";
        $_SESSION['error'] = $error;
    }
    
    $sql_insert = "UPDATE meetings set selected_time='$start_time' , meeting_date='$meeting_date', meeting_status_id=$status_id , meeting_selected_slots_id=$selected_slot_id WHERE id=$meeting_id";

    $result_update = mysqli_query($con, $sql_insert);
    if(!$result_update) 
    {
        $error = "ERROR 4: Something went wrong while adding meeting. Please try again later.";
        $_SESSION['error'] = $error;
    }
    
    
    $sql = "COMMIT";
    
    $result = mysqli_query($con, $sql);
    if(!$result)
    {
        $error = "commit: Something went wrong. Please try again later.";
        $_SESSION['error'] = $error;
    }
    if(!$error) {
        $_SESSION['success'] = 'Meeting Scheduled Successfully.';
    }
    
    // send email to mentee
    if(SCHEDULE_MEETING_MAIL_MENTEE)
    {
        $sql_get_mentee_email="SELECT fname,email FROM mentee WHERE id=$mentee_id";
        $result_get_mentee_email=mysqli_query($con,$sql_get_mentee_email);
        if($row=mysqli_fetch_assoc($result_get_mentee_email))
        {
            $email=$row['email'];
            $mentee_name = $row['fname'];
        }
        $email_subject = "PurpleLane.in: Your meeting request confirmed - $meeting_id";
        $mailbody = "Hi $mentee_name,<br/>
                    Your meeting has been scheduled as follows<br>
                    Meeting ID : ".$meeting_id."<br>
                    Selected Meeting Date : $meeting_date <br>
                    Selected Meeting Time : $start_time
                    <br/>
                    <a href='".SITE_URL."'>Check your meetings here in scheduled tab.</a><br/><br/><br/>
                    Thanks & Regards<br/>
                    PurpleLane Team";

        $mail_file = "../../../../includes/class.phpmailer.php";
        $send_mail = send_mail($email_subject, $mailbody, $email, $mail_file);

        if(!$send_mail)
        {
            $error = "ERROR 3: Something went wrong. Please try again later.";
            $_SESSION['error'] = $error;
        }
    }
    
    // send email to mentor
    
    $sql_get_mentor_email="SELECT fname,email FROM mentor WHERE id=$mentor_id";
        $result_get_mentor_email=mysqli_query($con,$sql_get_mentor_email);
        if($row=mysqli_fetch_assoc($result_get_mentor_email))
        {
            $email=$row['email'];
            $mentor_name = $row['fname'];
        }
        $email_subject = "PurpleLane.in: Meeting request confirmed - $meeting_id";
        $mailbody = "Hi $mentor_name,<br/>
                    Meeting has been successfully scheduled as follows.<br>
                    Meeting ID : ".$meeting_id."<br>
                    Selected Meeting Date : $meeting_date <br>
                    Selected Meeting Time : $start_time
                    <br/>
                    <a href='".SITE_URL."'>Check your meetings here in scheduled tab </a><br/><br/><br/>
                    Thanks & Regards<br/>
                    PurpleLane Team";

        // $mail_file = "../../../../includes/class.phpmailer.php";
        $send_mail = send_mail($email_subject, $mailbody, $email, $mail_file);
    
    header("Location: {$_SERVER['HTTP_REFERER']}");