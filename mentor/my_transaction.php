<?php
require_once "../includes/initialize.php";
require_once "logincheck.php";

$id = $_SESSION['id'];
$userMentor = getMentorBasicInfo($con, $id);
$credits=getMentorTransaction($con,'credit');
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<title>Purple Lane</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="keywords" content="" />
	<meta name="description" content="" />
	<meta name="author" content="" />
	<?php
	include("../head.php");
	// dd($user);
	?>
</head>

<body data-spy="scroll" data-target=".navbar" data-offset="82">


	<div class="site__layer"></div>
	<?php include('navbar.php');?>s
	<section class="burger2 app" id="app">
		<div class="container">
			<!-- container-start -->
			<div class="row">
				<div class="col-lg-9 col-sm-12">
					<h5 class="section-heading">My Transactions</h5>
					<p class="spcev">

					</p>
				</div>
				<!-- <div class="col-lg-3 col-sm-12 hidden-xs hidden-sm"> -->
				<!-- <label class="pull-right"> -->
				<!-- <div class="d-flex justify-content-center h-100"> -->
				<!-- <div class="searchbar"> -->
				<!-- <input class="search_input" type="text" name="" placeholder="Search..."> -->
				<!-- <a href="#" class="search_icon"><i class="fa fa-search"></i></a> -->
				<!-- </div> -->
				<!-- </div> -->
				<!-- </label> -->
				<!-- </div> -->

			</div>
			<div class="container">
				<div class="row">
					<div class="col-md-12 text-center" style="margin-top:40px">
						<nav class="nav-justified ">
							<div class="nav nav-tabs " id="nav-tab" role="tablist">

								<a class="nav-item nav-link active" id="pop1-tab" data-toggle="tab" href="#pop1"
									role="tab" aria-controls="pop1" aria-selected="true"> <b><i class="fa fa-money"
											aria-hidden="true"></i> Credits</b></a>

							</div>
						</nav>
						

						<div class="tabpanel active " id="pop1" role="tabpanel" aria-labelledby="pop1-tab">
							<div class="pt-3"></div>

							<!-- Requests start -->

							<div class="row">

								<!---/col-->
								<div class="col-md-12 col-sm-12  ">

									<div class="store-btns">
										<h2 class="section-heading review-head"></h2>
										<table class='table table-striped' id='menteeCTransaction' width='100%'>
											<thead>
												<tr>
													<td>Sr No</td>
													<td>Meeting ID</td>
													<td>Amount</td>
													<td>Credit</td>
													<td>Date</td>
												</tr>
											</thead>
											<tbody>
												<?php
													if(isset($credits))
													{
														$i=0;
														foreach($credits as $credit)
														{
															echo '<tr><td>'.++$i.'</td><td>'.$credit['meetings_id'].'</td><td>'.$credit['amount'].'</td><td>Credit</td><td>'.$credit['addedon'].'</td></tr>';
														}
													}
													else
													{

													}
												?>
											</tbody>
										</table>
									</div>
								</div>
								<!---/col-->

							</div>

							<!-- Requests END -->

						</div>

					</div>
				</div>
			</div>
		</div>




		</div>
		<!-- container end -->
	</section>
	<?php 
require_once('../footer.php');
require_once('../footer_tags.php');

?>